﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMReports
{
    public class cSavedOrderDetails
    {
        public static string Get_SavedOrderDetail(string jsonorder)
        {
            SavedOrderDetail SavedOrderDetail = new SavedOrderDetail();

            SavedOrderDetail.OrderDate = (DateTime.Now).AddDays(0);
            SavedOrderDetail.CustomerNumber = "140218-1";
            SavedOrderDetail.BillingCompanyName = "BIOCURE, LLC";
            SavedOrderDetail.BillingAddress1 = "8700 COMMERCE PARK DR, STE 241";
            SavedOrderDetail.BillingAddress2 = "";
            SavedOrderDetail.BillingCity = "HOUSTON";
            SavedOrderDetail.BillingState = "TX";
            SavedOrderDetail.BillingPostalCode = "77036";
            SavedOrderDetail.BillingCountry = "US";
            SavedOrderDetail.ShippingCompanyName = "BIOCURE, LLC";
            SavedOrderDetail.ShippingAddress1 = "8700 COMMERCE PARK DR, STE 241";
            SavedOrderDetail.ShippingAddress2 = "";
            SavedOrderDetail.ShippingCity = "HOUSTON";
            SavedOrderDetail.ShippingState = "TX";
            SavedOrderDetail.ShippingPostalCode = "77036";
            SavedOrderDetail.ShippingCountry = "US";

            SavedOrderDetail.ProductSubtotal = "$1,358.88";


            SavedOrderLine SavedOrderLine = new SavedOrderLine();

            SavedOrderLine.unit_quantity = "1.00000";
            SavedOrderLine.unit_of_measure = "BX24";
            SavedOrderLine.item_id = "3M1527-0";
            SavedOrderLine.item_desc = "TRANSPORE CLEAR TAPE 1/2X10IN";
            SavedOrderLine.unit_price = "$9.42";
            SavedOrderLine.extended_price = "$9.42";
            SavedOrderDetail.SavedOrderLine.Add(SavedOrderLine);

            SavedOrderLine = new SavedOrderLine();

            SavedOrderLine.unit_quantity = "5.00000";
            SavedOrderLine.unit_of_measure = "BX50";
            SavedOrderLine.item_id = "3M1685";
            SavedOrderLine.item_desc = "TEGADERM IV ADVANCED DRESSING 3.5X4.5IN";
            SavedOrderLine.unit_price = "$77.60";
            SavedOrderLine.extended_price = "$388.02";
            SavedOrderDetail.SavedOrderLine.Add(SavedOrderLine);

            SavedOrderLine = new SavedOrderLine();

            SavedOrderLine.unit_quantity = "5.00000";
            SavedOrderLine.unit_of_measure = "BX50";
            SavedOrderLine.item_id = "3M1816";
            SavedOrderLine.item_desc = "TIE ON SURGICAL MASK FLUID RESISTANT";
            SavedOrderLine.unit_price = "$4.19";
            SavedOrderLine.extended_price = "$20.94";
            SavedOrderDetail.SavedOrderLine.Add(SavedOrderLine);

            SavedOrderLine = new SavedOrderLine();

            SavedOrderLine.unit_quantity = "3.00000";
            SavedOrderLine.unit_of_measure = "CS12";
            SavedOrderLine.item_id = "3M9321A";
            SavedOrderLine.item_desc = "AVAGARD FOAMING HAND SANITIZER 500ML";
            SavedOrderLine.unit_price = "$105.00";
            SavedOrderLine.extended_price = "$315.00";
            SavedOrderDetail.SavedOrderLine.Add(SavedOrderLine);

            SavedOrderLine = new SavedOrderLine();

            SavedOrderLine.unit_quantity = "5.00000";
            SavedOrderLine.unit_of_measure = "CS24";
            SavedOrderLine.item_id = "RS56277";
            SavedOrderLine.item_desc = "VITAL 1.0 CAL VANILLA 8 FL OZ CAN";
            SavedOrderLine.unit_price = "$125.10";
            SavedOrderLine.extended_price = "$625.50";
            SavedOrderDetail.SavedOrderLine.Add(SavedOrderLine);

            string json = Newtonsoft.Json.JsonConvert.SerializeObject(SavedOrderDetail);
            //--------------------------------------------------------------------------------------------------------------
            return json;
        }
    }
}

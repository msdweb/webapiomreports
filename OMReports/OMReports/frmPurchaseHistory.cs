﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Text;
using System.Windows.Forms;

namespace OMReports
{
    public partial class frmPurchaseHistory : Form
    {
        private DataView DVPageHeader;

        DataView DVSection1;
        DataView DVSection2;
        DataView DVSection3;

        private Printer DataGridPrinter1 = null;

        public frmPurchaseHistory()
        {
            InitializeComponent();
        }

        //####################################################################################
        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            Graphics g = e.Graphics;
            bool more = DataGridPrinter1.DrawDataGrid(g);
            if (more == true)
            {
                e.HasMorePages = true;
                DataGridPrinter1.PageNumber++;
                DataGridPrinter1.TotalPageNumber++;
            }
        }

        public void PCS_Preview()
        {
            if (GLB.PurchaseHistories.Count > 0)
            {
                SetupGridPrinter();

                frmPrinterControl PC = new frmPrinterControl();
                // if (DataGridPrinter1.
                PC.RPT_Preview(DataGridPrinter1);
                PC.ShowDialog();
                PC = null;
            }
        }

        public void PCS_Print()
        {
            if (GLB.PurchaseHistories.Count > 0)
            {
                SetupGridPrinter();

                frmPrinterControl PC = new frmPrinterControl();
                PC.RPT_Print(DataGridPrinter1);
                PC = null;
            }
        }

        public void PCS_Salva()
        {
            if (GLB.PurchaseHistories.Count > 0)
            {
                string s = System.AppDomain.CurrentDomain.BaseDirectory;
                printDocument1.DefaultPageSettings.PrinterSettings.PrinterName = "Microsoft XPS Document Writer";
                string FileName = "C:\\temp\\OM-Report\\MSD-OMPurchaseHistory-" + GLB.Customer_no + "-" + GLB.dStartDate.ToString("MMddyyyy") + "-" + GLB.dEndDate.ToString("MMddyyyy") + ".xps";
                printDocument1.DefaultPageSettings.PrinterSettings.PrintFileName = FileName;
                printDocument1.DefaultPageSettings.PrinterSettings.PrintToFile = true;

                SetupGridPrinter();

                frmPrinterControl PC = new frmPrinterControl();

                PC.RPT_Print(DataGridPrinter1);
                PC = null;
            }
        }

        void SetupGridPrinter()
        {

            dataDetails dataDetails = new dataDetails();

            lblFooter.Text = dataDetails.Get_LastReview();
            dgvEmptyMessage.ColumnHeadersVisible = false;

            DVPageHeader = dataDetails.DV_PageHeader_Fill(DVPageHeader, "");

            dgvPageHeader.BorderStyle = BorderStyle.None;
            dgvPageHeader.ColumnHeadersVisible = true;

 
            DataGridPrinter1 = new Printer(printDocument1,
                                           dgvPageHeader,
                                           dgvEmptyMessage,
                                           null,
                                           DVPageHeader.Table.DefaultView, lblFooter);



            //----------------------------------------------------------------------------
            //----------------------------------------------------------------------------
            //----------------------------------------------------------------------------

            string currentInvoice = string.Empty;
            int g = 0;


            dgvGroup1.ColumnHeadersVisible = false;
            DataGridPrinter1.AddGroup(dgvGroup1, null, null);

            //----------------------------------------------------------------------------

            string strVal1 = string.Empty;
            //if (GLB.orderHistoryDetail.ShippingState != null)
            //{
            //    if (GLB.orderHistoryDetail.ShippingState.ToString() != "FL")
            //    {
            //        strVal1 += "800 Technology Center Drive" + (char)13;
            //        strVal1 += "Stoughton, MA 02072" + (char)13;
            //        strVal1 += "800.967.6400    781.344.7244";
            //    }
            //    else
            //    {
            //        strVal1 += "11600 Miramar Parkway Suite 300" + (char)13;
            //        strVal1 += "Miramar, FL 33025" + (char)13;
            //        strVal1 += "954.986.2000            954.983.2503";
            //    }
            //}
            strVal1 += "800 Technology Center Drive" + (char)13;
            strVal1 += "Stoughton, MA 02072" + (char)13;
            strVal1 += "800.967.6400    781.344.7244";

            DataView DVSectionHeaderA0 = new DataView();
            DVSectionHeaderA0 = dataDetails.DV_SectionHeaderA1_Fill(DVSectionHeaderA0, strVal1);
            dgvSectionHeaderA0.ColumnHeadersVisible = false;
            DataGridPrinter1.AddSectionHeader(g, false,
                                              null,
                                              dgvSectionHeaderA0,
                                              DVSectionHeaderA0);

            //----------------------------------------------------------------------------

            DataView DVSectionHeaderA1 = new DataView();
            DVSectionHeaderA1 = dataDetails.DV_Section_TwoFields_Open();

            DVSectionHeaderA1 = dataDetails.DV_Section_TwoFields_Fill(DVSectionHeaderA1, "                        Account #", " " + GLB.Customer_no.ToString());
            DVSectionHeaderA1 = dataDetails.DV_Section_TwoFields_Fill(DVSectionHeaderA1, "                                From", " " + GLB.dStartDate.ToString("MM/dd/yyyy"));
            DVSectionHeaderA1 = dataDetails.DV_Section_TwoFields_Fill(DVSectionHeaderA1, "                                     To", " " + GLB.dEndDate.ToString("MM/dd/yyyy"));

            dgvSectionHeaderA1.ColumnHeadersVisible = false;
            DataGridPrinter1.AddSectionHeader(g, false,
                                              null,
                                              dgvSectionHeaderA1,
                                              DVSectionHeaderA1);
            //
            //------------------------------------------------------------------------------------------
            //


            //-------------------------------------------------------------


            //
            //------------------------------------------------------------------------------------------
            //

            //----------------------------------------------------------------------------
            //----------------------------------------------------------------------------
            //----------------------------------------------------------------------------

            DVSection1 = dataDetails.DV_Section3_Open();

            string sAlternateColor = "1";

            foreach (PurchaseHistory PurchaseHistory in GLB.PurchaseHistories)
            {
                string OrderNumber = string.Empty;
                string OrderDate = string.Empty;
                string CustomerPO = string.Empty;

                string itemID_Description = string.Empty;
                string itemID = string.Empty;
                string itemDescription = string.Empty;

                string UOM = string.Empty;

                string qtyOrdered = string.Empty;
                string UnitPrice = string.Empty;
                string ExtendedPrice = string.Empty;

                OrderNumber = PurchaseHistory.OrderNumber;

                DateTime dOrderDate = Convert.ToDateTime(PurchaseHistory.OrderDate);
                string sdOrderDate = dOrderDate.ToString("MM/dd/yyyy");

                CustomerPO = PurchaseHistory.CustomerPO.ToString();

                itemID = PurchaseHistory.ItemID.ToString();
                itemID_Description = PurchaseHistory.ItemID.ToString() + (char)13 + PurchaseHistory.ItemDesc.ToString();

                UOM = PurchaseHistory.UOM.ToString();

                decimal decqtyOrdered = Convert.ToDecimal(PurchaseHistory.QtyOrdered);
                qtyOrdered = decqtyOrdered.ToString("0.##");

                if (PurchaseHistory.UnitPrice.ToString() != "")
                {
                    decimal decUnitPrice = Convert.ToDecimal(PurchaseHistory.UnitPrice);
                    UnitPrice = decUnitPrice.ToString("0.0000");
                    //UnitPrice = PurchaseHistory.UnitPrice;
                }

                if (PurchaseHistory.ExtendedPrice.ToString() != "")
                {
                    decimal decExtendedPrice = Convert.ToDecimal(PurchaseHistory.ExtendedPrice);
                    //decSubTotal += decExtendedPrice;
                    ExtendedPrice = string.Format("{0:C}", decExtendedPrice);
                    //ExtendedPrice = PurchaseHistory.ExtendedPrice.ToString();
                }

                //
                //------------------------------------------------------------------------------------------
                //

                if (sAlternateColor == "0")
                { sAlternateColor = "1"; }
                else { sAlternateColor = "0"; }

                DVSection1 = dataDetails.DV_Section3_Fill(DVSection1, sAlternateColor, OrderNumber, sdOrderDate, CustomerPO, itemID_Description, UOM, qtyOrdered, UnitPrice, ExtendedPrice);

                //
                //------------------------------------------------------------------------
                //
            }



            DataGridPrinter1.AddSection(1, false,
                            dgvSectionHeaderYourOrder,
                            dgvSectionYourOrder,
                            DVSection1);

            //
            //------------------------------------------------------------------------------------------
            //


            //
            //------------------------------------------------------------------------------------------
            //


            //
            //------------------------------------------------------------------------------------------
            //

            DataGridPrinter1.PrintLogo = true;
            DataGridPrinter1.PrintLogoCenter = false;
            DataGridPrinter1.Set_DefaultPageSettings(false);
        }
    }
}
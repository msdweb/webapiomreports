﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OMReports
{
    public partial class frmOMPurchaseHistory : Form
    {
        public frmOMPurchaseHistory()
        {
            InitializeComponent();
        }

        private void frmOMPurchaseHistory_Load(object sender, EventArgs e)
        {
            PaintGrid();
        }

        //
        //-------------------------------------------------------------
        //

        private void btnDisplay_Click(object sender, EventArgs e)
        {
            frmPurchaseHistory frmPCSReport = new frmPurchaseHistory();
            frmPCSReport.PCS_Preview();
            return;
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            frmPurchaseHistory frmPCSReport = new frmPurchaseHistory();
            frmPCSReport.PCS_Print();
        }

        private void btnSalvarRelatorio_Click(object sender, EventArgs e)
        {
            frmPurchaseHistory frmPCSReport = new frmPurchaseHistory();
            frmPCSReport.PCS_Salva();
        }

        //
        //-------------------------------------------------------------
        //

        private void PaintGrid()
        {
            dataGridView1.DataSource = GetTablePurchaseHistory(GLB.PurchaseHistories);
        }


        //
        //-----------------------------------------------------------------        
        //

        private DataTable GetTablePurchaseHistory(List<PurchaseHistory> PurchaseHistories)
        {
            DataTable table = new DataTable();

            try
            {

            table.Columns.Add("CustomerNumber", typeof(string));
            table.Columns.Add("OrderNumber", typeof(string));
            table.Columns.Add("OrderDate", typeof(string));
            table.Columns.Add("CustomerPO", typeof(string));
            table.Columns.Add("ItemID", typeof(string));
            table.Columns.Add("ItemDesc", typeof(string));
            table.Columns.Add("UOM", typeof(string));
            table.Columns.Add("QtyOrdered", typeof(string));
            table.Columns.Add("UnitPrice", typeof(string));
            table.Columns.Add("ExtendedPrice", typeof(string));

                foreach (PurchaseHistory PurchaseHistory in GLB.PurchaseHistories)
                {
                    table.Rows.Add(
                                    PurchaseHistory.CustomerNumber,
                                    PurchaseHistory.OrderNumber,
                                    PurchaseHistory.OrderDate,
                                    PurchaseHistory.CustomerPO,
                                    PurchaseHistory.ItemID,
                                    PurchaseHistory.ItemDesc,
                                    PurchaseHistory.UOM,
                                    PurchaseHistory.QtyOrdered,
                                    PurchaseHistory.UnitPrice, 
                                    PurchaseHistory.ExtendedPrice
                   );
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }

            return table;
        }
    }
}

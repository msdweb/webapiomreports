﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Text;
using System.Windows.Forms;

namespace OMReports
{
    public partial class frmPrinterControl : Form
    {
        private Printer DataGridPrinter1 = null;
        private PrintDocument PrintDocument1 = null;

        public frmPrinterControl()
        {
           
            InitializeComponent();

            this.MaximizeBox = true;
            this.FormBorderStyle = FormBorderStyle.Sizable;

         //   printPreviewControl1.Document = printDocument1;

        
         //// Construct the PrintPreviewControl.
        //  PrintPreviewControl PrintPreviewControl1 = new PrintPreviewControl();

        //// Set location, name, and dock style for PrintPreviewControl1.
        //PrintPreviewControl1.Location = new Point(88, 80);
        //PrintPreviewControl1.Name = "PrintPreviewControl1";
        //PrintPreviewControl1.Dock = DockStyle.Fill;

        //// Set the Document property to the PrintDocument 
        //// for which the PrintPage event has been handled.
        //PrintPreviewControl1.Document = printDocument1;

        //// Set the zoom to 25 percent.
        //PrintPreviewControl1.Zoom = 0.70;

        //// Set the document name. This will show be displayed when 
        //// the document is loading into the control.
        //PrintPreviewControl1.Document.DocumentName = "Document Name";

        //// Set the UseAntiAlias property to true so fonts are smoothed
        //// by the operating system.
        //PrintPreviewControl1.UseAntiAlias = true;

        //// Add the control to the form.
        //Controls.Add(PrintPreviewControl1);
        
        }

        public void RPT_Preview(Printer parDataGridPrinter)
        {
            DataGridPrinter1 = parDataGridPrinter;
            DataGridPrinter1.Reset_Flags();
            PrintDocument1 = DataGridPrinter1.ThePrintDocument;
            PrintDocument1.PrintPage += new PrintPageEventHandler(PrintDocument1_PrintPage);
            printPreviewControl1.Document = PrintDocument1;
            if (printPreviewControl1.StartPage == 0)
            {
                bntFirstPage.Enabled = false;
                bntPreviousPage.Enabled = false;
            }
        }

        public void RPT_Print(Printer parDataGridPrinter)
        {
            DataGridPrinter1 = parDataGridPrinter;
            DataGridPrinter1.Reset_Flags();
            PrintDocument1 = DataGridPrinter1.ThePrintDocument;
            PrintDocument1.PrintPage += new PrintPageEventHandler(PrintDocument1_PrintPage);
            PrintDocument1.Print();
        }

        private void PrintDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            Graphics g = e.Graphics;

            bool more = DataGridPrinter1.DrawDataGrid(g);
            if (more == true)
            {
                e.HasMorePages = true;
                DataGridPrinter1.PageNumber++;
                DataGridPrinter1.TotalPageNumber++;
            }
            else
            {
                e.HasMorePages = false;
                DataGridPrinter1.Reset_Flags();
            }
        }

        private void frmPrintControl_Resize(object sender, EventArgs e)
        {
            printPreviewControl1.Height = (this.Height-80);
            printPreviewControl1.Width = (this.Width-30);
            printPreviewControl1.Top = 40;
            printPreviewControl1.Left = 12;
            printPreviewControl1.Zoom = .85;
            printPreviewControl1.Rows = 1;
            printPreviewControl1.Columns = 1;
        }

        private void bntPrint_Click(object sender, EventArgs e)
        {
            DataGridPrinter1.Reset_Flags();
            PrintDocument1.Print();
        }

        private void bntPreviousPage_Click(object sender, EventArgs e)
        {
            if (printPreviewControl1.StartPage > 0) printPreviewControl1.StartPage = printPreviewControl1.StartPage - 1;
            if (printPreviewControl1.StartPage == 0)
            {
                bntFirstPage.Enabled = false;
                bntPreviousPage.Enabled = false;
            }
            bntNextPage.Enabled = true;
            bntLastPage.Enabled = true;
        }

        private void bntNextPage_Click(object sender, EventArgs e)
        {
            int startpage = printPreviewControl1.StartPage;
            printPreviewControl1.StartPage = printPreviewControl1.StartPage + 1;
            if (printPreviewControl1.StartPage == startpage)
            {
                bntNextPage.Enabled = false;
                bntLastPage.Enabled = false;
            }
            bntFirstPage.Enabled = true;
            bntPreviousPage.Enabled = true;
        }

        private void bntFirstPage_Click(object sender, EventArgs e)
        {
            if (printPreviewControl1.StartPage > 0) printPreviewControl1.StartPage = 0;
            if (printPreviewControl1.StartPage == 0)
            {
                bntFirstPage.Enabled = false;
                bntPreviousPage.Enabled = false;
            }
            bntNextPage.Enabled = true;
            bntLastPage.Enabled = true;
        }

        private void bntLastPage_Click_1(object sender, EventArgs e)
        {
            for (; ; )
            {
                int startpage = printPreviewControl1.StartPage;
                printPreviewControl1.StartPage = printPreviewControl1.StartPage + 1;
                if (printPreviewControl1.StartPage == startpage)
                {
                    bntNextPage.Enabled = false;
                    bntLastPage.Enabled = false;
                    break;
                }
            }
            bntFirstPage.Enabled = true;
            bntPreviousPage.Enabled = true;
        }

        private void bntZoomIn_Click(object sender, EventArgs e)
        {
            if (printPreviewControl1.Zoom < 200) printPreviewControl1.Zoom = printPreviewControl1.Zoom + 0.05;
        }

        private void bntZoomOut_Click(object sender, EventArgs e)
        {
            if (printPreviewControl1.Zoom > 0.05) printPreviewControl1.Zoom = printPreviewControl1.Zoom - 0.05;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (printPreviewControl1.Columns < 6) printPreviewControl1.Columns = printPreviewControl1.Columns + 1;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (printPreviewControl1.Columns > 1) printPreviewControl1.Columns = printPreviewControl1.Columns - 1;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (printPreviewControl1.Rows < 6) printPreviewControl1.Rows = printPreviewControl1.Rows + 1;

        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (printPreviewControl1.Rows > 1) printPreviewControl1.Rows = printPreviewControl1.Rows - 1;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            printPreviewControl1.Height = (this.Height - 80);
            printPreviewControl1.Width = (this.Width - 30);
            printPreviewControl1.Top = 40;
            printPreviewControl1.Left = 12;
            printPreviewControl1.Zoom = .85;
            printPreviewControl1.Rows = 1;
            printPreviewControl1.Columns = 1;
        }



    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace OMReports
{
    public partial class frmOMOrderConfirmation : Form
    {

        private Hashtable colPickTicketTracking = new Hashtable();
        private Hashtable colPickTicketDetail = new Hashtable();
        private Hashtable colPickTicketDetailLot = new Hashtable();

        public frmOMOrderConfirmation()
        {
            InitializeComponent();
        }

        private void frmOMOrder_Load(object sender, EventArgs e)
        {
            PaintGrid();
        }

        //
        //-------------------------------------------------------------
        //

        private void btnDisplay_Click(object sender, EventArgs e)
        {
            frmOrderConfirmation frmOrderConfirmation = new frmOrderConfirmation();
            frmOrderConfirmation.PCS_Preview();
            return;
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            frmOrderConfirmation frmOrderConfirmation = new frmOrderConfirmation();
            frmOrderConfirmation.PCS_Print();
        }

        private void btnSalvarRelatorio_Click(object sender, EventArgs e)
        {
            frmOrderConfirmation frmOrderConfirmation = new frmOrderConfirmation();
            frmOrderConfirmation.PCS_Salva();
        }

        //
        //-------------------------------------------------------------
        //


        //
        //-------------------------------------------------------------
        //

        private void PaintGrid()
        {
            dataGridView1.DataSource = GetTableOrderHistoryDetail(GLB.OrderConfirmationDetail);
            dataGridView2.DataSource = GetTableOrderLine(GLB.OrderConfirmationDetail.OrderConfirmationLine);
        }


        //
        //-----------------------------------------------------------------        
        //

        private DataTable GetTableOrderHistoryDetail(OrderConfirmationDetail OrderConfirmationDetail)
        {
            DataTable table = new DataTable();
            table.Columns.Add("OrderNumber", typeof(string));
            table.Columns.Add("OrderDate", typeof(string));
            table.Columns.Add("RequestedShipDate", typeof(string));
            table.Columns.Add("CustomerNumber", typeof(string));
            table.Columns.Add("CustomerPurchaseOrder", typeof(string));
            table.Columns.Add("PlacedBy", typeof(string));

            table.Columns.Add("BillingCompanyName", typeof(string));
            table.Columns.Add("BillingAddress1", typeof(string));
            table.Columns.Add("BillingAddress2", typeof(string));
            table.Columns.Add("BillingCity", typeof(string));
            table.Columns.Add("BillingState", typeof(string));
            table.Columns.Add("BillingPostalCode", typeof(string));
            table.Columns.Add("BillingCountry", typeof(string));

            table.Columns.Add("ShippingCompanyName", typeof(string));
            table.Columns.Add("ShippingAddress1", typeof(string));
            table.Columns.Add("ShippingAddress2", typeof(string));
            table.Columns.Add("ShippingCity", typeof(string));
            table.Columns.Add("ShippingState", typeof(string));
            table.Columns.Add("ShippingPostalCode", typeof(string));
            table.Columns.Add("ShippingCountry", typeof(string));

            table.Columns.Add("FreightInfoOption", typeof(string));


            table.Rows.Add(
                            OrderConfirmationDetail.OrderNumber,
                            OrderConfirmationDetail.OrderDate,
                            OrderConfirmationDetail.RequestedShipDate,
                            OrderConfirmationDetail.CustomerNumber,
                            OrderConfirmationDetail.CustomerPurchaseOrder,
                            OrderConfirmationDetail.PlacedBy,

                            OrderConfirmationDetail.BillingCompanyName,
                            OrderConfirmationDetail.BillingAddress1,
                            OrderConfirmationDetail.BillingAddress2,
                            OrderConfirmationDetail.BillingCity,
                            OrderConfirmationDetail.BillingState,
                            OrderConfirmationDetail.BillingPostalCode,
                            OrderConfirmationDetail.BillingCountry,

                            OrderConfirmationDetail.ShippingCompanyName,
                            OrderConfirmationDetail.ShippingAddress1,
                            OrderConfirmationDetail.ShippingAddress2,
                            OrderConfirmationDetail.ShippingCity,
                            OrderConfirmationDetail.ShippingState,
                            OrderConfirmationDetail.ShippingPostalCode,
                            OrderConfirmationDetail.ShippingCountry,

                            OrderConfirmationDetail.FreightInfoOption
           );

            return table;
        }

        private DataTable GetTableOrderLine(List<OrderConfirmationLine> ListOrderLine)
        {
            DataTable table = new DataTable();

            table.Columns.Add("unit_quantity", typeof(string));
            table.Columns.Add("unit_of_measure", typeof(string));
            table.Columns.Add("item_id", typeof(string));
            table.Columns.Add("item_desc", typeof(string));
            table.Columns.Add("unit_price", typeof(string));
            table.Columns.Add("extended_price", typeof(string));

            foreach (OrderConfirmationLine OrderConfirmationLine in ListOrderLine)
            {
                table.Rows.Add(
                        OrderConfirmationLine.unit_quantity,
                        OrderConfirmationLine.unit_of_measure,
                        OrderConfirmationLine.item_id,
                        OrderConfirmationLine.item_desc,
                        OrderConfirmationLine.unit_price,
                        OrderConfirmationLine.extended_price
                );
            }

            return table;
        }
    }
}

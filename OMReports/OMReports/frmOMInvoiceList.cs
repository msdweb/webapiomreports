﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OMReports
{
    public partial class frmOMInvoiceList : Form
    {
        public frmOMInvoiceList()
        {
            InitializeComponent();
        }

        private void frmOMInvoiceHistory_Load(object sender, EventArgs e)
        {
            PaintGrid();
        }

        //
        //-------------------------------------------------------------
        //

        private void btnDisplay_Click(object sender, EventArgs e)
        {
            frmInvoiceList frmPCSReport = new frmInvoiceList();
            frmPCSReport.PCS_Preview();
            return;
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            frmInvoiceList frmPCSReport = new frmInvoiceList();
            frmPCSReport.PCS_Print();
        }

        private void btnSalvarRelatorio_Click(object sender, EventArgs e)
        {
            frmInvoiceList frmPCSReport = new frmInvoiceList();
            frmPCSReport.PCS_Salva();
        }

        //
        //-------------------------------------------------------------
        //

        private void PaintGrid()
        {
            dataGridView1.DataSource = GetTableInvoiceHistory(GLB.InvoiceHistories);
        }


        //
        //-----------------------------------------------------------------        
        //

        private DataTable GetTableInvoiceHistory(List<InvoiceHistory> InvoiceHistories)
        {
            DataTable table = new DataTable();

            try
            {
                table.Columns.Add("Customer#", typeof(string));
                table.Columns.Add("Invoice Date", typeof(string));
                table.Columns.Add("Invoice#", typeof(string));
                table.Columns.Add("PO#", typeof(string));
                table.Columns.Add("Ship To", typeof(string));
                table.Columns.Add("Due Date", typeof(string));
                table.Columns.Add("Paid Date", typeof(string));
                table.Columns.Add("Terms", typeof(string));
                table.Columns.Add("Total", typeof(string));

                foreach (InvoiceHistory InvoiceHistory in GLB.InvoiceHistories)
                {
                    table.Rows.Add(
                                    InvoiceHistory.CustomerNumber,
                                    InvoiceHistory.InvoiceDate,
                                    InvoiceHistory.InvoiceNumber,
                                    InvoiceHistory.CustomerPurchaseOrder,
                                    InvoiceHistory.ShippingCompanyName,
                                    InvoiceHistory.DueDate,
                                    InvoiceHistory.InvoiceType,
                                    InvoiceHistory.Terms,
                                    InvoiceHistory.InvoiceTotal
                   );
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }

            return table;
        }
    }
}

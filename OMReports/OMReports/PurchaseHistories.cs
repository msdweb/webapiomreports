﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
//using System.Web.Http;
using System.Web;
using System.Collections.Specialized;

namespace OMReports
{
    public class cPurchaseHistories
    {
        public static List<PurchaseHistory> Get_PurchaseHistories(string Customer_no, DateTime StartDate, DateTime EndDate, string sConn)
        {
            var PurchaseHistories = new List<PurchaseHistory>();
            var PurchaseHistory = new PurchaseHistory();

            using (SqlConnection _SQLConn = new SqlConnection(sConn))
            {
                const string sSqlPurchaseHistoryCmd = "[dbo].[wsp_webapi_report_purchase_history]";
                _SQLConn.Open();

                using (var sqlCmd = new SqlCommand())
                {
                    sqlCmd.Connection = _SQLConn;
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.CommandText = sSqlPurchaseHistoryCmd;
                    sqlCmd.Parameters.AddWithValue("@CustomerNumber", Customer_no);
                    sqlCmd.Parameters.AddWithValue("@startDate", StartDate);
                    sqlCmd.Parameters.AddWithValue("@EndDate", EndDate);
                    sqlCmd.CommandTimeout = 120;

                    using (SqlDataReader sqlRdr = sqlCmd.ExecuteReader())
                    {
                        while (sqlRdr.Read())
                        {
                            if (sqlRdr["CustomerNumber"] != null) PurchaseHistory.CustomerNumber = sqlRdr["CustomerNumber"].ToString();
                            if (sqlRdr["OrderNumber"] != null) PurchaseHistory.OrderNumber = sqlRdr["OrderNumber"].ToString();
                            if (sqlRdr["OrderDate"] != null) PurchaseHistory.OrderDate = Convert.ToDateTime(sqlRdr["OrderDate"]);
                            if (sqlRdr["CustomerPO"] != null) PurchaseHistory.CustomerPO = sqlRdr["CustomerPO"].ToString();

                            if (sqlRdr["ItemID"] != null) PurchaseHistory.ItemID = sqlRdr["ItemID"].ToString();
                            if (sqlRdr["ItemDesc"] != null) PurchaseHistory.ItemDesc = sqlRdr["ItemDesc"].ToString();
                            if (sqlRdr["UOM"] != null) PurchaseHistory.UOM = sqlRdr["UOM"].ToString();
                            if (sqlRdr["QtyOrdered"] != null) PurchaseHistory.QtyOrdered = sqlRdr["QtyOrdered"].ToString();
                            if (sqlRdr["UnitPrice"] != null) PurchaseHistory.UnitPrice = sqlRdr["UnitPrice"].ToString();
                            if (sqlRdr["ExtendedPrice"] != null) PurchaseHistory.ExtendedPrice = sqlRdr["ExtendedPrice"].ToString();

                            PurchaseHistories.Add(PurchaseHistory);
                            PurchaseHistory = new PurchaseHistory();
                        }
                    }
                }
            }
            return PurchaseHistories;
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace OMReports
{
    public partial class frmOMSavedOrder : Form
    {
        private Hashtable colPickTicketTracking = new Hashtable();
        private Hashtable colPickTicketDetail = new Hashtable();
        private Hashtable colPickTicketDetailLot = new Hashtable();

        public frmOMSavedOrder()
        {
            InitializeComponent();
        }

        private void frmOMSavedOrder_Load(object sender, EventArgs e)
        {
            PaintGrid();
        }

        private void btnDisplay_Click(object sender, EventArgs e)
        {
            frmSavedOrder frmSavedOrder = new frmSavedOrder();
            frmSavedOrder.PCS_Preview();
            return;
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            frmSavedOrder frmSavedOrder = new frmSavedOrder();
            frmSavedOrder.PCS_Print();
        }

        private void btnSalvarRelatorio_Click(object sender, EventArgs e)
        {
            frmSavedOrder frmSavedOrder = new frmSavedOrder();
            frmSavedOrder.PCS_Salva();
        }

        //
        //-------------------------------------------------------------
        //


        //
        //-------------------------------------------------------------
        //

        private void PaintGrid()
        {
            dataGridView1.DataSource = GetTableOrderHistoryDetail(GLB.SavedOrderDetail);
            dataGridView2.DataSource = GetTableOrderLine(GLB.SavedOrderDetail.SavedOrderLine);
        }


        //
        //-----------------------------------------------------------------        
        //

        private DataTable GetTableOrderHistoryDetail(SavedOrderDetail SavedOrderDetail)
        {
            DataTable table = new DataTable();
            table.Columns.Add("OrderDate", typeof(string));
            table.Columns.Add("CustomerNumber", typeof(string));

            table.Columns.Add("BillingCompanyName", typeof(string));
            table.Columns.Add("BillingAddress1", typeof(string));
            table.Columns.Add("BillingAddress2", typeof(string));
            table.Columns.Add("BillingCity", typeof(string));
            table.Columns.Add("BillingState", typeof(string));
            table.Columns.Add("BillingPostalCode", typeof(string));
            table.Columns.Add("BillingCountry", typeof(string));

            table.Columns.Add("ShippingCompanyName", typeof(string));
            table.Columns.Add("ShippingAddress1", typeof(string));
            table.Columns.Add("ShippingAddress2", typeof(string));
            table.Columns.Add("ShippingCity", typeof(string));
            table.Columns.Add("ShippingState", typeof(string));
            table.Columns.Add("ShippingPostalCode", typeof(string));
            table.Columns.Add("ShippingCountry", typeof(string));

            table.Rows.Add(
                            SavedOrderDetail.OrderDate,
                            SavedOrderDetail.CustomerNumber,

                            SavedOrderDetail.BillingCompanyName,
                            SavedOrderDetail.BillingAddress1,
                            SavedOrderDetail.BillingAddress2,
                            SavedOrderDetail.BillingCity,
                            SavedOrderDetail.BillingState,
                            SavedOrderDetail.BillingPostalCode,
                            SavedOrderDetail.BillingCountry,

                            SavedOrderDetail.ShippingCompanyName,
                            SavedOrderDetail.ShippingAddress1,
                            SavedOrderDetail.ShippingAddress2,
                            SavedOrderDetail.ShippingCity,
                            SavedOrderDetail.ShippingState,
                            SavedOrderDetail.ShippingPostalCode,
                            SavedOrderDetail.ShippingCountry

           );

            return table;
        }

        private DataTable GetTableOrderLine(List<SavedOrderLine> ListOrderLine)
        {
            DataTable table = new DataTable();

            table.Columns.Add("unit_quantity", typeof(string));
            table.Columns.Add("unit_of_measure", typeof(string));
            table.Columns.Add("item_id", typeof(string));
            table.Columns.Add("item_desc", typeof(string));
            table.Columns.Add("unit_price", typeof(string));
            table.Columns.Add("extended_price", typeof(string));

            foreach (SavedOrderLine SavedOrderLine in ListOrderLine)
            {
                table.Rows.Add(
                        SavedOrderLine.unit_quantity,
                        SavedOrderLine.unit_of_measure,
                        SavedOrderLine.item_id,
                        SavedOrderLine.item_desc,
                        SavedOrderLine.unit_price,
                        SavedOrderLine.extended_price
                );
            }

            return table;
        }
    }
}

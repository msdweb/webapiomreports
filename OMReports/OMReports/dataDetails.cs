﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace OMReports
{
    class dataDetails
    {

        public string Get_LastReview()
        {
            string LastReview = "Medical Specialties Distributors, LLC";
            LastReview = "";
            return LastReview;
        }

        public DataView DV_PageHeader_Fill(DataView oDV, string strVal1)
        {
            oDV = DV_SectionHeader1_Open();
            oDV = DV_SectionHeader1_Pop(oDV, strVal1);

            return oDV;
        }

        //-------------------------------------------------------------------------------------

        public DataView DV_SectionHeaderA1_Fill(DataView oDV, string strVal1)
        {
            oDV = DV_SectionHeader1_Open();

            oDV = DV_SectionHeader1_Pop(oDV, strVal1);

            //oDV = DV_SectionHeader1_Pop(oDV, "800 Technology Center Drive");
            //oDV = DV_SectionHeader1_Pop(oDV, "Stoughton, MA 02072");
            //oDV = DV_SectionHeader1_Pop(oDV, "800.967.6400     781.344.7244");

            return oDV;
        }

        public DataView DV_SectionHeaderB1_Fill(DataView oDV)
        {
            oDV = DV_SectionHeader1_Open();

            string strVal1 = string.Empty;
            strVal1 += "Dept #1729" + (char)13;
            strVal1 += "Medical Specialties Distributors,LLC" + (char)13;
            strVal1 += "PO Box 11407" + (char)13;
            strVal1 += "Birmingham, AL 35246-1729";

            oDV = DV_SectionHeader1_Pop(oDV, strVal1);

            //oDV = DV_SectionHeader1_Pop(oDV, "Dept #1729");
            //oDV = DV_SectionHeader1_Pop(oDV, "Medical Specialties Distributors,LLC");
            //oDV = DV_SectionHeader1_Pop(oDV, "PO Box 11407");
            //oDV = DV_SectionHeader1_Pop(oDV, "Birmingham, AL 35246-1729");

            return oDV;
        }

        public DataView DV_SectionHeaderC1_Fill(DataView oDV, string strInvoiceNumber)
        {
            oDV = DV_SectionHeader1_Open();

            oDV = DV_SectionHeader1_Pop(oDV, strInvoiceNumber);
            return oDV;
        }

        public DataView DV_SectionHeaderD1_Fill(DataView oDV, string strInvoiceDate, string strPage)
        {
            oDV = DV_SectionHeader_Open();

            oDV = DV_SectionHeader_Pop(oDV, strInvoiceDate, strPage);
            return oDV;
        }

        public DataView DV_SectionHeaderE1_Fill(DataView oDV, string strOrderNumber)
        {
            oDV = DV_SectionHeader1_Open();

            oDV = DV_SectionHeader1_Pop(oDV, strOrderNumber);
            return oDV;
        }


        public DataView DV_SectionHeader1_Open()
        {
            DataTable oDT = new DataTable("PageHeader");
            //---------------------------------------------------------------
            DataColumn ALTCOLOR = new DataColumn("ALTCOLOR");
            ALTCOLOR.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(ALTCOLOR);

            DataColumn TEXT1 = new DataColumn("TEXT1");
            TEXT1.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT1);

            //---------------------------------------------------------------

            DataView oDV = new DataView();
            oDV.Table = oDT;
            return oDV;
        }

        //-------------------------------------------------------------------------------------

        private DataView DV_SectionHeader1_Pop(DataView oDV, string strVal1)
        {
            DataRow oDR = oDV.Table.NewRow();

            oDR["ALTCOLOR"] = "";
            oDR["TEXT1"] = strVal1;
            //--------------------------------------- 
            oDV.Table.Rows.Add(oDR);
            oDR = null;
            oDR = oDV.Table.NewRow();

            return oDV;
        }

        //-----------------------------------------------------------------------        
        //-----------------------------------------------------------------------        
        //-----------------------------------------------------------------------        


        private DataView DV_SectionHeader_Open()
        {
            DataTable oDT = new DataTable("PageHeader");
            //---------------------------------------------------------------
            DataColumn ALTCOLOR = new DataColumn("ALTCOLOR");
            ALTCOLOR.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(ALTCOLOR);

            DataColumn LABEL = new DataColumn("LABEL");
            LABEL.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(LABEL);

            DataColumn TEXT = new DataColumn("TEXT");
            TEXT.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT);

            //---------------------------------------------------------------

            DataView oDV = new DataView();
            oDV.Table = oDT;
            return oDV;
        }

        //-------------------------------------------------------------------------------------

        private DataView DV_SectionHeader_Pop(DataView oDV, string strVal1, string strVal2)
        {
            DataRow oDR = oDV.Table.NewRow();

            oDR["ALTCOLOR"] = "";
            oDR["LABEL"] = strVal1;
            oDR["TEXT"] = strVal2;
            //--------------------------------------- 
            oDV.Table.Rows.Add(oDR);
            oDR = null;
            oDR = oDV.Table.NewRow();

            return oDV;
        }

        //-----------------------------------------------------------------------        
        //-----------------------------------------------------------------------        
        //-----------------------------------------------------------------------        

        public DataView DV_SectionHeader2_Fill(DataView oDV, string strBillTo, string strShipTo, string strShippingMethod)
        {

            oDV = DV_SectionHeader2_Open();
            oDV = DV_SectionHeader2_Pop(oDV, strBillTo, "", strShipTo, "", strShippingMethod);
            return oDV;
        }

        //-----------------------------------------------------------------------        

        private DataView DV_SectionHeader2_Open()
        {
            DataTable oDT = new DataTable("PageHeader");
            //---------------------------------------------------------------
            DataColumn ALTCOLOR = new DataColumn("ALTCOLOR");
            ALTCOLOR.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(ALTCOLOR);

            DataColumn TEXT1 = new DataColumn("TEXT1");
            TEXT1.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT1);

            DataColumn TEXT2 = new DataColumn("TEXT2");
            TEXT2.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT2);

            DataColumn TEXT3 = new DataColumn("TEXT3");
            TEXT3.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT3);

            DataColumn TEXT4 = new DataColumn("TEXT4");
            TEXT4.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT4);

            DataColumn TEXT5 = new DataColumn("TEXT5");
            TEXT5.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT5);

            //---------------------------------------------------------------

            DataView oDV = new DataView();
            oDV.Table = oDT;
            return oDV;
        }

        //-------------------------------------------------------------------------------------

        private DataView DV_SectionHeader2_Pop(DataView oDV, string strVal1, string strVal2, string strVal3, string strVal4, string strVal5)
        {
            DataRow oDR = oDV.Table.NewRow();

            oDR["ALTCOLOR"] = "";
            oDR["TEXT1"] = strVal1;
            oDR["TEXT2"] = strVal2;
            oDR["TEXT3"] = strVal3;
            oDR["TEXT4"] = strVal4;
            oDR["TEXT5"] = strVal5;
            //--------------------------------------- 
            oDV.Table.Rows.Add(oDR);
            oDR = null;
            oDR = oDV.Table.NewRow();

            return oDV;
        }

        //-----------------------------------------------------------------------        
        //-----------------------------------------------------------------------        
        //-----------------------------------------------------------------------        

        public DataView DV_SectionHeader3_Fill(DataView oDV, string strVal1, string strVal2, string strVal3, string strVal4, string strVal5)
        {
            oDV = DV_SectionHeader3_Open();
            oDV = DV_SectionHeader3_Pop(oDV, strVal1, strVal2, strVal3, strVal4, strVal5);

            return oDV;
        }

        //-----------------------------------------------------------------------        

        public DataView DV_SectionHeader3_Open()
        {
            DataTable oDT = new DataTable("PageHeader");
            //---------------------------------------------------------------
            DataColumn ALTCOLOR = new DataColumn("ALTCOLOR");
            ALTCOLOR.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(ALTCOLOR);

            DataColumn TEXT1 = new DataColumn("TEXT1");
            TEXT1.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT1);

            DataColumn TEXT2 = new DataColumn("TEXT2");
            TEXT2.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT2);

            DataColumn TEXT3 = new DataColumn("TEXT3");
            TEXT3.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT3);

            DataColumn TEXT4 = new DataColumn("TEXT4");
            TEXT4.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT4);

            DataColumn TEXT5 = new DataColumn("TEXT5");
            TEXT5.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT5);
            //---------------------------------------------------------------

            DataView oDV = new DataView();
            oDV.Table = oDT;
            return oDV;
        }

        //-------------------------------------------------------------------------------------

        private DataView DV_SectionHeader3_Pop(DataView oDV, string strVal1, string strVal2, string strVal3, string strVal4, string strVal5)
        {
            DataRow oDR = oDV.Table.NewRow();

            oDR["ALTCOLOR"] = "";
            oDR["TEXT1"] = strVal1;
            oDR["TEXT2"] = strVal2;
            oDR["TEXT3"] = strVal3;
            oDR["TEXT4"] = strVal4;
            oDR["TEXT5"] = strVal5;
            //--------------------------------------- 
            oDV.Table.Rows.Add(oDR);
            oDR = null;
            oDR = oDV.Table.NewRow();

            return oDV;
        }

        //-----------------------------------------------------------------------        
        //-----------------------------------------------------------------------        
        //-----------------------------------------------------------------------        

        public DataView DV_SectionHeader4_Fill(DataView oDV, string strVal1, string strVal2, string strVal3, string strVal4)
        {
            oDV = DV_SectionHeader4_Open();
            oDV = DV_SectionHeader4_Pop(oDV, strVal1, strVal2, strVal3, strVal4);

            return oDV;
        }

        //-----------------------------------------------------------------------        

        public DataView DV_SectionHeader4_Open()
        {
            DataTable oDT = new DataTable("PageHeader");
            //---------------------------------------------------------------
            DataColumn ALTCOLOR = new DataColumn("ALTCOLOR");
            ALTCOLOR.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(ALTCOLOR);

            DataColumn TEXT1 = new DataColumn("TEXT1");
            TEXT1.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT1);

            DataColumn TEXT2 = new DataColumn("TEXT2");
            TEXT2.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT2);

            DataColumn TEXT3 = new DataColumn("TEXT3");
            TEXT3.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT3);

            DataColumn TEXT4 = new DataColumn("TEXT4");
            TEXT4.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT4);

            //---------------------------------------------------------------

            DataView oDV = new DataView();
            oDV.Table = oDT;
            return oDV;
        }

        //-------------------------------------------------------------------------------------

        private DataView DV_SectionHeader4_Pop(DataView oDV, string strVal1, string strVal2, string strVal3, string strVal4)
        {
            DataRow oDR = oDV.Table.NewRow();

            oDR["ALTCOLOR"] = "";
            oDR["TEXT1"] = strVal1;
            oDR["TEXT2"] = strVal2;
            oDR["TEXT3"] = strVal3;
            oDR["TEXT4"] = strVal4;
            //--------------------------------------- 
            oDV.Table.Rows.Add(oDR);
            oDR = null;
            oDR = oDV.Table.NewRow();

            return oDV;
        }

        //-----------------------------------------------------------------------        
        //-----------------------------------------------------------------------        
        //-----------------------------------------------------------------------        

        public DataView DV_SectionHeader55_Fill(DataView oDV, string strVal1)
        {
            oDV = DV_SectionHeader55_Open();
            oDV = DV_SectionHeader55_Pop(oDV, strVal1);

            return oDV;
        }

        //-----------------------------------------------------------------------        

        public DataView DV_SectionHeader55_Open()
        {
            DataTable oDT = new DataTable("PageHeader");
            //---------------------------------------------------------------
            DataColumn ALTCOLOR = new DataColumn("ALTCOLOR");
            ALTCOLOR.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(ALTCOLOR);

            DataColumn TEXT1 = new DataColumn("TEXT1");
            TEXT1.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT1);

            //---------------------------------------------------------------

            DataView oDV = new DataView();
            oDV.Table = oDT;
            return oDV;
        }

        //-------------------------------------------------------------------------------------

        private DataView DV_SectionHeader55_Pop(DataView oDV, string strVal1)
        {
            DataRow oDR = oDV.Table.NewRow();

            oDR["ALTCOLOR"] = "";
            oDR["TEXT1"] = strVal1;
            //--------------------------------------- 
            oDV.Table.Rows.Add(oDR);
            oDR = null;
            oDR = oDV.Table.NewRow();

            return oDV;
        }

        //-----------------------------------------------------------------------        
        //-----------------------------------------------------------------------        
        //-----------------------------------------------------------------------        

        public DataView DV_Section1_Fill(DataView oDV, string strVal0, string strVal1, string strVal2, string strVal3, string strVal4, string strVal5, string strVal6, string strVal7, string strVal8, string strVal9)
        {

            oDV = DV_Section1_Pop(oDV, strVal0, strVal1, strVal2, strVal3, strVal4, strVal5, strVal6, strVal7, strVal8, strVal9);

            return oDV;
        }

        //-----------------------------------------------------------------------        

        public DataView DV_Section1_Open()
        {
            DataTable oDT = new DataTable("PageHeader");
            //---------------------------------------------------------------
            DataColumn ALTCOLOR = new DataColumn("ALTCOLOR");
            ALTCOLOR.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(ALTCOLOR);

            DataColumn TEXT1 = new DataColumn("TEXT1");
            TEXT1.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT1);

            DataColumn TEXT2 = new DataColumn("TEXT2");
            TEXT2.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT2);

            DataColumn TEXT3 = new DataColumn("TEXT3");
            TEXT3.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT3);

            DataColumn TEXT4 = new DataColumn("TEXT4");
            TEXT4.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT4);

            DataColumn TEXT5 = new DataColumn("TEXT5");
            TEXT5.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT5);

            DataColumn TEXT6 = new DataColumn("TEXT6");
            TEXT6.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT6);

            DataColumn TEXT7 = new DataColumn("TEXT7");
            TEXT7.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT7);

            DataColumn TEXT8 = new DataColumn("TEXT8");
            TEXT8.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT8);

            DataColumn TEXT9 = new DataColumn("TEXT9");
            TEXT9.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT9);
            //---------------------------------------------------------------

            DataView oDV = new DataView();
            oDV.Table = oDT;
            return oDV;
        }

        //-------------------------------------------------------------------------------------

        private DataView DV_Section1_Pop(DataView oDV, string strVal0, string strVal1, string strVal2, string strVal3, string strVal4, string strVal5, string strVal6, string strVal7, string strVal8, string strVal9)
        {
            DataRow oDR = oDV.Table.NewRow();

            oDR["ALTCOLOR"] = strVal0;
            oDR["TEXT1"] = strVal1;
            oDR["TEXT2"] = strVal2;
            oDR["TEXT3"] = strVal3;
            oDR["TEXT4"] = strVal4;
            oDR["TEXT5"] = strVal5;
            oDR["TEXT6"] = strVal6;
            oDR["TEXT7"] = strVal7;
            oDR["TEXT8"] = strVal8;
            oDR["TEXT9"] = strVal9;
            //--------------------------------------- 
            oDV.Table.Rows.Add(oDR);
            oDR = null;
            oDR = oDV.Table.NewRow();

            return oDV;
        }

        //-----------------------------------------------------------------------        
        //-----------------------------------------------------------------------        
        //-----------------------------------------------------------------------        

        public DataView DV_Section2_Fill(DataView oDV, string strVal0, string strVal1, string strVal2, string strVal3, string strVal4)
        {

            oDV = DV_Section2_Pop(oDV, strVal0, strVal1, strVal2, strVal3, strVal4);

            return oDV;
        }

        //-----------------------------------------------------------------------        

        public DataView DV_Section2_Open()
        {
            DataTable oDT = new DataTable("PageHeader");
            //---------------------------------------------------------------
            DataColumn ALTCOLOR = new DataColumn("ALTCOLOR");
            ALTCOLOR.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(ALTCOLOR);

            DataColumn TEXT1 = new DataColumn("TEXT1");
            TEXT1.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT1);

            DataColumn TEXT2 = new DataColumn("TEXT2");
            TEXT2.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT2);

            DataColumn TEXT3 = new DataColumn("TEXT3");
            TEXT3.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT3);

            DataColumn TEXT4 = new DataColumn("TEXT4");
            TEXT4.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT4);

            //---------------------------------------------------------------

            DataView oDV = new DataView();
            oDV.Table = oDT;
            return oDV;
        }

        //-------------------------------------------------------------------------------------

        private DataView DV_Section2_Pop(DataView oDV, string strVal0, string strVal1, string strVal2, string strVal3, string strVal4)
        {
            DataRow oDR = oDV.Table.NewRow();

            oDR["ALTCOLOR"] = strVal0;
            oDR["TEXT1"] = strVal1;
            oDR["TEXT2"] = strVal2;
            oDR["TEXT3"] = strVal3;
            oDR["TEXT4"] = strVal4;

            //--------------------------------------- 
            oDV.Table.Rows.Add(oDR);
            oDR = null;
            oDR = oDV.Table.NewRow();

            return oDV;
        }

        //-----------------------------------------------------------------------        
        //-----------------------------------------------------------------------        
        //-----------------------------------------------------------------------        

        public DataView DV_Section3_Fill(DataView oDV, string strVal0, string strVal1, string strVal2, string strVal3, string strVal4, string strVal5, string strVal6, string strVal7, string strVal8)
        {

            oDV = DV_Section3_Pop(oDV, strVal0, strVal1, strVal2, strVal3, strVal4, strVal5, strVal6, strVal7, strVal8);

            return oDV;
        }

        //-----------------------------------------------------------------------        

        public DataView DV_Section3_Open()
        {
            DataTable oDT = new DataTable("PageHeader");
            //---------------------------------------------------------------
            DataColumn ALTCOLOR = new DataColumn("ALTCOLOR");
            ALTCOLOR.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(ALTCOLOR);

            DataColumn TEXT1 = new DataColumn("TEXT1");
            TEXT1.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT1);

            DataColumn TEXT2 = new DataColumn("TEXT2");
            TEXT2.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT2);

            DataColumn TEXT3 = new DataColumn("TEXT3");
            TEXT3.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT3);

            DataColumn TEXT4 = new DataColumn("TEXT4");
            TEXT4.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT4);

            DataColumn TEXT5 = new DataColumn("TEXT5");
            TEXT5.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT5);

            DataColumn TEXT6 = new DataColumn("TEXT6");
            TEXT6.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT6);

            DataColumn TEXT7 = new DataColumn("TEXT7");
            TEXT7.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT7);

            DataColumn TEXT8 = new DataColumn("TEXT8");
            TEXT8.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT8);

            //---------------------------------------------------------------

            DataView oDV = new DataView();
            oDV.Table = oDT;
            return oDV;
        }

        //-------------------------------------------------------------------------------------

        private DataView DV_Section3_Pop(DataView oDV, string strVal0, string strVal1, string strVal2, string strVal3, string strVal4, string strVal5, string strVal6, string strVal7, string strVal8)
        {
            DataRow oDR = oDV.Table.NewRow();

            oDR["ALTCOLOR"] = strVal0;
            oDR["TEXT1"] = strVal1;
            oDR["TEXT2"] = strVal2;
            oDR["TEXT3"] = strVal3;
            oDR["TEXT4"] = strVal4;
            oDR["TEXT5"] = strVal5;
            oDR["TEXT6"] = strVal6;
            oDR["TEXT7"] = strVal7;
            oDR["TEXT8"] = strVal8;

            //--------------------------------------- 
            oDV.Table.Rows.Add(oDR);
            oDR = null;
            oDR = oDV.Table.NewRow();

            return oDV;
        }

        //-----------------------------------------------------------------------        
        //-----------------------------------------------------------------------        
        //-----------------------------------------------------------------------        

        public DataView DV_Section4_Fill(DataView oDV, string strVal0, string strVal1, string strVal2, string strVal3, string strVal4, string strVal5)
        {

            oDV = DV_Section4_Pop(oDV, strVal0, strVal1, strVal2, strVal3, strVal4, strVal5);

            return oDV;
        }

        //-----------------------------------------------------------------------        

        public DataView DV_Section4_Open()
        {
            DataTable oDT = new DataTable("PageHeader");
            //---------------------------------------------------------------
            DataColumn ALTCOLOR = new DataColumn("ALTCOLOR");
            ALTCOLOR.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(ALTCOLOR);

            DataColumn TEXT1 = new DataColumn("TEXT1");
            TEXT1.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT1);

            DataColumn TEXT2 = new DataColumn("TEXT2");
            TEXT2.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT2);

            DataColumn TEXT3 = new DataColumn("TEXT3");
            TEXT3.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT3);

            DataColumn TEXT4 = new DataColumn("TEXT4");
            TEXT4.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT4);

            DataColumn TEXT5 = new DataColumn("TEXT5");
            TEXT5.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT5);

            //---------------------------------------------------------------

            DataView oDV = new DataView();
            oDV.Table = oDT;
            return oDV;
        }

        //-------------------------------------------------------------------------------------

        private DataView DV_Section4_Pop(DataView oDV, string strVal0, string strVal1, string strVal2, string strVal3, string strVal4, string strVal5)
        {
            DataRow oDR = oDV.Table.NewRow();

            oDR["ALTCOLOR"] = strVal0;
            oDR["TEXT1"] = strVal1;
            oDR["TEXT2"] = strVal2;
            oDR["TEXT3"] = strVal3;
            oDR["TEXT4"] = strVal4;
            oDR["TEXT5"] = strVal5;

            //--------------------------------------- 
            oDV.Table.Rows.Add(oDR);
            oDR = null;
            oDR = oDV.Table.NewRow();

            return oDV;
        }

        //-----------------------------------------------------------------------        
        //-----------------------------------------------------------------------        
        //-----------------------------------------------------------------------        

        //-----------------------------------------------------------------------        
        //-----------------------------------------------------------------------        
        //-----------------------------------------------------------------------        

        public DataView DV_Section_TwoFields_Fill(DataView oDV, string strVal1, string strVal2)
        {
            oDV = DV_Section_TwoFields_Pop(oDV, strVal1, strVal2);

            return oDV;
        }

        //-----------------------------------------------------------------------        

        public DataView DV_Section_TwoFields_Open()
        {
            DataTable oDT = new DataTable("PageHeader");
            //---------------------------------------------------------------
            DataColumn ALTCOLOR = new DataColumn("ALTCOLOR");
            ALTCOLOR.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(ALTCOLOR);

            DataColumn TEXT1 = new DataColumn("TEXT1");
            TEXT1.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT1);

            DataColumn TEXT2 = new DataColumn("TEXT2");
            TEXT2.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT2);

            //---------------------------------------------------------------

            DataView oDV = new DataView();
            oDV.Table = oDT;
            return oDV;
        }

        //-------------------------------------------------------------------------------------

        private DataView DV_Section_TwoFields_Pop(DataView oDV, string strVal1, string strVal2)
        {
            DataRow oDR = oDV.Table.NewRow();

            oDR["ALTCOLOR"] = "";
            oDR["TEXT1"] = strVal1;
            oDR["TEXT2"] = strVal2;
            //--------------------------------------- 
            oDV.Table.Rows.Add(oDR);
            oDR = null;
            oDR = oDV.Table.NewRow();

            return oDV;
        }

        //-----------------------------------------------------------------------        
        //-----------------------------------------------------------------------        
        //-----------------------------------------------------------------------        


        //-----------------------------------------------------------------------        
        //-----------------------------------------------------------------------        
        //-----------------------------------------------------------------------        



        ~dataDetails()
        {
        }

    }
}

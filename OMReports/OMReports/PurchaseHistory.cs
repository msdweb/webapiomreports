﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OMReports
{
    public class PurchaseHistory
    {
        public string CustomerNumber { get; set; }
        public string OrderNumber { get; set; }
        public DateTime OrderDate { get; set; }
        public string CustomerPO { get; set; }
        public string ItemID { get; set; }
        public string ItemDesc { get; set; }
        public string UOM { get; set; }
        public string QtyOrdered { get; set; }
        public string UnitPrice { get; set; }
        public string ExtendedPrice { get; set; }
    }
}

﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.IO;
using System.Data.SqlClient;
using Newtonsoft.Json;

namespace OMReports
{
    sealed class MainClass
    {
        public static void Main()
        {
            GLB.Customer_no = "103846"; // "140218"; //"127317";

            GLB.dEndDate = DateTime.Now;
            GLB.dStartDate = GLB.dEndDate.AddMonths(-12);

            string sConn;
            //DbAccess.Set_MSDSQL();
            //DbAccess.Set_MSDSQL01();
            DbAccess.Set_MSDSQL03();
            sConn = DbAccess.ConnectionString;

            GLB.GetSavedOrderData("");
            Application.Run(new frmOMSavedOrder());

            //GLB.GetOrderConfirmationData("");
            //Application.Run(new frmOMOrderConfirmation());

            //GLB.GetBackordersData(sConn);
            //Application.Run(new frmOMBackorder());

            //GLB.GetPurchaseHistoryData(sConn);
            //Application.Run(new frmOMPurchaseHistory());

            //var shipTo = "";
            //var invoiceNumber = string.Empty;
            //var customerPo = string.Empty;
            //var orderNumber = string.Empty;
            //var StartDate = GLB.dStartDate.ToString();
            //var EndDate = GLB.dEndDate.ToString();
            //var pageSize = 1000;
            //var page = 1;
            //var sort = "InvoiceDate";
            //var sortDirection = "DESC";
            //var InvoiceStatus = 1;

            //object[] parameters = 
            //{
            //    shipTo,
            //    invoiceNumber,
            //    orderNumber,
            //    customerPo,
            //    StartDate,
            //    EndDate,
            //    pageSize,
            //    page,
            //    sort,
            //    sortDirection,
            //    InvoiceStatus
            //};

            //GLB.shipTo = parameters[0] != null ? JsonConvert.DeserializeObject<cInvoiceHistories.ShipToAddress>(parameters[0].ToString()) : new cInvoiceHistories.ShipToAddress(); //.shipTo
            //GLB.invoiceNumber = (string)parameters[1]; //.invoiceNumber;
            //GLB.orderNumber = (string)parameters[2]; //.orderNumber;
            //GLB.customerPo = (string)parameters[3]; //.customerPo;
            //GLB.StartDate = (string)parameters[4]; //.StartDate;
            //GLB.EndDate = (string)parameters[5]; //.EndDate;
            //GLB.pageSize = (int)parameters[6]; //.pageSize;
            //GLB.page = (int)parameters[7]; //.page;
            //GLB.sort = (string)parameters[8]; //.sort;
            //GLB.sortDirection = (string)parameters[9]; //.sortDirection;
            //GLB.InvoiceStatus = (int)parameters[10]; //.InvoiceStatus;

            //GLB.dStartDate = Convert.ToDateTime(GLB.StartDate);
            //GLB.dEndDate = Convert.ToDateTime(GLB.EndDate);

            //if (string.IsNullOrWhiteSpace(GLB.StartDate)) GLB.dStartDate = Convert.ToDateTime("01/01/1753");
            //if (string.IsNullOrWhiteSpace(GLB.EndDate)) GLB.dEndDate = DateTime.Now;

            //GLB.GetInvoiceListData(sConn);
            //Application.Run(new frmOMInvoiceList());
        }
    }

    //
    //******************************************************************************
    //

    public class OMSavedOrder
    {
        public static string Create_OMSavedOrder(string savedOrdersJson)
        {
            string FilePath = string.Empty;
            GLB.GetSavedOrderData(savedOrdersJson);
            frmSavedOrder frmSavedOrder = new frmSavedOrder();
            if (GLB.SavedOrderDetail.SavedOrderLine.Count > 0)
            {
                FilePath = "OMSavedOrder/MSD-OMSavedOrder-" + GLB.SavedOrderDetail.CustomerNumber + ".xps";
                FilePath = string.Format("{0}?t={1}", FilePath, DateTime.Now.Ticks);
                frmSavedOrder.PCS_Salva();
            }
            return FilePath;
        }
    }

    public class OMOrderConfirmation
    {
        public static string Create_OMOrderConfirmation(string orderDetailsJson)
        {
            string FilePath = string.Empty;
            GLB.GetOrderConfirmationData(orderDetailsJson);
            frmOrderConfirmation frmOrderConfirmation = new frmOrderConfirmation();
            if (GLB.OrderConfirmationDetail.OrderConfirmationLine.Count > 0)
            {
                FilePath = "OMOrderConfirmation/MSD-OMOrderConfirmation-" + GLB.OrderConfirmationDetail.OrderNumber + ".xps";
                FilePath = string.Format("{0}?t={1}", FilePath, DateTime.Now.Ticks);
                frmOrderConfirmation.PCS_Salva();
            }
            return FilePath;
        }
    }

    public class OMBackorders
    {
        public static string Create_OMBackorders(string Customer_no, string sConn)
        {
            string FilePath = string.Empty;
            GLB.Customer_no = Customer_no;
            GLB.GetBackordersData(sConn);
            frmBackorder frmBackorder = new frmBackorder();
            if (GLB.Backorders.Count > 0)
            {
                FilePath = "OMBackorders/MSD-OMBackorders-" + Customer_no + ".xps";
                FilePath = string.Format("{0}?t={1}", FilePath, DateTime.Now.Ticks);
                frmBackorder.PCS_Salva();
            }
            return FilePath;
        }
    }

    public class OMPurchaseHistory
    {
        public static string Create_OMPurchaseHistory(string Customer_no, DateTime dStartDate, DateTime dEndDate, string sConn)
        {
            string FilePath = string.Empty;
            GLB.Customer_no = Customer_no;
            GLB.dEndDate = dEndDate;
            GLB.dStartDate = dStartDate;
            GLB.GetPurchaseHistoryData(sConn);
            frmPurchaseHistory frmPurchaseHistory = new frmPurchaseHistory();
            if (GLB.PurchaseHistories.Count > 0)
            {
                FilePath = "OMPurchaseHistory/MSD-OMPurchaseHistory-" + Customer_no + "-" + dStartDate.ToString("MMddyyyy") + "-" + dEndDate.ToString("MMddyyyy") + ".xps";
                FilePath = string.Format("{0}?t={1}", FilePath, DateTime.Now.Ticks);
                frmPurchaseHistory.PCS_Salva();
            }
            return FilePath;
        }
    }

    public class OMInvoiceList
    {
        public static string Create_OMInvoiceList(string Customer_no, object[] parameters, string sConn)
        {
            string FilePath = string.Empty;
            GLB.Customer_no = Customer_no;

            GLB.shipTo = parameters[0] != null ? JsonConvert.DeserializeObject<cInvoiceHistories.ShipToAddress>(parameters[0].ToString()) : new cInvoiceHistories.ShipToAddress(); //.shipTo
            GLB.invoiceNumber = (string)parameters[1]; //.invoiceNumber;
            GLB.orderNumber = (string)parameters[2]; //.orderNumber;
            GLB.customerPo = (string)parameters[3]; //.customerPo;
            GLB.StartDate = (string)parameters[4]; //.StartDate;
            GLB.EndDate = (string)parameters[5]; //.EndDate;
            GLB.pageSize = Convert.ToInt32(parameters[6]); //.pageSize;
            GLB.page = Convert.ToInt32(parameters[7]); //.page;
            GLB.sort = (string)parameters[8]; //.sort;
            GLB.sortDirection = (string)parameters[9]; //.sortDirection;
            GLB.InvoiceStatus = Convert.ToInt32(parameters[10]); //.InvoiceStatus;

            GLB.dStartDate = !string.IsNullOrWhiteSpace(GLB.StartDate) ? Convert.ToDateTime(GLB.StartDate) : Convert.ToDateTime("01/01/1753");
            GLB.dEndDate = !string.IsNullOrWhiteSpace(GLB.EndDate) ? Convert.ToDateTime(GLB.EndDate) : DateTime.Now;

            //if (string.IsNullOrWhiteSpace(GLB.StartDate)) GLB.dStartDate = Convert.ToDateTime("01/01/1753");
            //if (string.IsNullOrWhiteSpace(GLB.EndDate)) GLB.dEndDate = DateTime.Now;

            GLB.GetInvoiceListData(sConn);
            frmInvoiceList frmInvoiceList = new frmInvoiceList();
            if (GLB.InvoiceHistories.Count > 0)
            {
                FilePath = "OMInvoiceList/MSD-OMInvoiceList-" + Customer_no + ".xps";
                FilePath = string.Format("{0}?t={1}", FilePath, DateTime.Now.Ticks);
                frmInvoiceList.PCS_Salva();
            }
            return FilePath;
        }
    }

    //
    //******************************************************************************
    //

    public class  GLB
    {
        public static SavedOrderDetail SavedOrderDetail = new SavedOrderDetail();
        public static OrderConfirmationDetail OrderConfirmationDetail = new OrderConfirmationDetail();
        public static List<Backorder> Backorders = new List<Backorder>();
        public static List<PurchaseHistory> PurchaseHistories = new List<PurchaseHistory>();
        public static List<InvoiceHistory> InvoiceHistories = new List<InvoiceHistory>();

        public static string Customer_no = string.Empty;

        public static cInvoiceHistories.ShipToAddress address;

        public static string StartDate;
        public static string EndDate;

        public static cInvoiceHistories.ShipToAddress shipTo;
        public static string invoiceNumber;
        public static string customerPo;
        public static string orderNumber;
        public static int InvoiceStatus;
        public static DateTime dStartDate;
        public static DateTime dEndDate;
        public static int pageSize;
        public static int page;
        public static string sort;
        public static string sortDirection;

        public static void GetSavedOrderData(string savedOrderDetailsJson)
        {
            //savedOrderDetailsJson = cSavedOrderDetails.Get_SavedOrderDetail("");
            //###################################################################

            SavedOrderDetail = Newtonsoft.Json.JsonConvert.DeserializeObject<SavedOrderDetail>(savedOrderDetailsJson);

            return;
        }

        public static void GetOrderConfirmationData(string orderDetailsJson)
        {
            //orderDetailsJson = cOrderConfirmationDetails.Get_OrderConfirmationDetail("");
            //###################################################################

            OrderConfirmationDetail = Newtonsoft.Json.JsonConvert.DeserializeObject<OrderConfirmationDetail>(orderDetailsJson);

            return;
        }

        public static void GetBackordersData(string sConn)
        {

            //###################################################################

            Backorders = cBackorders.Get_Backorders(Customer_no, sConn);

            return;
        }

        public static void GetPurchaseHistoryData(string sConn)
        {

            //###################################################################

            PurchaseHistories = cPurchaseHistories.Get_PurchaseHistories(Customer_no, dStartDate, dEndDate, sConn);

            return;
        }

        public static void GetInvoiceListData(string sConn)
        {
            address = new cInvoiceHistories.ShipToAddress();

            //###################################################################

            InvoiceHistories = cInvoiceHistories.Get_InvoiceHistories(Customer_no, address, invoiceNumber.ToString(), orderNumber.ToString(), customerPo.ToString(),
                                                                      dStartDate, dEndDate, pageSize, page, sort.ToString(), sortDirection.ToString(), InvoiceStatus.ToString(), sConn);

            return;
        }


        //
        //******************************************************************************
        //

        public static bool IsNumeric(object Expression)
        {
            bool isNum;
            double retNum;
            isNum = Double.TryParse(Convert.ToString(Expression),
                    System.Globalization.NumberStyles.Any, 
                    System.Globalization.NumberFormatInfo.InvariantInfo, 
                    out retNum);
            return isNum;
        }

        public static bool HasAnyNum(string numberString)
        { 
          char [] ca = numberString.ToCharArray();

          foreach (char i in ca)
          {
              if (char.IsNumber(i))
                  return true;
          } 
          return false; 
        }
        public static bool HasAnyNum1(string numberString)
        {
            char[] ca = numberString.ToCharArray();
            for (int i = 0; i < ca.Length; i++)
            {
                if (char.IsNumber(ca[i]))
                    return true;
            }
            return false;
        }

        ~GLB()
        {
        }

    }
    
    public class AddValue
    {
        private string m_Display;
        private string m_Value;
        public AddValue(string Display, string Value)
        {
            m_Display = Display;
            m_Value = Value;
        }
        public string Display
        {
            get { return m_Display; }
        }
        public string Value
        {
            get { return m_Value; }
        }
    }
}

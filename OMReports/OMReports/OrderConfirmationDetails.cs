﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Collections.Specialized;
using Newtonsoft.Json;

namespace OMReports
{
    public class cOrderConfirmationDetails
    {
        public static string Get_OrderConfirmationDetail(string jsonorder)
        {
            OrderConfirmationDetail OrderConfirmationDetail = new OrderConfirmationDetail();

            OrderConfirmationDetail.OrderNumber = "S001173";
            OrderConfirmationDetail.OrderDate = (DateTime.Now).AddDays(0);
            OrderConfirmationDetail.RequestedShipDate = (DateTime.Now).AddDays(3);
            OrderConfirmationDetail.CustomerNumber = "140218-1";
            OrderConfirmationDetail.CustomerPurchaseOrder = "LEO 2016-06-15 1215";
            OrderConfirmationDetail.PlacedBy = "PlacedBy";
            OrderConfirmationDetail.BillingCompanyName = "BIOCURE, LLC";
            OrderConfirmationDetail.BillingAddress1 = "8700 COMMERCE PARK DR, STE 241";
            OrderConfirmationDetail.BillingAddress2 = "";
            OrderConfirmationDetail.BillingCity = "HOUSTON";
            OrderConfirmationDetail.BillingState = "TX";
            OrderConfirmationDetail.BillingPostalCode = "77036";
            OrderConfirmationDetail.BillingCountry = "US";
            OrderConfirmationDetail.ShippingCompanyName = "BIOCURE, LLC";
            OrderConfirmationDetail.ShippingAddress1 = "8700 COMMERCE PARK DR, STE 241";
            OrderConfirmationDetail.ShippingAddress2 = "";
            OrderConfirmationDetail.ShippingCity = "HOUSTON";
            OrderConfirmationDetail.ShippingState = "TX";
            OrderConfirmationDetail.ShippingPostalCode = "77036";
            OrderConfirmationDetail.ShippingCountry = "US";

            OrderConfirmationDetail.Notes = "Order confirmation print out test";
            OrderConfirmationDetail.FreightInfoOption = "Standard Ground Shipping - Inside Delivery, Guaranteed Delivery, Sort And Segregation";
            OrderConfirmationDetail.WebOrderNumber = "S001173";
            OrderConfirmationDetail.PlacedBy = "PlacedBy";

            OrderConfirmationDetail.ProductSubtotal = "$1,358.88";


            OrderConfirmationLine OrderConfirmationLine = new OrderConfirmationLine();

            OrderConfirmationLine.unit_quantity = "1.00000";
            OrderConfirmationLine.unit_of_measure = "BX24";
            OrderConfirmationLine.item_id = "3M1527-0";
            OrderConfirmationLine.item_desc = "TRANSPORE CLEAR TAPE 1/2X10IN";
            OrderConfirmationLine.unit_price = "$9.42";
            OrderConfirmationLine.extended_price = "$9.42";
            OrderConfirmationDetail.OrderConfirmationLine.Add(OrderConfirmationLine);

            OrderConfirmationLine = new OrderConfirmationLine();

            OrderConfirmationLine.unit_quantity = "5.00000";
            OrderConfirmationLine.unit_of_measure = "BX50";
            OrderConfirmationLine.item_id = "3M1685";
            OrderConfirmationLine.item_desc = "TEGADERM IV ADVANCED DRESSING 3.5X4.5IN";
            OrderConfirmationLine.unit_price = "$77.60";
            OrderConfirmationLine.extended_price = "$388.02";
            OrderConfirmationDetail.OrderConfirmationLine.Add(OrderConfirmationLine);

            OrderConfirmationLine = new OrderConfirmationLine();

            OrderConfirmationLine.unit_quantity = "5.00000";
            OrderConfirmationLine.unit_of_measure = "BX50";
            OrderConfirmationLine.item_id = "3M1816";
            OrderConfirmationLine.item_desc = "TIE ON SURGICAL MASK FLUID RESISTANT";
            OrderConfirmationLine.unit_price = "$4.19";
            OrderConfirmationLine.extended_price = "$20.94";
            OrderConfirmationDetail.OrderConfirmationLine.Add(OrderConfirmationLine);

            OrderConfirmationLine = new OrderConfirmationLine();

            OrderConfirmationLine.unit_quantity = "3.00000";
            OrderConfirmationLine.unit_of_measure = "CS12";
            OrderConfirmationLine.item_id = "3M9321A";
            OrderConfirmationLine.item_desc = "AVAGARD FOAMING HAND SANITIZER 500ML";
            OrderConfirmationLine.unit_price = "$105.00";
            OrderConfirmationLine.extended_price = "$315.00";
            OrderConfirmationDetail.OrderConfirmationLine.Add(OrderConfirmationLine);

            OrderConfirmationLine = new OrderConfirmationLine();

            OrderConfirmationLine.unit_quantity = "5.00000";
            OrderConfirmationLine.unit_of_measure = "CS24";
            OrderConfirmationLine.item_id = "RS56277";
            OrderConfirmationLine.item_desc = "VITAL 1.0 CAL VANILLA 8 FL OZ CAN";
            OrderConfirmationLine.unit_price = "$125.10";
            OrderConfirmationLine.extended_price = "$625.50";
            OrderConfirmationDetail.OrderConfirmationLine.Add(OrderConfirmationLine);

            string json = Newtonsoft.Json.JsonConvert.SerializeObject(OrderConfirmationDetail);

            //--------------------------------------------------------------------------------------------------------------
            //var OrderConfirmationDetail1 = Newtonsoft.Json.JsonConvert.DeserializeObject<OrderConfirmationDetail>(jsonorder);

            return json;
        }
    }
}

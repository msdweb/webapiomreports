﻿namespace OMReports
{
    partial class frmSavedOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle40 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle41 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle42 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle43 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle44 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle47 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle48 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle45 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle46 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle49 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle50 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle51 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle52 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle53 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle54 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle55 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle61 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle62 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle56 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle57 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle58 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle59 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle60 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabHeader = new System.Windows.Forms.TabPage();
            this.dgvPageHeader = new System.Windows.Forms.DataGridView();
            this.dgvGroup1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn31 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvEndOTInvoices = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvEndGroupMsg = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblFooter = new System.Windows.Forms.Label();
            this.dgvEmptyMessage = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn32 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabHeaderSections = new System.Windows.Forms.TabPage();
            this.dgvSection3 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn39 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn40 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvSectionHeader2 = new System.Windows.Forms.DataGridView();
            this.dgvSectionHeaderYourOrder = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvSectionHeaderA0 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvSectionHeaderA1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvSectionLine1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabSections = new System.Windows.Forms.TabPage();
            this.dgvSectionYourOrder = new System.Windows.Forms.DataGridView();
            this.Field1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Field2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Field3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Field5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Field6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabControl1.SuspendLayout();
            this.tabHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPageHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEndOTInvoices)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEndGroupMsg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmptyMessage)).BeginInit();
            this.tabHeaderSections.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSection3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeader2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeaderYourOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeaderA0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeaderA1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionLine1)).BeginInit();
            this.tabSections.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionYourOrder)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabHeader);
            this.tabControl1.Controls.Add(this.tabHeaderSections);
            this.tabControl1.Controls.Add(this.tabSections);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(832, 569);
            this.tabControl1.TabIndex = 19;
            // 
            // tabHeader
            // 
            this.tabHeader.Controls.Add(this.dgvPageHeader);
            this.tabHeader.Controls.Add(this.dgvGroup1);
            this.tabHeader.Controls.Add(this.dgvEndOTInvoices);
            this.tabHeader.Controls.Add(this.dgvEndGroupMsg);
            this.tabHeader.Controls.Add(this.lblFooter);
            this.tabHeader.Controls.Add(this.dgvEmptyMessage);
            this.tabHeader.Location = new System.Drawing.Point(4, 22);
            this.tabHeader.Name = "tabHeader";
            this.tabHeader.Padding = new System.Windows.Forms.Padding(3);
            this.tabHeader.Size = new System.Drawing.Size(824, 543);
            this.tabHeader.TabIndex = 1;
            this.tabHeader.Text = "Header";
            this.tabHeader.UseVisualStyleBackColor = true;
            // 
            // dgvPageHeader
            // 
            this.dgvPageHeader.AllowUserToAddRows = false;
            this.dgvPageHeader.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvPageHeader.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvPageHeader.BackgroundColor = System.Drawing.Color.White;
            this.dgvPageHeader.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvPageHeader.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvPageHeader.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvPageHeader.ColumnHeadersHeight = 50;
            this.dgvPageHeader.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Arial", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvPageHeader.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvPageHeader.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvPageHeader.GridColor = System.Drawing.Color.Black;
            this.dgvPageHeader.Location = new System.Drawing.Point(580, 17);
            this.dgvPageHeader.MultiSelect = false;
            this.dgvPageHeader.Name = "dgvPageHeader";
            this.dgvPageHeader.RowHeadersVisible = false;
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvPageHeader.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvPageHeader.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvPageHeader.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPageHeader.Size = new System.Drawing.Size(200, 30);
            this.dgvPageHeader.TabIndex = 40;
            // 
            // dgvGroup1
            // 
            this.dgvGroup1.AllowUserToAddRows = false;
            this.dgvGroup1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvGroup1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvGroup1.BackgroundColor = System.Drawing.Color.Gray;
            this.dgvGroup1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvGroup1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dgvGroup1.ColumnHeadersHeight = 30;
            this.dgvGroup1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn31});
            this.dgvGroup1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvGroup1.GridColor = System.Drawing.Color.Black;
            this.dgvGroup1.Location = new System.Drawing.Point(35, 126);
            this.dgvGroup1.MultiSelect = false;
            this.dgvGroup1.Name = "dgvGroup1";
            this.dgvGroup1.RowHeadersVisible = false;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvGroup1.RowsDefaultCellStyle = dataGridViewCellStyle8;
            this.dgvGroup1.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvGroup1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvGroup1.Size = new System.Drawing.Size(745, 28);
            this.dgvGroup1.TabIndex = 39;
            // 
            // dataGridViewTextBoxColumn31
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn31.DefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridViewTextBoxColumn31.HeaderText = "Group Header";
            this.dataGridViewTextBoxColumn31.Name = "dataGridViewTextBoxColumn31";
            this.dataGridViewTextBoxColumn31.Width = 745;
            // 
            // dgvEndOTInvoices
            // 
            this.dgvEndOTInvoices.AllowUserToAddRows = false;
            this.dgvEndOTInvoices.AllowUserToDeleteRows = false;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvEndOTInvoices.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle9;
            this.dgvEndOTInvoices.BackgroundColor = System.Drawing.Color.Gray;
            this.dgvEndOTInvoices.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvEndOTInvoices.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dgvEndOTInvoices.ColumnHeadersHeight = 30;
            this.dgvEndOTInvoices.ColumnHeadersVisible = false;
            this.dgvEndOTInvoices.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1});
            this.dgvEndOTInvoices.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvEndOTInvoices.GridColor = System.Drawing.Color.Black;
            this.dgvEndOTInvoices.Location = new System.Drawing.Point(35, 340);
            this.dgvEndOTInvoices.MultiSelect = false;
            this.dgvEndOTInvoices.Name = "dgvEndOTInvoices";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEndOTInvoices.RowHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dgvEndOTInvoices.RowHeadersVisible = false;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle11.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvEndOTInvoices.RowsDefaultCellStyle = dataGridViewCellStyle11;
            this.dgvEndOTInvoices.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvEndOTInvoices.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvEndOTInvoices.Size = new System.Drawing.Size(745, 25);
            this.dgvEndOTInvoices.TabIndex = 38;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "";
            this.Column1.Name = "Column1";
            this.Column1.Width = 745;
            // 
            // dgvEndGroupMsg
            // 
            this.dgvEndGroupMsg.AllowUserToAddRows = false;
            this.dgvEndGroupMsg.AllowUserToDeleteRows = false;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle12.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvEndGroupMsg.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle12;
            this.dgvEndGroupMsg.BackgroundColor = System.Drawing.Color.White;
            this.dgvEndGroupMsg.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvEndGroupMsg.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvEndGroupMsg.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEndGroupMsg.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.dgvEndGroupMsg.ColumnHeadersHeight = 30;
            this.dgvEndGroupMsg.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn3});
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvEndGroupMsg.DefaultCellStyle = dataGridViewCellStyle15;
            this.dgvEndGroupMsg.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvEndGroupMsg.GridColor = System.Drawing.Color.Black;
            this.dgvEndGroupMsg.Location = new System.Drawing.Point(35, 283);
            this.dgvEndGroupMsg.MultiSelect = false;
            this.dgvEndGroupMsg.Name = "dgvEndGroupMsg";
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEndGroupMsg.RowHeadersDefaultCellStyle = dataGridViewCellStyle16;
            this.dgvEndGroupMsg.RowHeadersVisible = false;
            dataGridViewCellStyle17.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle17.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvEndGroupMsg.RowsDefaultCellStyle = dataGridViewCellStyle17;
            this.dgvEndGroupMsg.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvEndGroupMsg.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvEndGroupMsg.Size = new System.Drawing.Size(745, 31);
            this.dgvEndGroupMsg.TabIndex = 37;
            // 
            // dataGridViewTextBoxColumn3
            // 
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle14;
            this.dataGridViewTextBoxColumn3.HeaderText = "End Group Header";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 745;
            // 
            // lblFooter
            // 
            this.lblFooter.BackColor = System.Drawing.Color.White;
            this.lblFooter.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFooter.ForeColor = System.Drawing.Color.Black;
            this.lblFooter.Location = new System.Drawing.Point(35, 382);
            this.lblFooter.Name = "lblFooter";
            this.lblFooter.Size = new System.Drawing.Size(745, 15);
            this.lblFooter.TabIndex = 27;
            this.lblFooter.Text = "MSD";
            // 
            // dgvEmptyMessage
            // 
            this.dgvEmptyMessage.AllowUserToAddRows = false;
            this.dgvEmptyMessage.AllowUserToDeleteRows = false;
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle18.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle18.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvEmptyMessage.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle18;
            this.dgvEmptyMessage.BackgroundColor = System.Drawing.Color.Gray;
            this.dgvEmptyMessage.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvEmptyMessage.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle19.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle19.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEmptyMessage.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle19;
            this.dgvEmptyMessage.ColumnHeadersHeight = 20;
            this.dgvEmptyMessage.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn32});
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle21.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle21.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle21.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle21.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle21.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvEmptyMessage.DefaultCellStyle = dataGridViewCellStyle21;
            this.dgvEmptyMessage.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvEmptyMessage.GridColor = System.Drawing.Color.Black;
            this.dgvEmptyMessage.Location = new System.Drawing.Point(35, 412);
            this.dgvEmptyMessage.MultiSelect = false;
            this.dgvEmptyMessage.Name = "dgvEmptyMessage";
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle22.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle22.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle22.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle22.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle22.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEmptyMessage.RowHeadersDefaultCellStyle = dataGridViewCellStyle22;
            this.dgvEmptyMessage.RowHeadersVisible = false;
            dataGridViewCellStyle23.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle23.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle23.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle23.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvEmptyMessage.RowsDefaultCellStyle = dataGridViewCellStyle23;
            this.dgvEmptyMessage.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvEmptyMessage.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvEmptyMessage.Size = new System.Drawing.Size(745, 19);
            this.dgvEmptyMessage.TabIndex = 26;
            // 
            // dataGridViewTextBoxColumn32
            // 
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn32.DefaultCellStyle = dataGridViewCellStyle20;
            this.dataGridViewTextBoxColumn32.HeaderText = "No items in this section";
            this.dataGridViewTextBoxColumn32.Name = "dataGridViewTextBoxColumn32";
            this.dataGridViewTextBoxColumn32.Width = 745;
            // 
            // tabHeaderSections
            // 
            this.tabHeaderSections.Controls.Add(this.dgvSection3);
            this.tabHeaderSections.Controls.Add(this.dgvSectionHeader2);
            this.tabHeaderSections.Controls.Add(this.dgvSectionHeaderYourOrder);
            this.tabHeaderSections.Controls.Add(this.dgvSectionHeaderA0);
            this.tabHeaderSections.Controls.Add(this.dgvSectionHeaderA1);
            this.tabHeaderSections.Controls.Add(this.dgvSectionLine1);
            this.tabHeaderSections.Location = new System.Drawing.Point(4, 22);
            this.tabHeaderSections.Name = "tabHeaderSections";
            this.tabHeaderSections.Size = new System.Drawing.Size(824, 543);
            this.tabHeaderSections.TabIndex = 2;
            this.tabHeaderSections.Text = "HeaderSection";
            this.tabHeaderSections.UseVisualStyleBackColor = true;
            // 
            // dgvSection3
            // 
            this.dgvSection3.AllowUserToAddRows = false;
            this.dgvSection3.AllowUserToDeleteRows = false;
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle24.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle24.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle24.SelectionBackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle24.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSection3.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle24;
            this.dgvSection3.BackgroundColor = System.Drawing.Color.White;
            this.dgvSection3.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvSection3.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle25.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle25.Font = new System.Drawing.Font("Times New Roman", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle25.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle25.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle25.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle25.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSection3.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle25;
            this.dgvSection3.ColumnHeadersHeight = 30;
            this.dgvSection3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn39,
            this.dataGridViewTextBoxColumn40});
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle28.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle28.Font = new System.Drawing.Font("Times New Roman", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle28.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle28.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle28.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle28.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSection3.DefaultCellStyle = dataGridViewCellStyle28;
            this.dgvSection3.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSection3.GridColor = System.Drawing.Color.Black;
            this.dgvSection3.Location = new System.Drawing.Point(480, 217);
            this.dgvSection3.MultiSelect = false;
            this.dgvSection3.Name = "dgvSection3";
            this.dgvSection3.RowHeadersVisible = false;
            dataGridViewCellStyle29.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle29.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle29.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle29.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSection3.RowsDefaultCellStyle = dataGridViewCellStyle29;
            this.dgvSection3.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvSection3.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSection3.Size = new System.Drawing.Size(300, 30);
            this.dgvSection3.TabIndex = 70;
            // 
            // dataGridViewTextBoxColumn39
            // 
            this.dataGridViewTextBoxColumn39.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn39.DefaultCellStyle = dataGridViewCellStyle26;
            this.dataGridViewTextBoxColumn39.HeaderText = "SubTotal";
            this.dataGridViewTextBoxColumn39.Name = "dataGridViewTextBoxColumn39";
            this.dataGridViewTextBoxColumn39.Width = 200;
            // 
            // dataGridViewTextBoxColumn40
            // 
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn40.DefaultCellStyle = dataGridViewCellStyle27;
            this.dataGridViewTextBoxColumn40.HeaderText = "Amount";
            this.dataGridViewTextBoxColumn40.Name = "dataGridViewTextBoxColumn40";
            // 
            // dgvSectionHeader2
            // 
            this.dgvSectionHeader2.AllowUserToAddRows = false;
            this.dgvSectionHeader2.AllowUserToDeleteRows = false;
            dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle30.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle30.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle30.SelectionBackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle30.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeader2.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle30;
            this.dgvSectionHeader2.BackgroundColor = System.Drawing.Color.White;
            this.dgvSectionHeader2.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvSectionHeader2.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle31.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle31.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle31.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle31.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle31.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle31.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSectionHeader2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle31;
            this.dgvSectionHeader2.ColumnHeadersHeight = 22;
            this.dgvSectionHeader2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.Column2});
            dataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle32.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle32.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle32.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle32.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle32.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle32.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSectionHeader2.DefaultCellStyle = dataGridViewCellStyle32;
            this.dgvSectionHeader2.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSectionHeader2.GridColor = System.Drawing.Color.Black;
            this.dgvSectionHeader2.Location = new System.Drawing.Point(35, 69);
            this.dgvSectionHeader2.MultiSelect = false;
            this.dgvSectionHeader2.Name = "dgvSectionHeader2";
            this.dgvSectionHeader2.RowHeadersVisible = false;
            dataGridViewCellStyle33.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle33.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeader2.RowsDefaultCellStyle = dataGridViewCellStyle33;
            this.dgvSectionHeader2.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvSectionHeader2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSectionHeader2.Size = new System.Drawing.Size(745, 22);
            this.dgvSectionHeader2.TabIndex = 69;
            // 
            // dgvSectionHeaderYourOrder
            // 
            this.dgvSectionHeaderYourOrder.AllowUserToAddRows = false;
            this.dgvSectionHeaderYourOrder.AllowUserToDeleteRows = false;
            dataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle34.BackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle34.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle34.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle34.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeaderYourOrder.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle34;
            this.dgvSectionHeaderYourOrder.BackgroundColor = System.Drawing.Color.Gray;
            this.dgvSectionHeaderYourOrder.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvSectionHeaderYourOrder.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvSectionHeaderYourOrder.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle35.BackColor = System.Drawing.SystemColors.AppWorkspace;
            dataGridViewCellStyle35.Font = new System.Drawing.Font("Times New Roman", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle35.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle35.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle35.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle35.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSectionHeaderYourOrder.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle35;
            this.dgvSectionHeaderYourOrder.ColumnHeadersHeight = 33;
            this.dgvSectionHeaderYourOrder.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn4});
            dataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle37.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle37.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle37.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle37.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle37.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle37.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSectionHeaderYourOrder.DefaultCellStyle = dataGridViewCellStyle37;
            this.dgvSectionHeaderYourOrder.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSectionHeaderYourOrder.GridColor = System.Drawing.Color.Black;
            this.dgvSectionHeaderYourOrder.Location = new System.Drawing.Point(35, 165);
            this.dgvSectionHeaderYourOrder.MultiSelect = false;
            this.dgvSectionHeaderYourOrder.Name = "dgvSectionHeaderYourOrder";
            this.dgvSectionHeaderYourOrder.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvSectionHeaderYourOrder.RowHeadersVisible = false;
            dataGridViewCellStyle38.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle38.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle38.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle38.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeaderYourOrder.RowsDefaultCellStyle = dataGridViewCellStyle38;
            this.dgvSectionHeaderYourOrder.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvSectionHeaderYourOrder.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSectionHeaderYourOrder.Size = new System.Drawing.Size(745, 33);
            this.dgvSectionHeaderYourOrder.TabIndex = 68;
            // 
            // dataGridViewTextBoxColumn4
            // 
            dataGridViewCellStyle36.NullValue = null;
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle36;
            this.dataGridViewTextBoxColumn4.HeaderText = "Your Order";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Width = 745;
            // 
            // dgvSectionHeaderA0
            // 
            this.dgvSectionHeaderA0.AllowUserToAddRows = false;
            this.dgvSectionHeaderA0.AllowUserToDeleteRows = false;
            dataGridViewCellStyle39.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle39.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle39.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle39.SelectionBackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle39.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeaderA0.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle39;
            this.dgvSectionHeaderA0.BackgroundColor = System.Drawing.Color.White;
            this.dgvSectionHeaderA0.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvSectionHeaderA0.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvSectionHeaderA0.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle40.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle40.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle40.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle40.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle40.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle40.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle40.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSectionHeaderA0.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle40;
            this.dgvSectionHeaderA0.ColumnHeadersHeight = 22;
            this.dgvSectionHeaderA0.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1});
            dataGridViewCellStyle41.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle41.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle41.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle41.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle41.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle41.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle41.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSectionHeaderA0.DefaultCellStyle = dataGridViewCellStyle41;
            this.dgvSectionHeaderA0.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSectionHeaderA0.GridColor = System.Drawing.Color.Black;
            this.dgvSectionHeaderA0.Location = new System.Drawing.Point(35, 22);
            this.dgvSectionHeaderA0.MultiSelect = false;
            this.dgvSectionHeaderA0.Name = "dgvSectionHeaderA0";
            this.dgvSectionHeaderA0.RowHeadersVisible = false;
            dataGridViewCellStyle42.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle42.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeaderA0.RowsDefaultCellStyle = dataGridViewCellStyle42;
            this.dgvSectionHeaderA0.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvSectionHeaderA0.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSectionHeaderA0.Size = new System.Drawing.Size(250, 22);
            this.dgvSectionHeaderA0.TabIndex = 67;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 250;
            // 
            // dgvSectionHeaderA1
            // 
            this.dgvSectionHeaderA1.AllowUserToAddRows = false;
            this.dgvSectionHeaderA1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle43.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle43.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle43.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle43.SelectionBackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle43.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeaderA1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle43;
            this.dgvSectionHeaderA1.BackgroundColor = System.Drawing.Color.White;
            this.dgvSectionHeaderA1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvSectionHeaderA1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle44.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle44.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle44.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle44.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle44.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle44.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle44.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSectionHeaderA1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle44;
            this.dgvSectionHeaderA1.ColumnHeadersHeight = 22;
            this.dgvSectionHeaderA1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn19,
            this.dataGridViewTextBoxColumn21});
            dataGridViewCellStyle47.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle47.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle47.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle47.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle47.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle47.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle47.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSectionHeaderA1.DefaultCellStyle = dataGridViewCellStyle47;
            this.dgvSectionHeaderA1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSectionHeaderA1.GridColor = System.Drawing.Color.Black;
            this.dgvSectionHeaderA1.Location = new System.Drawing.Point(380, 22);
            this.dgvSectionHeaderA1.MultiSelect = false;
            this.dgvSectionHeaderA1.Name = "dgvSectionHeaderA1";
            this.dgvSectionHeaderA1.RowHeadersVisible = false;
            dataGridViewCellStyle48.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle48.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle48.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle48.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeaderA1.RowsDefaultCellStyle = dataGridViewCellStyle48;
            this.dgvSectionHeaderA1.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvSectionHeaderA1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSectionHeaderA1.Size = new System.Drawing.Size(400, 22);
            this.dgvSectionHeaderA1.TabIndex = 66;
            // 
            // dataGridViewTextBoxColumn19
            // 
            this.dataGridViewTextBoxColumn19.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle45.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dataGridViewTextBoxColumn19.DefaultCellStyle = dataGridViewCellStyle45;
            this.dataGridViewTextBoxColumn19.HeaderText = "Order Header";
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.Width = 150;
            // 
            // dataGridViewTextBoxColumn21
            // 
            dataGridViewCellStyle46.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dataGridViewTextBoxColumn21.DefaultCellStyle = dataGridViewCellStyle46;
            this.dataGridViewTextBoxColumn21.HeaderText = "";
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            this.dataGridViewTextBoxColumn21.Width = 250;
            // 
            // dgvSectionLine1
            // 
            this.dgvSectionLine1.AllowUserToAddRows = false;
            this.dgvSectionLine1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle49.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle49.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle49.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle49.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionLine1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle49;
            this.dgvSectionLine1.BackgroundColor = System.Drawing.Color.White;
            this.dgvSectionLine1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvSectionLine1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle50.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle50.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle50.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle50.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle50.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle50.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSectionLine1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle50;
            this.dgvSectionLine1.ColumnHeadersHeight = 15;
            this.dgvSectionLine1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn18});
            dataGridViewCellStyle51.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle51.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle51.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle51.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle51.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle51.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle51.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSectionLine1.DefaultCellStyle = dataGridViewCellStyle51;
            this.dgvSectionLine1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSectionLine1.GridColor = System.Drawing.Color.Black;
            this.dgvSectionLine1.Location = new System.Drawing.Point(35, 120);
            this.dgvSectionLine1.MultiSelect = false;
            this.dgvSectionLine1.Name = "dgvSectionLine1";
            dataGridViewCellStyle52.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle52.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle52.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle52.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle52.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle52.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSectionLine1.RowHeadersDefaultCellStyle = dataGridViewCellStyle52;
            this.dgvSectionLine1.RowHeadersVisible = false;
            dataGridViewCellStyle53.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle53.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle53.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionLine1.RowsDefaultCellStyle = dataGridViewCellStyle53;
            this.dgvSectionLine1.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvSectionLine1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSectionLine1.Size = new System.Drawing.Size(745, 15);
            this.dgvSectionLine1.TabIndex = 49;
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.HeaderText = "";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.Width = 745;
            // 
            // tabSections
            // 
            this.tabSections.Controls.Add(this.dgvSectionYourOrder);
            this.tabSections.Location = new System.Drawing.Point(4, 22);
            this.tabSections.Name = "tabSections";
            this.tabSections.Size = new System.Drawing.Size(824, 543);
            this.tabSections.TabIndex = 3;
            this.tabSections.Text = "Sections";
            this.tabSections.UseVisualStyleBackColor = true;
            // 
            // dgvSectionYourOrder
            // 
            this.dgvSectionYourOrder.AllowUserToAddRows = false;
            this.dgvSectionYourOrder.AllowUserToDeleteRows = false;
            dataGridViewCellStyle54.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle54.BackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle54.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle54.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle54.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionYourOrder.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle54;
            this.dgvSectionYourOrder.BackgroundColor = System.Drawing.Color.Gray;
            this.dgvSectionYourOrder.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvSectionYourOrder.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvSectionYourOrder.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle55.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            dataGridViewCellStyle55.BackColor = System.Drawing.SystemColors.AppWorkspace;
            dataGridViewCellStyle55.Font = new System.Drawing.Font("Times New Roman", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle55.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle55.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle55.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle55.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSectionYourOrder.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle55;
            this.dgvSectionYourOrder.ColumnHeadersHeight = 35;
            this.dgvSectionYourOrder.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Field1,
            this.Field2,
            this.Field3,
            this.Field5,
            this.Field6});
            dataGridViewCellStyle61.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle61.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle61.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle61.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle61.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle61.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle61.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSectionYourOrder.DefaultCellStyle = dataGridViewCellStyle61;
            this.dgvSectionYourOrder.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSectionYourOrder.GridColor = System.Drawing.Color.Black;
            this.dgvSectionYourOrder.Location = new System.Drawing.Point(35, 67);
            this.dgvSectionYourOrder.MultiSelect = false;
            this.dgvSectionYourOrder.Name = "dgvSectionYourOrder";
            this.dgvSectionYourOrder.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvSectionYourOrder.RowHeadersVisible = false;
            dataGridViewCellStyle62.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle62.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle62.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle62.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionYourOrder.RowsDefaultCellStyle = dataGridViewCellStyle62;
            this.dgvSectionYourOrder.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvSectionYourOrder.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSectionYourOrder.Size = new System.Drawing.Size(745, 35);
            this.dgvSectionYourOrder.TabIndex = 53;
            // 
            // Field1
            // 
            dataGridViewCellStyle56.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            dataGridViewCellStyle56.NullValue = null;
            this.Field1.DefaultCellStyle = dataGridViewCellStyle56;
            this.Field1.HeaderText = "Product Information";
            this.Field1.Name = "Field1";
            this.Field1.Width = 435;
            // 
            // Field2
            // 
            dataGridViewCellStyle57.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle57.NullValue = null;
            this.Field2.DefaultCellStyle = dataGridViewCellStyle57;
            this.Field2.HeaderText = "    UOM";
            this.Field2.Name = "Field2";
            this.Field2.Width = 60;
            // 
            // Field3
            // 
            dataGridViewCellStyle58.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle58.NullValue = null;
            this.Field3.DefaultCellStyle = dataGridViewCellStyle58;
            this.Field3.HeaderText = "Quantities";
            this.Field3.Name = "Field3";
            this.Field3.Width = 70;
            // 
            // Field5
            // 
            dataGridViewCellStyle59.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            this.Field5.DefaultCellStyle = dataGridViewCellStyle59;
            this.Field5.HeaderText = "   Item Price";
            this.Field5.Name = "Field5";
            this.Field5.Width = 85;
            // 
            // Field6
            // 
            dataGridViewCellStyle60.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            this.Field6.DefaultCellStyle = dataGridViewCellStyle60;
            this.Field6.HeaderText = "     Subtotal";
            this.Field6.Name = "Field6";
            this.Field6.Width = 95;
            // 
            // dataGridViewTextBoxColumn2
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewTextBoxColumn2.HeaderText = "Saved Order";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 200;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "Bill To:";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Width = 240;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Width = 30;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "Ship To:";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.Width = 240;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.HeaderText = "";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.Width = 25;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "";
            this.Column2.Name = "Column2";
            this.Column2.Width = 210;
            // 
            // frmSavedOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(855, 595);
            this.Controls.Add(this.tabControl1);
            this.Name = "frmSavedOrder";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.tabControl1.ResumeLayout(false);
            this.tabHeader.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPageHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEndOTInvoices)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEndGroupMsg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmptyMessage)).EndInit();
            this.tabHeaderSections.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSection3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeader2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeaderYourOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeaderA0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeaderA1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionLine1)).EndInit();
            this.tabSections.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionYourOrder)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabHeader;
        private System.Windows.Forms.DataGridView dgvPageHeader;
        private System.Windows.Forms.DataGridView dgvGroup1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn31;
        private System.Windows.Forms.DataGridView dgvEndOTInvoices;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridView dgvEndGroupMsg;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.Label lblFooter;
        private System.Windows.Forms.DataGridView dgvEmptyMessage;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn32;
        private System.Windows.Forms.TabPage tabHeaderSections;
        private System.Windows.Forms.DataGridView dgvSection3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn39;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn40;
        private System.Windows.Forms.DataGridView dgvSectionHeader2;
        private System.Windows.Forms.DataGridView dgvSectionHeaderYourOrder;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridView dgvSectionHeaderA0;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridView dgvSectionHeaderA1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.DataGridView dgvSectionLine1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.TabPage tabSections;
        private System.Windows.Forms.DataGridView dgvSectionYourOrder;
        private System.Windows.Forms.DataGridViewTextBoxColumn Field1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Field2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Field3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Field5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Field6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
    }
}
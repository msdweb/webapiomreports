﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OMReports
{
    
    public class Backorder
    {
        public string CustomerNumber { get; set; }
        public string OrderNumber { get; set; }
        public DateTime OrderDate { get; set; }
        public string CustomerPO { get; set; }
        public string ShippingName { get; set; }
        public string ShippingAddress1 { get; set; }
        public string ShippingAddress2 { get; set; }
        public string ShippingCity { get; set; }
        public string ShippingState { get; set; }
        public string ShippingPostalCode { get; set; }
        public string ShippingCountry { get; set; }
        public string ItemID { get; set; }
        public string ItemDesc { get; set; }
        public string UOM { get; set; }
        public string QtyOrdered { get; set; }
        public string QtyBackorder { get; set; }
        public string DC { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
//using System.Web.Http;
using System.Web;
using System.Collections.Specialized;

namespace OMReports
{
    public class cBackorders
    {
        public static List<Backorder> Get_Backorders(string Customer_no, string sConn)
        {
            var Backorders = new List<Backorder>();
            var Backorder = new Backorder();

            using (SqlConnection _SQLConn = new SqlConnection(sConn))
            {
                const string sSqlBackordersCmd = "[dbo].[wsp_webapi_report_backorder]";
                _SQLConn.Open();

                using (var sqlCmd = new SqlCommand())
                {
                    sqlCmd.Connection = _SQLConn;
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.CommandText = sSqlBackordersCmd;
                    sqlCmd.Parameters.AddWithValue("@CustomerNumber", Customer_no);
                    sqlCmd.CommandTimeout = 120;

                    using (SqlDataReader sqlRdr = sqlCmd.ExecuteReader())
                    {
                        while (sqlRdr.Read())
                        {
                            if (sqlRdr["CustomerNumber"] != null) Backorder.CustomerNumber = sqlRdr["CustomerNumber"].ToString();
                            if (sqlRdr["OrderNumber"] != null) Backorder.OrderNumber = sqlRdr["OrderNumber"].ToString();
                            if (sqlRdr["OrderDate"] != null) Backorder.OrderDate = Convert.ToDateTime(sqlRdr["OrderDate"]);
                            if (sqlRdr["CustomerPO"] != null) Backorder.CustomerPO = sqlRdr["CustomerPO"].ToString();

                            if (sqlRdr["ShippingName"] != null) Backorder.ShippingName = sqlRdr["ShippingName"].ToString();
                            if (sqlRdr["ShippingAddress1"] != null) Backorder.ShippingAddress1 = sqlRdr["ShippingAddress1"].ToString();
                            if (sqlRdr["ShippingAddress2"] != null) Backorder.ShippingAddress2 = sqlRdr["ShippingAddress2"].ToString();
                            if (sqlRdr["ShippingCity"] != null) Backorder.ShippingCity = sqlRdr["ShippingCity"].ToString();
                            if (sqlRdr["ShippingState"] != null) Backorder.ShippingState = sqlRdr["ShippingState"].ToString();
                            if (sqlRdr["ShippingPostalCode"] != null) Backorder.ShippingPostalCode = sqlRdr["ShippingPostalCode"].ToString();
                            if (sqlRdr["ShippingCountry"] != null) Backorder.ShippingCountry = sqlRdr["ShippingCountry"].ToString();

                            if (sqlRdr["ItemID"] != null) Backorder.ItemID = sqlRdr["ItemID"].ToString();
                            if (sqlRdr["ItemDesc"] != null) Backorder.ItemDesc = sqlRdr["ItemDesc"].ToString();
                            if (sqlRdr["UOM"] != null) Backorder.UOM = sqlRdr["UOM"].ToString();
                            if (sqlRdr["QtyOrdered"] != null) Backorder.QtyOrdered = sqlRdr["QtyOrdered"].ToString();
                            if (sqlRdr["QtyBackorder"] != null) Backorder.QtyBackorder = sqlRdr["QtyBackorder"].ToString();
                            if (sqlRdr["DC"] != null) Backorder.DC = sqlRdr["DC"].ToString();

                            Backorders.Add(Backorder);
                            Backorder = new Backorder();
                        
                        }
                    }
                }
            }
            return Backorders;
        }
    }
}

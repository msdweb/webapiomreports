﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace OMReports
{
    public partial class frmOMBackorder : Form
    {
        public frmOMBackorder()
        {
            InitializeComponent();
        }

        private void frmOMBackorder_Load(object sender, EventArgs e)
        {
            PaintGrid();
        }

        //
        //-------------------------------------------------------------
        //

        private void btnDisplay_Click(object sender, EventArgs e)
        {
            frmBackorder frmPCSReport = new frmBackorder();
            frmPCSReport.PCS_Preview();
            return;
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            frmBackorder frmPCSReport = new frmBackorder();
            frmPCSReport.PCS_Print();
        }

        private void btnSalvarRelatorio_Click(object sender, EventArgs e)
        {
            frmBackorder frmPCSReport = new frmBackorder();
            frmPCSReport.PCS_Salva();
        }

        //
        //-------------------------------------------------------------
        //

        private void PaintGrid()
        {
            dataGridView1.DataSource = GetTableBackorders(GLB.Backorders);
        }


        //
        //-----------------------------------------------------------------        
        //

        private DataTable GetTableBackorders(List<Backorder> Backorders)
        {
            DataTable table = new DataTable();
            table.Columns.Add("CustomerNumber", typeof(string));
            table.Columns.Add("OrderNumber", typeof(string));
            table.Columns.Add("OrderDate", typeof(string));
            table.Columns.Add("CustomerPO", typeof(string));

            table.Columns.Add("ShippingName", typeof(string));
            table.Columns.Add("ShippingAddress1", typeof(string));
            table.Columns.Add("ShippingAddress2", typeof(string));
            table.Columns.Add("ShippingCity", typeof(string));
            table.Columns.Add("ShippingState", typeof(string));
            table.Columns.Add("ShippingPostalCode", typeof(string));
            table.Columns.Add("ShippingCountry", typeof(string));

            table.Columns.Add("ItemID", typeof(string));
            table.Columns.Add("ItemDesc", typeof(string));

            table.Columns.Add("UOM", typeof(string));
            table.Columns.Add("QtyOrdered", typeof(string));
            table.Columns.Add("QtyBackorder", typeof(string));
            table.Columns.Add("DC", typeof(string));

            foreach (Backorder Backorder in Backorders)
            {
                table.Rows.Add(
                                Backorder.CustomerNumber,
                                Backorder.OrderNumber,
                                Backorder.OrderDate,
                                Backorder.CustomerPO,

                                Backorder.ShippingName,
                                Backorder.ShippingAddress1,
                                Backorder.ShippingAddress2,
                                Backorder.ShippingCity,
                                Backorder.ShippingState,
                                Backorder.ShippingPostalCode,
                                Backorder.ShippingCountry,

                                Backorder.ItemID,
                                Backorder.ItemDesc,
                                Backorder.UOM,
                                Backorder.QtyOrdered, Backorder.QtyBackorder, Backorder.DC
               );
            }

            return table;
        }
     }
}

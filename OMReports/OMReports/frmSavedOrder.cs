﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Text;
using System.Windows.Forms;

namespace OMReports
{
    public partial class frmSavedOrder : Form
    {
        private DataView DVPageHeader;

        DataView DVSection1;
        DataView DVSection2;
        DataView DVSection3;

        DataView DVSectionLine1;

        private string sMessage = string.Empty;

        private Printer DataGridPrinter1 = null;

        public frmSavedOrder()
        {
            InitializeComponent();
        }

        //####################################################################################
        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            Graphics g = e.Graphics;
            bool more = DataGridPrinter1.DrawDataGrid(g);
            if (more == true)
            {
                e.HasMorePages = true;
                DataGridPrinter1.PageNumber++;
                DataGridPrinter1.TotalPageNumber++;
            }
        }

        public void PCS_Preview()
        {
            if (GLB.SavedOrderDetail.SavedOrderLine.Count > 0)
            {
                SetupGridPrinter();

                frmPrinterControl PC = new frmPrinterControl();
                // if (DataGridPrinter1.
                PC.RPT_Preview(DataGridPrinter1);
                PC.ShowDialog();
                PC = null;
            }
        }

        public void PCS_Print()
        {
            if (GLB.SavedOrderDetail.SavedOrderLine.Count > 0)
            {
                SetupGridPrinter();

                frmPrinterControl PC = new frmPrinterControl();
                PC.RPT_Print(DataGridPrinter1);
                PC = null;
            }
        }

        public void PCS_Salva()
        {
            if (GLB.SavedOrderDetail.SavedOrderLine.Count > 0)
            {
                string s = System.AppDomain.CurrentDomain.BaseDirectory;
                printDocument1.DefaultPageSettings.PrinterSettings.PrinterName = "Microsoft XPS Document Writer";
                string FileName = "C:\\temp\\OM-Report\\MSD-OMSavedOrder-" + GLB.SavedOrderDetail.CustomerNumber + ".xps";
                printDocument1.DefaultPageSettings.PrinterSettings.PrintFileName = FileName;
                printDocument1.DefaultPageSettings.PrinterSettings.PrintToFile = true;

                SetupGridPrinter();

                frmPrinterControl PC = new frmPrinterControl();

                PC.RPT_Print(DataGridPrinter1);
                PC = null;
            }
        }

        void SetupGridPrinter()
        {
            dataDetails dataDetails = new dataDetails();

            lblFooter.Text = dataDetails.Get_LastReview();
            dgvEmptyMessage.ColumnHeadersVisible = false;

            DVPageHeader = dataDetails.DV_PageHeader_Fill(DVPageHeader, "");

            dgvPageHeader.BorderStyle = BorderStyle.None;
            dgvPageHeader.ColumnHeadersVisible = true;
            DataGridPrinter1 = new Printer(printDocument1,
                                           dgvPageHeader,
                                           dgvEmptyMessage,
                                           null,
                                           DVPageHeader.Table.DefaultView, lblFooter);

            //----------------------------------------------------------------------------

            string currentInvoice = string.Empty;
            int g = 0;


            dgvGroup1.ColumnHeadersVisible = false;
            DataGridPrinter1.AddGroup(dgvGroup1, null, null);

            //----------------------------------------------------------------------------

            string strVal1 = string.Empty;
            //if (GLB.orderHistoryDetail.ShippingState != null)
            //{
            //    if (GLB.orderHistoryDetail.ShippingState.ToString() != "FL")
            //    {
            //        strVal1 += "800 Technology Center Drive" + (char)13;
            //        strVal1 += "Stoughton, MA 02072" + (char)13;
            //        strVal1 += "800.967.6400    781.344.7244";
            //    }
            //    else
            //    {
            //        strVal1 += "11600 Miramar Parkway Suite 300" + (char)13;
            //        strVal1 += "Miramar, FL 33025" + (char)13;
            //        strVal1 += "954.986.2000            954.983.2503";
            //    }
            //}
            strVal1 += "800 Technology Center Drive" + (char)13;
            strVal1 += "Stoughton, MA 02072" + (char)13;
            strVal1 += "800.967.6400    781.344.7244";

            DataView DVSectionHeaderA0 = new DataView();
            DVSectionHeaderA0 = dataDetails.DV_SectionHeaderA1_Fill(DVSectionHeaderA0, strVal1);
            dgvSectionHeaderA0.ColumnHeadersVisible = false;
            DataGridPrinter1.AddSectionHeader(g, false,
                                              null,
                                              dgvSectionHeaderA0,
                                              DVSectionHeaderA0);

            //----------------------------------------------------------------------------

            DateTime dOrderDate = Convert.ToDateTime(GLB.SavedOrderDetail.OrderDate).Date;
            string sdOrderDate = dOrderDate.ToString("MM/dd/yyyy");

            //string sSubTotal = string.Format("{0:C}", decSubTotal);

            DataView DVSectionHeaderA1 = new DataView();
            DVSectionHeaderA1 = dataDetails.DV_Section_TwoFields_Open();

            DVSectionHeaderA1 = dataDetails.DV_Section_TwoFields_Fill(DVSectionHeaderA1, "          Saved Order Date", " " + sdOrderDate);
            DVSectionHeaderA1 = dataDetails.DV_Section_TwoFields_Fill(DVSectionHeaderA1, "                        Account #", " " + GLB.SavedOrderDetail.CustomerNumber.ToString());

            dgvSectionHeaderA1.ColumnHeadersVisible = false;
            DataGridPrinter1.AddSectionHeader(g, false,
                                              null,
                                              dgvSectionHeaderA1,
                                              DVSectionHeaderA1);

            //
            //------------------------------------------------------------------------------------------
            //

            string sBill = string.Empty;

            if (GLB.SavedOrderDetail.BillingCompanyName != null)
            {
                sBill += GLB.SavedOrderDetail.BillingCompanyName.ToString().Trim();
            }

            if (GLB.SavedOrderDetail.BillingAddress1 != null)
            {
                sBill += (char)13;
                sBill += GLB.SavedOrderDetail.BillingAddress1.ToString().Trim();
            }

            if (GLB.SavedOrderDetail.BillingAddress2 != null)
            {
                if (GLB.SavedOrderDetail.BillingAddress2.ToString().Trim() != "")
                {
                    sBill += (char)13;
                    sBill += GLB.SavedOrderDetail.BillingAddress2.ToString().Trim();
                }
            }

            if (GLB.SavedOrderDetail.BillingCity != null)
            {
                sBill += (char)13;
                sBill += GLB.SavedOrderDetail.BillingCity.ToString().Trim();
            }

            if (GLB.SavedOrderDetail.BillingState != null)
            {
                sBill += ", "; //(char)13;
                sBill += GLB.SavedOrderDetail.BillingState.ToString().Trim();
            }

            if (GLB.SavedOrderDetail.BillingPostalCode != null)
            {
                sBill += ", "; //(char)13;
                sBill += GLB.SavedOrderDetail.BillingPostalCode.ToString().Trim();
            }

            if (!string.IsNullOrWhiteSpace(GLB.SavedOrderDetail.BillingCountry))
            {
                sBill += ", "; //(char)13;
                sBill += GLB.SavedOrderDetail.BillingCountry.ToString().Trim();
            }

            if (!string.IsNullOrWhiteSpace(GLB.SavedOrderDetail.BillingPhone1))
            {
                sBill += (char)13;
                sBill += GLB.SavedOrderDetail.BillingPhone1.ToString().Trim();
            }

            if (!string.IsNullOrWhiteSpace(GLB.SavedOrderDetail.BillingEmail))
            {
                sBill += (char)13;
                sBill += GLB.SavedOrderDetail.BillingEmail.ToString().Trim();
            }

            //-------------------------------------------------------------

            string sShip = string.Empty;

            if (GLB.SavedOrderDetail.ShippingCompanyName != null)
            {
                sShip += GLB.SavedOrderDetail.ShippingCompanyName.ToString().Trim();
            }

            if (GLB.SavedOrderDetail.ShippingAddress1 != null)
            {
                sShip += (char)13;
                sShip += GLB.SavedOrderDetail.ShippingAddress1.ToString().Trim();
            }

            if (GLB.SavedOrderDetail.ShippingAddress2 != null)
            {
                if (GLB.SavedOrderDetail.ShippingAddress2.ToString().Trim() != "")
                {
                    sShip += (char)13;
                    sShip += GLB.SavedOrderDetail.ShippingAddress2.ToString().Trim();
                }
            }

            if (GLB.SavedOrderDetail.ShippingCity != null)
            {
                sShip += (char)13;
                sShip += GLB.SavedOrderDetail.ShippingCity.ToString().Trim();
            }

            if (GLB.SavedOrderDetail.ShippingState != null)
            {
                sShip += ", "; //(char)13;
                sShip += GLB.SavedOrderDetail.ShippingState.ToString().Trim();
            }

            if (GLB.SavedOrderDetail.ShippingPostalCode != null)
            {
                sShip += ", "; //(char)13;
                sShip += GLB.SavedOrderDetail.ShippingPostalCode.ToString().Trim();
            }

            if (!string.IsNullOrWhiteSpace(GLB.SavedOrderDetail.ShippingCountry))
            {
                sShip += ", "; //(char)13;
                sShip += GLB.SavedOrderDetail.ShippingCountry.ToString().Trim();
            }

            if (!string.IsNullOrWhiteSpace(GLB.SavedOrderDetail.ShippingPhone1))
            {
                sShip += (char)13;
                sShip += GLB.SavedOrderDetail.ShippingPhone1.ToString().Trim();
            }

            if (!string.IsNullOrWhiteSpace(GLB.SavedOrderDetail.ShippingEmail))
            {
                sShip += (char)13;
                sShip += GLB.SavedOrderDetail.ShippingEmail.ToString().Trim();
            }

            DataView DVSectionHeader2 = new DataView();

            DVSectionHeader2 = dataDetails.DV_SectionHeader2_Fill(DVSectionHeader2, sBill, sShip, "");

            dgvSectionHeader2.ColumnHeadersVisible = true;
            dgvSectionHeader2.BorderStyle = BorderStyle.None;
            DataGridPrinter1.AddSection(1, false,
                                              null,
                                              dgvSectionHeader2,
                                              DVSectionHeader2);


            //
            //------------------------------------------------------------------------------------------
            //

            DVSectionLine1 = dataDetails.DV_PageHeader_Fill(DVSectionLine1, sMessage);

            dgvSectionLine1.ColumnHeadersVisible = true;
            dgvSectionLine1.BorderStyle = BorderStyle.None;
            DataGridPrinter1.AddSection(1, false,
                                        null,
                                        dgvSectionLine1,
                                        DVSectionLine1);

            //
            //------------------------------------------------------------------------------------------
            //

            decimal decSubTotal = 0;

            DVSection1 = dataDetails.DV_Section1_Open();

            string sAlternateColor = "1";

            foreach (SavedOrderLine SavedOrderLine in GLB.SavedOrderDetail.SavedOrderLine)
            {

                string itemID_Description = string.Empty;
                string itemID = string.Empty;
                string itemDescription = string.Empty;

                string UOM = string.Empty;

                string qtyOrdered = string.Empty;
                string qtyBackorder = string.Empty;

                string UnitPrice = string.Empty;
                string ExtendPrice = string.Empty;

                itemID = "Product Code: " + SavedOrderLine.item_id.ToString();
                itemID_Description = "Product Code: " + SavedOrderLine.item_id.ToString() + (char)13 + SavedOrderLine.item_desc.ToString();

                UOM = SavedOrderLine.unit_of_measure.ToString();

                decimal decqtyOrdered = Convert.ToDecimal(SavedOrderLine.unit_quantity);
                qtyOrdered = decqtyOrdered.ToString("0.##");

                if (SavedOrderLine.unit_price.ToString() != "")
                {
                    //decimal decUnitPrice = Convert.ToDecimal(OrderLine.unit_price);
                    //UnitPrice = string.Format("{0:C}", decUnitPrice);
                    UnitPrice = SavedOrderLine.unit_price;
                }

                if (SavedOrderLine.extended_price.ToString() != "")
                {
                    //decimal decExtendPrice = Convert.ToDecimal(OrderLine.extended_price.ToString());
                    //decSubTotal += decExtendPrice;
                    //ExtendPrice = string.Format("{0:C}", decExtendPrice);
                    ExtendPrice = SavedOrderLine.extended_price.ToString();
                }

                //
                //------------------------------------------------------------------------------------------
                //

                if (sAlternateColor == "0")
                { sAlternateColor = "1"; }
                else { sAlternateColor = "0"; }

                DVSection1 = dataDetails.DV_Section4_Fill(DVSection1, sAlternateColor, itemID_Description, UOM, qtyOrdered, UnitPrice, ExtendPrice);

                //
                //------------------------------------------------------------------------
                //
            }

            //----------------------------------------------------------------------------

            DataGridPrinter1.AddSection(1, false,
                            dgvSectionHeaderYourOrder,
                            dgvSectionYourOrder,
                            DVSection1);


            //
            //------------------------------------------------------------------------------------------
            //

            //
            //------------------------------------------------------------------------------------------
            //

            DVSection3 = dataDetails.DV_Section_TwoFields_Open();

            DVSection3 = dataDetails.DV_Section_TwoFields_Fill(DVSection3, "SUB-TOTAL:", GLB.SavedOrderDetail.ProductSubtotal.ToString());

            dgvSection3.ColumnHeadersVisible = false;
            dgvSection3.BorderStyle = BorderStyle.None;
            DataGridPrinter1.AddSection(1, false,
                                        null,
                                        dgvSection3,
                                        DVSection3);

            //
            //------------------------------------------------------------------------------------------
            //



            DataGridPrinter1.PrintLogo = true;
            DataGridPrinter1.PrintLogoCenter = false;
        }
    }
}

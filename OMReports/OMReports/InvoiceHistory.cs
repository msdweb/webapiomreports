﻿using System;
using System.Collections.Generic;
using System.Reflection.Emit;

namespace OMReports
{
    public class InvoiceHistory
    {
        public string InvoiceNumber { get; set; }
        public DateTime InvoiceDate { get; set; }
        public DateTime DueDate { get; set; }
        public string InvoiceType { get; set; }
        public string CustomerNumber { get; set; }
        public string CustomerSequence { get; set; }
        public string CustomerPurchaseOrder { get; set; }
        public string Status { get; set; }
        public bool IsOpen { get; set; }
        public string CurrencyCode { get; set; }
        public string Terms { get; set; }
        public string ShipCode { get; set; }
        public string Salesperson { get; set; }
        public string BillingCompanyName { get; set; }
        public string BillingAddress1 { get; set; }
        public string BillingAddress2 { get; set; }
        public string BillingCity { get; set; }
        public string BillingState { get; set; }
        public string BillingPostalCode { get; set; }
        public string BillingCountry { get; set; }
        public string ShippingCompanyName { get; set; }
        public string ShippingAddress1 { get; set; }
        public string ShippingAddress2 { get; set; }
        public string ShippingCity { get; set; }
        public string ShippingState { get; set; }
        public string ShippingPostalCode { get; set; }
        public string ShippingCountry { get; set; }
        public string Notes { get; set; }
        public string CarrierUrl { get; set; }
        public string TrackingNumbers { get; set; }
        public string PickTicketNumber { get; set; }
        public decimal AmountPaid { get; set; }
        public decimal MemoAmount { get; set; }
        public decimal ProductTotal { get; set; }
        public decimal DiscountAmount { get; set; }
        public decimal AllowedAmount { get; set; }
        public decimal ShippingAndHandling { get; set; }
        public decimal OtherCharges { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal InvoiceTotal { get; set; }
        public decimal CurrentBalance { get; set; }
    }



}

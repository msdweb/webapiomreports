﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Text;
using System.Web;
using System.Collections.Specialized;

namespace OMReports
{
    public class cInvoiceHistories
    {
        public static List<InvoiceHistory> Get_InvoiceHistories(string customerId, ShipToAddress address, string invoiceNumber, string orderNumber, string customerPo,
                                                                DateTime startDate, DateTime endDate, int pageSize, int page, string sort, string sortDirection, string InvoiceStatus, string sConn)
        {
            var InvoiceHistories = new List<InvoiceHistory>();
            var InvoiceHistory = new InvoiceHistory();

            var parSort = string.IsNullOrEmpty(sort) ? null : sort;
            var parSortDirection = string.IsNullOrEmpty(sortDirection) ? null : sortDirection;


            using (SqlConnection _SQLConn = new SqlConnection(sConn))
            {
                const string sSqlInvoiceHistoryCmd = "[dbo].[wsp_webapi_InvoiceHistorySearch]";
                _SQLConn.Open();

                using (var sqlCmd = new SqlCommand())
                {
                    sqlCmd.Connection = _SQLConn;
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.CommandText = sSqlInvoiceHistoryCmd;

                    sqlCmd.Parameters.AddWithValue("@customerNumber", customerId);
                    sqlCmd.Parameters.AddWithValue("@isOpen", InvoiceStatus);
                    sqlCmd.Parameters.AddWithValue("@pageSize", pageSize);
                    sqlCmd.Parameters.AddWithValue("@page", page);
                    sqlCmd.Parameters.AddWithValue("@sort", parSort);
                    sqlCmd.Parameters.AddWithValue("@sortDirection", parSortDirection);
                    sqlCmd.Parameters.AddWithValue("@name", address.Company);
                    sqlCmd.Parameters.AddWithValue("@address1", address.Address1);
                    sqlCmd.Parameters.AddWithValue("@address2", address.Address2);
                    sqlCmd.Parameters.AddWithValue("@city", address.City);
                    sqlCmd.Parameters.AddWithValue("@state", address.State);
                    sqlCmd.Parameters.AddWithValue("@zip", address.Zip);
                    sqlCmd.Parameters.AddWithValue("@country", address.Country);
                    sqlCmd.Parameters.AddWithValue("@customerPo", customerPo);
                    sqlCmd.Parameters.AddWithValue("@totalOperator", "");
                    sqlCmd.Parameters.AddWithValue("@total", 0);
                    sqlCmd.Parameters.AddWithValue("@status", "");
                    sqlCmd.Parameters.AddWithValue("@orderNumber", orderNumber);
                    sqlCmd.Parameters.AddWithValue("@startDate", startDate);
                    sqlCmd.Parameters.AddWithValue("@endDate", endDate);
                    sqlCmd.Parameters.AddWithValue("@invoiceNumber", invoiceNumber);

                    sqlCmd.CommandTimeout = 120;

                    using (SqlDataReader sqlRdr = sqlCmd.ExecuteReader())
                    {
                        while (sqlRdr.Read())
                        {
                            if (sqlRdr["InvoiceNumber"] != null) InvoiceHistory.InvoiceNumber = sqlRdr["InvoiceNumber"].ToString();
                            if (sqlRdr["InvoiceDate"] != null) InvoiceHistory.InvoiceDate = Convert.ToDateTime(sqlRdr["InvoiceDate"]);
                            if (sqlRdr["DueDate"] != null) InvoiceHistory.DueDate = Convert.ToDateTime(sqlRdr["DueDate"]);
                            if (sqlRdr["InvoiceType"] != null) InvoiceHistory.InvoiceType = sqlRdr["InvoiceType"].ToString();
                            if (sqlRdr["CustomerNumber"] != null) InvoiceHistory.CustomerNumber = sqlRdr["CustomerNumber"].ToString();
                            if (sqlRdr["CustomerSequence"] != null) InvoiceHistory.CustomerSequence = sqlRdr["CustomerSequence"].ToString();
                            if (sqlRdr["CustomerPO"] != null) InvoiceHistory.CustomerPurchaseOrder = sqlRdr["CustomerPO"].ToString();
                            if (sqlRdr["Status"] != null) InvoiceHistory.Status = sqlRdr["Status"].ToString();
                            if (sqlRdr["IsOpen"] != null) InvoiceHistory.IsOpen = Convert.ToBoolean(sqlRdr["IsOpen"]);
                            if (sqlRdr["CurrencyCode"] != null) InvoiceHistory.CurrencyCode = sqlRdr["CurrencyCode"].ToString();
                            if (sqlRdr["Terms"] != null) InvoiceHistory.Terms = sqlRdr["Terms"].ToString();
                            if (sqlRdr["ShipCode"] != null) InvoiceHistory.ShipCode = sqlRdr["ShipCode"].ToString();
                            if (sqlRdr["Salesperson"] != null) InvoiceHistory.Salesperson = sqlRdr["Salesperson"].ToString();
                            if (sqlRdr["BTCompanyName"] != null) InvoiceHistory.BillingCompanyName = sqlRdr["BTCompanyName"].ToString();
                            if (sqlRdr["BTAddress1"] != null) InvoiceHistory.BillingAddress1 = sqlRdr["BTAddress1"].ToString();
                            if (sqlRdr["BTAddress2"] != null) InvoiceHistory.BillingAddress2 = sqlRdr["BTAddress2"].ToString();
                            if (sqlRdr["BTCity"] != null) InvoiceHistory.BillingCity = sqlRdr["BTCity"].ToString();
                            if (sqlRdr["BTState"] != null) InvoiceHistory.BillingState = sqlRdr["BTState"].ToString();
                            if (sqlRdr["BTPostalCode"] != null) InvoiceHistory.BillingPostalCode = sqlRdr["BTPostalCode"].ToString();
                            if (sqlRdr["BTCountry"] != null) InvoiceHistory.BillingCountry = sqlRdr["BTCountry"].ToString();
                            if (sqlRdr["STCompanyName"] != null) InvoiceHistory.ShippingCompanyName = sqlRdr["STCompanyName"].ToString();
                            if (sqlRdr["STAddress1"] != null) InvoiceHistory.ShippingAddress1 = sqlRdr["STAddress1"].ToString();
                            if (sqlRdr["STAddress2"] != null) InvoiceHistory.ShippingAddress2 = sqlRdr["STAddress2"].ToString();
                            if (sqlRdr["STCity"] != null) InvoiceHistory.ShippingCity = sqlRdr["STCity"].ToString();
                            if (sqlRdr["STCountry"] != null) InvoiceHistory.ShippingState = sqlRdr["STCountry"].ToString();
                            if (sqlRdr["STPostalCode"] != null) InvoiceHistory.ShippingPostalCode = sqlRdr["STPostalCode"].ToString();
                            if (sqlRdr["STCountry"] != null) InvoiceHistory.ShippingCountry = sqlRdr["STCountry"].ToString();
                            if (sqlRdr["Notes"] != null) InvoiceHistory.Notes = sqlRdr["Notes"].ToString();
                            if (sqlRdr["CarrierUrl"] != null) InvoiceHistory.CarrierUrl = sqlRdr["CarrierUrl"].ToString();
                            if (sqlRdr["TrackingNumbers"] != null) InvoiceHistory.TrackingNumbers = sqlRdr["TrackingNumbers"].ToString();
                            if (sqlRdr["ProductTotal"] != null) InvoiceHistory.ProductTotal = Convert.ToDecimal(sqlRdr["ProductTotal"]);
                            if (sqlRdr["DiscountAmount"] != null) InvoiceHistory.DiscountAmount = Convert.ToDecimal(sqlRdr["DiscountAmount"]);
                            if (sqlRdr["AllowedAmount"] != null) InvoiceHistory.AllowedAmount = Convert.ToDecimal(sqlRdr["AllowedAmount"]);
                            if (sqlRdr["ShippingAndHandling"] != null) InvoiceHistory.ShippingAndHandling = Convert.ToDecimal(sqlRdr["ShippingAndHandling"]);
                            if (sqlRdr["OtherCharges"] != null) InvoiceHistory.OtherCharges = Convert.ToDecimal(sqlRdr["OtherCharges"]);
                            if (sqlRdr["TaxAmount"] != null) InvoiceHistory.TaxAmount = Convert.ToDecimal(sqlRdr["TaxAmount"]);
                            if (sqlRdr["amount_paid"] != null) InvoiceHistory.AmountPaid = Convert.ToDecimal(sqlRdr["amount_paid"]);
                            if (sqlRdr["memo_amount"] != null) InvoiceHistory.MemoAmount = Convert.ToDecimal(sqlRdr["memo_amount"]);
                            if (sqlRdr["InvoiceTotal"] != null) InvoiceHistory.InvoiceTotal = Convert.ToDecimal(sqlRdr["InvoiceTotal"]);
                            if (sqlRdr["CurrentBalance"] != null) InvoiceHistory.CurrentBalance = Convert.ToDecimal(sqlRdr["CurrentBalance"]);

                            InvoiceHistories.Add(InvoiceHistory);
                            InvoiceHistory = new InvoiceHistory();
                        }
                    }
                }
            }
            return InvoiceHistories;
        }
        
        public class ShipToAddress
        {
            public int Id { get; set; }
            public string CustomerNumber { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Company { get; set; }
            public string Address1 { get; set; }
            public string Address2 { get; set; }
            public string Country { get; set; }
            public string State { get; set; }
            public string City { get; set; }
            public string Zip { get; set; }
            public string Phone { get; set; }
            public DateTime LastModifiedDate { get; set; }
            public DateTime CreatedDate { get; set; }
        }

    }
}

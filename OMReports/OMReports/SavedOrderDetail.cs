﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OMReports
{
    public class SavedOrderDetail
    {
        public DateTime OrderDate { get; set; }
        public string CustomerNumber { get; set; }

        public string BillingCompanyName { get; set; }
        public string BillingAddress1 { get; set; }
        public string BillingAddress2 { get; set; }
        public string BillingCity { get; set; }
        public string BillingState { get; set; }
        public string BillingPostalCode { get; set; }
        public string BillingCountry { get; set; }
        public string BillingPhone1 { get; set; }
        public string BillingEmail { get; set; }
        public string ShippingCompanyName { get; set; }
        public string ShippingAddress1 { get; set; }
        public string ShippingAddress2 { get; set; }
        public string ShippingCity { get; set; }
        public string ShippingState { get; set; }
        public string ShippingPostalCode { get; set; }
        public string ShippingCountry { get; set; }
        public string ShippingPhone1 { get; set; }
        public string ShippingEmail { get; set; }

        public List<SavedOrderLine> SavedOrderLine { get; set; }

        public string ProductSubtotal { get; set; }

        public SavedOrderDetail()
        {
            SavedOrderLine = new List<SavedOrderLine>();
        }
    }

    public class SavedOrderLine
    {
        public string unit_quantity { get; set; }
        public string unit_of_measure { get; set; }
        public string item_id { get; set; }
        public string item_desc { get; set; }
        public string unit_price { get; set; }
        public string extended_price { get; set; }
    }
}
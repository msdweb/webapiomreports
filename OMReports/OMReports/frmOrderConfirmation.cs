﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Text;
using System.Windows.Forms;

namespace OMReports
{
    public partial class frmOrderConfirmation : Form
    {
        private DataView DVPageHeader;
        
        DataView DVSection1;
        DataView DVSection2;
        DataView DVSection3;

        DataView DVSectionLine1;

        private string sMessage = string.Empty;

        private Printer DataGridPrinter1 = null;

        public frmOrderConfirmation()
        {
            InitializeComponent();
        }

        //####################################################################################
        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            Graphics g = e.Graphics;
            bool more = DataGridPrinter1.DrawDataGrid(g);
            if (more == true)
            {
                e.HasMorePages = true;
                DataGridPrinter1.PageNumber++;
                DataGridPrinter1.TotalPageNumber++;
            }
        }

        public void PCS_Preview()
        {
            if (GLB.OrderConfirmationDetail.OrderConfirmationLine.Count > 0)
            {
                SetupGridPrinter();

                frmPrinterControl PC = new frmPrinterControl();
                // if (DataGridPrinter1.
                PC.RPT_Preview(DataGridPrinter1);
                PC.ShowDialog();
                PC = null;
            }
        }

        public void PCS_Print()
        {
            if (GLB.OrderConfirmationDetail.OrderConfirmationLine.Count > 0)
            {
                SetupGridPrinter();

                frmPrinterControl PC = new frmPrinterControl();
                PC.RPT_Print(DataGridPrinter1);
                PC = null;
            }
        }

        public void PCS_Salva()
        {
            if (GLB.OrderConfirmationDetail.OrderConfirmationLine.Count > 0)
            {
                string s = System.AppDomain.CurrentDomain.BaseDirectory;
                printDocument1.DefaultPageSettings.PrinterSettings.PrinterName = "Microsoft XPS Document Writer";
                string FileName = "C:\\temp\\OM-Report\\MSD-OMOrderConfirmation-" + GLB.OrderConfirmationDetail.OrderNumber + ".xps";
                printDocument1.DefaultPageSettings.PrinterSettings.PrintFileName = FileName;
                printDocument1.DefaultPageSettings.PrinterSettings.PrintToFile = true;

                SetupGridPrinter();

                frmPrinterControl PC = new frmPrinterControl();

                PC.RPT_Print(DataGridPrinter1);
                PC = null;
            }
        }

        void SetupGridPrinter()
        {
            dataDetails dataDetails = new dataDetails();

            lblFooter.Text = dataDetails.Get_LastReview();
            dgvEmptyMessage.ColumnHeadersVisible = false;

            DVPageHeader = dataDetails.DV_PageHeader_Fill(DVPageHeader, "");

            dgvPageHeader.BorderStyle = BorderStyle.None;
            dgvPageHeader.ColumnHeadersVisible = true;
            DataGridPrinter1 = new Printer(printDocument1,
                                           dgvPageHeader,
                                           dgvEmptyMessage,
                                           null,
                                           DVPageHeader.Table.DefaultView, lblFooter);

            //----------------------------------------------------------------------------

            string currentInvoice = string.Empty;
            int g = 0;


            dgvGroup1.ColumnHeadersVisible = false;
            DataGridPrinter1.AddGroup(dgvGroup1, null, null);

            //----------------------------------------------------------------------------

            string strVal1 = string.Empty;
            //if (GLB.orderHistoryDetail.ShippingState != null)
            //{
            //    if (GLB.orderHistoryDetail.ShippingState.ToString() != "FL")
            //    {
            //        strVal1 += "800 Technology Center Drive" + (char)13;
            //        strVal1 += "Stoughton, MA 02072" + (char)13;
            //        strVal1 += "800.967.6400    781.344.7244";
            //    }
            //    else
            //    {
            //        strVal1 += "11600 Miramar Parkway Suite 300" + (char)13;
            //        strVal1 += "Miramar, FL 33025" + (char)13;
            //        strVal1 += "954.986.2000            954.983.2503";
            //    }
            //}
            strVal1 += "800 Technology Center Drive" + (char)13;
            strVal1 += "Stoughton, MA 02072" + (char)13;
            strVal1 += "800.967.6400    781.344.7244";

            DataView DVSectionHeaderA0 = new DataView();
            DVSectionHeaderA0 = dataDetails.DV_SectionHeaderA1_Fill(DVSectionHeaderA0, strVal1);
            dgvSectionHeaderA0.ColumnHeadersVisible = false;
            DataGridPrinter1.AddSectionHeader(g, false,
                                              null,
                                              dgvSectionHeaderA0,
                                              DVSectionHeaderA0);

            //----------------------------------------------------------------------------

            DateTime dOrderDate = Convert.ToDateTime(GLB.OrderConfirmationDetail.OrderDate).Date;
            string sdOrderDate = dOrderDate.ToString("MM/dd/yyyy");

            DateTime dRequestedShipDate = Convert.ToDateTime(GLB.OrderConfirmationDetail.RequestedShipDate).Date;
            string sdRequestedShipDate = dRequestedShipDate.ToString("MM/dd/yyyy");

            bool isFutureOrder = (dOrderDate != dRequestedShipDate) ? true : false;

            if (isFutureOrder)
            {
                sMessage = "This order will be shipped on the requested ship date. An order number will be available in your order history by the requested ship date.";
            }
            else
            {
                sMessage = "This order is being processed. An order number will be available in a few minutes in your order history.";
            }


            //string sSubTotal = string.Format("{0:C}", decSubTotal);

            DataView DVSectionHeaderA1 = new DataView();
            DVSectionHeaderA1 = dataDetails.DV_Section_TwoFields_Open();

            DVSectionHeaderA1 = dataDetails.DV_Section_TwoFields_Fill(DVSectionHeaderA1, "                                 PO #", " " + GLB.OrderConfirmationDetail.CustomerPurchaseOrder.ToString());
            if (isFutureOrder)
            {
                DVSectionHeaderA1 = dataDetails.DV_Section_TwoFields_Fill(DVSectionHeaderA1, "           Order Entry Date", " " + sdOrderDate);
                DVSectionHeaderA1 = dataDetails.DV_Section_TwoFields_Fill(DVSectionHeaderA1, "      Requested Ship Date", " " + sdRequestedShipDate);
            }
            else
            {
                DVSectionHeaderA1 = dataDetails.DV_Section_TwoFields_Fill(DVSectionHeaderA1, "                      Order Date", " " + sdOrderDate);
            }
            DVSectionHeaderA1 = dataDetails.DV_Section_TwoFields_Fill(DVSectionHeaderA1, "                        Account #", " " + GLB.OrderConfirmationDetail.CustomerNumber.ToString());
            DVSectionHeaderA1 = dataDetails.DV_Section_TwoFields_Fill(DVSectionHeaderA1, "                                Notes", " " + GLB.OrderConfirmationDetail.Notes.ToString());
            DVSectionHeaderA1 = dataDetails.DV_Section_TwoFields_Fill(DVSectionHeaderA1, "                     Ordered By", " " + GLB.OrderConfirmationDetail.PlacedBy.ToString());

            dgvSectionHeaderA1.ColumnHeadersVisible = false;
            DataGridPrinter1.AddSectionHeader(g, false,
                                              null,
                                              dgvSectionHeaderA1,
                                              DVSectionHeaderA1);

            //
            //------------------------------------------------------------------------------------------
            //

            string sBill = string.Empty;

            if (GLB.OrderConfirmationDetail.BillingCompanyName != null)
            {
                sBill += GLB.OrderConfirmationDetail.BillingCompanyName.ToString().Trim();
            }

            if (GLB.OrderConfirmationDetail.BillingAddress1 != null)
            {
                sBill += (char)13;
                sBill += GLB.OrderConfirmationDetail.BillingAddress1.ToString().Trim();
            }

            if (GLB.OrderConfirmationDetail.BillingAddress2 != null)
            {
                if (GLB.OrderConfirmationDetail.BillingAddress2.ToString().Trim() != "")
                {
                    sBill += (char)13;
                    sBill += GLB.OrderConfirmationDetail.BillingAddress2.ToString().Trim();
                }
            }

            if (GLB.OrderConfirmationDetail.BillingCity != null)
            {
                sBill += (char)13;
                sBill += GLB.OrderConfirmationDetail.BillingCity.ToString().Trim();
            }

            if (GLB.OrderConfirmationDetail.BillingState != null)
            {
                sBill += ", "; //(char)13;
                sBill += GLB.OrderConfirmationDetail.BillingState.ToString().Trim();
            }

            if (GLB.OrderConfirmationDetail.BillingPostalCode != null)
            {
                sBill += ", "; //(char)13;
                sBill += GLB.OrderConfirmationDetail.BillingPostalCode.ToString().Trim();
            }

            if (!string.IsNullOrWhiteSpace(GLB.OrderConfirmationDetail.BillingCountry))
            {
                sBill += ", "; //(char)13;
                sBill += GLB.OrderConfirmationDetail.BillingCountry.ToString().Trim();
            }

            if (!string.IsNullOrWhiteSpace(GLB.OrderConfirmationDetail.BillingPhone1))
            {
                sBill += (char)13;
                sBill += GLB.OrderConfirmationDetail.BillingPhone1.ToString().Trim();
            }

            if (!string.IsNullOrWhiteSpace(GLB.OrderConfirmationDetail.BillingEmail))
            {
                sBill += (char)13;
                sBill += GLB.OrderConfirmationDetail.BillingEmail.ToString().Trim();
            }

            //-------------------------------------------------------------

            string sShip = string.Empty;

            if (GLB.OrderConfirmationDetail.ShippingCompanyName != null)
            {
                sShip += GLB.OrderConfirmationDetail.ShippingCompanyName.ToString().Trim();
            }

            if (GLB.OrderConfirmationDetail.ShippingAddress1 != null)
            {
                sShip += (char)13;
                sShip += GLB.OrderConfirmationDetail.ShippingAddress1.ToString().Trim();
            }

            if (GLB.OrderConfirmationDetail.ShippingAddress2 != null)
            {
                if (GLB.OrderConfirmationDetail.ShippingAddress2.ToString().Trim() != "")
                {
                    sShip += (char)13;
                    sShip += GLB.OrderConfirmationDetail.ShippingAddress2.ToString().Trim();
                }
            }

            if (GLB.OrderConfirmationDetail.ShippingCity != null)
            {
                sShip += (char)13;
                sShip += GLB.OrderConfirmationDetail.ShippingCity.ToString().Trim();
            }

            if (GLB.OrderConfirmationDetail.ShippingState != null)
            {
                sShip += ", "; //(char)13;
                sShip += GLB.OrderConfirmationDetail.ShippingState.ToString().Trim();
            }

            if (GLB.OrderConfirmationDetail.ShippingPostalCode != null)
            {
                sShip += ", "; //(char)13;
                sShip += GLB.OrderConfirmationDetail.ShippingPostalCode.ToString().Trim();
            }

            if (!string.IsNullOrWhiteSpace(GLB.OrderConfirmationDetail.ShippingCountry))
            {
                sShip += ", "; //(char)13;
                sShip += GLB.OrderConfirmationDetail.ShippingCountry.ToString().Trim();
            }

            if (!string.IsNullOrWhiteSpace(GLB.OrderConfirmationDetail.ShippingPhone1))
            {
                sShip += (char)13;
                sShip += GLB.OrderConfirmationDetail.ShippingPhone1.ToString().Trim();
            }

            if (!string.IsNullOrWhiteSpace(GLB.OrderConfirmationDetail.ShippingEmail))
            {
                sShip += (char)13;
                sShip += GLB.OrderConfirmationDetail.ShippingEmail.ToString().Trim();
            }

            DataView DVSectionHeader2 = new DataView();

            DVSectionHeader2 = dataDetails.DV_SectionHeader2_Fill(DVSectionHeader2, sBill, sShip, GLB.OrderConfirmationDetail.FreightInfoOption.ToString());

            dgvSectionHeader2.ColumnHeadersVisible = true;
            dgvSectionHeader2.BorderStyle = BorderStyle.None;
            DataGridPrinter1.AddSection(1, false,
                                              null,
                                              dgvSectionHeader2,
                                              DVSectionHeader2);


            //
            //------------------------------------------------------------------------------------------
            //

            DVSectionLine1 = dataDetails.DV_PageHeader_Fill(DVSectionLine1, sMessage);

            dgvSectionLine1.ColumnHeadersVisible = true;
            dgvSectionLine1.BorderStyle = BorderStyle.None;
            DataGridPrinter1.AddSection(1, false,
                                        null,
                                        dgvSectionLine1,
                                        DVSectionLine1);

            //
            //------------------------------------------------------------------------------------------
            //

            decimal decSubTotal = 0;

            DVSection1 = dataDetails.DV_Section1_Open();

            string sAlternateColor = "1";

            foreach (OrderConfirmationLine OrderConfirmationLine in GLB.OrderConfirmationDetail.OrderConfirmationLine)
            {

                string itemID_Description = string.Empty;
                string itemID = string.Empty;
                string itemDescription = string.Empty;

                string UOM = string.Empty;

                string qtyOrdered = string.Empty;
                string qtyBackorder = string.Empty;

                string UnitPrice = string.Empty;
                string ExtendPrice = string.Empty;

                itemID = "Product Code: " + OrderConfirmationLine.item_id.ToString();
                itemID_Description = "Product Code: " + OrderConfirmationLine.item_id.ToString() + (char)13 + OrderConfirmationLine.item_desc.ToString();

                UOM = OrderConfirmationLine.unit_of_measure.ToString();

                decimal decqtyOrdered = Convert.ToDecimal(OrderConfirmationLine.unit_quantity);
                qtyOrdered = decqtyOrdered.ToString("0.##");

                if (OrderConfirmationLine.unit_price.ToString() != "")
                {
                    //decimal decUnitPrice = Convert.ToDecimal(OrderLine.unit_price);
                    //UnitPrice = string.Format("{0:C}", decUnitPrice);
                    UnitPrice = OrderConfirmationLine.unit_price;
                }

                if (OrderConfirmationLine.extended_price.ToString() != "")
                {
                    //decimal decExtendPrice = Convert.ToDecimal(OrderLine.extended_price.ToString());
                    //decSubTotal += decExtendPrice;
                    //ExtendPrice = string.Format("{0:C}", decExtendPrice);
                    ExtendPrice = OrderConfirmationLine.extended_price.ToString();
                }

                //
                //------------------------------------------------------------------------------------------
                //

                if (sAlternateColor == "0")
                { sAlternateColor = "1"; }
                else { sAlternateColor = "0"; }

                DVSection1 = dataDetails.DV_Section4_Fill(DVSection1, sAlternateColor, itemID_Description, UOM, qtyOrdered, UnitPrice, ExtendPrice);

                //
                //------------------------------------------------------------------------
                //
            }

            //----------------------------------------------------------------------------

            DataGridPrinter1.AddSection(1, false,
                            dgvSectionHeaderYourOrder,
                            dgvSectionYourOrder,
                            DVSection1);


            //
            //------------------------------------------------------------------------------------------
            //

            //
            //------------------------------------------------------------------------------------------
            //

            DVSection3 = dataDetails.DV_Section_TwoFields_Open();

            DVSection3 = dataDetails.DV_Section_TwoFields_Fill(DVSection3, "SUB-TOTAL:", GLB.OrderConfirmationDetail.ProductSubtotal.ToString());

            dgvSection3.ColumnHeadersVisible = false;
            dgvSection3.BorderStyle = BorderStyle.None;
            DataGridPrinter1.AddSection(1, false,
                                        null,
                                        dgvSection3,
                                        DVSection3);

            //
            //------------------------------------------------------------------------------------------
            //



            DataGridPrinter1.PrintLogo = true;
            DataGridPrinter1.PrintLogoCenter = false;
        }
    }
}

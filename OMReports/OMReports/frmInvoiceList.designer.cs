﻿namespace OMReports
{
    partial class frmInvoiceList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle40 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle41 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle42 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle43 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle44 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle45 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle51 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle52 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle46 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle47 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle48 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle49 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle50 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabHeader = new System.Windows.Forms.TabPage();
            this.dgvPageHeader = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvGroup1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn31 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvEndOTInvoices = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvEndGroupMsg = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblFooter = new System.Windows.Forms.Label();
            this.dgvEmptyMessage = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn32 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabHeaderSections = new System.Windows.Forms.TabPage();
            this.dgvSectionHeaderYourOrder = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvSectionHeaderA0 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvSectionHeaderA1 = new System.Windows.Forms.DataGridView();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabSections = new System.Windows.Forms.TabPage();
            this.dgvSectionYourOrder = new System.Windows.Forms.DataGridView();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Field5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Field6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Field1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Field2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Field3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UnitPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ExtendedPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabControl1.SuspendLayout();
            this.tabHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPageHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEndOTInvoices)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEndGroupMsg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmptyMessage)).BeginInit();
            this.tabHeaderSections.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeaderYourOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeaderA0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeaderA1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabSections.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionYourOrder)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabHeader);
            this.tabControl1.Controls.Add(this.tabHeaderSections);
            this.tabControl1.Controls.Add(this.tabSections);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(828, 569);
            this.tabControl1.TabIndex = 19;
            // 
            // tabHeader
            // 
            this.tabHeader.Controls.Add(this.dgvPageHeader);
            this.tabHeader.Controls.Add(this.dgvGroup1);
            this.tabHeader.Controls.Add(this.dgvEndOTInvoices);
            this.tabHeader.Controls.Add(this.dgvEndGroupMsg);
            this.tabHeader.Controls.Add(this.lblFooter);
            this.tabHeader.Controls.Add(this.dgvEmptyMessage);
            this.tabHeader.Location = new System.Drawing.Point(4, 22);
            this.tabHeader.Name = "tabHeader";
            this.tabHeader.Padding = new System.Windows.Forms.Padding(3);
            this.tabHeader.Size = new System.Drawing.Size(820, 543);
            this.tabHeader.TabIndex = 1;
            this.tabHeader.Text = "Header";
            this.tabHeader.UseVisualStyleBackColor = true;
            // 
            // dgvPageHeader
            // 
            this.dgvPageHeader.AllowUserToAddRows = false;
            this.dgvPageHeader.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvPageHeader.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvPageHeader.BackgroundColor = System.Drawing.Color.White;
            this.dgvPageHeader.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvPageHeader.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvPageHeader.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvPageHeader.ColumnHeadersHeight = 50;
            this.dgvPageHeader.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Arial", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvPageHeader.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvPageHeader.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvPageHeader.GridColor = System.Drawing.Color.Black;
            this.dgvPageHeader.Location = new System.Drawing.Point(580, 20);
            this.dgvPageHeader.MultiSelect = false;
            this.dgvPageHeader.Name = "dgvPageHeader";
            this.dgvPageHeader.RowHeadersVisible = false;
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvPageHeader.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvPageHeader.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvPageHeader.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPageHeader.Size = new System.Drawing.Size(200, 30);
            this.dgvPageHeader.TabIndex = 40;
            // 
            // dataGridViewTextBoxColumn2
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewTextBoxColumn2.HeaderText = "Invoice History";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 200;
            // 
            // dgvGroup1
            // 
            this.dgvGroup1.AllowUserToAddRows = false;
            this.dgvGroup1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvGroup1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvGroup1.BackgroundColor = System.Drawing.Color.Gray;
            this.dgvGroup1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvGroup1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dgvGroup1.ColumnHeadersHeight = 30;
            this.dgvGroup1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn31});
            this.dgvGroup1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvGroup1.GridColor = System.Drawing.Color.Black;
            this.dgvGroup1.Location = new System.Drawing.Point(35, 126);
            this.dgvGroup1.MultiSelect = false;
            this.dgvGroup1.Name = "dgvGroup1";
            this.dgvGroup1.RowHeadersVisible = false;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvGroup1.RowsDefaultCellStyle = dataGridViewCellStyle8;
            this.dgvGroup1.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvGroup1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvGroup1.Size = new System.Drawing.Size(745, 28);
            this.dgvGroup1.TabIndex = 39;
            // 
            // dataGridViewTextBoxColumn31
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn31.DefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridViewTextBoxColumn31.HeaderText = "Group Header";
            this.dataGridViewTextBoxColumn31.Name = "dataGridViewTextBoxColumn31";
            this.dataGridViewTextBoxColumn31.Width = 745;
            // 
            // dgvEndOTInvoices
            // 
            this.dgvEndOTInvoices.AllowUserToAddRows = false;
            this.dgvEndOTInvoices.AllowUserToDeleteRows = false;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvEndOTInvoices.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle9;
            this.dgvEndOTInvoices.BackgroundColor = System.Drawing.Color.Gray;
            this.dgvEndOTInvoices.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvEndOTInvoices.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dgvEndOTInvoices.ColumnHeadersHeight = 30;
            this.dgvEndOTInvoices.ColumnHeadersVisible = false;
            this.dgvEndOTInvoices.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1});
            this.dgvEndOTInvoices.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvEndOTInvoices.GridColor = System.Drawing.Color.Black;
            this.dgvEndOTInvoices.Location = new System.Drawing.Point(35, 340);
            this.dgvEndOTInvoices.MultiSelect = false;
            this.dgvEndOTInvoices.Name = "dgvEndOTInvoices";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEndOTInvoices.RowHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dgvEndOTInvoices.RowHeadersVisible = false;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle11.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvEndOTInvoices.RowsDefaultCellStyle = dataGridViewCellStyle11;
            this.dgvEndOTInvoices.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvEndOTInvoices.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvEndOTInvoices.Size = new System.Drawing.Size(745, 25);
            this.dgvEndOTInvoices.TabIndex = 38;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "";
            this.Column1.Name = "Column1";
            this.Column1.Width = 1000;
            // 
            // dgvEndGroupMsg
            // 
            this.dgvEndGroupMsg.AllowUserToAddRows = false;
            this.dgvEndGroupMsg.AllowUserToDeleteRows = false;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle12.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvEndGroupMsg.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle12;
            this.dgvEndGroupMsg.BackgroundColor = System.Drawing.Color.White;
            this.dgvEndGroupMsg.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvEndGroupMsg.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvEndGroupMsg.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEndGroupMsg.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.dgvEndGroupMsg.ColumnHeadersHeight = 30;
            this.dgvEndGroupMsg.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn3});
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvEndGroupMsg.DefaultCellStyle = dataGridViewCellStyle15;
            this.dgvEndGroupMsg.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvEndGroupMsg.GridColor = System.Drawing.Color.Black;
            this.dgvEndGroupMsg.Location = new System.Drawing.Point(35, 283);
            this.dgvEndGroupMsg.MultiSelect = false;
            this.dgvEndGroupMsg.Name = "dgvEndGroupMsg";
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEndGroupMsg.RowHeadersDefaultCellStyle = dataGridViewCellStyle16;
            this.dgvEndGroupMsg.RowHeadersVisible = false;
            dataGridViewCellStyle17.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle17.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvEndGroupMsg.RowsDefaultCellStyle = dataGridViewCellStyle17;
            this.dgvEndGroupMsg.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvEndGroupMsg.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvEndGroupMsg.Size = new System.Drawing.Size(745, 31);
            this.dgvEndGroupMsg.TabIndex = 37;
            // 
            // dataGridViewTextBoxColumn3
            // 
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle14;
            this.dataGridViewTextBoxColumn3.HeaderText = "End Group Header";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 745;
            // 
            // lblFooter
            // 
            this.lblFooter.BackColor = System.Drawing.Color.White;
            this.lblFooter.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFooter.ForeColor = System.Drawing.Color.Black;
            this.lblFooter.Location = new System.Drawing.Point(35, 382);
            this.lblFooter.Name = "lblFooter";
            this.lblFooter.Size = new System.Drawing.Size(1000, 15);
            this.lblFooter.TabIndex = 27;
            this.lblFooter.Text = "MSD";
            // 
            // dgvEmptyMessage
            // 
            this.dgvEmptyMessage.AllowUserToAddRows = false;
            this.dgvEmptyMessage.AllowUserToDeleteRows = false;
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle18.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle18.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvEmptyMessage.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle18;
            this.dgvEmptyMessage.BackgroundColor = System.Drawing.Color.Gray;
            this.dgvEmptyMessage.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvEmptyMessage.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle19.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle19.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEmptyMessage.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle19;
            this.dgvEmptyMessage.ColumnHeadersHeight = 20;
            this.dgvEmptyMessage.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn32});
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle21.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle21.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle21.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle21.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle21.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvEmptyMessage.DefaultCellStyle = dataGridViewCellStyle21;
            this.dgvEmptyMessage.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvEmptyMessage.GridColor = System.Drawing.Color.Black;
            this.dgvEmptyMessage.Location = new System.Drawing.Point(35, 412);
            this.dgvEmptyMessage.MultiSelect = false;
            this.dgvEmptyMessage.Name = "dgvEmptyMessage";
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle22.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle22.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle22.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle22.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle22.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEmptyMessage.RowHeadersDefaultCellStyle = dataGridViewCellStyle22;
            this.dgvEmptyMessage.RowHeadersVisible = false;
            dataGridViewCellStyle23.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle23.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle23.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle23.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvEmptyMessage.RowsDefaultCellStyle = dataGridViewCellStyle23;
            this.dgvEmptyMessage.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvEmptyMessage.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvEmptyMessage.Size = new System.Drawing.Size(745, 19);
            this.dgvEmptyMessage.TabIndex = 26;
            // 
            // dataGridViewTextBoxColumn32
            // 
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn32.DefaultCellStyle = dataGridViewCellStyle20;
            this.dataGridViewTextBoxColumn32.HeaderText = "No items in this section";
            this.dataGridViewTextBoxColumn32.Name = "dataGridViewTextBoxColumn32";
            this.dataGridViewTextBoxColumn32.Width = 1000;
            // 
            // tabHeaderSections
            // 
            this.tabHeaderSections.Controls.Add(this.dgvSectionHeaderYourOrder);
            this.tabHeaderSections.Controls.Add(this.dgvSectionHeaderA0);
            this.tabHeaderSections.Controls.Add(this.dgvSectionHeaderA1);
            this.tabHeaderSections.Controls.Add(this.dataGridView1);
            this.tabHeaderSections.Location = new System.Drawing.Point(4, 22);
            this.tabHeaderSections.Name = "tabHeaderSections";
            this.tabHeaderSections.Size = new System.Drawing.Size(820, 543);
            this.tabHeaderSections.TabIndex = 2;
            this.tabHeaderSections.Text = "HeaderSection";
            this.tabHeaderSections.UseVisualStyleBackColor = true;
            // 
            // dgvSectionHeaderYourOrder
            // 
            this.dgvSectionHeaderYourOrder.AllowUserToAddRows = false;
            this.dgvSectionHeaderYourOrder.AllowUserToDeleteRows = false;
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle24.BackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle24.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle24.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle24.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeaderYourOrder.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle24;
            this.dgvSectionHeaderYourOrder.BackgroundColor = System.Drawing.Color.Gray;
            this.dgvSectionHeaderYourOrder.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvSectionHeaderYourOrder.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvSectionHeaderYourOrder.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle25.BackColor = System.Drawing.SystemColors.AppWorkspace;
            dataGridViewCellStyle25.Font = new System.Drawing.Font("Times New Roman", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle25.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle25.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle25.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle25.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSectionHeaderYourOrder.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle25;
            this.dgvSectionHeaderYourOrder.ColumnHeadersHeight = 33;
            this.dgvSectionHeaderYourOrder.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn4});
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle27.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle27.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle27.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle27.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle27.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle27.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSectionHeaderYourOrder.DefaultCellStyle = dataGridViewCellStyle27;
            this.dgvSectionHeaderYourOrder.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSectionHeaderYourOrder.GridColor = System.Drawing.Color.Black;
            this.dgvSectionHeaderYourOrder.Location = new System.Drawing.Point(35, 165);
            this.dgvSectionHeaderYourOrder.MultiSelect = false;
            this.dgvSectionHeaderYourOrder.Name = "dgvSectionHeaderYourOrder";
            this.dgvSectionHeaderYourOrder.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvSectionHeaderYourOrder.RowHeadersVisible = false;
            dataGridViewCellStyle28.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle28.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle28.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle28.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeaderYourOrder.RowsDefaultCellStyle = dataGridViewCellStyle28;
            this.dgvSectionHeaderYourOrder.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvSectionHeaderYourOrder.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSectionHeaderYourOrder.Size = new System.Drawing.Size(745, 33);
            this.dgvSectionHeaderYourOrder.TabIndex = 68;
            // 
            // dataGridViewTextBoxColumn4
            // 
            dataGridViewCellStyle26.NullValue = null;
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle26;
            this.dataGridViewTextBoxColumn4.HeaderText = "Invoice History";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Width = 745;
            // 
            // dgvSectionHeaderA0
            // 
            this.dgvSectionHeaderA0.AllowUserToAddRows = false;
            this.dgvSectionHeaderA0.AllowUserToDeleteRows = false;
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle29.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle29.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle29.SelectionBackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle29.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeaderA0.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle29;
            this.dgvSectionHeaderA0.BackgroundColor = System.Drawing.Color.White;
            this.dgvSectionHeaderA0.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvSectionHeaderA0.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvSectionHeaderA0.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle30.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle30.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle30.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle30.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle30.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle30.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSectionHeaderA0.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle30;
            this.dgvSectionHeaderA0.ColumnHeadersHeight = 22;
            this.dgvSectionHeaderA0.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1});
            dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle31.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle31.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle31.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle31.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle31.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle31.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSectionHeaderA0.DefaultCellStyle = dataGridViewCellStyle31;
            this.dgvSectionHeaderA0.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSectionHeaderA0.GridColor = System.Drawing.Color.Black;
            this.dgvSectionHeaderA0.Location = new System.Drawing.Point(35, 22);
            this.dgvSectionHeaderA0.MultiSelect = false;
            this.dgvSectionHeaderA0.Name = "dgvSectionHeaderA0";
            this.dgvSectionHeaderA0.RowHeadersVisible = false;
            dataGridViewCellStyle32.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle32.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeaderA0.RowsDefaultCellStyle = dataGridViewCellStyle32;
            this.dgvSectionHeaderA0.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvSectionHeaderA0.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSectionHeaderA0.Size = new System.Drawing.Size(250, 22);
            this.dgvSectionHeaderA0.TabIndex = 67;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 250;
            // 
            // dgvSectionHeaderA1
            // 
            this.dgvSectionHeaderA1.AllowUserToAddRows = false;
            this.dgvSectionHeaderA1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle33.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle33.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle33.SelectionBackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle33.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeaderA1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle33;
            this.dgvSectionHeaderA1.BackgroundColor = System.Drawing.Color.White;
            this.dgvSectionHeaderA1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvSectionHeaderA1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle34.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle34.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle34.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle34.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle34.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle34.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSectionHeaderA1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle34;
            this.dgvSectionHeaderA1.ColumnHeadersHeight = 22;
            this.dgvSectionHeaderA1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn19,
            this.dataGridViewTextBoxColumn21});
            dataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle37.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle37.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle37.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle37.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle37.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle37.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSectionHeaderA1.DefaultCellStyle = dataGridViewCellStyle37;
            this.dgvSectionHeaderA1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSectionHeaderA1.GridColor = System.Drawing.Color.Black;
            this.dgvSectionHeaderA1.Location = new System.Drawing.Point(480, 22);
            this.dgvSectionHeaderA1.MultiSelect = false;
            this.dgvSectionHeaderA1.Name = "dgvSectionHeaderA1";
            this.dgvSectionHeaderA1.RowHeadersVisible = false;
            dataGridViewCellStyle38.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle38.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle38.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle38.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeaderA1.RowsDefaultCellStyle = dataGridViewCellStyle38;
            this.dgvSectionHeaderA1.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvSectionHeaderA1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSectionHeaderA1.Size = new System.Drawing.Size(300, 22);
            this.dgvSectionHeaderA1.TabIndex = 66;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle39.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle39.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle39.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle39.SelectionForeColor = System.Drawing.Color.Black;
            this.dataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle39;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dataGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle40.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle40.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle40.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle40.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle40.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle40.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle40;
            this.dataGridView1.ColumnHeadersHeight = 22;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn18});
            dataGridViewCellStyle41.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle41.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle41.Font = new System.Drawing.Font("Microsoft Sans Serif", 3.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle41.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle41.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle41.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle41.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle41;
            this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridView1.GridColor = System.Drawing.Color.Black;
            this.dataGridView1.Location = new System.Drawing.Point(35, 120);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            dataGridViewCellStyle42.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle42.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle42.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle42.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle42.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle42.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle42;
            this.dataGridView1.RowHeadersVisible = false;
            dataGridViewCellStyle43.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle43.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle43.SelectionForeColor = System.Drawing.Color.Black;
            this.dataGridView1.RowsDefaultCellStyle = dataGridViewCellStyle43;
            this.dataGridView1.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(745, 22);
            this.dataGridView1.TabIndex = 49;
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.HeaderText = "";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.Width = 745;
            // 
            // tabSections
            // 
            this.tabSections.Controls.Add(this.dgvSectionYourOrder);
            this.tabSections.Location = new System.Drawing.Point(4, 22);
            this.tabSections.Name = "tabSections";
            this.tabSections.Size = new System.Drawing.Size(820, 543);
            this.tabSections.TabIndex = 3;
            this.tabSections.Text = "Sections";
            this.tabSections.UseVisualStyleBackColor = true;
            // 
            // dgvSectionYourOrder
            // 
            this.dgvSectionYourOrder.AllowUserToAddRows = false;
            this.dgvSectionYourOrder.AllowUserToDeleteRows = false;
            dataGridViewCellStyle44.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle44.BackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle44.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle44.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle44.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionYourOrder.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle44;
            this.dgvSectionYourOrder.BackgroundColor = System.Drawing.Color.Gray;
            this.dgvSectionYourOrder.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvSectionYourOrder.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvSectionYourOrder.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle45.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            dataGridViewCellStyle45.BackColor = System.Drawing.SystemColors.AppWorkspace;
            dataGridViewCellStyle45.Font = new System.Drawing.Font("Times New Roman", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle45.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle45.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle45.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle45.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSectionYourOrder.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle45;
            this.dgvSectionYourOrder.ColumnHeadersHeight = 35;
            this.dgvSectionYourOrder.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Field5,
            this.Field6,
            this.PO,
            this.Field1,
            this.Field2,
            this.Field3,
            this.UnitPrice,
            this.ExtendedPrice});
            dataGridViewCellStyle51.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle51.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle51.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle51.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle51.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle51.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle51.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSectionYourOrder.DefaultCellStyle = dataGridViewCellStyle51;
            this.dgvSectionYourOrder.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSectionYourOrder.GridColor = System.Drawing.Color.Black;
            this.dgvSectionYourOrder.Location = new System.Drawing.Point(35, 67);
            this.dgvSectionYourOrder.MultiSelect = false;
            this.dgvSectionYourOrder.Name = "dgvSectionYourOrder";
            this.dgvSectionYourOrder.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvSectionYourOrder.RowHeadersVisible = false;
            dataGridViewCellStyle52.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle52.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle52.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle52.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionYourOrder.RowsDefaultCellStyle = dataGridViewCellStyle52;
            this.dgvSectionYourOrder.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvSectionYourOrder.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSectionYourOrder.Size = new System.Drawing.Size(745, 35);
            this.dgvSectionYourOrder.TabIndex = 53;
            // 
            // dataGridViewTextBoxColumn19
            // 
            this.dataGridViewTextBoxColumn19.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn19.DefaultCellStyle = dataGridViewCellStyle35;
            this.dataGridViewTextBoxColumn19.HeaderText = "Invoice Header";
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.Width = 150;
            // 
            // dataGridViewTextBoxColumn21
            // 
            dataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dataGridViewTextBoxColumn21.DefaultCellStyle = dataGridViewCellStyle36;
            this.dataGridViewTextBoxColumn21.HeaderText = "";
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            this.dataGridViewTextBoxColumn21.Width = 150;
            // 
            // Field5
            // 
            dataGridViewCellStyle46.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            this.Field5.DefaultCellStyle = dataGridViewCellStyle46;
            this.Field5.HeaderText = "Invoice Date";
            this.Field5.Name = "Field5";
            this.Field5.Width = 80;
            // 
            // Field6
            // 
            dataGridViewCellStyle47.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            this.Field6.DefaultCellStyle = dataGridViewCellStyle47;
            this.Field6.HeaderText = "Invoice #";
            this.Field6.Name = "Field6";
            this.Field6.Width = 80;
            // 
            // PO
            // 
            this.PO.HeaderText = "PO#";
            this.PO.Name = "PO";
            this.PO.Width = 110;
            // 
            // Field1
            // 
            dataGridViewCellStyle48.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            dataGridViewCellStyle48.NullValue = null;
            this.Field1.DefaultCellStyle = dataGridViewCellStyle48;
            this.Field1.HeaderText = "Ship To";
            this.Field1.Name = "Field1";
            this.Field1.Width = 175;
            // 
            // Field2
            // 
            dataGridViewCellStyle49.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle49.NullValue = null;
            this.Field2.DefaultCellStyle = dataGridViewCellStyle49;
            this.Field2.HeaderText = "  Due Date";
            this.Field2.Name = "Field2";
            this.Field2.Width = 70;
            // 
            // Field3
            // 
            dataGridViewCellStyle50.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle50.NullValue = null;
            this.Field3.DefaultCellStyle = dataGridViewCellStyle50;
            this.Field3.HeaderText = "  Paid Date";
            this.Field3.Name = "Field3";
            this.Field3.Width = 70;
            // 
            // UnitPrice
            // 
            this.UnitPrice.HeaderText = "Terms";
            this.UnitPrice.Name = "UnitPrice";
            this.UnitPrice.Width = 80;
            // 
            // ExtendedPrice
            // 
            this.ExtendedPrice.HeaderText = "Total";
            this.ExtendedPrice.Name = "ExtendedPrice";
            this.ExtendedPrice.Width = 80;
            // 
            // frmInvoiceHistory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(859, 599);
            this.Controls.Add(this.tabControl1);
            this.Name = "frmInvoiceHistory";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.tabControl1.ResumeLayout(false);
            this.tabHeader.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPageHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEndOTInvoices)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEndGroupMsg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmptyMessage)).EndInit();
            this.tabHeaderSections.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeaderYourOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeaderA0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeaderA1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabSections.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionYourOrder)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabHeader;
        private System.Windows.Forms.DataGridView dgvPageHeader;
        private System.Windows.Forms.DataGridView dgvGroup1;
        private System.Windows.Forms.DataGridView dgvEndOTInvoices;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridView dgvEndGroupMsg;
        private System.Windows.Forms.Label lblFooter;
        private System.Windows.Forms.DataGridView dgvEmptyMessage;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn32;
        private System.Windows.Forms.TabPage tabHeaderSections;
        private System.Windows.Forms.DataGridView dgvSectionHeaderYourOrder;
        private System.Windows.Forms.DataGridView dgvSectionHeaderA0;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridView dgvSectionHeaderA1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TabPage tabSections;
        private System.Windows.Forms.DataGridView dgvSectionYourOrder;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn31;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.DataGridViewTextBoxColumn Field5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Field6;
        private System.Windows.Forms.DataGridViewTextBoxColumn PO;
        private System.Windows.Forms.DataGridViewTextBoxColumn Field1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Field2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Field3;
        private System.Windows.Forms.DataGridViewTextBoxColumn UnitPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn ExtendedPrice;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OMInvoices
{
    public static class DbAccess
    {
        private static string _ConnectionString = "Data Source=MSDSQL; Initial Catalog=medcc_custom; User ID=MsdWebApi; Password=pwd4MsdWebApi";
        //private static string _ConnectionString = ConfigurationManager.ConnectionStrings["MSDSQLConnectionString"].ConnectionString;

        private static string MSDSQLServer = "MSDSQL";
        private static string MSDSQL01Server = "MSDSQL01.msd.local";
        private static string MSDSQL03Server = "devsql03.msddev.local";

        private static string database = "medcc_custom";
        private static string user_id = "MsdWebApi";
        private static string password = "pwd4MsdWebApi";

        public static void Set_MSDSQL()
        {
            _ConnectionString = "server=" + MSDSQLServer + "; " +
                             "database=" + database + "; " +
                             "user id=" + user_id + "; " +
                             "password=" + password + "; " +
                             "connection timeout=30";
            //_ConnectionString = ConfigurationManager.ConnectionStrings["MSDSQLConnectionString"].ConnectionString;
        }

        public static void Set_MSDSQL01()
        {
            _ConnectionString = "server=" + MSDSQL01Server + "; " +
                             "database=" + database + "; " +
                             "user id=" + user_id + "; " +
                             "password=" + password + "; " +
                             "connection timeout=30";
            //_ConnectionString = ConfigurationManager.ConnectionStrings["MSDSQL01ConnectionString"].ConnectionString;
        }

        public static void Set_MSDSQL03()
        {
            _ConnectionString = "server=" + MSDSQL03Server + "; " +
                             "database=" + database + "; " +
                             "user id=" + user_id + "; " +
                             "password=" + password + "; " +
                             "connection timeout=30";
            //_ConnectionString = ConfigurationManager.ConnectionStrings["MSDSQL03ConnectionString"].ConnectionString;
        }

        public static string ConnectionString
        {
            get { return _ConnectionString; }
            set { _ConnectionString = value; }
        }
    }

    //-------------------------------------------------------------------------
}

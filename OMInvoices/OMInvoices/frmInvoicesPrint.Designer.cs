﻿namespace OMInvoices
{
    partial class frmInvoicesPrint
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle40 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle41 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle42 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle43 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle53 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle54 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle44 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle45 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle46 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle47 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle48 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle49 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle50 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle51 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle52 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle55 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle56 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle61 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle62 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle57 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle58 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle59 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle60 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle63 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle64 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle66 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle67 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle65 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle68 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle69 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle70 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle71 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle72 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle73 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle74 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle75 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle76 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle77 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle81 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle82 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle78 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle79 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle80 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle83 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle84 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle88 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle89 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle85 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle86 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle87 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle90 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle91 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle97 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle98 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle92 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle93 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle94 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle95 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle96 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle99 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle100 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle102 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle103 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle101 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle104 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle105 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle106 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle107 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle108 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle109 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle110 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle113 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle114 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle111 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle112 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle115 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle116 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle117 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle118 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle119 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle120 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle122 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle123 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle121 = new System.Windows.Forms.DataGridViewCellStyle();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabHeader = new System.Windows.Forms.TabPage();
            this.dgvGroup1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn31 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvEndOTInvoices = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvEndGroupMsg = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblFooter = new System.Windows.Forms.Label();
            this.dgvEmptyMessage = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn32 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvPageHeader = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabHeaderSections = new System.Windows.Forms.TabPage();
            this.dgvSectionLine6 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn42 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvSection4 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn41 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvSection3 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn39 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn40 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvSectionLine5 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn33 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn34 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn35 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn36 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn37 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn38 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvSectionHeader4 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvSectionHeaderE1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvSectionHeaderA1 = new System.Windows.Forms.DataGridView();
            this.dgvSectionHeaderB1 = new System.Windows.Forms.DataGridView();
            this.dgvSectionHeader5 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BillableDays = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvSectionSubHeader5 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PRICEEACH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvSectionHeader3 = new System.Windows.Forms.DataGridView();
            this.SALESPERSON = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TAKER = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMERPONUMBER = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TERMS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvSectionLine1 = new System.Windows.Forms.DataGridView();
            this.InvoiceNote = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvSectionHeaderD1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvSectionHeader2 = new System.Windows.Forms.DataGridView();
            this.space1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BillToAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ShipTo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ShipToAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvSectionHeaderC1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabSections = new System.Windows.Forms.TabPage();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabControl1.SuspendLayout();
            this.tabHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEndOTInvoices)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEndGroupMsg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmptyMessage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPageHeader)).BeginInit();
            this.tabHeaderSections.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionLine6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSection4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSection3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionLine5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeader4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeaderE1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeaderA1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeaderB1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeader5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionSubHeader5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeader3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionLine1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeaderD1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeader2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeaderC1)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabHeader);
            this.tabControl1.Controls.Add(this.tabHeaderSections);
            this.tabControl1.Controls.Add(this.tabSections);
            this.tabControl1.Location = new System.Drawing.Point(12, 5);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(832, 569);
            this.tabControl1.TabIndex = 18;
            // 
            // tabHeader
            // 
            this.tabHeader.Controls.Add(this.dgvGroup1);
            this.tabHeader.Controls.Add(this.dgvEndOTInvoices);
            this.tabHeader.Controls.Add(this.dgvEndGroupMsg);
            this.tabHeader.Controls.Add(this.lblFooter);
            this.tabHeader.Controls.Add(this.dgvEmptyMessage);
            this.tabHeader.Controls.Add(this.dgvPageHeader);
            this.tabHeader.Location = new System.Drawing.Point(4, 22);
            this.tabHeader.Name = "tabHeader";
            this.tabHeader.Padding = new System.Windows.Forms.Padding(3);
            this.tabHeader.Size = new System.Drawing.Size(824, 543);
            this.tabHeader.TabIndex = 1;
            this.tabHeader.Text = "Header";
            this.tabHeader.UseVisualStyleBackColor = true;
            // 
            // dgvGroup1
            // 
            this.dgvGroup1.AllowUserToAddRows = false;
            this.dgvGroup1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvGroup1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvGroup1.BackgroundColor = System.Drawing.Color.Gray;
            this.dgvGroup1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvGroup1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dgvGroup1.ColumnHeadersHeight = 30;
            this.dgvGroup1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn31});
            this.dgvGroup1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvGroup1.GridColor = System.Drawing.Color.Black;
            this.dgvGroup1.Location = new System.Drawing.Point(35, 73);
            this.dgvGroup1.MultiSelect = false;
            this.dgvGroup1.Name = "dgvGroup1";
            this.dgvGroup1.RowHeadersVisible = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvGroup1.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvGroup1.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvGroup1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvGroup1.Size = new System.Drawing.Size(745, 28);
            this.dgvGroup1.TabIndex = 39;
            // 
            // dataGridViewTextBoxColumn31
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn31.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewTextBoxColumn31.HeaderText = "Group Header";
            this.dataGridViewTextBoxColumn31.Name = "dataGridViewTextBoxColumn31";
            this.dataGridViewTextBoxColumn31.Width = 745;
            // 
            // dgvEndOTInvoices
            // 
            this.dgvEndOTInvoices.AllowUserToAddRows = false;
            this.dgvEndOTInvoices.AllowUserToDeleteRows = false;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvEndOTInvoices.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvEndOTInvoices.BackgroundColor = System.Drawing.Color.Gray;
            this.dgvEndOTInvoices.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvEndOTInvoices.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dgvEndOTInvoices.ColumnHeadersHeight = 30;
            this.dgvEndOTInvoices.ColumnHeadersVisible = false;
            this.dgvEndOTInvoices.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1});
            this.dgvEndOTInvoices.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvEndOTInvoices.GridColor = System.Drawing.Color.Black;
            this.dgvEndOTInvoices.Location = new System.Drawing.Point(35, 340);
            this.dgvEndOTInvoices.MultiSelect = false;
            this.dgvEndOTInvoices.Name = "dgvEndOTInvoices";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEndOTInvoices.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvEndOTInvoices.RowHeadersVisible = false;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvEndOTInvoices.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvEndOTInvoices.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvEndOTInvoices.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvEndOTInvoices.Size = new System.Drawing.Size(745, 25);
            this.dgvEndOTInvoices.TabIndex = 38;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "";
            this.Column1.Name = "Column1";
            this.Column1.Width = 745;
            // 
            // dgvEndGroupMsg
            // 
            this.dgvEndGroupMsg.AllowUserToAddRows = false;
            this.dgvEndGroupMsg.AllowUserToDeleteRows = false;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvEndGroupMsg.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle7;
            this.dgvEndGroupMsg.BackgroundColor = System.Drawing.Color.White;
            this.dgvEndGroupMsg.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvEndGroupMsg.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvEndGroupMsg.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEndGroupMsg.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.dgvEndGroupMsg.ColumnHeadersHeight = 30;
            this.dgvEndGroupMsg.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn3});
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvEndGroupMsg.DefaultCellStyle = dataGridViewCellStyle10;
            this.dgvEndGroupMsg.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvEndGroupMsg.GridColor = System.Drawing.Color.Black;
            this.dgvEndGroupMsg.Location = new System.Drawing.Point(35, 283);
            this.dgvEndGroupMsg.MultiSelect = false;
            this.dgvEndGroupMsg.Name = "dgvEndGroupMsg";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEndGroupMsg.RowHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.dgvEndGroupMsg.RowHeadersVisible = false;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle12.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvEndGroupMsg.RowsDefaultCellStyle = dataGridViewCellStyle12;
            this.dgvEndGroupMsg.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvEndGroupMsg.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvEndGroupMsg.Size = new System.Drawing.Size(745, 31);
            this.dgvEndGroupMsg.TabIndex = 37;
            // 
            // dataGridViewTextBoxColumn3
            // 
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridViewTextBoxColumn3.HeaderText = "End Group Header";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 745;
            // 
            // lblFooter
            // 
            this.lblFooter.BackColor = System.Drawing.Color.White;
            this.lblFooter.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFooter.ForeColor = System.Drawing.Color.Black;
            this.lblFooter.Location = new System.Drawing.Point(35, 382);
            this.lblFooter.Name = "lblFooter";
            this.lblFooter.Size = new System.Drawing.Size(745, 15);
            this.lblFooter.TabIndex = 27;
            this.lblFooter.Text = "MSD";
            // 
            // dgvEmptyMessage
            // 
            this.dgvEmptyMessage.AllowUserToAddRows = false;
            this.dgvEmptyMessage.AllowUserToDeleteRows = false;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle13.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvEmptyMessage.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle13;
            this.dgvEmptyMessage.BackgroundColor = System.Drawing.Color.Gray;
            this.dgvEmptyMessage.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvEmptyMessage.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEmptyMessage.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle14;
            this.dgvEmptyMessage.ColumnHeadersHeight = 20;
            this.dgvEmptyMessage.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn32});
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvEmptyMessage.DefaultCellStyle = dataGridViewCellStyle16;
            this.dgvEmptyMessage.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvEmptyMessage.GridColor = System.Drawing.Color.Black;
            this.dgvEmptyMessage.Location = new System.Drawing.Point(35, 412);
            this.dgvEmptyMessage.MultiSelect = false;
            this.dgvEmptyMessage.Name = "dgvEmptyMessage";
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle17.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle17.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEmptyMessage.RowHeadersDefaultCellStyle = dataGridViewCellStyle17;
            this.dgvEmptyMessage.RowHeadersVisible = false;
            dataGridViewCellStyle18.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle18.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvEmptyMessage.RowsDefaultCellStyle = dataGridViewCellStyle18;
            this.dgvEmptyMessage.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvEmptyMessage.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvEmptyMessage.Size = new System.Drawing.Size(745, 19);
            this.dgvEmptyMessage.TabIndex = 26;
            // 
            // dataGridViewTextBoxColumn32
            // 
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn32.DefaultCellStyle = dataGridViewCellStyle15;
            this.dataGridViewTextBoxColumn32.HeaderText = "No items in this section";
            this.dataGridViewTextBoxColumn32.Name = "dataGridViewTextBoxColumn32";
            this.dataGridViewTextBoxColumn32.Width = 745;
            // 
            // dgvPageHeader
            // 
            this.dgvPageHeader.AllowUserToAddRows = false;
            this.dgvPageHeader.AllowUserToDeleteRows = false;
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle19.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle19.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvPageHeader.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle19;
            this.dgvPageHeader.BackgroundColor = System.Drawing.Color.White;
            this.dgvPageHeader.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvPageHeader.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle20.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle20.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle20.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle20.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle20.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvPageHeader.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle20;
            this.dgvPageHeader.ColumnHeadersHeight = 50;
            this.dgvPageHeader.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2});
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            dataGridViewCellStyle22.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle22.Font = new System.Drawing.Font("Arial", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle22.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle22.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle22.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvPageHeader.DefaultCellStyle = dataGridViewCellStyle22;
            this.dgvPageHeader.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvPageHeader.GridColor = System.Drawing.Color.Black;
            this.dgvPageHeader.Location = new System.Drawing.Point(530, 19);
            this.dgvPageHeader.MultiSelect = false;
            this.dgvPageHeader.Name = "dgvPageHeader";
            this.dgvPageHeader.RowHeadersVisible = false;
            dataGridViewCellStyle23.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle23.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvPageHeader.RowsDefaultCellStyle = dataGridViewCellStyle23;
            this.dgvPageHeader.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvPageHeader.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPageHeader.Size = new System.Drawing.Size(250, 30);
            this.dgvPageHeader.TabIndex = 21;
            // 
            // dataGridViewTextBoxColumn2
            // 
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle21;
            this.dataGridViewTextBoxColumn2.HeaderText = "INVOICE";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 250;
            // 
            // tabHeaderSections
            // 
            this.tabHeaderSections.Controls.Add(this.dgvSectionLine6);
            this.tabHeaderSections.Controls.Add(this.dgvSection4);
            this.tabHeaderSections.Controls.Add(this.dgvSection3);
            this.tabHeaderSections.Controls.Add(this.dgvSectionLine5);
            this.tabHeaderSections.Controls.Add(this.dgvSectionHeader4);
            this.tabHeaderSections.Controls.Add(this.dgvSectionHeaderE1);
            this.tabHeaderSections.Controls.Add(this.dgvSectionHeaderA1);
            this.tabHeaderSections.Controls.Add(this.dgvSectionHeaderB1);
            this.tabHeaderSections.Controls.Add(this.dgvSectionHeader5);
            this.tabHeaderSections.Controls.Add(this.dgvSectionSubHeader5);
            this.tabHeaderSections.Controls.Add(this.dgvSectionHeader3);
            this.tabHeaderSections.Controls.Add(this.dgvSectionLine1);
            this.tabHeaderSections.Controls.Add(this.dataGridView1);
            this.tabHeaderSections.Controls.Add(this.dgvSectionHeaderD1);
            this.tabHeaderSections.Controls.Add(this.dgvSectionHeader2);
            this.tabHeaderSections.Controls.Add(this.dgvSectionHeaderC1);
            this.tabHeaderSections.Location = new System.Drawing.Point(4, 22);
            this.tabHeaderSections.Name = "tabHeaderSections";
            this.tabHeaderSections.Size = new System.Drawing.Size(824, 543);
            this.tabHeaderSections.TabIndex = 2;
            this.tabHeaderSections.Text = "HeaderSection";
            this.tabHeaderSections.UseVisualStyleBackColor = true;
            // 
            // dgvSectionLine6
            // 
            this.dgvSectionLine6.AllowUserToAddRows = false;
            this.dgvSectionLine6.AllowUserToDeleteRows = false;
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle24.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle24.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle24.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle24.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionLine6.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle24;
            this.dgvSectionLine6.BackgroundColor = System.Drawing.Color.Gray;
            this.dgvSectionLine6.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvSectionLine6.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvSectionLine6.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle25.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle25.Font = new System.Drawing.Font("Times New Roman", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle25.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle25.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle25.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle25.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSectionLine6.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle25;
            this.dgvSectionLine6.ColumnHeadersHeight = 22;
            this.dgvSectionLine6.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn26,
            this.dataGridViewTextBoxColumn42});
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle28.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle28.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle28.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle28.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle28.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle28.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSectionLine6.DefaultCellStyle = dataGridViewCellStyle28;
            this.dgvSectionLine6.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSectionLine6.GridColor = System.Drawing.Color.Black;
            this.dgvSectionLine6.Location = new System.Drawing.Point(107, 384);
            this.dgvSectionLine6.MultiSelect = false;
            this.dgvSectionLine6.Name = "dgvSectionLine6";
            this.dgvSectionLine6.RowHeadersVisible = false;
            dataGridViewCellStyle29.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle29.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle29.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle29.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionLine6.RowsDefaultCellStyle = dataGridViewCellStyle29;
            this.dgvSectionLine6.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvSectionLine6.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSectionLine6.Size = new System.Drawing.Size(600, 22);
            this.dgvSectionLine6.TabIndex = 65;
            // 
            // dataGridViewTextBoxColumn26
            // 
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.BottomCenter;
            dataGridViewCellStyle26.NullValue = null;
            this.dataGridViewTextBoxColumn26.DefaultCellStyle = dataGridViewCellStyle26;
            this.dataGridViewTextBoxColumn26.HeaderText = "Carrier";
            this.dataGridViewTextBoxColumn26.Name = "dataGridViewTextBoxColumn26";
            this.dataGridViewTextBoxColumn26.Width = 300;
            // 
            // dataGridViewTextBoxColumn42
            // 
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.BottomCenter;
            this.dataGridViewTextBoxColumn42.DefaultCellStyle = dataGridViewCellStyle27;
            this.dataGridViewTextBoxColumn42.HeaderText = "Tracking #";
            this.dataGridViewTextBoxColumn42.Name = "dataGridViewTextBoxColumn42";
            this.dataGridViewTextBoxColumn42.Width = 300;
            // 
            // dgvSection4
            // 
            this.dgvSection4.AllowUserToAddRows = false;
            this.dgvSection4.AllowUserToDeleteRows = false;
            dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle30.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle30.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle30.SelectionBackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle30.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSection4.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle30;
            this.dgvSection4.BackgroundColor = System.Drawing.Color.White;
            this.dgvSection4.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvSection4.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle31.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle31.Font = new System.Drawing.Font("Times New Roman", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle31.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle31.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle31.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle31.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSection4.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle31;
            this.dgvSection4.ColumnHeadersHeight = 30;
            this.dgvSection4.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn25,
            this.dataGridViewTextBoxColumn41});
            dataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle34.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle34.Font = new System.Drawing.Font("Times New Roman", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle34.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle34.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle34.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle34.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSection4.DefaultCellStyle = dataGridViewCellStyle34;
            this.dgvSection4.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSection4.GridColor = System.Drawing.Color.Black;
            this.dgvSection4.Location = new System.Drawing.Point(480, 499);
            this.dgvSection4.MultiSelect = false;
            this.dgvSection4.Name = "dgvSection4";
            this.dgvSection4.RowHeadersVisible = false;
            dataGridViewCellStyle35.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle35.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle35.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle35.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSection4.RowsDefaultCellStyle = dataGridViewCellStyle35;
            this.dgvSection4.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvSection4.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSection4.Size = new System.Drawing.Size(300, 30);
            this.dgvSection4.TabIndex = 64;
            // 
            // dataGridViewTextBoxColumn25
            // 
            this.dataGridViewTextBoxColumn25.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn25.DefaultCellStyle = dataGridViewCellStyle32;
            this.dataGridViewTextBoxColumn25.HeaderText = "AMOUNT DUE";
            this.dataGridViewTextBoxColumn25.Name = "dataGridViewTextBoxColumn25";
            this.dataGridViewTextBoxColumn25.Width = 200;
            // 
            // dataGridViewTextBoxColumn41
            // 
            dataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn41.DefaultCellStyle = dataGridViewCellStyle33;
            this.dataGridViewTextBoxColumn41.HeaderText = "Amount";
            this.dataGridViewTextBoxColumn41.Name = "dataGridViewTextBoxColumn41";
            // 
            // dgvSection3
            // 
            this.dgvSection3.AllowUserToAddRows = false;
            this.dgvSection3.AllowUserToDeleteRows = false;
            dataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle36.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle36.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle36.SelectionBackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle36.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSection3.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle36;
            this.dgvSection3.BackgroundColor = System.Drawing.Color.White;
            this.dgvSection3.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvSection3.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle37.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle37.Font = new System.Drawing.Font("Times New Roman", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle37.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle37.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle37.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle37.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSection3.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle37;
            this.dgvSection3.ColumnHeadersHeight = 30;
            this.dgvSection3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn39,
            this.dataGridViewTextBoxColumn40});
            dataGridViewCellStyle40.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle40.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle40.Font = new System.Drawing.Font("Times New Roman", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle40.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle40.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle40.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle40.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSection3.DefaultCellStyle = dataGridViewCellStyle40;
            this.dgvSection3.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSection3.GridColor = System.Drawing.Color.Black;
            this.dgvSection3.Location = new System.Drawing.Point(480, 463);
            this.dgvSection3.MultiSelect = false;
            this.dgvSection3.Name = "dgvSection3";
            this.dgvSection3.RowHeadersVisible = false;
            dataGridViewCellStyle41.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle41.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle41.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle41.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSection3.RowsDefaultCellStyle = dataGridViewCellStyle41;
            this.dgvSection3.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvSection3.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSection3.Size = new System.Drawing.Size(300, 30);
            this.dgvSection3.TabIndex = 63;
            // 
            // dataGridViewTextBoxColumn39
            // 
            this.dataGridViewTextBoxColumn39.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn39.DefaultCellStyle = dataGridViewCellStyle38;
            this.dataGridViewTextBoxColumn39.HeaderText = "SubTotal";
            this.dataGridViewTextBoxColumn39.Name = "dataGridViewTextBoxColumn39";
            this.dataGridViewTextBoxColumn39.Width = 200;
            // 
            // dataGridViewTextBoxColumn40
            // 
            dataGridViewCellStyle39.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn40.DefaultCellStyle = dataGridViewCellStyle39;
            this.dataGridViewTextBoxColumn40.HeaderText = "Amount";
            this.dataGridViewTextBoxColumn40.Name = "dataGridViewTextBoxColumn40";
            // 
            // dgvSectionLine5
            // 
            this.dgvSectionLine5.AllowUserToAddRows = false;
            this.dgvSectionLine5.AllowUserToDeleteRows = false;
            dataGridViewCellStyle42.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle42.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle42.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle42.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle42.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionLine5.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle42;
            this.dgvSectionLine5.BackgroundColor = System.Drawing.Color.Gray;
            this.dgvSectionLine5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvSectionLine5.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvSectionLine5.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle43.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle43.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle43.Font = new System.Drawing.Font("Times New Roman", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle43.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle43.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle43.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle43.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSectionLine5.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle43;
            this.dgvSectionLine5.ColumnHeadersHeight = 25;
            this.dgvSectionLine5.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn13,
            this.dataGridViewTextBoxColumn29,
            this.dataGridViewTextBoxColumn30,
            this.dataGridViewTextBoxColumn33,
            this.dataGridViewTextBoxColumn34,
            this.dataGridViewTextBoxColumn35,
            this.dataGridViewTextBoxColumn36,
            this.dataGridViewTextBoxColumn37,
            this.dataGridViewTextBoxColumn38});
            dataGridViewCellStyle53.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle53.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle53.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle53.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle53.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle53.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle53.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSectionLine5.DefaultCellStyle = dataGridViewCellStyle53;
            this.dgvSectionLine5.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSectionLine5.GridColor = System.Drawing.Color.Black;
            this.dgvSectionLine5.Location = new System.Drawing.Point(35, 353);
            this.dgvSectionLine5.MultiSelect = false;
            this.dgvSectionLine5.Name = "dgvSectionLine5";
            this.dgvSectionLine5.RowHeadersVisible = false;
            dataGridViewCellStyle54.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle54.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle54.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle54.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionLine5.RowsDefaultCellStyle = dataGridViewCellStyle54;
            this.dgvSectionLine5.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvSectionLine5.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSectionLine5.Size = new System.Drawing.Size(745, 25);
            this.dgvSectionLine5.TabIndex = 62;
            // 
            // dataGridViewTextBoxColumn13
            // 
            dataGridViewCellStyle44.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle44.NullValue = null;
            this.dataGridViewTextBoxColumn13.DefaultCellStyle = dataGridViewCellStyle44;
            this.dataGridViewTextBoxColumn13.HeaderText = "Ordered";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.Width = 60;
            // 
            // dataGridViewTextBoxColumn29
            // 
            dataGridViewCellStyle45.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            this.dataGridViewTextBoxColumn29.DefaultCellStyle = dataGridViewCellStyle45;
            this.dataGridViewTextBoxColumn29.HeaderText = "Shipped";
            this.dataGridViewTextBoxColumn29.Name = "dataGridViewTextBoxColumn29";
            this.dataGridViewTextBoxColumn29.Width = 60;
            // 
            // dataGridViewTextBoxColumn30
            // 
            dataGridViewCellStyle46.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle46.NullValue = null;
            this.dataGridViewTextBoxColumn30.DefaultCellStyle = dataGridViewCellStyle46;
            this.dataGridViewTextBoxColumn30.HeaderText = "Remaining";
            this.dataGridViewTextBoxColumn30.Name = "dataGridViewTextBoxColumn30";
            this.dataGridViewTextBoxColumn30.Width = 90;
            // 
            // dataGridViewTextBoxColumn33
            // 
            dataGridViewCellStyle47.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle47.NullValue = null;
            this.dataGridViewTextBoxColumn33.DefaultCellStyle = dataGridViewCellStyle47;
            this.dataGridViewTextBoxColumn33.HeaderText = "Unit Size";
            this.dataGridViewTextBoxColumn33.Name = "dataGridViewTextBoxColumn33";
            this.dataGridViewTextBoxColumn33.Width = 60;
            // 
            // dataGridViewTextBoxColumn34
            // 
            dataGridViewCellStyle48.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            this.dataGridViewTextBoxColumn34.DefaultCellStyle = dataGridViewCellStyle48;
            this.dataGridViewTextBoxColumn34.HeaderText = "";
            this.dataGridViewTextBoxColumn34.Name = "dataGridViewTextBoxColumn34";
            this.dataGridViewTextBoxColumn34.Width = 30;
            // 
            // dataGridViewTextBoxColumn35
            // 
            dataGridViewCellStyle49.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            this.dataGridViewTextBoxColumn35.DefaultCellStyle = dataGridViewCellStyle49;
            this.dataGridViewTextBoxColumn35.HeaderText = "Item Description";
            this.dataGridViewTextBoxColumn35.Name = "dataGridViewTextBoxColumn35";
            this.dataGridViewTextBoxColumn35.Width = 235;
            // 
            // dataGridViewTextBoxColumn36
            // 
            dataGridViewCellStyle50.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            this.dataGridViewTextBoxColumn36.DefaultCellStyle = dataGridViewCellStyle50;
            this.dataGridViewTextBoxColumn36.HeaderText = "Unit Size";
            this.dataGridViewTextBoxColumn36.Name = "dataGridViewTextBoxColumn36";
            this.dataGridViewTextBoxColumn36.Width = 60;
            // 
            // dataGridViewTextBoxColumn37
            // 
            dataGridViewCellStyle51.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            this.dataGridViewTextBoxColumn37.DefaultCellStyle = dataGridViewCellStyle51;
            this.dataGridViewTextBoxColumn37.HeaderText = "";
            this.dataGridViewTextBoxColumn37.Name = "dataGridViewTextBoxColumn37";
            this.dataGridViewTextBoxColumn37.Width = 60;
            // 
            // dataGridViewTextBoxColumn38
            // 
            dataGridViewCellStyle52.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            this.dataGridViewTextBoxColumn38.DefaultCellStyle = dataGridViewCellStyle52;
            this.dataGridViewTextBoxColumn38.HeaderText = "";
            this.dataGridViewTextBoxColumn38.Name = "dataGridViewTextBoxColumn38";
            this.dataGridViewTextBoxColumn38.Width = 90;
            // 
            // dgvSectionHeader4
            // 
            this.dgvSectionHeader4.AllowUserToAddRows = false;
            this.dgvSectionHeader4.AllowUserToDeleteRows = false;
            dataGridViewCellStyle55.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle55.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle55.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle55.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle55.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeader4.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle55;
            this.dgvSectionHeader4.BackgroundColor = System.Drawing.Color.Gray;
            this.dgvSectionHeader4.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvSectionHeader4.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle56.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle56.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle56.Font = new System.Drawing.Font("Times New Roman", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle56.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle56.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle56.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle56.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSectionHeader4.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle56;
            this.dgvSectionHeader4.ColumnHeadersHeight = 30;
            this.dgvSectionHeader4.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn23,
            this.dataGridViewTextBoxColumn24,
            this.dataGridViewTextBoxColumn27,
            this.dataGridViewTextBoxColumn28});
            dataGridViewCellStyle61.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle61.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle61.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle61.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle61.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle61.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle61.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSectionHeader4.DefaultCellStyle = dataGridViewCellStyle61;
            this.dgvSectionHeader4.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSectionHeader4.GridColor = System.Drawing.Color.Black;
            this.dgvSectionHeader4.Location = new System.Drawing.Point(35, 215);
            this.dgvSectionHeader4.MultiSelect = false;
            this.dgvSectionHeader4.Name = "dgvSectionHeader4";
            this.dgvSectionHeader4.RowHeadersVisible = false;
            dataGridViewCellStyle62.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle62.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle62.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle62.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeader4.RowsDefaultCellStyle = dataGridViewCellStyle62;
            this.dgvSectionHeader4.RowTemplate.Height = 30;
            this.dgvSectionHeader4.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvSectionHeader4.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSectionHeader4.Size = new System.Drawing.Size(745, 25);
            this.dgvSectionHeader4.TabIndex = 61;
            // 
            // dataGridViewTextBoxColumn23
            // 
            dataGridViewCellStyle57.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle57.NullValue = null;
            this.dataGridViewTextBoxColumn23.DefaultCellStyle = dataGridViewCellStyle57;
            this.dataGridViewTextBoxColumn23.HeaderText = "Order Date";
            this.dataGridViewTextBoxColumn23.Name = "dataGridViewTextBoxColumn23";
            this.dataGridViewTextBoxColumn23.Width = 150;
            // 
            // dataGridViewTextBoxColumn24
            // 
            dataGridViewCellStyle58.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn24.DefaultCellStyle = dataGridViewCellStyle58;
            this.dataGridViewTextBoxColumn24.HeaderText = "Pick Ticket No";
            this.dataGridViewTextBoxColumn24.Name = "dataGridViewTextBoxColumn24";
            this.dataGridViewTextBoxColumn24.Width = 145;
            // 
            // dataGridViewTextBoxColumn27
            // 
            dataGridViewCellStyle59.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn27.DefaultCellStyle = dataGridViewCellStyle59;
            this.dataGridViewTextBoxColumn27.HeaderText = "Primary Salesrep Name";
            this.dataGridViewTextBoxColumn27.Name = "dataGridViewTextBoxColumn27";
            this.dataGridViewTextBoxColumn27.Width = 250;
            // 
            // dataGridViewTextBoxColumn28
            // 
            dataGridViewCellStyle60.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn28.DefaultCellStyle = dataGridViewCellStyle60;
            this.dataGridViewTextBoxColumn28.HeaderText = "Taker";
            this.dataGridViewTextBoxColumn28.Name = "dataGridViewTextBoxColumn28";
            this.dataGridViewTextBoxColumn28.Width = 200;
            // 
            // dgvSectionHeaderE1
            // 
            this.dgvSectionHeaderE1.AllowUserToAddRows = false;
            this.dgvSectionHeaderE1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle63.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle63.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle63.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle63.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle63.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeaderE1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle63;
            this.dgvSectionHeaderE1.BackgroundColor = System.Drawing.Color.White;
            this.dgvSectionHeaderE1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvSectionHeaderE1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle64.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle64.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle64.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle64.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle64.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle64.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle64.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSectionHeaderE1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle64;
            this.dgvSectionHeaderE1.ColumnHeadersHeight = 22;
            this.dgvSectionHeaderE1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn14});
            dataGridViewCellStyle66.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle66.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle66.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle66.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle66.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle66.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle66.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSectionHeaderE1.DefaultCellStyle = dataGridViewCellStyle66;
            this.dgvSectionHeaderE1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSectionHeaderE1.GridColor = System.Drawing.Color.Black;
            this.dgvSectionHeaderE1.Location = new System.Drawing.Point(580, 68);
            this.dgvSectionHeaderE1.MultiSelect = false;
            this.dgvSectionHeaderE1.Name = "dgvSectionHeaderE1";
            this.dgvSectionHeaderE1.RowHeadersVisible = false;
            dataGridViewCellStyle67.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle67.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle67.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle67.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeaderE1.RowsDefaultCellStyle = dataGridViewCellStyle67;
            this.dgvSectionHeaderE1.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvSectionHeaderE1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSectionHeaderE1.Size = new System.Drawing.Size(200, 22);
            this.dgvSectionHeaderE1.TabIndex = 60;
            // 
            // dataGridViewTextBoxColumn14
            // 
            dataGridViewCellStyle65.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn14.DefaultCellStyle = dataGridViewCellStyle65;
            this.dataGridViewTextBoxColumn14.HeaderText = "ORDER NUMBER";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.Width = 200;
            // 
            // dgvSectionHeaderA1
            // 
            this.dgvSectionHeaderA1.AllowUserToAddRows = false;
            this.dgvSectionHeaderA1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle68.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle68.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle68.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle68.SelectionBackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle68.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeaderA1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle68;
            this.dgvSectionHeaderA1.BackgroundColor = System.Drawing.Color.White;
            this.dgvSectionHeaderA1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvSectionHeaderA1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvSectionHeaderA1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle69.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle69.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle69.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle69.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle69.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle69.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle69.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSectionHeaderA1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle69;
            this.dgvSectionHeaderA1.ColumnHeadersHeight = 22;
            this.dgvSectionHeaderA1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1});
            dataGridViewCellStyle70.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle70.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle70.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle70.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle70.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle70.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle70.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSectionHeaderA1.DefaultCellStyle = dataGridViewCellStyle70;
            this.dgvSectionHeaderA1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSectionHeaderA1.GridColor = System.Drawing.Color.Black;
            this.dgvSectionHeaderA1.Location = new System.Drawing.Point(35, 38);
            this.dgvSectionHeaderA1.MultiSelect = false;
            this.dgvSectionHeaderA1.Name = "dgvSectionHeaderA1";
            this.dgvSectionHeaderA1.RowHeadersVisible = false;
            dataGridViewCellStyle71.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle71.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeaderA1.RowsDefaultCellStyle = dataGridViewCellStyle71;
            this.dgvSectionHeaderA1.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvSectionHeaderA1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSectionHeaderA1.Size = new System.Drawing.Size(200, 22);
            this.dgvSectionHeaderA1.TabIndex = 59;
            // 
            // dgvSectionHeaderB1
            // 
            this.dgvSectionHeaderB1.AllowUserToAddRows = false;
            this.dgvSectionHeaderB1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle72.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle72.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle72.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle72.SelectionBackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle72.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeaderB1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle72;
            this.dgvSectionHeaderB1.BackgroundColor = System.Drawing.Color.White;
            this.dgvSectionHeaderB1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvSectionHeaderB1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvSectionHeaderB1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle73.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle73.BackColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle73.Font = new System.Drawing.Font("Times New Roman", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle73.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle73.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle73.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle73.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSectionHeaderB1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle73;
            this.dgvSectionHeaderB1.ColumnHeadersHeight = 22;
            this.dgvSectionHeaderB1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn22});
            dataGridViewCellStyle74.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle74.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle74.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle74.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle74.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle74.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle74.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSectionHeaderB1.DefaultCellStyle = dataGridViewCellStyle74;
            this.dgvSectionHeaderB1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSectionHeaderB1.GridColor = System.Drawing.Color.Black;
            this.dgvSectionHeaderB1.Location = new System.Drawing.Point(258, 38);
            this.dgvSectionHeaderB1.MultiSelect = false;
            this.dgvSectionHeaderB1.Name = "dgvSectionHeaderB1";
            this.dgvSectionHeaderB1.RowHeadersVisible = false;
            dataGridViewCellStyle75.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle75.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeaderB1.RowsDefaultCellStyle = dataGridViewCellStyle75;
            this.dgvSectionHeaderB1.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvSectionHeaderB1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSectionHeaderB1.Size = new System.Drawing.Size(300, 22);
            this.dgvSectionHeaderB1.TabIndex = 58;
            // 
            // dgvSectionHeader5
            // 
            this.dgvSectionHeader5.AllowUserToAddRows = false;
            this.dgvSectionHeader5.AllowUserToDeleteRows = false;
            dataGridViewCellStyle76.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle76.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle76.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle76.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle76.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeader5.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle76;
            this.dgvSectionHeader5.BackgroundColor = System.Drawing.Color.Gray;
            this.dgvSectionHeader5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvSectionHeader5.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvSectionHeader5.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle77.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle77.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle77.Font = new System.Drawing.Font("Times New Roman", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle77.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle77.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle77.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle77.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSectionHeader5.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle77;
            this.dgvSectionHeader5.ColumnHeadersHeight = 25;
            this.dgvSectionHeader5.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.BillableDays,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12});
            dataGridViewCellStyle81.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle81.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle81.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle81.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle81.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle81.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle81.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSectionHeader5.DefaultCellStyle = dataGridViewCellStyle81;
            this.dgvSectionHeader5.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSectionHeader5.GridColor = System.Drawing.Color.Black;
            this.dgvSectionHeader5.Location = new System.Drawing.Point(35, 288);
            this.dgvSectionHeader5.MultiSelect = false;
            this.dgvSectionHeader5.Name = "dgvSectionHeader5";
            this.dgvSectionHeader5.RowHeadersVisible = false;
            dataGridViewCellStyle82.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle82.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle82.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle82.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeader5.RowsDefaultCellStyle = dataGridViewCellStyle82;
            this.dgvSectionHeader5.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvSectionHeader5.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSectionHeader5.Size = new System.Drawing.Size(745, 25);
            this.dgvSectionHeader5.TabIndex = 53;
            // 
            // dataGridViewTextBoxColumn6
            // 
            dataGridViewCellStyle78.NullValue = null;
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle78;
            this.dataGridViewTextBoxColumn6.HeaderText = "Ordered";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Width = 60;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "Shipped";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.Width = 60;
            // 
            // dataGridViewTextBoxColumn5
            // 
            dataGridViewCellStyle79.NullValue = null;
            this.dataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle79;
            this.dataGridViewTextBoxColumn5.HeaderText = "Remaining";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Width = 90;
            // 
            // dataGridViewTextBoxColumn8
            // 
            dataGridViewCellStyle80.NullValue = null;
            this.dataGridViewTextBoxColumn8.DefaultCellStyle = dataGridViewCellStyle80;
            this.dataGridViewTextBoxColumn8.HeaderText = "Unit Size";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.Width = 60;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.HeaderText = "";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.Width = 30;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.HeaderText = "Item Description";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.Width = 235;
            // 
            // BillableDays
            // 
            this.BillableDays.HeaderText = "Unit Size";
            this.BillableDays.Name = "BillableDays";
            this.BillableDays.Width = 60;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.HeaderText = "";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.Width = 60;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.HeaderText = "";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.Width = 90;
            // 
            // dgvSectionSubHeader5
            // 
            this.dgvSectionSubHeader5.AllowUserToAddRows = false;
            this.dgvSectionSubHeader5.AllowUserToDeleteRows = false;
            dataGridViewCellStyle83.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle83.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle83.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle83.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle83.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionSubHeader5.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle83;
            this.dgvSectionSubHeader5.BackgroundColor = System.Drawing.Color.Gray;
            this.dgvSectionSubHeader5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvSectionSubHeader5.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvSectionSubHeader5.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle84.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle84.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle84.Font = new System.Drawing.Font("Times New Roman", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle84.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle84.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle84.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle84.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSectionSubHeader5.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle84;
            this.dgvSectionSubHeader5.ColumnHeadersHeight = 33;
            this.dgvSectionSubHeader5.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn15,
            this.dataGridViewTextBoxColumn16,
            this.dataGridViewTextBoxColumn17,
            this.PO,
            this.PRICEEACH,
            this.AMOUNT});
            dataGridViewCellStyle88.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle88.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle88.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle88.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle88.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle88.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle88.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSectionSubHeader5.DefaultCellStyle = dataGridViewCellStyle88;
            this.dgvSectionSubHeader5.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSectionSubHeader5.GridColor = System.Drawing.Color.Black;
            this.dgvSectionSubHeader5.Location = new System.Drawing.Point(35, 250);
            this.dgvSectionSubHeader5.MultiSelect = false;
            this.dgvSectionSubHeader5.Name = "dgvSectionSubHeader5";
            this.dgvSectionSubHeader5.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvSectionSubHeader5.RowHeadersVisible = false;
            dataGridViewCellStyle89.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle89.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle89.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle89.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionSubHeader5.RowsDefaultCellStyle = dataGridViewCellStyle89;
            this.dgvSectionSubHeader5.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvSectionSubHeader5.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSectionSubHeader5.Size = new System.Drawing.Size(745, 33);
            this.dgvSectionSubHeader5.TabIndex = 52;
            // 
            // dataGridViewTextBoxColumn4
            // 
            dataGridViewCellStyle85.NullValue = null;
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle85;
            this.dataGridViewTextBoxColumn4.HeaderText = "Quantities";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Width = 210;
            // 
            // dataGridViewTextBoxColumn15
            // 
            dataGridViewCellStyle86.NullValue = null;
            this.dataGridViewTextBoxColumn15.DefaultCellStyle = dataGridViewCellStyle86;
            this.dataGridViewTextBoxColumn15.HeaderText = "UOM";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.Width = 60;
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.HeaderText = "Disp.";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.Width = 30;
            // 
            // dataGridViewTextBoxColumn17
            // 
            dataGridViewCellStyle87.NullValue = null;
            this.dataGridViewTextBoxColumn17.DefaultCellStyle = dataGridViewCellStyle87;
            this.dataGridViewTextBoxColumn17.HeaderText = "Item ID";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.Width = 235;
            // 
            // PO
            // 
            this.PO.HeaderText = "Price UOM";
            this.PO.Name = "PO";
            this.PO.Width = 60;
            // 
            // PRICEEACH
            // 
            this.PRICEEACH.HeaderText = "Unit Price";
            this.PRICEEACH.Name = "PRICEEACH";
            this.PRICEEACH.Width = 60;
            // 
            // AMOUNT
            // 
            this.AMOUNT.HeaderText = "Extended Price";
            this.AMOUNT.Name = "AMOUNT";
            this.AMOUNT.Width = 90;
            // 
            // dgvSectionHeader3
            // 
            this.dgvSectionHeader3.AllowUserToAddRows = false;
            this.dgvSectionHeader3.AllowUserToDeleteRows = false;
            dataGridViewCellStyle90.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle90.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle90.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle90.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle90.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeader3.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle90;
            this.dgvSectionHeader3.BackgroundColor = System.Drawing.Color.Gray;
            this.dgvSectionHeader3.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvSectionHeader3.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle91.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle91.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle91.Font = new System.Drawing.Font("Times New Roman", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle91.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle91.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle91.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle91.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSectionHeader3.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle91;
            this.dgvSectionHeader3.ColumnHeadersHeight = 30;
            this.dgvSectionHeader3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SALESPERSON,
            this.DATE,
            this.TAKER,
            this.CUSTOMERPONUMBER,
            this.TERMS});
            dataGridViewCellStyle97.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle97.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle97.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle97.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle97.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle97.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle97.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSectionHeader3.DefaultCellStyle = dataGridViewCellStyle97;
            this.dgvSectionHeader3.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSectionHeader3.GridColor = System.Drawing.Color.Black;
            this.dgvSectionHeader3.Location = new System.Drawing.Point(35, 178);
            this.dgvSectionHeader3.MultiSelect = false;
            this.dgvSectionHeader3.Name = "dgvSectionHeader3";
            this.dgvSectionHeader3.RowHeadersVisible = false;
            dataGridViewCellStyle98.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle98.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle98.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle98.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeader3.RowsDefaultCellStyle = dataGridViewCellStyle98;
            this.dgvSectionHeader3.RowTemplate.Height = 30;
            this.dgvSectionHeader3.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvSectionHeader3.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSectionHeader3.Size = new System.Drawing.Size(745, 25);
            this.dgvSectionHeader3.TabIndex = 51;
            // 
            // SALESPERSON
            // 
            dataGridViewCellStyle92.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle92.NullValue = null;
            this.SALESPERSON.DefaultCellStyle = dataGridViewCellStyle92;
            this.SALESPERSON.HeaderText = "Customer Number";
            this.SALESPERSON.Name = "SALESPERSON";
            this.SALESPERSON.Width = 150;
            // 
            // DATE
            // 
            dataGridViewCellStyle93.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.DATE.DefaultCellStyle = dataGridViewCellStyle93;
            this.DATE.HeaderText = "Customer License";
            this.DATE.Name = "DATE";
            this.DATE.Width = 145;
            // 
            // TAKER
            // 
            dataGridViewCellStyle94.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.TAKER.DefaultCellStyle = dataGridViewCellStyle94;
            this.TAKER.HeaderText = "Purchase Order Number";
            this.TAKER.Name = "TAKER";
            this.TAKER.Width = 200;
            // 
            // CUSTOMERPONUMBER
            // 
            dataGridViewCellStyle95.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.CUSTOMERPONUMBER.DefaultCellStyle = dataGridViewCellStyle95;
            this.CUSTOMERPONUMBER.HeaderText = "Term Description";
            this.CUSTOMERPONUMBER.Name = "CUSTOMERPONUMBER";
            this.CUSTOMERPONUMBER.Width = 150;
            // 
            // TERMS
            // 
            dataGridViewCellStyle96.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.TERMS.DefaultCellStyle = dataGridViewCellStyle96;
            this.TERMS.HeaderText = "Net Due Date";
            this.TERMS.Name = "TERMS";
            // 
            // dgvSectionLine1
            // 
            this.dgvSectionLine1.AllowUserToAddRows = false;
            this.dgvSectionLine1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle99.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle99.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle99.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle99.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle99.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionLine1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle99;
            this.dgvSectionLine1.BackgroundColor = System.Drawing.Color.Gray;
            this.dgvSectionLine1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvSectionLine1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle100.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle100.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle100.Font = new System.Drawing.Font("Times New Roman", 2.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle100.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle100.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle100.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle100.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSectionLine1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle100;
            this.dgvSectionLine1.ColumnHeadersHeight = 5;
            this.dgvSectionLine1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.InvoiceNote});
            dataGridViewCellStyle102.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle102.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle102.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle102.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle102.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle102.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle102.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSectionLine1.DefaultCellStyle = dataGridViewCellStyle102;
            this.dgvSectionLine1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSectionLine1.GridColor = System.Drawing.Color.Black;
            this.dgvSectionLine1.Location = new System.Drawing.Point(35, 322);
            this.dgvSectionLine1.MultiSelect = false;
            this.dgvSectionLine1.Name = "dgvSectionLine1";
            this.dgvSectionLine1.RowHeadersVisible = false;
            dataGridViewCellStyle103.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle103.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle103.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle103.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionLine1.RowsDefaultCellStyle = dataGridViewCellStyle103;
            this.dgvSectionLine1.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvSectionLine1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSectionLine1.Size = new System.Drawing.Size(745, 22);
            this.dgvSectionLine1.TabIndex = 50;
            // 
            // InvoiceNote
            // 
            dataGridViewCellStyle101.NullValue = null;
            this.InvoiceNote.DefaultCellStyle = dataGridViewCellStyle101;
            this.InvoiceNote.HeaderText = "SectionLine1";
            this.InvoiceNote.Name = "InvoiceNote";
            this.InvoiceNote.Width = 745;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle104.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle104.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle104.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle104.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle104.SelectionForeColor = System.Drawing.Color.Black;
            this.dataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle104;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.Gray;
            this.dataGridView1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dataGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle105.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle105.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle105.Font = new System.Drawing.Font("Microsoft Sans Serif", 2.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle105.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle105.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle105.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle105.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle105;
            this.dataGridView1.ColumnHeadersHeight = 10;
            this.dataGridView1.ColumnHeadersVisible = false;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn18});
            dataGridViewCellStyle106.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle106.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle106.Font = new System.Drawing.Font("Microsoft Sans Serif", 2.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle106.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle106.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle106.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle106.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle106;
            this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridView1.GridColor = System.Drawing.Color.Black;
            this.dataGridView1.Location = new System.Drawing.Point(35, 137);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            dataGridViewCellStyle107.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle107.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle107.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle107.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle107.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle107.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle107.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle107;
            this.dataGridView1.RowHeadersVisible = false;
            dataGridViewCellStyle108.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle108.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle108.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle108.SelectionForeColor = System.Drawing.Color.Black;
            this.dataGridView1.RowsDefaultCellStyle = dataGridViewCellStyle108;
            this.dataGridView1.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(745, 10);
            this.dataGridView1.TabIndex = 49;
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.HeaderText = "";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.Width = 745;
            // 
            // dgvSectionHeaderD1
            // 
            this.dgvSectionHeaderD1.AllowUserToAddRows = false;
            this.dgvSectionHeaderD1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle109.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle109.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle109.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle109.SelectionBackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle109.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeaderD1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle109;
            this.dgvSectionHeaderD1.BackgroundColor = System.Drawing.Color.White;
            this.dgvSectionHeaderD1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvSectionHeaderD1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle110.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle110.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle110.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle110.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle110.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle110.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle110.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSectionHeaderD1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle110;
            this.dgvSectionHeaderD1.ColumnHeadersHeight = 22;
            this.dgvSectionHeaderD1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn19,
            this.dataGridViewTextBoxColumn21});
            dataGridViewCellStyle113.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle113.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle113.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle113.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle113.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle113.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle113.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSectionHeaderD1.DefaultCellStyle = dataGridViewCellStyle113;
            this.dgvSectionHeaderD1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSectionHeaderD1.GridColor = System.Drawing.Color.Black;
            this.dgvSectionHeaderD1.Location = new System.Drawing.Point(580, 43);
            this.dgvSectionHeaderD1.MultiSelect = false;
            this.dgvSectionHeaderD1.Name = "dgvSectionHeaderD1";
            this.dgvSectionHeaderD1.RowHeadersVisible = false;
            dataGridViewCellStyle114.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle114.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle114.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle114.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeaderD1.RowsDefaultCellStyle = dataGridViewCellStyle114;
            this.dgvSectionHeaderD1.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvSectionHeaderD1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSectionHeaderD1.Size = new System.Drawing.Size(200, 22);
            this.dgvSectionHeaderD1.TabIndex = 47;
            // 
            // dataGridViewTextBoxColumn19
            // 
            this.dataGridViewTextBoxColumn19.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle111.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn19.DefaultCellStyle = dataGridViewCellStyle111;
            this.dataGridViewTextBoxColumn19.HeaderText = "Invoice Date";
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            // 
            // dataGridViewTextBoxColumn21
            // 
            dataGridViewCellStyle112.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn21.DefaultCellStyle = dataGridViewCellStyle112;
            this.dataGridViewTextBoxColumn21.HeaderText = "Page";
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            // 
            // dgvSectionHeader2
            // 
            this.dgvSectionHeader2.AllowUserToAddRows = false;
            this.dgvSectionHeader2.AllowUserToDeleteRows = false;
            dataGridViewCellStyle115.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle115.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle115.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle115.SelectionBackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle115.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeader2.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle115;
            this.dgvSectionHeader2.BackgroundColor = System.Drawing.Color.White;
            this.dgvSectionHeader2.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvSectionHeader2.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle116.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle116.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle116.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle116.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle116.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle116.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle116.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSectionHeader2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle116;
            this.dgvSectionHeader2.ColumnHeadersHeight = 22;
            this.dgvSectionHeader2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.space1,
            this.BillToAddress,
            this.ShipTo,
            this.ShipToAddress});
            dataGridViewCellStyle117.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle117.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle117.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle117.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle117.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle117.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle117.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSectionHeader2.DefaultCellStyle = dataGridViewCellStyle117;
            this.dgvSectionHeader2.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSectionHeader2.GridColor = System.Drawing.Color.Black;
            this.dgvSectionHeader2.Location = new System.Drawing.Point(35, 96);
            this.dgvSectionHeader2.MultiSelect = false;
            this.dgvSectionHeader2.Name = "dgvSectionHeader2";
            this.dgvSectionHeader2.RowHeadersVisible = false;
            dataGridViewCellStyle118.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle118.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeader2.RowsDefaultCellStyle = dataGridViewCellStyle118;
            this.dgvSectionHeader2.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvSectionHeader2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSectionHeader2.Size = new System.Drawing.Size(745, 22);
            this.dgvSectionHeader2.TabIndex = 46;
            // 
            // space1
            // 
            this.space1.HeaderText = "";
            this.space1.Name = "space1";
            this.space1.Width = 65;
            // 
            // BillToAddress
            // 
            this.BillToAddress.HeaderText = "Bill To:";
            this.BillToAddress.Name = "BillToAddress";
            this.BillToAddress.Width = 300;
            // 
            // ShipTo
            // 
            this.ShipTo.HeaderText = "";
            this.ShipTo.Name = "ShipTo";
            this.ShipTo.Width = 80;
            // 
            // ShipToAddress
            // 
            this.ShipToAddress.HeaderText = "Ship To:";
            this.ShipToAddress.Name = "ShipToAddress";
            this.ShipToAddress.Width = 300;
            // 
            // dgvSectionHeaderC1
            // 
            this.dgvSectionHeaderC1.AllowUserToAddRows = false;
            this.dgvSectionHeaderC1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle119.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle119.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle119.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle119.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle119.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeaderC1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle119;
            this.dgvSectionHeaderC1.BackgroundColor = System.Drawing.Color.White;
            this.dgvSectionHeaderC1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvSectionHeaderC1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle120.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle120.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle120.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle120.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle120.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle120.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle120.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSectionHeaderC1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle120;
            this.dgvSectionHeaderC1.ColumnHeadersHeight = 22;
            this.dgvSectionHeaderC1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn20});
            dataGridViewCellStyle122.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle122.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle122.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle122.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle122.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle122.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle122.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSectionHeaderC1.DefaultCellStyle = dataGridViewCellStyle122;
            this.dgvSectionHeaderC1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSectionHeaderC1.GridColor = System.Drawing.Color.Black;
            this.dgvSectionHeaderC1.Location = new System.Drawing.Point(580, 15);
            this.dgvSectionHeaderC1.MultiSelect = false;
            this.dgvSectionHeaderC1.Name = "dgvSectionHeaderC1";
            this.dgvSectionHeaderC1.RowHeadersVisible = false;
            dataGridViewCellStyle123.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle123.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle123.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle123.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeaderC1.RowsDefaultCellStyle = dataGridViewCellStyle123;
            this.dgvSectionHeaderC1.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvSectionHeaderC1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSectionHeaderC1.Size = new System.Drawing.Size(200, 22);
            this.dgvSectionHeaderC1.TabIndex = 45;
            // 
            // dataGridViewTextBoxColumn20
            // 
            dataGridViewCellStyle121.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn20.DefaultCellStyle = dataGridViewCellStyle121;
            this.dataGridViewTextBoxColumn20.HeaderText = "INVOICE";
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            this.dataGridViewTextBoxColumn20.Width = 200;
            // 
            // tabSections
            // 
            this.tabSections.Location = new System.Drawing.Point(4, 22);
            this.tabSections.Name = "tabSections";
            this.tabSections.Size = new System.Drawing.Size(824, 543);
            this.tabSections.TabIndex = 3;
            this.tabSections.Text = "Sections";
            this.tabSections.UseVisualStyleBackColor = true;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 200;
            // 
            // dataGridViewTextBoxColumn22
            // 
            this.dataGridViewTextBoxColumn22.HeaderText = "Remit To:";
            this.dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
            this.dataGridViewTextBoxColumn22.Width = 300;
            // 
            // frmInvoicesPrint
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(856, 600);
            this.Controls.Add(this.tabControl1);
            this.Name = "frmInvoicesPrint";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.tabControl1.ResumeLayout(false);
            this.tabHeader.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEndOTInvoices)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEndGroupMsg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmptyMessage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPageHeader)).EndInit();
            this.tabHeaderSections.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionLine6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSection4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSection3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionLine5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeader4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeaderE1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeaderA1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeaderB1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeader5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionSubHeader5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeader3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionLine1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeaderD1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeader2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeaderC1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabHeader;
        private System.Windows.Forms.TabPage tabHeaderSections;
        private System.Windows.Forms.TabPage tabSections;
        private System.Windows.Forms.DataGridView dgvGroup1;
        private System.Windows.Forms.DataGridView dgvEndOTInvoices;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridView dgvEndGroupMsg;
        private System.Windows.Forms.Label lblFooter;
        private System.Windows.Forms.DataGridView dgvEmptyMessage;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn32;
        private System.Windows.Forms.DataGridView dgvPageHeader;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridView dgvSectionHeaderD1;
        private System.Windows.Forms.DataGridView dgvSectionHeader2;
        private System.Windows.Forms.DataGridView dgvSectionHeaderC1;
        private System.Windows.Forms.DataGridView dgvSectionLine1;
        private System.Windows.Forms.DataGridView dgvSectionHeader3;
        private System.Windows.Forms.DataGridView dgvSectionSubHeader5;
        private System.Windows.Forms.DataGridView dgvSectionHeader5;
        private System.Windows.Forms.DataGridView dgvSectionHeaderB1;
        private System.Windows.Forms.DataGridView dgvSectionHeaderA1;
        private System.Windows.Forms.DataGridView dgvSectionHeaderE1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn31;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridView dgvSectionHeader4;
        private System.Windows.Forms.DataGridView dgvSectionLine5;
        private System.Windows.Forms.DataGridView dgvSection3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn39;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn40;
        private System.Windows.Forms.DataGridView dgvSection4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn25;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn41;
        private System.Windows.Forms.DataGridViewTextBoxColumn space1;
        private System.Windows.Forms.DataGridViewTextBoxColumn BillToAddress;
        private System.Windows.Forms.DataGridViewTextBoxColumn ShipTo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ShipToAddress;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private System.Windows.Forms.DataGridViewTextBoxColumn SALESPERSON;
        private System.Windows.Forms.DataGridViewTextBoxColumn DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn TAKER;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUSTOMERPONUMBER;
        private System.Windows.Forms.DataGridViewTextBoxColumn TERMS;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn23;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn24;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn27;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn28;
        private System.Windows.Forms.DataGridView dgvSectionLine6;
        private System.Windows.Forms.DataGridViewTextBoxColumn InvoiceNote;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn BillableDays;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn PO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PRICEEACH;
        private System.Windows.Forms.DataGridViewTextBoxColumn AMOUNT;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn29;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn30;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn33;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn34;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn35;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn36;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn37;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn38;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn26;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn42;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
    }
}
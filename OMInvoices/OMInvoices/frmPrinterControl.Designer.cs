﻿namespace OMInvoices
{
    partial class frmPrinterControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.printPreviewControl1 = new System.Windows.Forms.PrintPreviewControl();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.bntPrint = new System.Windows.Forms.Button();
            this.bntPreviousPage = new System.Windows.Forms.Button();
            this.bntNextPage = new System.Windows.Forms.Button();
            this.bntZoomOut = new System.Windows.Forms.Button();
            this.bntZoomIn = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.bntFirstPage = new System.Windows.Forms.Button();
            this.bntLastPage = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // printPreviewControl1
            // 
            this.printPreviewControl1.AutoZoom = false;
            this.printPreviewControl1.Location = new System.Drawing.Point(12, 45);
            this.printPreviewControl1.Name = "printPreviewControl1";
            this.printPreviewControl1.Size = new System.Drawing.Size(126, 86);
            this.printPreviewControl1.TabIndex = 19;
            this.printPreviewControl1.Zoom = 0.23941176470588235;
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.White;
            this.button5.Location = new System.Drawing.Point(497, 3);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(79, 34);
            this.button5.TabIndex = 24;
            this.button5.Text = "Add Side    Page";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.White;
            this.button6.Location = new System.Drawing.Point(582, 3);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(79, 34);
            this.button6.TabIndex = 25;
            this.button6.Text = "Remove Side Page";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.White;
            this.button7.Location = new System.Drawing.Point(667, 3);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(93, 34);
            this.button7.TabIndex = 26;
            this.button7.Text = "Add Vertical Page";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.White;
            this.button8.Location = new System.Drawing.Point(766, 3);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(93, 34);
            this.button8.TabIndex = 27;
            this.button8.Text = "Remove Vertical Page";
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // bntPrint
            // 
            this.bntPrint.BackColor = System.Drawing.Color.LightSkyBlue;
            this.bntPrint.Location = new System.Drawing.Point(12, 3);
            this.bntPrint.Name = "bntPrint";
            this.bntPrint.Size = new System.Drawing.Size(50, 34);
            this.bntPrint.TabIndex = 38;
            this.bntPrint.Text = "Print";
            this.bntPrint.UseVisualStyleBackColor = false;
            this.bntPrint.Click += new System.EventHandler(this.bntPrint_Click);
            // 
            // bntPreviousPage
            // 
            this.bntPreviousPage.BackColor = System.Drawing.Color.Silver;
            this.bntPreviousPage.Location = new System.Drawing.Point(124, 3);
            this.bntPreviousPage.Name = "bntPreviousPage";
            this.bntPreviousPage.Size = new System.Drawing.Size(59, 34);
            this.bntPreviousPage.TabIndex = 37;
            this.bntPreviousPage.Text = "Previous Page";
            this.bntPreviousPage.UseVisualStyleBackColor = false;
            this.bntPreviousPage.Click += new System.EventHandler(this.bntPreviousPage_Click);
            // 
            // bntNextPage
            // 
            this.bntNextPage.BackColor = System.Drawing.Color.Silver;
            this.bntNextPage.Location = new System.Drawing.Point(189, 3);
            this.bntNextPage.Name = "bntNextPage";
            this.bntNextPage.Size = new System.Drawing.Size(59, 34);
            this.bntNextPage.TabIndex = 36;
            this.bntNextPage.Text = "Next  Page";
            this.bntNextPage.UseVisualStyleBackColor = false;
            this.bntNextPage.Click += new System.EventHandler(this.bntNextPage_Click);
            // 
            // bntZoomOut
            // 
            this.bntZoomOut.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bntZoomOut.Location = new System.Drawing.Point(441, 3);
            this.bntZoomOut.Name = "bntZoomOut";
            this.bntZoomOut.Size = new System.Drawing.Size(50, 34);
            this.bntZoomOut.TabIndex = 35;
            this.bntZoomOut.Text = "Zoom Out";
            this.bntZoomOut.UseVisualStyleBackColor = false;
            this.bntZoomOut.Click += new System.EventHandler(this.bntZoomOut_Click);
            // 
            // bntZoomIn
            // 
            this.bntZoomIn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bntZoomIn.Location = new System.Drawing.Point(384, 3);
            this.bntZoomIn.Name = "bntZoomIn";
            this.bntZoomIn.Size = new System.Drawing.Size(51, 34);
            this.bntZoomIn.TabIndex = 34;
            this.bntZoomIn.Text = "Zoom In";
            this.bntZoomIn.UseVisualStyleBackColor = false;
            this.bntZoomIn.Click += new System.EventHandler(this.bntZoomIn_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.LightCoral;
            this.button1.Location = new System.Drawing.Point(68, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(50, 34);
            this.button1.TabIndex = 39;
            this.button1.Text = "Reset";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // bntFirstPage
            // 
            this.bntFirstPage.BackColor = System.Drawing.Color.Silver;
            this.bntFirstPage.Location = new System.Drawing.Point(254, 3);
            this.bntFirstPage.Name = "bntFirstPage";
            this.bntFirstPage.Size = new System.Drawing.Size(59, 34);
            this.bntFirstPage.TabIndex = 40;
            this.bntFirstPage.Text = "First Page";
            this.bntFirstPage.UseVisualStyleBackColor = false;
            this.bntFirstPage.Click += new System.EventHandler(this.bntFirstPage_Click);
            // 
            // bntLastPage
            // 
            this.bntLastPage.BackColor = System.Drawing.Color.Silver;
            this.bntLastPage.Location = new System.Drawing.Point(319, 3);
            this.bntLastPage.Name = "bntLastPage";
            this.bntLastPage.Size = new System.Drawing.Size(59, 34);
            this.bntLastPage.TabIndex = 41;
            this.bntLastPage.Text = "Last Page";
            this.bntLastPage.UseVisualStyleBackColor = false;
            this.bntLastPage.Click += new System.EventHandler(this.bntLastPage_Click_1);
            // 
            // frmPrinterControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(880, 149);
            this.Controls.Add(this.bntLastPage);
            this.Controls.Add(this.bntFirstPage);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.bntPrint);
            this.Controls.Add(this.bntPreviousPage);
            this.Controls.Add(this.bntNextPage);
            this.Controls.Add(this.bntZoomOut);
            this.Controls.Add(this.bntZoomIn);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.printPreviewControl1);
            this.Name = "frmPrinterControl";
            this.ShowIcon = false;
            this.Text = "Copyright © 2011 Jun - Leonardo Furtado. All Rights Reserved.";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Resize += new System.EventHandler(this.frmPrintControl_Resize);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PrintPreviewControl printPreviewControl1;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button bntPrint;
        private System.Windows.Forms.Button bntPreviousPage;
        private System.Windows.Forms.Button bntNextPage;
        private System.Windows.Forms.Button bntZoomOut;
        private System.Windows.Forms.Button bntZoomIn;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button bntFirstPage;
        private System.Windows.Forms.Button bntLastPage;
    }
}
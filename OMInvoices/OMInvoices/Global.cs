﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.IO;
using System.Data.SqlClient;

namespace OMInvoices
{
    sealed class MainClass
    {
        public static void Main()
        {
            GLB.Customer_no = "136726"; //"115287"; //"103946";//"126775"; //"138809";  // "136726"; //"105796";
            GLB.Invoice_no = "3593776"; //"6035963"; //"5651750"; //"5950749";//"5877239"; //"5883579"; //"6000252"; //"5391486";

            GLB.InvoiceStartDate =  "01/31/2010"; // "05/01/2015";  // "10/01/2013"; // "01/31/2010";
            GLB.InvoiceEndDate = "01/31/2010";  // "05/27/2015"; // "10/31/2013"; // "01/31/2010";

            string sConn;
            //DbAccess.Set_MSDSQL();
            DbAccess.Set_MSDSQL01();
            //DbAccess.Set_MSDSQL03();
            sConn = DbAccess.ConnectionString;

            GLB.GetData(sConn);
            Application.Run(new frmOMInvoices());
        }
    }

    public class OMInvoices
    {
        public static string Create_OMInvoices(string Customer_no, string Invoice_no, string sConn)
        {
            string FilePath = string.Empty;
            GLB.Customer_no = Customer_no;
            GLB.Invoice_no = Invoice_no;
            GLB.GetData(sConn); 
            frmInvoicesPrint frmPCSReport = new frmInvoicesPrint();
            if (GLB.OMInvoices.Rows.Count > 0)
            {
                FilePath = "OMInvoices/MSD-Invoice-" + Invoice_no + ".xps";
                FilePath = string.Format("{0}?t={1}", FilePath, DateTime.Now.Ticks);
                frmPCSReport.PCS_Salva();
            }
            return FilePath;
        }
    }

    public class  GLB
    {

        public static DataTable OMInvoices;

        public static string InvoiceStartDate = string.Empty;
        public static string InvoiceEndDate = string.Empty;
        public static string Customer_no = string.Empty;
        public static string Invoice_no = string.Empty;

        public static void GetData(string sConn)
        {
            //SqlDataReader oDR = default(SqlDataReader);

            SqlCommand oCmd = new SqlCommand();

            string sSqlConnection;

            string sSqlSelectCmd = string.Empty;

            //DbAccess.Set_MSDSQL();
            //DbAccess.Set_MSDSQL01();
            //DbAccess.Set_MSDSQL03();
            //sSqlConnection = DbAccess.ConnectionString;

            sSqlConnection = sConn;
            //###################################################################

            DataSet oDS = new DataSet();

            sSqlSelectCmd = GetSelect();

            try
            {
                SqlDataAdapter oDA = default(SqlDataAdapter);

                oDA = new SqlDataAdapter(sSqlSelectCmd, sSqlConnection);

                oDA.Fill(oDS);

            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
                //  if (oConn.State == ConnectionState.Open) oConn.Close();
            }

            OMInvoices = oDS.Tables[0];
            return;
        }

        private static string GetSelect()
        {

            string sSqlSelectCmd = string.Empty;

            //------------------------------------------------------------------------------------------------------------------------------------------------------------
            // 
            //------------------------------------------------------------------------------------------------------------------------------------------------------------


            sSqlSelectCmd += " SELECT ih.[invoice_no], ih.[order_no], ih.[order_date], ih.[invoice_date], ih.[customer_id], ih.[bill2_name], ih.[bill2_contact], ih.[bill2_address1], ih.[bill2_address2] ";
            sSqlSelectCmd += " ,ih.[bill2_city], ih.[bill2_state], ih.[bill2_postal_code], ih.[ship2_name], ih.[ship2_contact], ih.[ship2_address1], ih.[ship2_address2], ih.[ship2_city], ih.[ship2_state] ";
            sSqlSelectCmd += " ,ih.[ship2_postal_code], ih.[terms_desc], ih.[po_no], ih.[ship_to_id], ih.[ship_date], ih.[total_amount], ih.[net_due_date], ih.[terms_id] ";
            sSqlSelectCmd += " ,ih.[other_charge_amount], ih.[tax_amount], ih.[shipping_route_uid], ih.[tax_amount_paid], ih.[amount_paid], ih.[memo_amount] ";
            sSqlSelectCmd += " ,il.[qty_requested], il.[qty_shipped], il.[unit_of_measure], il.[item_id], il.[item_desc], oel.[extended_desc], il.[customer_part_number] ";
            sSqlSelectCmd += " ,il.[unit_price], il.[extended_price], il.[pricing_quantity], il.[pricing_unit], il.[pricing_unit_size], il.[sales_unit_size], IIF(ISNULL(il.[order_no],'')='',il.[other_charge_item],im.[other_charge_item]) as other_charge_item ";
            sSqlSelectCmd += " ,oep.[pick_ticket_no], oep.[instructions], oeh.[taker], oeh.[po_no], oel.[disposition], std.[dea_number], (c.[first_name] + ' ' + c.[last_name]) as salesrep_name ";
            sSqlSelectCmd += " ,oeh.rma_flag, ih.allowed, ih.invoice_adjustment_type ";

            sSqlSelectCmd += " ,(Select top 1 carrier_name FROM medcc_live..clippership_return_10004 WHERE pick_ticket_no=oep.pick_ticket_no and tracking_no<>'' and delete_flag='N') as carrier_name ";
            sSqlSelectCmd += " ,[medcc_custom].[dbo].[fn_GetClippershipTrackingNumbers](oep.pick_ticket_no) as Trackings ";
            sSqlSelectCmd += " ,[medcc_custom].[dbo].[fn_GetPickTicketDetailLot](oep.pick_ticket_no,oed.line_number) as Lots ";

            sSqlSelectCmd += " FROM [medcc_live].[dbo].[invoice_hdr] ih ";
            sSqlSelectCmd += " inner join [medcc_live].[dbo].[invoice_line] il on il.invoice_no=ih.invoice_no ";
            sSqlSelectCmd += " left join [medcc_live].[dbo].[oe_pick_ticket] oep on oep.invoice_no=ih.invoice_no ";
            sSqlSelectCmd += " left join [medcc_live].[dbo].[oe_hdr] oeh on oeh.order_no=ih.order_no ";
            sSqlSelectCmd += " left join [medcc_live].[dbo].[inv_mast] im on im.inv_mast_uid=il.inv_mast_uid ";
            sSqlSelectCmd += " left join [medcc_live].[dbo].[oe_line] oel on oel.order_no=ih.order_no and oel.line_no = il.oe_line_number ";
            sSqlSelectCmd += " left join [medcc_live].[dbo].[oe_pick_ticket_detail] oed on oed.pick_ticket_no=oep.pick_ticket_no and oed.oe_line_no=oel.line_no ";
            sSqlSelectCmd += " left join [medcc_live].[dbo].[ship_to_dea] std on std.ship_to_id=ih.ship_to_id ";
            sSqlSelectCmd += " left join [medcc_live].[dbo].[contacts] c on c.id=ih.[salesrep_id]  ";
            sSqlSelectCmd += "where ih.[invoice_no]='" + GLB.Invoice_no + "'";
            sSqlSelectCmd += "and ih.[customer_id]='" + GLB.Customer_no + "' ";

            sSqlSelectCmd += "order by ih.[invoice_no], il.oe_line_number";


            return sSqlSelectCmd;
        }

        //
        //******************************************************************************
        //
        public static bool IsNumeric(object Expression)
        {
            bool isNum;
            double retNum;
            isNum = Double.TryParse(Convert.ToString(Expression),
                    System.Globalization.NumberStyles.Any, 
                    System.Globalization.NumberFormatInfo.InvariantInfo, 
                    out retNum);
            return isNum;
        }

        public static bool HasAnyNum(string numberString)
        { 
          char [] ca = numberString.ToCharArray();

          foreach (char i in ca)
          {
              if (char.IsNumber(i))
                  return true;
          } 
          return false; 
        }
        public static bool HasAnyNum1(string numberString)
        {
            char[] ca = numberString.ToCharArray();
            for (int i = 0; i < ca.Length; i++)
            {
                if (char.IsNumber(ca[i]))
                    return true;
            }
            return false;
        }

        ~GLB()
        {
        }

    }
    
    public class AddValue
    {
        private string m_Display;
        private string m_Value;
        public AddValue(string Display, string Value)
        {
            m_Display = Display;
            m_Value = Value;
        }
        public string Display
        {
            get { return m_Display; }
        }
        public string Value
        {
            get { return m_Value; }
        }
    }
}

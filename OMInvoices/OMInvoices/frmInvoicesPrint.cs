﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Text;
using System.Windows.Forms;

namespace OMInvoices
{
    public partial class frmInvoicesPrint : Form
    {
        private DataView DVPageHeader;
        
        DataView DVSection1;
        DataView DVSection2;
        DataView DVSection3;
        DataView DVSection4;

        DataView DVSectionLine1;

        private int TotalLines = 0;

        private decimal SubTotal = 0;
        private decimal TaxAmount = 0;
        private decimal AmountPaid = 0;
        private decimal MemoAmount = 0;
        private decimal Allowed = 0;

        private decimal TotalAmount = 0;

        private string MemoAmountType = string.Empty;
        private string Freight = string.Empty;

        private string sDeliveryInstructions = string.Empty;
        private string sMessage = string.Empty;

        private string sCarrier = string.Empty;
        private string sTrackings = string.Empty;


        private Printer DataGridPrinter1 = null;

        public frmInvoicesPrint()
        {
            InitializeComponent();
        }

        //####################################################################################
        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            Graphics g = e.Graphics;
            bool more = DataGridPrinter1.DrawDataGrid(g);
            if (more == true)
            {
                e.HasMorePages = true;
                DataGridPrinter1.PageNumber++;
                DataGridPrinter1.TotalPageNumber++;
            }
        }

        public void PCS_Preview()
        {
            if (GLB.OMInvoices.Rows.Count > 0)
            {
                SetupGridPrinter();

                frmPrinterControl PC = new frmPrinterControl();
                // if (DataGridPrinter1.
                PC.RPT_Preview(DataGridPrinter1);
                PC.ShowDialog();
                PC = null;
            }
        }

        public void PCS_Print()
        {
            if (GLB.OMInvoices.Rows.Count > 0)
            {
                SetupGridPrinter();

                frmPrinterControl PC = new frmPrinterControl();
                PC.RPT_Print(DataGridPrinter1);
                PC = null;
            }
        }

        public void PCS_Salva()
        {
            if (GLB.OMInvoices.Rows.Count > 0)
            {
                string s = System.AppDomain.CurrentDomain.BaseDirectory;
                printDocument1.DefaultPageSettings.PrinterSettings.PrinterName = "Microsoft XPS Document Writer";
                string FileName = "C:\\temp\\OM-Report\\MSD-Invoice-" + GLB.Invoice_no + ".xps";
                printDocument1.DefaultPageSettings.PrinterSettings.PrintFileName = FileName;
                printDocument1.DefaultPageSettings.PrinterSettings.PrintToFile = true;

                SetupGridPrinter();

                frmPrinterControl PC = new frmPrinterControl();

                PC.RPT_Print(DataGridPrinter1);
                PC = null;
            }
        }

        void SetupGridPrinter()
        {

            sMessage = "For a copy of the Pedigree Documentation for items on this invoice visit https://msd.track-n-trace.net for Track and Trace information" + (char)13;
            sMessage += "Terms of sale are as stated in our Terms & Conditions available at http://www.msdonline.com/About-Us/Terms-and-Conditions";

            dataDetails dataDetails = new dataDetails();

            lblFooter.Text = dataDetails.Get_LastReview();
            dgvEmptyMessage.ColumnHeadersVisible = false;

            DVPageHeader = dataDetails.DV_PageHeader_Fill(DVPageHeader, "");

            dgvPageHeader.BorderStyle = BorderStyle.None;
            dgvPageHeader.ColumnHeadersVisible = true;

            dgvPageHeader.Columns[0].HeaderText = "INVOICE";

            DataGridPrinter1 = new Printer(printDocument1,
                                           dgvPageHeader,
                                           dgvEmptyMessage,
                                           null,
                                           DVPageHeader.Table.DefaultView, lblFooter);

            //----------------------------------------------------------------------------
            //----------------------------------------------------------------------------
            //----------------------------------------------------------------------------

            string currentInvoice = string.Empty;
            int g = 0;

            foreach (DataRow row in GLB.OMInvoices.Rows)
            {

                if (row["invoice_no"].ToString() != currentInvoice)
                {
                    currentInvoice = row["invoice_no"].ToString();

                    dgvGroup1.ColumnHeadersVisible = false;
                    DataGridPrinter1.AddGroup(dgvGroup1, null, null);

                    //----------------------------------------------------------------------------
                    if ((row["rma_flag"].ToString() == "Y")||(row["invoice_adjustment_type"].ToString() == "C"))
                    {
                        dgvPageHeader.Columns[0].HeaderText = "CREDIT MEMO";
                        dgvSectionHeaderC1.Columns[0].HeaderText = "CREDIT MEMO";
                    }
                    //----------------------------------------------------------------------------


                    string strVal1 = string.Empty;
                    if (row["ship2_state"].ToString() != null)
                    {
                        if (row["ship2_state"].ToString() != "FL")
                        {
                            strVal1 += "800 Technology Center Drive" + (char)13;
                            strVal1 += "Stoughton, MA 02072" + (char)13;
                            strVal1 += "800.967.6400    781.344.7244";
                        }
                        else
                        {
                            strVal1 += "11600 Miramar Parkway Suite 300" + (char)13;
                            strVal1 += "Miramar, FL 33025" + (char)13;
                            strVal1 += "954.986.2000            954.983.2503";
                        }
                    }

                    DataView DVSectionHeaderA1 = new DataView();
                    DVSectionHeaderA1 = dataDetails.DV_SectionHeaderA1_Fill(DVSectionHeaderA1, strVal1);
                    dgvSectionHeaderA1.ColumnHeadersVisible = false;
                    DataGridPrinter1.AddSectionHeader(g, false,
                                                      null,
                                                      dgvSectionHeaderA1,
                                                      DVSectionHeaderA1);

                    //----------------------------------------------------------------------------

                    DataView DVSectionHeaderB1 = new DataView();
                    DVSectionHeaderB1 = dataDetails.DV_SectionHeaderB1_Fill(DVSectionHeaderB1);
                    dgvSectionHeaderB1.ColumnHeadersVisible = true;
                    DataGridPrinter1.AddSectionHeader(g, false,
                                                      null,
                                                      dgvSectionHeaderB1,
                                                      DVSectionHeaderB1);

                    //----------------------------------------------------------------------------

                    DataView DVSectionHeaderC1 = new DataView();
                    DVSectionHeaderC1 = dataDetails.DV_SectionHeaderC1_Fill(DVSectionHeaderC1, row["invoice_no"].ToString());
                    dgvSectionHeaderC1.ColumnHeadersVisible = true;
                    DataGridPrinter1.AddSectionHeader(g, false,
                                                      null,
                                                      dgvSectionHeaderC1,
                                                      DVSectionHeaderC1);

                    //----------------------------------------------------------------------------

                    DateTime dInvoiceDate = Convert.ToDateTime(row["invoice_date"]);
                    string sInvoiceDate = dInvoiceDate.ToString("MM/dd/yyyy");

                    DataView DVSectionHeaderD1 = new DataView();
                    DVSectionHeaderD1 = dataDetails.DV_SectionHeaderD1_Fill(DVSectionHeaderD1, sInvoiceDate, "");

                    dgvSectionHeaderD1.ColumnHeadersVisible = true;
                    //dgvSectionHeaderD1.BorderStyle = BorderStyle.None;
                    DataGridPrinter1.AddSectionHeader(g, false,
                                                      null,
                                                      dgvSectionHeaderD1,
                                                      DVSectionHeaderD1);

                    //----------------------------------------------------------------------------

                    DataView DVSectionHeaderE1 = new DataView();
                    DVSectionHeaderE1 = dataDetails.DV_SectionHeaderE1_Fill(DVSectionHeaderE1, row["order_no"].ToString());
                    dgvSectionHeaderE1.ColumnHeadersVisible = true;
                    DataGridPrinter1.AddSectionHeader(g, false,
                                                      null,
                                                      dgvSectionHeaderE1,
                                                      DVSectionHeaderE1);

                    //
                    //------------------------------------------------------------------------------------------
                    //

                    string sBill = string.Empty;

                    if (row["bill2_name"].ToString() != null)
                    {
                        sBill += row["bill2_name"].ToString().Trim();
                    }

                    if (row["bill2_address1"].ToString() != null)
                    {
                        sBill += (char)13;
                        sBill += row["bill2_address1"].ToString().Trim();
                    }

                    if (row["bill2_address2"].ToString() != null)
                    {
                        if (row["bill2_address2"].ToString().Trim() != "")
                        {
                            sBill += (char)13;
                            sBill += row["bill2_address2"].ToString().Trim();
                        }
                    }

                    if (row["bill2_city"].ToString() != null)
                    {
                        sBill += (char)13;
                        sBill += row["bill2_city"].ToString().Trim();
                    }

                    if (row["bill2_state"].ToString() != null)
                    {
                        sBill += ", "; //(char)13;
                        sBill += row["bill2_state"].ToString().Trim();
                    }

                    if (row["bill2_postal_code"].ToString() != null)
                    {
                        sBill += ", "; //(char)13;
                        sBill += row["bill2_postal_code"].ToString().Trim();
                    }

                    //-------------------------------------------------------------

                    string sShip = string.Empty;

                    if (row["ship2_name"].ToString() != null)
                    {
                        sShip += row["ship2_name"].ToString().Trim();
                    }

                    if (row["ship2_address1"].ToString() != null)
                    {
                        sShip += (char)13;
                        sShip += row["ship2_address1"].ToString().Trim();
                    }

                    if (row["ship2_address2"].ToString() != null)
                    {
                        if (row["ship2_address2"].ToString().Trim() != "")
                        {
                            sShip += (char)13;
                            sShip += row["ship2_address2"].ToString().Trim();
                        }
                    }

                    if (row["ship2_city"].ToString() != null)
                    {
                        sShip += (char)13;
                        sShip += row["ship2_city"].ToString().Trim();
                    }

                    if (row["ship2_state"].ToString() != null)
                    {
                        sShip += ", "; //(char)13;
                        sShip += row["ship2_state"].ToString().Trim();
                    }

                    if (row["ship2_postal_code"].ToString() != null)
                    {
                        sShip += ", "; //(char)13;
                        sShip += row["ship2_postal_code"].ToString().Trim();
                    }

                    DataView DVSectionHeader2 = new DataView();
                    DVSectionHeader2 = dataDetails.DV_SectionHeader2_Fill(DVSectionHeader2, sBill, sShip);

                    dgvSectionHeader2.ColumnHeadersVisible = true;
                    dgvSectionHeader2.BorderStyle = BorderStyle.None;
                    DataGridPrinter1.AddSectionHeader(g, false,
                                                      null,
                                                      dgvSectionHeader2,
                                                      DVSectionHeader2);

                    //
                    //------------------------------------------------------------------------------------------
                    //

                    DateTime dNetDueDate = Convert.ToDateTime(row["net_due_date"]);
                    string sNetDueDate = dNetDueDate.ToString("MM/dd/yyyy");

                    DataView DVSectionHeader3 = new DataView();
                    DVSectionHeader3 = dataDetails.DV_SectionHeader3_Fill(DVSectionHeader3,
                                                                          row["customer_id"].ToString(),
                                                                          row["dea_number"].ToString(),
                                                                          row["po_no"].ToString(),
                                                                          row["terms_desc"].ToString().ToUpper(),
                                                                          sNetDueDate);

                    dgvSectionHeader3.ColumnHeadersVisible = true;
                    dgvSectionHeader3.BorderStyle = BorderStyle.FixedSingle;
                    DataGridPrinter1.AddSectionHeader(g, false,
                                                      null,
                                                      dgvSectionHeader3,
                                                      DVSectionHeader3);

                    //
                    //------------------------------------------------------------------------------------------
                    //

                    string sOrderDate = string.Empty;
                    if (row["order_date"].ToString() != "")
                    {
                        DateTime dOrderDate = Convert.ToDateTime(row["order_date"]);
                        sOrderDate = dOrderDate.ToString("MM/dd/yyyy HH:mm:ss");
                    }

                    DataView DVSectionHeader4 = new DataView();
                    DVSectionHeader4 = dataDetails.DV_SectionHeader4_Fill(DVSectionHeader4,
                                                                          sOrderDate,
                                                                          row["pick_ticket_no"].ToString(),
                                                                          row["salesrep_name"].ToString(),
                                                                          row["taker"].ToString());

                    dgvSectionHeader4.ColumnHeadersVisible = true;
                    dgvSectionHeader4.BorderStyle = BorderStyle.FixedSingle;
                    DataGridPrinter1.AddSectionHeader(g, false,
                                                      null,
                                                      dgvSectionHeader4,
                                                      DVSectionHeader4);

                    //
                    //------------------------------------------------------------------------------------------
                    //


                    DataView DVSectionHeader5 = new DataView();

                    dgvSectionSubHeader5.ColumnHeadersVisible = true;
                    dgvSectionSubHeader5.BorderStyle = BorderStyle.FixedSingle;

                    dgvSectionHeader5.ColumnHeadersVisible = true;
                    dgvSectionHeader5.BorderStyle = BorderStyle.FixedSingle;

                    DataGridPrinter1.AddSectionHeader(g, false,
                                                      dgvSectionSubHeader5,
                                                      dgvSectionHeader5,
                                                      DVSectionHeader5);

                    //
                    //------------------------------------------------------------------------------------------
                    //

                    if (row["total_amount"].ToString() != "")
                    {
                        TotalAmount = Convert.ToDecimal(row["total_amount"]);
                    }

                    if (row["tax_amount"].ToString() != "")
                    {
                        TaxAmount = Convert.ToDecimal(row["tax_amount"]);
                    }

                    if (row["amount_paid"].ToString() != "")
                    {
                        AmountPaid = Convert.ToDecimal(row["amount_paid"]);
                    }

                    if (row["memo_amount"].ToString() != "")
                    {
                        MemoAmount = Convert.ToDecimal(row["memo_amount"]);
                    }

                    if (row["allowed"].ToString() != "")
                    {
                        Allowed = Convert.ToDecimal(row["allowed"]);
                    }

                    TotalAmount -= AmountPaid;
                    TotalAmount += MemoAmount;
                    TotalAmount -= Allowed;

                    //if ((Allowed < 0) && (row["rma_flag"].ToString() != "Y"))
                    //{
                    //    Allowed = Allowed * (-1);
                    //}

                    MemoAmountType = "debit";
                    //if ((MemoAmount < 0) && TotalAmount != 0 && (row["rma_flag"].ToString() != "Y"))
                    if (MemoAmount < 0)
                    {
                        MemoAmount = MemoAmount * (-1);
                        MemoAmountType = "credit";
                    }

                    //if ((AmountPaid < 0) && TotalAmount != 0 && (row["rma_flag"].ToString() != "Y"))
                    if (AmountPaid < 0)
                    {
                        AmountPaid = AmountPaid * (-1);
                    }

                    //if ((TotalAmount < 0) && (row["rma_flag"].ToString() != "Y"))
                    //{
                    //    TotalAmount = TotalAmount * (-1);
                    //}

                    DVSection3 = dataDetails.DV_Section_TwoFields_Open();
                    DVSection3 = dataDetails.DV_Section_TwoFields_Fill(DVSection3, "SUB-TOTAL:", "");

                    //
                    //------------------------------------------------------------------------------------------
                    //

                    DataView DVSectionHeader0 = new DataView();
                    dataGridView1.ColumnHeadersVisible = false;
                    dataGridView1.BorderStyle = BorderStyle.None;
                    DataGridPrinter1.AddSectionHeader(g, false,
                                                      null,
                                                      dataGridView1,
                                                      DVSectionHeader0);

                    //
                    //------------------------------------------------------------------------------------------
                    //

                    if (row["instructions"].ToString() != "")
                    {
                        sDeliveryInstructions = "Delivery Instructions: " + row["instructions"].ToString();
                    }

                    //
                    //------------------------------------------------------------------------------------------
                    //

                    DVSection2 = dataDetails.DV_Section_TwoFields_Open();

                    if (!string.IsNullOrWhiteSpace(row["carrier_name"].ToString()))
                    {
                        sCarrier = row["carrier_name"].ToString();
                    }

                    if (!string.IsNullOrWhiteSpace(row["Trackings"].ToString()))
                    {
                        sTrackings = Get_Trackings(row["Trackings"].ToString());
                    }

                    //
                    //------------------------------------------------------------------------------------------
                    //

                    if (DVSection1 != null)
                    {

                        if (sDeliveryInstructions != "")
                        {

                            DVSectionLine1 = dataDetails.DV_Section4_Fill(DVSectionLine1, sDeliveryInstructions);

                            dgvSectionLine1.ColumnHeadersVisible = true;
                            dgvSectionLine1.BorderStyle = BorderStyle.None;
                            DataGridPrinter1.AddSection(g, false,
                                                        null,
                                                        dgvSectionLine1,
                                                        DVSectionLine1);
                        }

                        //
                        //------------------------------------------------------------------------------------------
                        //

                        if (sCarrier != "")
                        {
                            DVSection2 = dataDetails.DV_Section_TwoFields_Fill(DVSection2, sCarrier, sTrackings);

                            dgvSectionLine6.ColumnHeadersVisible = true;
                            dgvSectionLine6.BorderStyle = BorderStyle.None;
                            DataGridPrinter1.AddSection(g, false,
                                                        null,
                                                        dgvSectionLine6,
                                                        DVSection2);
                        }

                        //
                        //------------------------------------------------------------------------------------------
                        //


                        dgvSectionLine5.ColumnHeadersVisible = false;
                        dgvSectionLine5.BorderStyle = BorderStyle.None;
                        DataGridPrinter1.AddSection(g, false,
                                                    null,
                                                    dgvSectionLine5,
                                                    DVSection1);
                        //
                        //------------------------------------------------------------------------------------------
                        //

                        DVSectionLine1 = dataDetails.DV_Section4_Fill(DVSectionLine1, sMessage);

                        dgvSectionLine1.ColumnHeadersVisible = true;
                        dgvSectionLine1.BorderStyle = BorderStyle.None;
                        DataGridPrinter1.AddSection(g, false,
                                                    null,
                                                    dgvSectionLine1,
                                                    DVSectionLine1);


                        DVSectionLine1 = dataDetails.DV_Section4_Fill(DVSectionLine1, "Total Lines: "+ TotalLines);

                        dgvSectionLine1.ColumnHeadersVisible = true;
                        dgvSectionLine1.BorderStyle = BorderStyle.None;
                        DataGridPrinter1.AddSection(g, false,
                                                    null,
                                                    dgvSectionLine1,
                                                    DVSectionLine1);

                        //
                        //------------------------------------------------------------------------------------------
                        //

                        //DVSection3 = dataDetails.DV_Section3_Open();
                        string sSubTotal = string.Format("{0:C}", SubTotal);
                        DVSection3.Table.Rows[0][2] = sSubTotal;

                        if (TaxAmount == 0)
                        {
                            string sTaxAmount = string.Format("{0:C}", TaxAmount);
                            DVSection3 = dataDetails.DV_Section_TwoFields_Fill(DVSection3, "TAX:", sTaxAmount);
                        }

                        if (AmountPaid > 0)
                        {
                            string sAmountPaid = string.Format("{0:C}", AmountPaid);
                            DVSection3 = dataDetails.DV_Section_TwoFields_Fill(DVSection3, "CASH RECEIPTS:", sAmountPaid);
                        }

                        if (MemoAmount > 0)
                        {
                            string sMemoAmount = string.Format("{0:C}", MemoAmount);
                            DVSection3 = dataDetails.DV_Section_TwoFields_Fill(DVSection3, "MEMO AMT ("+MemoAmountType+"):", sMemoAmount);
                        }

                        if (Allowed > 0)
                        {
                            string sAllowed = string.Format("{0:C}", Allowed);
                            DVSection3 = dataDetails.DV_Section_TwoFields_Fill(DVSection3, "ALLOWED AMOUNT:", sAllowed);
                        }

                        dgvSection3.ColumnHeadersVisible = false;
                        dgvSection3.BorderStyle = BorderStyle.None;
                        DataGridPrinter1.AddSection(g, false,
                                                    null,
                                                    dgvSection3,
                                                    DVSection3);
                        //
                        //------------------------------------------------------------------------------------------
                        //

                        DVSection4 = dataDetails.DV_Section_TwoFields_Open();

                        string sTotalAmount = string.Format("{0:C}", TotalAmount);
                        DVSection4 = dataDetails.DV_Section_TwoFields_Fill(DVSection4, "AMOUNT DUE:", sTotalAmount);

                        dgvSection4.ColumnHeadersVisible = false;
                        dgvSection4.BorderStyle = BorderStyle.None;
                        DataGridPrinter1.AddSection(g, false,
                                                    null,
                                                    dgvSection4,
                                                    DVSection4);

                        //
                        //------------------------------------------------------------------------------------------
                        //

                        TotalAmount = 0;
                        TaxAmount = 0;
                        AmountPaid = 0;
                        MemoAmount = 0;
                        Allowed = 0;
                        SubTotal = 0;

                        TotalLines = 0;

                    }

                    DVSection1 = dataDetails.DV_Section1_Open();
                    DVSection1 = dataDetails.DV_Section1_Fill(DVSection1, "_________", "_________", "______________", "_________", "____", "_______________________________________", "_________", "_________", "______________");
                    ++g;
                    //if (g > 5) { break; }
                }

                //
                //------------------------------------------------------------------------------------------
                //

                string qtyOrdered = string.Empty;
                string qtyShipped = string.Empty;
                string qtyRemaning = string.Empty;

                string UOM_UnitSize = string.Empty;
                string UnitSize = string.Empty;

                string Disp = string.Empty;

                string itemID_Description_Extend = string.Empty;
                string itemID = string.Empty;
                string itemDescription = string.Empty;
                string customer_part_number = string.Empty;

                string PriceUOM_PriceSize = string.Empty;
                string PriceSize = string.Empty;

                string UnitPrice = string.Empty;
                string ExtendPrice = string.Empty;


                decimal decsales_unit_size = 0;
                UnitSize = row["sales_unit_size"].ToString();
                if (!string.IsNullOrWhiteSpace(UnitSize))
                {
                    decsales_unit_size = Convert.ToDecimal(UnitSize);
                    UnitSize = decsales_unit_size.ToString("0.##");
                }

                UOM_UnitSize = row["unit_of_measure"].ToString() + (char)13 + UnitSize;

                decimal decqtyOrdered = Convert.ToDecimal(row["qty_requested"]);
                if (decsales_unit_size > 0)
                {
                    decqtyOrdered = decqtyOrdered / decsales_unit_size;
                }
                qtyOrdered = decqtyOrdered.ToString("0.##");

                decimal decqtyShipped = Convert.ToDecimal(row["qty_shipped"]);
                if (decsales_unit_size > 0)
                {
                    decqtyShipped = decqtyShipped / decsales_unit_size;
                }
                qtyShipped = decqtyShipped.ToString("0.##");

                decimal decqtyRemaning = decqtyOrdered - decqtyShipped;
                if (decqtyRemaning > 0)
                {
                    qtyRemaning = decqtyRemaning.ToString("0.##");
                }

                Disp = row["disposition"].ToString();

                itemID = row["item_id"].ToString();
                itemID_Description_Extend = row["item_id"].ToString() + (char)13 + row["item_desc"].ToString();

                if (row["extended_desc"].ToString() != null)
                {
                    if (row["extended_desc"].ToString() != string.Empty)
                    {
                        itemID_Description_Extend += (char)13 + row["extended_desc"].ToString();
                    }
                }

                if (row["customer_part_number"].ToString() != null)
                {
                    if ((row["customer_part_number"].ToString() != string.Empty) && (row["customer_part_number"].ToString() != row["item_id"].ToString()))
                    {
                        itemID_Description_Extend += (char)13 + "Ordered As " + row["customer_part_number"].ToString();
                    }
                }

                PriceSize = row["pricing_unit_size"].ToString();
                if (!string.IsNullOrWhiteSpace(PriceSize))
                {
                    decimal decpricing_unit_size = Convert.ToDecimal(PriceSize);
                    PriceSize = decpricing_unit_size.ToString("0.##");
                }

                PriceUOM_PriceSize = row["pricing_unit"].ToString() + (char)13 + PriceSize;

                if (row["unit_price"].ToString() != "")
                {
                    decimal decUnitPrice = Convert.ToDecimal(row["unit_price"]);
                   // UnitPrice = string.Format("{0:c}", decUnitPrice);
                    UnitPrice = "$"+decUnitPrice.ToString("0.0000");
                }

                if (row["extended_price"].ToString() != "")
                {
                    decimal decExtendPrice = Convert.ToDecimal(row["extended_price"]);
                    ExtendPrice = string.Format("{0:C}", decExtendPrice);
                }

                //
                //------------------------------------------------------------------------------------------
                //

                if (row["extended_price"].ToString() != null)
                {
                    if (row["extended_price"].ToString() != "")
                    {
                        if (row["other_charge_item"].ToString() == "N")
                        {
                            DVSection1 = dataDetails.DV_Section1_Fill(DVSection1, qtyOrdered, qtyShipped, qtyRemaning, UOM_UnitSize, Disp, itemID_Description_Extend, PriceUOM_PriceSize, UnitPrice, ExtendPrice);
                            TotalLines++;
                            //
                            //------------------------------------------------------------------------------------------
                            //

                            string Lots = row["Lots"].ToString();
                            string lot_no = string.Empty;
                            string lot_expiration_date = string.Empty;
                            string lot_unit_of_measure = string.Empty;
                            string lot_unit_quantity = string.Empty;

                            string[] splitLots;
                            splitLots = Lots.Split(new string[] { "[d2]" }, StringSplitOptions.None);

                            foreach (string Lot in splitLots)
                            {
                                if (!string.IsNullOrWhiteSpace(Lot))
                                {
                                    string[] splitLot;
                                    splitLot = Lot.Split(new string[] { "[d3]" }, StringSplitOptions.None);
                                    decimal declot_unit_quantity = 0;

                                    if (splitLot[0].ToString() != "")
                                    {
                                        lot_no = splitLot[0].ToString();
                                    }

                                    if (splitLot[1].ToString() != "")
                                    {
                                        DateTime dlot_expiration_date = Convert.ToDateTime(splitLot[1].ToString());
                                        lot_expiration_date = dlot_expiration_date.ToString("MM/dd/yyyy");
                                    }

                                    if (splitLot[2].ToString() != "")
                                    {
                                        lot_unit_of_measure = splitLot[2].ToString();
                                    }

                                    if (splitLot[3].ToString() != "")
                                    {
                                        declot_unit_quantity = Convert.ToDecimal(splitLot[3].ToString());
                                        lot_unit_quantity = "Qty: " + declot_unit_quantity.ToString("0.##");
                                    }

                                    if (declot_unit_quantity > 0)
                                    {
                                        DVSection1 = dataDetails.DV_Section1_Fill(DVSection1, "", "         Lot #:", lot_no, "", "", lot_unit_quantity + "  " + lot_unit_of_measure, "Expiration", "Date:", lot_expiration_date);
                                    }
                                }
                            }

                            //
                            //------------------------------------------------------------------------------------------
                            //

                            DVSection1 = dataDetails.DV_Section1_Fill(DVSection1, "_________", "_________", "______________", "_________", "____", "_______________________________________", "_________", "_________", "______________");
                            SubTotal += Convert.ToDecimal(row["extended_price"]);
                        }

                        if (row["other_charge_item"].ToString() != "N")
                        {
                            itemDescription = row["item_desc"].ToString();

                            //if ((itemID == "250") | (itemID == "500") | (itemID == "FREIGHT AMT"))
                            if ((itemID == "250") | (itemID == "500"))
                            {
                                itemDescription = "TOTAL FREIGHT";
                            }
                            if (itemID == "FREIGHT AMT")
                            {
                                itemDescription = itemID;
                            }
                            DVSection3 = dataDetails.DV_Section_TwoFields_Fill(DVSection3, itemDescription + ":", ExtendPrice);
                        }
                    }

                }


                //
                //------------------------------------------------------------------------
                //


            }

            if (DVSection1 != null)
            {

                if (sDeliveryInstructions != "")
                {

                    DVSectionLine1 = dataDetails.DV_Section4_Fill(DVSectionLine1, sDeliveryInstructions);

                    dgvSectionLine1.ColumnHeadersVisible = true;
                    dgvSectionLine1.BorderStyle = BorderStyle.None;
                    DataGridPrinter1.AddSection(g, false,
                                                null,
                                                dgvSectionLine1,
                                                DVSectionLine1);
                }

                //
                //------------------------------------------------------------------------------------------
                //

                if (sCarrier != "")
                {
                    DVSection2 = dataDetails.DV_Section_TwoFields_Fill(DVSection2, sCarrier, sTrackings);

                    dgvSectionLine6.ColumnHeadersVisible = true;
                    dgvSectionLine6.BorderStyle = BorderStyle.None;
                    DataGridPrinter1.AddSection(g, false,
                                                null,
                                                dgvSectionLine6,
                                                DVSection2);
                }

                //
                //------------------------------------------------------------------------------------------
                //

                dgvSectionLine5.ColumnHeadersVisible = false;
                dgvSectionLine5.BorderStyle = BorderStyle.None;
                DataGridPrinter1.AddSection(g, false,
                                            null,
                                            dgvSectionLine5,
                                            DVSection1);


                //
                //------------------------------------------------------------------------------------------
                //

                DVSectionLine1 = dataDetails.DV_Section4_Fill(DVSectionLine1, sMessage);

                dgvSectionLine1.ColumnHeadersVisible = false;
                dgvSectionLine1.BorderStyle = BorderStyle.None;
                DataGridPrinter1.AddSection(g, false,
                                            null,
                                            dgvSectionLine1,
                                            DVSectionLine1);

                DVSectionLine1 = dataDetails.DV_Section4_Fill(DVSectionLine1, "Total Lines: " + TotalLines);

                dgvSectionLine1.ColumnHeadersVisible = false;
                dgvSectionLine1.BorderStyle = BorderStyle.None;
                DataGridPrinter1.AddSection(g, false,
                                            null,
                                            dgvSectionLine1,
                                            DVSectionLine1);

                //
                //------------------------------------------------------------------------------------------
                //

                //DVSection3 = dataDetails.DV_Section3_Open();
                string sSubTotal = string.Format("{0:C}", SubTotal);
                DVSection3.Table.Rows[0][2] = sSubTotal;


                if (TaxAmount == 0)
                {
                    string sTaxAmount = string.Format("{0:C}", TaxAmount);
                    DVSection3 = dataDetails.DV_Section_TwoFields_Fill(DVSection3, "TAX:", sTaxAmount);
                }

                if (AmountPaid != 0)
                {
                    string sAmountPaid = string.Format("{0:C}", AmountPaid);
                    DVSection3 = dataDetails.DV_Section_TwoFields_Fill(DVSection3, "CASH RECEIPTS:", sAmountPaid);
                }

                if (MemoAmount != 0)
                {
                    string sMemoAmount = string.Format("{0:C}", MemoAmount);
                    DVSection3 = dataDetails.DV_Section_TwoFields_Fill(DVSection3, "MEMO AMT (" + MemoAmountType + "):", sMemoAmount);
                }

                if (Allowed != 0)
                {
                    string sAllowed = string.Format("{0:C}", Allowed);
                    DVSection3 = dataDetails.DV_Section_TwoFields_Fill(DVSection3, "ALLOWED AMOUNT:", sAllowed);
                }

                dgvSection3.ColumnHeadersVisible = false;
                dgvSection3.BorderStyle = BorderStyle.None;
                DataGridPrinter1.AddSection(g, false,
                                            null,
                                            dgvSection3,
                                            DVSection3);
                //
                //------------------------------------------------------------------------------------------
                //

                DVSection4 = dataDetails.DV_Section_TwoFields_Open();

                string sTotalAmount = string.Format("{0:C}", TotalAmount);
                DVSection4 = dataDetails.DV_Section_TwoFields_Fill(DVSection4, "AMOUNT DUE:", sTotalAmount);

                dgvSection4.ColumnHeadersVisible = false;
                dgvSection4.BorderStyle = BorderStyle.None;
                DataGridPrinter1.AddSection(g, false,
                                            null,
                                            dgvSection4,
                                            DVSection4);

                //
                //------------------------------------------------------------------------------------------
                //

                TotalAmount = 0;
                TaxAmount = 0;
                AmountPaid = 0;
                MemoAmount = 0;
                Allowed = 0;
                SubTotal = 0;

                TotalLines = 0;

            }

            DataGridPrinter1.PrintLogo = true;
            DataGridPrinter1.PrintLogoCenter = false;

        }

        private string Get_Trackings(string Trackings)
        {
            string InvoiceTrackings=string.Empty;
            string[] splitTrackings;
            splitTrackings = Trackings.Split('^');
            foreach (string Tracking in splitTrackings)
            {
                if (!string.IsNullOrWhiteSpace(Tracking))
                {
                    string[] splitTracking;
                    splitTracking = Tracking.Split(',');
                    if (InvoiceTrackings != "") InvoiceTrackings += (char)13;
                    InvoiceTrackings += splitTracking[0].ToString();
                }
            }
            return InvoiceTrackings;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace OTInvoices
{
    public partial class frmOTInvoices : Form
    {
        public frmOTInvoices()
        {
            InitializeComponent();
        }

        private void frmOTInvoices_Load(object sender, EventArgs e)
        {
            PaintGrid();
        }

        //
        //-------------------------------------------------------------
        //

        private void btnDisplay_Click(object sender, EventArgs e)
        {
            frmOTInvoicesPrint frmPCSReport = new frmOTInvoicesPrint();
            frmPCSReport.PCS_Preview();
            return;
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            frmOTInvoicesPrint frmPCSReport = new frmOTInvoicesPrint();
            frmPCSReport.PCS_Print();
        }

        private void btnSalvarRelatorio_Click(object sender, EventArgs e)
        {
            frmOTInvoicesPrint frmPCSReport = new frmOTInvoicesPrint();
            frmPCSReport.PCS_Salva();
        }

        //
        //-------------------------------------------------------------
        //


        //
        //-------------------------------------------------------------
        //

        private void PaintGrid()
        {
            dataGridView1.DataSource = GLB.OTInvoices;
        }
        //
        //-----------------------------------------------------------------        
        //

     }
}

﻿namespace OTInvoices
{
    partial class frmOTInvoicesPrint
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle42 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle43 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle40 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle41 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle44 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle45 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle47 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle48 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle46 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle49 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle50 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle53 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle54 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle51 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle52 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle55 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle56 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle65 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle66 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle57 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle58 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle59 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle60 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle61 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle62 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle63 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle64 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle67 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle68 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle76 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle77 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle69 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle70 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle71 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle72 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle73 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle74 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle75 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle78 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle79 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle81 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle82 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle80 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle83 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle84 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle85 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle86 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle87 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle90 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle91 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle88 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle89 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle92 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle93 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle98 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle99 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle94 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle95 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle96 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle97 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle100 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle101 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle103 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle104 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle102 = new System.Windows.Forms.DataGridViewCellStyle();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabHeader = new System.Windows.Forms.TabPage();
            this.dgvGroup1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvGroup2 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn31 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvEndOTInvoices = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvEndGroupMsg = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblFooter = new System.Windows.Forms.Label();
            this.dgvEmptyMessage = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn32 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabHeaderSections = new System.Windows.Forms.TabPage();
            this.dgvSection4B = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvSection4A = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvSectionHeader4 = new System.Windows.Forms.DataGridView();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SALESPERSON = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TAKER = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMERPONUMBER = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TERMS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvSection3 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvSection2 = new System.Windows.Forms.DataGridView();
            this.SUBTOTAL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvSectionRental = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BillableDays = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvSectionService = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PRICEEACH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvSectionHeader5 = new System.Windows.Forms.DataGridView();
            this.InvoiceNote = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvSectionHeader2 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvSectionHeader3 = new System.Windows.Forms.DataGridView();
            this.space1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BillTo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BillToAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ShipTo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ShipToAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvSectionHeader1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabSections = new System.Windows.Forms.TabPage();
            this.tabControl1.SuspendLayout();
            this.tabHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEndOTInvoices)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEndGroupMsg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmptyMessage)).BeginInit();
            this.tabHeaderSections.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSection4B)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSection4A)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeader4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSection3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSection2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionRental)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionService)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeader5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeader2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeader3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeader1)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabHeader);
            this.tabControl1.Controls.Add(this.tabHeaderSections);
            this.tabControl1.Controls.Add(this.tabSections);
            this.tabControl1.Location = new System.Drawing.Point(12, 5);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(832, 481);
            this.tabControl1.TabIndex = 18;
            // 
            // tabHeader
            // 
            this.tabHeader.Controls.Add(this.dgvGroup1);
            this.tabHeader.Controls.Add(this.dgvGroup2);
            this.tabHeader.Controls.Add(this.dgvEndOTInvoices);
            this.tabHeader.Controls.Add(this.dgvEndGroupMsg);
            this.tabHeader.Controls.Add(this.lblFooter);
            this.tabHeader.Controls.Add(this.dgvEmptyMessage);
            this.tabHeader.Location = new System.Drawing.Point(4, 22);
            this.tabHeader.Name = "tabHeader";
            this.tabHeader.Padding = new System.Windows.Forms.Padding(3);
            this.tabHeader.Size = new System.Drawing.Size(824, 455);
            this.tabHeader.TabIndex = 1;
            this.tabHeader.Text = "Header";
            this.tabHeader.UseVisualStyleBackColor = true;
            // 
            // dgvGroup1
            // 
            this.dgvGroup1.AllowUserToAddRows = false;
            this.dgvGroup1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvGroup1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvGroup1.BackgroundColor = System.Drawing.Color.White;
            this.dgvGroup1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvGroup1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvGroup1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvGroup1.ColumnHeadersHeight = 50;
            this.dgvGroup1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvGroup1.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgvGroup1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvGroup1.GridColor = System.Drawing.Color.Black;
            this.dgvGroup1.Location = new System.Drawing.Point(280, 20);
            this.dgvGroup1.MultiSelect = false;
            this.dgvGroup1.Name = "dgvGroup1";
            this.dgvGroup1.RowHeadersVisible = false;
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvGroup1.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvGroup1.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvGroup1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvGroup1.Size = new System.Drawing.Size(500, 25);
            this.dgvGroup1.TabIndex = 41;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewTextBoxColumn1.HeaderText = "REMIT TO:";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 250;
            // 
            // dataGridViewTextBoxColumn2
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewTextBoxColumn2.HeaderText = "                    INVOICE";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 250;
            // 
            // dgvGroup2
            // 
            this.dgvGroup2.AllowUserToAddRows = false;
            this.dgvGroup2.AllowUserToDeleteRows = false;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvGroup2.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle7;
            this.dgvGroup2.BackgroundColor = System.Drawing.Color.Gray;
            this.dgvGroup2.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvGroup2.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dgvGroup2.ColumnHeadersHeight = 30;
            this.dgvGroup2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn31});
            this.dgvGroup2.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvGroup2.GridColor = System.Drawing.Color.Black;
            this.dgvGroup2.Location = new System.Drawing.Point(35, 73);
            this.dgvGroup2.MultiSelect = false;
            this.dgvGroup2.Name = "dgvGroup2";
            this.dgvGroup2.RowHeadersVisible = false;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvGroup2.RowsDefaultCellStyle = dataGridViewCellStyle9;
            this.dgvGroup2.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvGroup2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvGroup2.Size = new System.Drawing.Size(745, 28);
            this.dgvGroup2.TabIndex = 39;
            // 
            // dataGridViewTextBoxColumn31
            // 
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn31.DefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridViewTextBoxColumn31.HeaderText = "Calculo de OTInvoices ";
            this.dataGridViewTextBoxColumn31.Name = "dataGridViewTextBoxColumn31";
            this.dataGridViewTextBoxColumn31.Width = 745;
            // 
            // dgvEndOTInvoices
            // 
            this.dgvEndOTInvoices.AllowUserToAddRows = false;
            this.dgvEndOTInvoices.AllowUserToDeleteRows = false;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvEndOTInvoices.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle10;
            this.dgvEndOTInvoices.BackgroundColor = System.Drawing.Color.Gray;
            this.dgvEndOTInvoices.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvEndOTInvoices.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dgvEndOTInvoices.ColumnHeadersHeight = 30;
            this.dgvEndOTInvoices.ColumnHeadersVisible = false;
            this.dgvEndOTInvoices.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1});
            this.dgvEndOTInvoices.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvEndOTInvoices.GridColor = System.Drawing.Color.Black;
            this.dgvEndOTInvoices.Location = new System.Drawing.Point(35, 340);
            this.dgvEndOTInvoices.MultiSelect = false;
            this.dgvEndOTInvoices.Name = "dgvEndOTInvoices";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEndOTInvoices.RowHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.dgvEndOTInvoices.RowHeadersVisible = false;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle12.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvEndOTInvoices.RowsDefaultCellStyle = dataGridViewCellStyle12;
            this.dgvEndOTInvoices.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvEndOTInvoices.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvEndOTInvoices.Size = new System.Drawing.Size(745, 25);
            this.dgvEndOTInvoices.TabIndex = 38;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "";
            this.Column1.Name = "Column1";
            this.Column1.Width = 745;
            // 
            // dgvEndGroupMsg
            // 
            this.dgvEndGroupMsg.AllowUserToAddRows = false;
            this.dgvEndGroupMsg.AllowUserToDeleteRows = false;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle13.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvEndGroupMsg.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle13;
            this.dgvEndGroupMsg.BackgroundColor = System.Drawing.Color.White;
            this.dgvEndGroupMsg.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvEndGroupMsg.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvEndGroupMsg.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEndGroupMsg.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle14;
            this.dgvEndGroupMsg.ColumnHeadersHeight = 30;
            this.dgvEndGroupMsg.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn3});
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvEndGroupMsg.DefaultCellStyle = dataGridViewCellStyle16;
            this.dgvEndGroupMsg.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvEndGroupMsg.GridColor = System.Drawing.Color.Black;
            this.dgvEndGroupMsg.Location = new System.Drawing.Point(35, 283);
            this.dgvEndGroupMsg.MultiSelect = false;
            this.dgvEndGroupMsg.Name = "dgvEndGroupMsg";
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle17.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle17.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEndGroupMsg.RowHeadersDefaultCellStyle = dataGridViewCellStyle17;
            this.dgvEndGroupMsg.RowHeadersVisible = false;
            dataGridViewCellStyle18.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle18.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvEndGroupMsg.RowsDefaultCellStyle = dataGridViewCellStyle18;
            this.dgvEndGroupMsg.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvEndGroupMsg.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvEndGroupMsg.Size = new System.Drawing.Size(745, 31);
            this.dgvEndGroupMsg.TabIndex = 37;
            // 
            // dataGridViewTextBoxColumn3
            // 
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle15;
            this.dataGridViewTextBoxColumn3.HeaderText = "Terms and Conditions";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 745;
            // 
            // lblFooter
            // 
            this.lblFooter.BackColor = System.Drawing.Color.White;
            this.lblFooter.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFooter.ForeColor = System.Drawing.Color.Black;
            this.lblFooter.Location = new System.Drawing.Point(35, 382);
            this.lblFooter.Name = "lblFooter";
            this.lblFooter.Size = new System.Drawing.Size(745, 15);
            this.lblFooter.TabIndex = 27;
            this.lblFooter.Text = "MSD";
            // 
            // dgvEmptyMessage
            // 
            this.dgvEmptyMessage.AllowUserToAddRows = false;
            this.dgvEmptyMessage.AllowUserToDeleteRows = false;
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle19.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle19.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvEmptyMessage.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle19;
            this.dgvEmptyMessage.BackgroundColor = System.Drawing.Color.Gray;
            this.dgvEmptyMessage.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvEmptyMessage.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle20.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle20.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle20.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle20.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEmptyMessage.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle20;
            this.dgvEmptyMessage.ColumnHeadersHeight = 20;
            this.dgvEmptyMessage.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn32});
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle22.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle22.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle22.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle22.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvEmptyMessage.DefaultCellStyle = dataGridViewCellStyle22;
            this.dgvEmptyMessage.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvEmptyMessage.GridColor = System.Drawing.Color.Black;
            this.dgvEmptyMessage.Location = new System.Drawing.Point(35, 412);
            this.dgvEmptyMessage.MultiSelect = false;
            this.dgvEmptyMessage.Name = "dgvEmptyMessage";
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle23.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle23.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle23.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle23.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle23.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEmptyMessage.RowHeadersDefaultCellStyle = dataGridViewCellStyle23;
            this.dgvEmptyMessage.RowHeadersVisible = false;
            dataGridViewCellStyle24.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle24.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle24.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle24.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvEmptyMessage.RowsDefaultCellStyle = dataGridViewCellStyle24;
            this.dgvEmptyMessage.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvEmptyMessage.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvEmptyMessage.Size = new System.Drawing.Size(745, 19);
            this.dgvEmptyMessage.TabIndex = 26;
            // 
            // dataGridViewTextBoxColumn32
            // 
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn32.DefaultCellStyle = dataGridViewCellStyle21;
            this.dataGridViewTextBoxColumn32.HeaderText = "No items in this section";
            this.dataGridViewTextBoxColumn32.Name = "dataGridViewTextBoxColumn32";
            this.dataGridViewTextBoxColumn32.Width = 745;
            // 
            // tabHeaderSections
            // 
            this.tabHeaderSections.Controls.Add(this.dgvSection4B);
            this.tabHeaderSections.Controls.Add(this.dgvSection4A);
            this.tabHeaderSections.Controls.Add(this.dgvSectionHeader4);
            this.tabHeaderSections.Controls.Add(this.dgvSection3);
            this.tabHeaderSections.Controls.Add(this.dgvSection2);
            this.tabHeaderSections.Controls.Add(this.dgvSectionRental);
            this.tabHeaderSections.Controls.Add(this.dgvSectionService);
            this.tabHeaderSections.Controls.Add(this.dgvSectionHeader5);
            this.tabHeaderSections.Controls.Add(this.dataGridView1);
            this.tabHeaderSections.Controls.Add(this.dgvSectionHeader2);
            this.tabHeaderSections.Controls.Add(this.dgvSectionHeader3);
            this.tabHeaderSections.Controls.Add(this.dgvSectionHeader1);
            this.tabHeaderSections.Location = new System.Drawing.Point(4, 22);
            this.tabHeaderSections.Name = "tabHeaderSections";
            this.tabHeaderSections.Size = new System.Drawing.Size(824, 455);
            this.tabHeaderSections.TabIndex = 2;
            this.tabHeaderSections.Text = "HeaderSection";
            this.tabHeaderSections.UseVisualStyleBackColor = true;
            // 
            // dgvSection4B
            // 
            this.dgvSection4B.AllowUserToAddRows = false;
            this.dgvSection4B.AllowUserToDeleteRows = false;
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle25.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle25.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle25.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle25.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSection4B.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle25;
            this.dgvSection4B.BackgroundColor = System.Drawing.Color.Gray;
            this.dgvSection4B.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvSection4B.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            dataGridViewCellStyle26.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle26.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle26.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle26.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle26.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle26.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSection4B.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle26;
            this.dgvSection4B.ColumnHeadersHeight = 25;
            this.dgvSection4B.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn12});
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle28.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle28.Font = new System.Drawing.Font("Microsoft Sans Serif", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle28.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle28.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle28.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle28.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSection4B.DefaultCellStyle = dataGridViewCellStyle28;
            this.dgvSection4B.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSection4B.GridColor = System.Drawing.Color.Black;
            this.dgvSection4B.Location = new System.Drawing.Point(251, 335);
            this.dgvSection4B.MultiSelect = false;
            this.dgvSection4B.Name = "dgvSection4B";
            this.dgvSection4B.RowHeadersVisible = false;
            dataGridViewCellStyle29.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle29.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle29.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle29.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSection4B.RowsDefaultCellStyle = dataGridViewCellStyle29;
            this.dgvSection4B.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvSection4B.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSection4B.Size = new System.Drawing.Size(200, 25);
            this.dgvSection4B.TabIndex = 61;
            // 
            // dataGridViewTextBoxColumn12
            // 
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            this.dataGridViewTextBoxColumn12.DefaultCellStyle = dataGridViewCellStyle27;
            this.dataGridViewTextBoxColumn12.HeaderText = "Electronic payment instructions";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.Width = 200;
            // 
            // dgvSection4A
            // 
            this.dgvSection4A.AllowUserToAddRows = false;
            this.dgvSection4A.AllowUserToDeleteRows = false;
            dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle30.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle30.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle30.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle30.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSection4A.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle30;
            this.dgvSection4A.BackgroundColor = System.Drawing.Color.Gray;
            this.dgvSection4A.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvSection4A.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            dataGridViewCellStyle31.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle31.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle31.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle31.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle31.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle31.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSection4A.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle31;
            this.dgvSection4A.ColumnHeadersHeight = 25;
            this.dgvSection4A.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn13});
            dataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle33.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle33.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.5F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle33.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle33.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle33.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle33.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSection4A.DefaultCellStyle = dataGridViewCellStyle33;
            this.dgvSection4A.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSection4A.GridColor = System.Drawing.Color.Black;
            this.dgvSection4A.Location = new System.Drawing.Point(35, 335);
            this.dgvSection4A.MultiSelect = false;
            this.dgvSection4A.Name = "dgvSection4A";
            this.dgvSection4A.RowHeadersVisible = false;
            dataGridViewCellStyle34.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle34.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle34.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle34.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSection4A.RowsDefaultCellStyle = dataGridViewCellStyle34;
            this.dgvSection4A.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvSection4A.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSection4A.Size = new System.Drawing.Size(200, 25);
            this.dgvSection4A.TabIndex = 60;
            // 
            // dataGridViewTextBoxColumn13
            // 
            dataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            this.dataGridViewTextBoxColumn13.DefaultCellStyle = dataGridViewCellStyle32;
            this.dataGridViewTextBoxColumn13.HeaderText = "REMIT TO:";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.Width = 200;
            // 
            // dgvSectionHeader4
            // 
            this.dgvSectionHeader4.AllowUserToAddRows = false;
            this.dgvSectionHeader4.AllowUserToDeleteRows = false;
            dataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle35.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle35.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle35.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle35.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeader4.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle35;
            this.dgvSectionHeader4.BackgroundColor = System.Drawing.Color.Gray;
            this.dgvSectionHeader4.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvSectionHeader4.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle36.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle36.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle36.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle36.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle36.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle36.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSectionHeader4.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle36;
            this.dgvSectionHeader4.ColumnHeadersHeight = 30;
            this.dgvSectionHeader4.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column2,
            this.SALESPERSON,
            this.DATE,
            this.TAKER,
            this.CUSTOMERPONUMBER,
            this.TERMS});
            dataGridViewCellStyle42.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle42.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle42.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle42.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle42.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle42.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle42.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSectionHeader4.DefaultCellStyle = dataGridViewCellStyle42;
            this.dgvSectionHeader4.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSectionHeader4.GridColor = System.Drawing.Color.Black;
            this.dgvSectionHeader4.Location = new System.Drawing.Point(35, 181);
            this.dgvSectionHeader4.MultiSelect = false;
            this.dgvSectionHeader4.Name = "dgvSectionHeader4";
            this.dgvSectionHeader4.RowHeadersVisible = false;
            dataGridViewCellStyle43.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle43.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle43.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle43.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeader4.RowsDefaultCellStyle = dataGridViewCellStyle43;
            this.dgvSectionHeader4.RowTemplate.Height = 30;
            this.dgvSectionHeader4.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvSectionHeader4.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSectionHeader4.Size = new System.Drawing.Size(745, 25);
            this.dgvSectionHeader4.TabIndex = 58;
            // 
            // Column2
            // 
            dataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            this.Column2.DefaultCellStyle = dataGridViewCellStyle37;
            this.Column2.HeaderText = "CUSTOMER NO.";
            this.Column2.Name = "Column2";
            // 
            // SALESPERSON
            // 
            dataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle38.NullValue = null;
            this.SALESPERSON.DefaultCellStyle = dataGridViewCellStyle38;
            this.SALESPERSON.HeaderText = "SALES PERSON";
            this.SALESPERSON.Name = "SALESPERSON";
            this.SALESPERSON.Width = 150;
            // 
            // DATE
            // 
            dataGridViewCellStyle39.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            this.DATE.DefaultCellStyle = dataGridViewCellStyle39;
            this.DATE.HeaderText = "DATE";
            this.DATE.Name = "DATE";
            // 
            // TAKER
            // 
            this.TAKER.HeaderText = "TAKER";
            this.TAKER.Name = "TAKER";
            this.TAKER.Width = 145;
            // 
            // CUSTOMERPONUMBER
            // 
            dataGridViewCellStyle40.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            this.CUSTOMERPONUMBER.DefaultCellStyle = dataGridViewCellStyle40;
            this.CUSTOMERPONUMBER.HeaderText = "CUSTOMER P.O. NUMBER";
            this.CUSTOMERPONUMBER.Name = "CUSTOMERPONUMBER";
            this.CUSTOMERPONUMBER.Width = 150;
            // 
            // TERMS
            // 
            dataGridViewCellStyle41.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            this.TERMS.DefaultCellStyle = dataGridViewCellStyle41;
            this.TERMS.HeaderText = "TERMS";
            this.TERMS.Name = "TERMS";
            // 
            // dgvSection3
            // 
            this.dgvSection3.AllowUserToAddRows = false;
            this.dgvSection3.AllowUserToDeleteRows = false;
            dataGridViewCellStyle44.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle44.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle44.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle44.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle44.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSection3.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle44;
            this.dgvSection3.BackgroundColor = System.Drawing.Color.Gray;
            this.dgvSection3.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvSection3.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle45.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle45.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle45.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle45.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle45.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle45.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle45.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSection3.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle45;
            this.dgvSection3.ColumnHeadersHeight = 25;
            this.dgvSection3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn26});
            dataGridViewCellStyle47.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle47.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle47.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle47.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle47.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle47.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle47.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSection3.DefaultCellStyle = dataGridViewCellStyle47;
            this.dgvSection3.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSection3.GridColor = System.Drawing.Color.Black;
            this.dgvSection3.Location = new System.Drawing.Point(600, 366);
            this.dgvSection3.MultiSelect = false;
            this.dgvSection3.Name = "dgvSection3";
            this.dgvSection3.RowHeadersVisible = false;
            dataGridViewCellStyle48.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle48.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle48.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle48.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSection3.RowsDefaultCellStyle = dataGridViewCellStyle48;
            this.dgvSection3.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvSection3.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSection3.Size = new System.Drawing.Size(180, 25);
            this.dgvSection3.TabIndex = 55;
            // 
            // dataGridViewTextBoxColumn26
            // 
            dataGridViewCellStyle46.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle46.NullValue = null;
            this.dataGridViewTextBoxColumn26.DefaultCellStyle = dataGridViewCellStyle46;
            this.dataGridViewTextBoxColumn26.HeaderText = "TOTAL CHARGES";
            this.dataGridViewTextBoxColumn26.Name = "dataGridViewTextBoxColumn26";
            this.dataGridViewTextBoxColumn26.Width = 180;
            // 
            // dgvSection2
            // 
            this.dgvSection2.AllowUserToAddRows = false;
            this.dgvSection2.AllowUserToDeleteRows = false;
            dataGridViewCellStyle49.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle49.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle49.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle49.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle49.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSection2.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle49;
            this.dgvSection2.BackgroundColor = System.Drawing.Color.Gray;
            this.dgvSection2.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvSection2.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle50.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle50.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle50.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle50.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle50.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle50.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle50.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSection2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle50;
            this.dgvSection2.ColumnHeadersHeight = 25;
            this.dgvSection2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SUBTOTAL,
            this.dataGridViewTextBoxColumn25});
            dataGridViewCellStyle53.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle53.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle53.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle53.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle53.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle53.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle53.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSection2.DefaultCellStyle = dataGridViewCellStyle53;
            this.dgvSection2.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSection2.GridColor = System.Drawing.Color.Black;
            this.dgvSection2.Location = new System.Drawing.Point(600, 335);
            this.dgvSection2.MultiSelect = false;
            this.dgvSection2.Name = "dgvSection2";
            this.dgvSection2.RowHeadersVisible = false;
            dataGridViewCellStyle54.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle54.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle54.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle54.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSection2.RowsDefaultCellStyle = dataGridViewCellStyle54;
            this.dgvSection2.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvSection2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSection2.Size = new System.Drawing.Size(180, 25);
            this.dgvSection2.TabIndex = 54;
            // 
            // SUBTOTAL
            // 
            dataGridViewCellStyle51.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.SUBTOTAL.DefaultCellStyle = dataGridViewCellStyle51;
            this.SUBTOTAL.HeaderText = "SUB TOTAL";
            this.SUBTOTAL.Name = "SUBTOTAL";
            this.SUBTOTAL.Width = 80;
            // 
            // dataGridViewTextBoxColumn25
            // 
            dataGridViewCellStyle52.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn25.DefaultCellStyle = dataGridViewCellStyle52;
            this.dataGridViewTextBoxColumn25.HeaderText = "AMOUNT";
            this.dataGridViewTextBoxColumn25.Name = "dataGridViewTextBoxColumn25";
            // 
            // dgvSectionRental
            // 
            this.dgvSectionRental.AllowUserToAddRows = false;
            this.dgvSectionRental.AllowUserToDeleteRows = false;
            dataGridViewCellStyle55.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle55.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle55.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle55.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle55.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionRental.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle55;
            this.dgvSectionRental.BackgroundColor = System.Drawing.Color.Gray;
            this.dgvSectionRental.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvSectionRental.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle56.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle56.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle56.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle56.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle56.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle56.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle56.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSectionRental.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle56;
            this.dgvSectionRental.ColumnHeadersHeight = 25;
            this.dgvSectionRental.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.BillableDays,
            this.dataGridViewTextBoxColumn11});
            dataGridViewCellStyle65.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle65.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle65.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle65.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle65.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle65.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle65.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSectionRental.DefaultCellStyle = dataGridViewCellStyle65;
            this.dgvSectionRental.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSectionRental.GridColor = System.Drawing.Color.Black;
            this.dgvSectionRental.Location = new System.Drawing.Point(35, 304);
            this.dgvSectionRental.MultiSelect = false;
            this.dgvSectionRental.Name = "dgvSectionRental";
            this.dgvSectionRental.RowHeadersVisible = false;
            dataGridViewCellStyle66.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle66.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle66.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle66.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionRental.RowsDefaultCellStyle = dataGridViewCellStyle66;
            this.dgvSectionRental.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvSectionRental.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSectionRental.Size = new System.Drawing.Size(745, 25);
            this.dgvSectionRental.TabIndex = 53;
            // 
            // dataGridViewTextBoxColumn6
            // 
            dataGridViewCellStyle57.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            dataGridViewCellStyle57.Format = "N2";
            dataGridViewCellStyle57.NullValue = null;
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle57;
            this.dataGridViewTextBoxColumn6.HeaderText = "ITEM CODE";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Width = 125;
            // 
            // dataGridViewTextBoxColumn7
            // 
            dataGridViewCellStyle58.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            this.dataGridViewTextBoxColumn7.DefaultCellStyle = dataGridViewCellStyle58;
            this.dataGridViewTextBoxColumn7.HeaderText = "DESCRIPTION";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.Width = 170;
            // 
            // dataGridViewTextBoxColumn5
            // 
            dataGridViewCellStyle59.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle59.Format = "N2";
            dataGridViewCellStyle59.NullValue = null;
            this.dataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle59;
            this.dataGridViewTextBoxColumn5.HeaderText = "Start Date - End Date";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Width = 50;
            // 
            // dataGridViewTextBoxColumn8
            // 
            dataGridViewCellStyle60.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            dataGridViewCellStyle60.Format = "N2";
            dataGridViewCellStyle60.NullValue = null;
            this.dataGridViewTextBoxColumn8.DefaultCellStyle = dataGridViewCellStyle60;
            this.dataGridViewTextBoxColumn8.HeaderText = "SERIAL NUMBER";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            // 
            // dataGridViewTextBoxColumn9
            // 
            dataGridViewCellStyle61.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            this.dataGridViewTextBoxColumn9.DefaultCellStyle = dataGridViewCellStyle61;
            this.dataGridViewTextBoxColumn9.HeaderText = "PO#";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            // 
            // dataGridViewTextBoxColumn10
            // 
            dataGridViewCellStyle62.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            this.dataGridViewTextBoxColumn10.DefaultCellStyle = dataGridViewCellStyle62;
            this.dataGridViewTextBoxColumn10.HeaderText = "Rate";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.Width = 50;
            // 
            // BillableDays
            // 
            dataGridViewCellStyle63.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            this.BillableDays.DefaultCellStyle = dataGridViewCellStyle63;
            this.BillableDays.HeaderText = "Billable Days";
            this.BillableDays.Name = "BillableDays";
            this.BillableDays.Width = 50;
            // 
            // dataGridViewTextBoxColumn11
            // 
            dataGridViewCellStyle64.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn11.DefaultCellStyle = dataGridViewCellStyle64;
            this.dataGridViewTextBoxColumn11.HeaderText = "AMOUNT";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            // 
            // dgvSectionService
            // 
            this.dgvSectionService.AllowUserToAddRows = false;
            this.dgvSectionService.AllowUserToDeleteRows = false;
            dataGridViewCellStyle67.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle67.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle67.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle67.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle67.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionService.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle67;
            this.dgvSectionService.BackgroundColor = System.Drawing.Color.Gray;
            this.dgvSectionService.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvSectionService.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle68.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle68.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle68.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle68.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle68.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle68.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle68.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSectionService.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle68;
            this.dgvSectionService.ColumnHeadersHeight = 25;
            this.dgvSectionService.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn15,
            this.dataGridViewTextBoxColumn16,
            this.dataGridViewTextBoxColumn17,
            this.PO,
            this.PRICEEACH,
            this.AMOUNT});
            dataGridViewCellStyle76.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle76.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle76.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle76.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle76.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle76.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle76.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSectionService.DefaultCellStyle = dataGridViewCellStyle76;
            this.dgvSectionService.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSectionService.GridColor = System.Drawing.Color.Black;
            this.dgvSectionService.Location = new System.Drawing.Point(35, 262);
            this.dgvSectionService.MultiSelect = false;
            this.dgvSectionService.Name = "dgvSectionService";
            this.dgvSectionService.RowHeadersVisible = false;
            dataGridViewCellStyle77.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle77.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle77.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle77.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionService.RowsDefaultCellStyle = dataGridViewCellStyle77;
            this.dgvSectionService.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvSectionService.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSectionService.Size = new System.Drawing.Size(745, 25);
            this.dgvSectionService.TabIndex = 52;
            // 
            // dataGridViewTextBoxColumn4
            // 
            dataGridViewCellStyle69.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle69.Format = "N2";
            dataGridViewCellStyle69.NullValue = null;
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle69;
            this.dataGridViewTextBoxColumn4.HeaderText = "QUANTITY";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Width = 55;
            // 
            // dataGridViewTextBoxColumn15
            // 
            dataGridViewCellStyle70.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            dataGridViewCellStyle70.Format = "N2";
            dataGridViewCellStyle70.NullValue = null;
            this.dataGridViewTextBoxColumn15.DefaultCellStyle = dataGridViewCellStyle70;
            this.dataGridViewTextBoxColumn15.HeaderText = "ITEM CODE";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.Width = 140;
            // 
            // dataGridViewTextBoxColumn16
            // 
            dataGridViewCellStyle71.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            this.dataGridViewTextBoxColumn16.DefaultCellStyle = dataGridViewCellStyle71;
            this.dataGridViewTextBoxColumn16.HeaderText = "DESCRIPTION";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.Width = 170;
            // 
            // dataGridViewTextBoxColumn17
            // 
            dataGridViewCellStyle72.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            dataGridViewCellStyle72.Format = "N2";
            dataGridViewCellStyle72.NullValue = null;
            this.dataGridViewTextBoxColumn17.DefaultCellStyle = dataGridViewCellStyle72;
            this.dataGridViewTextBoxColumn17.HeaderText = "SERIAL NUMBER";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            // 
            // PO
            // 
            dataGridViewCellStyle73.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            this.PO.DefaultCellStyle = dataGridViewCellStyle73;
            this.PO.HeaderText = "PO#";
            this.PO.Name = "PO";
            // 
            // PRICEEACH
            // 
            dataGridViewCellStyle74.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.PRICEEACH.DefaultCellStyle = dataGridViewCellStyle74;
            this.PRICEEACH.HeaderText = "PRICE EACH";
            this.PRICEEACH.Name = "PRICEEACH";
            this.PRICEEACH.Width = 80;
            // 
            // AMOUNT
            // 
            dataGridViewCellStyle75.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.AMOUNT.DefaultCellStyle = dataGridViewCellStyle75;
            this.AMOUNT.HeaderText = "AMOUNT";
            this.AMOUNT.Name = "AMOUNT";
            // 
            // dgvSectionHeader5
            // 
            this.dgvSectionHeader5.AllowUserToAddRows = false;
            this.dgvSectionHeader5.AllowUserToDeleteRows = false;
            dataGridViewCellStyle78.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle78.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle78.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle78.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle78.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeader5.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle78;
            this.dgvSectionHeader5.BackgroundColor = System.Drawing.Color.Gray;
            this.dgvSectionHeader5.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvSectionHeader5.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle79.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle79.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle79.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle79.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle79.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle79.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle79.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSectionHeader5.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle79;
            this.dgvSectionHeader5.ColumnHeadersHeight = 30;
            this.dgvSectionHeader5.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.InvoiceNote});
            dataGridViewCellStyle81.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle81.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle81.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle81.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle81.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle81.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle81.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSectionHeader5.DefaultCellStyle = dataGridViewCellStyle81;
            this.dgvSectionHeader5.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSectionHeader5.GridColor = System.Drawing.Color.Black;
            this.dgvSectionHeader5.Location = new System.Drawing.Point(35, 222);
            this.dgvSectionHeader5.MultiSelect = false;
            this.dgvSectionHeader5.Name = "dgvSectionHeader5";
            this.dgvSectionHeader5.RowHeadersVisible = false;
            dataGridViewCellStyle82.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle82.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle82.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle82.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeader5.RowsDefaultCellStyle = dataGridViewCellStyle82;
            this.dgvSectionHeader5.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvSectionHeader5.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSectionHeader5.Size = new System.Drawing.Size(745, 25);
            this.dgvSectionHeader5.TabIndex = 50;
            // 
            // InvoiceNote
            // 
            dataGridViewCellStyle80.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle80.Format = "N2";
            dataGridViewCellStyle80.NullValue = null;
            this.InvoiceNote.DefaultCellStyle = dataGridViewCellStyle80;
            this.InvoiceNote.HeaderText = "InvoiceNote";
            this.InvoiceNote.Name = "InvoiceNote";
            this.InvoiceNote.Width = 745;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle83.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle83.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle83.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle83.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle83.SelectionForeColor = System.Drawing.Color.Black;
            this.dataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle83;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.Gray;
            this.dataGridView1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dataGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dataGridView1.ColumnHeadersHeight = 30;
            this.dataGridView1.ColumnHeadersVisible = false;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn18});
            this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridView1.GridColor = System.Drawing.Color.Black;
            this.dataGridView1.Location = new System.Drawing.Point(35, 137);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            dataGridViewCellStyle84.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle84.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle84.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle84.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle84.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle84.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle84.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle84;
            this.dataGridView1.RowHeadersVisible = false;
            dataGridViewCellStyle85.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle85.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle85.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle85.SelectionForeColor = System.Drawing.Color.Black;
            this.dataGridView1.RowsDefaultCellStyle = dataGridViewCellStyle85;
            this.dataGridView1.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(745, 25);
            this.dataGridView1.TabIndex = 49;
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.HeaderText = "";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.Width = 745;
            // 
            // dgvSectionHeader2
            // 
            this.dgvSectionHeader2.AllowUserToAddRows = false;
            this.dgvSectionHeader2.AllowUserToDeleteRows = false;
            dataGridViewCellStyle86.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle86.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle86.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle86.SelectionBackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle86.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeader2.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle86;
            this.dgvSectionHeader2.BackgroundColor = System.Drawing.Color.White;
            this.dgvSectionHeader2.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvSectionHeader2.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle87.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle87.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle87.Font = new System.Drawing.Font("Microsoft Sans Serif", 2.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle87.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle87.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle87.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle87.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSectionHeader2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle87;
            this.dgvSectionHeader2.ColumnHeadersHeight = 50;
            this.dgvSectionHeader2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn19,
            this.dataGridViewTextBoxColumn21});
            dataGridViewCellStyle90.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle90.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle90.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle90.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle90.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle90.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle90.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSectionHeader2.DefaultCellStyle = dataGridViewCellStyle90;
            this.dgvSectionHeader2.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSectionHeader2.GridColor = System.Drawing.Color.Black;
            this.dgvSectionHeader2.Location = new System.Drawing.Point(280, 70);
            this.dgvSectionHeader2.MultiSelect = false;
            this.dgvSectionHeader2.Name = "dgvSectionHeader2";
            this.dgvSectionHeader2.RowHeadersVisible = false;
            dataGridViewCellStyle91.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle91.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeader2.RowsDefaultCellStyle = dataGridViewCellStyle91;
            this.dgvSectionHeader2.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvSectionHeader2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSectionHeader2.Size = new System.Drawing.Size(500, 20);
            this.dgvSectionHeader2.TabIndex = 47;
            // 
            // dataGridViewTextBoxColumn19
            // 
            this.dataGridViewTextBoxColumn19.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle88.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            this.dataGridViewTextBoxColumn19.DefaultCellStyle = dataGridViewCellStyle88;
            this.dataGridViewTextBoxColumn19.HeaderText = "";
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.Width = 280;
            // 
            // dataGridViewTextBoxColumn21
            // 
            dataGridViewCellStyle89.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            this.dataGridViewTextBoxColumn21.DefaultCellStyle = dataGridViewCellStyle89;
            this.dataGridViewTextBoxColumn21.HeaderText = "INVOICE";
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            this.dataGridViewTextBoxColumn21.Width = 220;
            // 
            // dgvSectionHeader3
            // 
            this.dgvSectionHeader3.AllowUserToAddRows = false;
            this.dgvSectionHeader3.AllowUserToDeleteRows = false;
            dataGridViewCellStyle92.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle92.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle92.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle92.SelectionBackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle92.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeader3.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle92;
            this.dgvSectionHeader3.BackgroundColor = System.Drawing.Color.White;
            this.dgvSectionHeader3.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvSectionHeader3.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle93.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle93.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle93.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle93.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle93.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle93.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle93.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSectionHeader3.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle93;
            this.dgvSectionHeader3.ColumnHeadersHeight = 50;
            this.dgvSectionHeader3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.space1,
            this.BillTo,
            this.BillToAddress,
            this.ShipTo,
            this.ShipToAddress});
            dataGridViewCellStyle98.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            dataGridViewCellStyle98.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle98.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle98.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle98.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle98.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle98.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSectionHeader3.DefaultCellStyle = dataGridViewCellStyle98;
            this.dgvSectionHeader3.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSectionHeader3.GridColor = System.Drawing.Color.Black;
            this.dgvSectionHeader3.Location = new System.Drawing.Point(35, 96);
            this.dgvSectionHeader3.MultiSelect = false;
            this.dgvSectionHeader3.Name = "dgvSectionHeader3";
            this.dgvSectionHeader3.RowHeadersVisible = false;
            dataGridViewCellStyle99.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle99.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeader3.RowsDefaultCellStyle = dataGridViewCellStyle99;
            this.dgvSectionHeader3.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvSectionHeader3.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSectionHeader3.Size = new System.Drawing.Size(745, 20);
            this.dgvSectionHeader3.TabIndex = 46;
            // 
            // space1
            // 
            this.space1.HeaderText = "space1";
            this.space1.Name = "space1";
            this.space1.Width = 35;
            // 
            // BillTo
            // 
            this.BillTo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle94.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            this.BillTo.DefaultCellStyle = dataGridViewCellStyle94;
            this.BillTo.HeaderText = "BillTo";
            this.BillTo.Name = "BillTo";
            this.BillTo.Width = 80;
            // 
            // BillToAddress
            // 
            dataGridViewCellStyle95.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            this.BillToAddress.DefaultCellStyle = dataGridViewCellStyle95;
            this.BillToAddress.HeaderText = "BillToAddress";
            this.BillToAddress.Name = "BillToAddress";
            this.BillToAddress.Width = 300;
            // 
            // ShipTo
            // 
            dataGridViewCellStyle96.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            this.ShipTo.DefaultCellStyle = dataGridViewCellStyle96;
            this.ShipTo.HeaderText = "ShipTo";
            this.ShipTo.Name = "ShipTo";
            this.ShipTo.Width = 80;
            // 
            // ShipToAddress
            // 
            dataGridViewCellStyle97.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            this.ShipToAddress.DefaultCellStyle = dataGridViewCellStyle97;
            this.ShipToAddress.HeaderText = "ShipToAddress";
            this.ShipToAddress.Name = "ShipToAddress";
            this.ShipToAddress.Width = 250;
            // 
            // dgvSectionHeader1
            // 
            this.dgvSectionHeader1.AllowUserToAddRows = false;
            this.dgvSectionHeader1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle100.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle100.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle100.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle100.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle100.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeader1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle100;
            this.dgvSectionHeader1.BackgroundColor = System.Drawing.Color.Gray;
            this.dgvSectionHeader1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvSectionHeader1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle101.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle101.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle101.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle101.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle101.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle101.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle101.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSectionHeader1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle101;
            this.dgvSectionHeader1.ColumnHeadersHeight = 22;
            this.dgvSectionHeader1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn20});
            dataGridViewCellStyle103.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle103.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle103.Font = new System.Drawing.Font("Microsoft Sans Serif", 2.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle103.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle103.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle103.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle103.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSectionHeader1.DefaultCellStyle = dataGridViewCellStyle103;
            this.dgvSectionHeader1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSectionHeader1.GridColor = System.Drawing.Color.Black;
            this.dgvSectionHeader1.Location = new System.Drawing.Point(595, 23);
            this.dgvSectionHeader1.MultiSelect = false;
            this.dgvSectionHeader1.Name = "dgvSectionHeader1";
            this.dgvSectionHeader1.RowHeadersVisible = false;
            dataGridViewCellStyle104.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle104.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle104.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle104.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeader1.RowsDefaultCellStyle = dataGridViewCellStyle104;
            this.dgvSectionHeader1.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvSectionHeader1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSectionHeader1.Size = new System.Drawing.Size(150, 22);
            this.dgvSectionHeader1.TabIndex = 45;
            // 
            // dataGridViewTextBoxColumn20
            // 
            dataGridViewCellStyle102.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            this.dataGridViewTextBoxColumn20.DefaultCellStyle = dataGridViewCellStyle102;
            this.dataGridViewTextBoxColumn20.HeaderText = "INVOICE NUMBER";
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            this.dataGridViewTextBoxColumn20.Width = 150;
            // 
            // tabSections
            // 
            this.tabSections.Location = new System.Drawing.Point(4, 22);
            this.tabSections.Name = "tabSections";
            this.tabSections.Size = new System.Drawing.Size(824, 455);
            this.tabSections.TabIndex = 3;
            this.tabSections.Text = "Sections";
            this.tabSections.UseVisualStyleBackColor = true;
            // 
            // frmOTInvoicesPrint
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(856, 498);
            this.Controls.Add(this.tabControl1);
            this.Name = "frmOTInvoicesPrint";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.tabControl1.ResumeLayout(false);
            this.tabHeader.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEndOTInvoices)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEndGroupMsg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmptyMessage)).EndInit();
            this.tabHeaderSections.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSection4B)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSection4A)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeader4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSection3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSection2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionRental)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionService)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeader5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeader2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeader3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeader1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabHeader;
        private System.Windows.Forms.TabPage tabHeaderSections;
        private System.Windows.Forms.TabPage tabSections;
        private System.Windows.Forms.DataGridView dgvGroup2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn31;
        private System.Windows.Forms.DataGridView dgvEndOTInvoices;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridView dgvEndGroupMsg;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.Label lblFooter;
        private System.Windows.Forms.DataGridView dgvEmptyMessage;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn32;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridView dgvSectionHeader2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.DataGridView dgvSectionHeader3;
        private System.Windows.Forms.DataGridViewTextBoxColumn space1;
        private System.Windows.Forms.DataGridViewTextBoxColumn BillTo;
        private System.Windows.Forms.DataGridViewTextBoxColumn BillToAddress;
        private System.Windows.Forms.DataGridViewTextBoxColumn ShipTo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ShipToAddress;
        private System.Windows.Forms.DataGridView dgvSectionHeader1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private System.Windows.Forms.DataGridView dgvSectionHeader5;
        private System.Windows.Forms.DataGridViewTextBoxColumn InvoiceNote;
        private System.Windows.Forms.DataGridView dgvSectionService;
        private System.Windows.Forms.DataGridView dgvSection3;
        private System.Windows.Forms.DataGridView dgvSection2;
        private System.Windows.Forms.DataGridView dgvSectionRental;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn26;
        private System.Windows.Forms.DataGridViewTextBoxColumn SUBTOTAL;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn25;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn PO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PRICEEACH;
        private System.Windows.Forms.DataGridViewTextBoxColumn AMOUNT;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn BillableDays;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridView dgvGroup1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridView dgvSection4B;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridView dgvSection4A;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridView dgvSectionHeader4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn SALESPERSON;
        private System.Windows.Forms.DataGridViewTextBoxColumn DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn TAKER;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUSTOMERPONUMBER;
        private System.Windows.Forms.DataGridViewTextBoxColumn TERMS;
    }
}
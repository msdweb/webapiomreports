﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace OTInvoices
{
    class dataDetails
    {

        public string Get_LastReview()
        {
            string LastReview = "Medical Specialties Distributors, LLC";
            LastReview = "";
            return LastReview;
        }

        //-------------------------------------------------------------------------------------

        public DataView DV_GroupEnd1_Fill(DataView oDV)
        {

            string str1 = "These General Terms and Conditions govern all sales made by Medical Specialties Distributors, LLC. (“MSD”), unless covered by a prior existing written contract signed by MSD.  Terms and conditions submitted that are inconsistent with these or modify or add to these, are rejected unless made in writing and signed by MSD.  These terms and conditions of sale exclusively govern the sale of all products and/or services (“Products”) sold by MSD.  If these terms and conditions are not acceptable to Customer, then Customer must so notify MSD immediately, in writing.";
            string str2 = "PRICING AND PAYMENT:";
            string str3 = "All invoices shall be payable in accordance with the terms of this invoice.  Invoice amounts shall be deemed paid when payment is received by MSD.  Any taxes imposed upon the Products, their sale, delivery use or consumption shall be the responsibility of the Customer.  All sums not paid by Customer within such thirty (30) day period shall accrue interest at the lower of two percent (2%) per month or the maximum interest rate allowed by law, prorated on a daily basis.  Customer shall be liable for MSD’s cost of collection, including, without limitation, reasonable attorney fees and disbursements.  Any disputed amounts should be reported immediately and remitted with the undisputed amount by the payment due date.  If MSD agrees with the billing dispute, MSD will credit Customer the amount of the agreed upon billing dispute.  Customer understands that failure to pay an invoiced amount when due shall constitute a material breach of the contract between MSD and Customer.  Without prejudice to any of its other rights, MSD may withhold shipments of any orders if Customer has not paid an invoice when due.";

            string str4 = "SHIPPING AND HANDLING:";
            string str5 = "Except as expressly set forth on the face hereof, all orders totaling less than $200.00 will be subject to a $15.00 handling charge and all normal freight charges will apply.  Customer orders totaling $500.00 or more shall be shipped free of freight charges, F.O.B. shipping point via normal ground delivery, with the exception of products that have weight and dimensional freight surcharges, e.g. including but not limited to, all I.V. irrigation and cleaning solutions, enteral formulas, disposable incontinence products, I.V. poles, cold/hot packs, sharps containers, “mail back systems and reclosable poly bags, such products will be assessed freight charges regardless of order value.";
            str5 += "Title to products and all risk of loss and damage shall pass to Customer upon delivery to a common carrier as selected by MSD unless specified by Customer in its order.  Service charges will be added to the invoice for expedited shipping or specialized service requests.  Request for proof of delivery must be submitted in writing within 30 days from the date of shipment.  MSD shall have no responsibility for supplying proof of delivery for shipments older than 30 days.  It is understood that no request for proof of delivery is an acknowledgement that the Product(s) as invoiced, has been received.";

            string str6 = "RETURNS:";
            string str7 = "Please call our Customer Service Department at 800-967-6400 within 48 hours of receipt of a shipment to obtain a Return Goods Authorization number (RGA).  All product returned to MSD must be freight prepaid, have an RGA number, be in the original manufacturers packaging and be suitable for resale.  Credit for returned goods are conditioned upon MSD’s inspection and approval of such goods upon their return.  Certain items that are not eligible for return include items that have deteriorated because of improper handling, abuse or other factors beyond the control of MSD, partial cases, open packages, expired items, or special orders that are not returnable to the original manufacturer.  All returns are subject to a minimum restocking charge of 20% of the invoice amount or $25.00, whichever is greater.  All cost imposed upon MSD for returns to the original manufacturer shall be reimbursed by Customer.  No credit will be issued after thirty (30) days following the date of purchase.";

            string str8 = "DAMAGED GOODS:";
            string str9 = "Count and inspect  your delivery before the carrier departs, shipments with known and/or visible damage upon delivery should be refused and returned to the carrier.  If Customer accepts delivery of product with visible damage, it must be reported to the carrier and documented on the Bill of Lading at the time of delivery in order to avoid being held responsible for payment of the product delivered.  Acceptance of a package with no known or visible sign of damage that upon full inspection is determined to have been damaged in transit must be reported to MSD’s Customer Service Department within 48 hours in order to obtain further instructions.";

            string str10 = "LIMITATIONS OF LIABILITIES AND WARRANTIES:";
            string str11 = "IN NO EVENT SHALL EITHER PARTY BE LIABLE FOR (I) ANY SPECIAL, CONSEQUENTIAL, INCIDENTAL, OR RELIANCE DAMAGES, OR (II) ANY COST OF PROCUREMENT OF SUBSTITUTE HEALTHCARE PRODUCTS, RENTAL EQUIPMENT OR EQUIPMENT SERVICES, OR (III) ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE OR PROFITS THAT THE PARTY MAY SUFFER, DIRECTLY OR INDIRECTLY7, REGARDLESS OF THE FORM OF THE ACTION, WHETHER IN CONTRACT, TORT (INCLUDING NEGLIGENCE), STRICT PRODUCT LIABILITY OR OTHERWISE, EVEN IF THE OFFENDING PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.  IN NO EVENT SHALL MSD BE LIABLE FOR ANY DAMAGES IN EXCESS OF AMOUNTS THAT THE CUSTOMER HAS PAID TO MSD IN RENTAL FEES WITH RESPECT TO SUCH DEVICE OR THE HEALTHCARE PRODUCT OR EQUIPMENT SERVICES WITH RESPECT TO SUCH PRODUCT OR SERVICE.";
            str11 += "EXCEPT AS PROVIDED HEREIN, ALL WARRANTIES ARE HEREBY DISCLAIMED, WHETHER EXPRESSED OR IMPLIED OR ARISING BY OPERATION OF LAW, TRADE, USAGE OR COURSE OF DEALING, INCLUDING BUT NOT LIMITED TO MERCHANTABILITY AND WARRANTIES OF FITNESS FOR A PARTICULAR PURPOSE.  PRODUCTS DISTRIBUTED, BUT NOT MANUFACTURED, BY MSD ARE NOT WARRANTED BY MSD AND CUSTOMER MUST INSTEAD RELY ON THE REPRESENTATIONS AND WARRANTIES, IF ANY, PROVIDED DIRECTLY TO THE CUSTOMER BY THE MANUFACTURER OF SUCH PRODUCTS.";

            string str12 = "GOVERNING LAW:";
            string str13 = "All sales are made subject to and shall be governed by and construed in accordance with the laws of the Commonwealth of Massachusetts, without regard to conflict of laws and principle thereof.  Other than as necessary to enforce any final judgment, award or determination, any action brought pursuant to or in connection with this transaction shall be brought only in the state courts of, or the federal courts in, the Commonwealth of Massachusetts without  regard to its conflict of laws provisions.  In any such action, both parties submit  to the personal jurisdiction of the state courts of, and federal courts located in, the Commonwealth of Massachusetts and waive any objections to venue of such courts.";

            oDV = DV_GroupEnd_Open();

            oDV = DV_GroupEnd_Pop(oDV, str1);
            oDV = DV_GroupEnd_Pop(oDV, str2);
            oDV = DV_GroupEnd_Pop(oDV, str3);
            oDV = DV_GroupEnd_Pop(oDV, str4);
            oDV = DV_GroupEnd_Pop(oDV, str5);
            oDV = DV_GroupEnd_Pop(oDV, str6);
            oDV = DV_GroupEnd_Pop(oDV, str7);
            oDV = DV_GroupEnd_Pop(oDV, str8);
            oDV = DV_GroupEnd_Pop(oDV, str9);
            oDV = DV_GroupEnd_Pop(oDV, str10);
            oDV = DV_GroupEnd_Pop(oDV, str11);
            oDV = DV_GroupEnd_Pop(oDV, str12);
            oDV = DV_GroupEnd_Pop(oDV, str13);

            return oDV;
        }

        public DataView DV_GroupEnd2_Fill(DataView oDV)
        {

            string str1 = " 1. RENTAL EQUIPMENT SCHEDULE: The terms and conditions set forth below govern the rental, from MSD to CUSTOMER, of all biomedical Rental Equipment (the “Rental Equipment”), hereinafter referred to as “Rental Equipment,” from MSD.  ";

            string str2 = " 2. RENTAL EQUIPMENT BILLING: CUSTOMER shall receive an invoice for Rental Equipment from MSD on a monthly basis.  MSD’s periodic invoices will list each unit of Rental Equipment in the possession of CUSTOMER during the previous rental period.  It is the responsibility of CUSTOMER to review each periodic rental invoice and notify MSD of any Rental Equipment, which is not in the possession of CUSTOMER.  Such discrepancies must be reported to MSD in writing within thirty (30) calendar days of CUSTOMER’S receipt of the invoice, or CUSTOMER will be deemed to have acknowledged possession of the Rental Equipment.  ";

            string str3 = " 3. PAYMENT OF FEES:  Payment of the invoice amounts by CUSTOMER shall be due, in accordance with instructions on the relevant invoice, within thirty (30) days of the date of such invoice.  Invoice amounts shall be deemed paid when payment is received by MSD.  All sums not paid by CUSTOMER within such thirty (30) day period shall accrue interest at the lower of two percent (2%) per month or the maximum interest rate allowed by law, prorated on a daily basis.  CUSTOMER shall be liable for MSD’s cost of collection, including, without limitation, disbursements and costs and reasonable attorney fees.  ";

            string str4 = " 4. ACCEPTANCE OF RENTAL EQUIPMENT:  With each rental order, Customer will receive a packing slip, detailing all Rental Equipment and accessories shipped.  All Rental Equipment and accessories shall be deemed accepted by CUSTOMER as conforming and free from defect unless CUSTOMER notifies MSD in writing, within ten (10) days of receipt by CUSTOMER, that the Rental Equipment and accessories delivered do not conform to CUSTOMER’S order or are defective.  ";

            string str5 = " 5. REPAIR:  In the event that MSD determines that malfunctioning of Rental Equipment is a result of accident, alteration, misuse, damage, repair by other than an MSD authorized representative, or, in the event that, at the time of failure, such Rental Equipment was not being operated in the manner prescribed in the original manufacturers operating manual, MSD will charge to CUSTOMER and CUSTOMER agrees to pay MSD for either the cost of repair to bring the Rental Equipment to original manufacturer specifications or the replacement cost of the Rental Equipment.  Under no circumstances will previously remitted rental fees be applied to cover the cost of repair or replacement. ";

            string str6 = " 6. RETURN PROCEDURE:  CUSTOMER may terminate the rental of any Rental Equipment under this Agreement by phoning MSD’s Customer Service Department at the then current MSD telephone number and expressing intent to terminate.  MSD shall then issue CUSTOMER a Returned Goods Authorization Number (an “RGA number”).  The rental term and the accrual of rental fees shall be deemed to terminate on the date that MSD receives the returned Rental Equipment. Risk of loss shall remain with CUSTOMER, until such returned Rental Equipment is actually received by MSD.  CUSTOMER shall also return all manuals and accessories within the time limitation provided for the return of the Rental Equipment. Upon termination, for any reason, of a rental under this Agreement, CUSTOMER must return Rental Equipment in good and workable condition, ordinary wear and tear accepted. CUSTOMER shall be liable for any loss of or damage to the Rental Equipment or accessories, occurring between the date that the Rental Equipment is accepted or deemed to be accepted and the date that the Rental Equipment is returned and received by MSD (the “Possession Period”).  If the Rental Equipment or accessories are lost, damaged or destroyed during the Possession Period, CUSTOMER shall pay the full cost of either the repair or the replacement of the Rental Equipment or accessories.  Under no circumstances will previously remitted rental fees be applied to cover the cost of repair or replacement. ";

            string str7 = " 7. FREIGHT:  If CUSTOMER requests special packaging, the cost of such packaging shall be borne by CUSTOMER. MSD shall ship in accordance with its standard practices, and MSD shall select a carrier reasonably acceptable to CUSTOMER. ";

            string str8 = " 8. TITLE:  All Rental Equipment and accessories shall remain the sole and exclusive property of MSD.  CUSTOMER shall have no right, title or interest to or in the Rental Equipment and accessories, except the right to make use thereof and the right to maintain possession thereof in accordance with the terms of this Agreement.  MSD hereby represents and warrants to CUSTOMER that MSD has the full legal right and authority to rent the Rental Equipment to CUSTOMER as set forth herein.  CUSTOMER agrees to clearly identify all Equipment rented hereunder as MSD property.  In accordance with § 9-509(a)(1) of the Uniform Commercial Code ('UCC'), CUSTOMER hereby authorizes the filing by MSD at any time from and after the date of this Agreement (including without limitation prior to the execution and delivery of a security agreement in accordance with § 9-502(d) of the UCC) of initial Uniform Commercial Code financing statements, and to the extent necessary or appropriate, amendments thereto, in all appropriate jurisdictions which reflect the ownership of title and any and all other valid security interests taken in favor of MSD with respect to any and all Equipment rented hereunder. ";

            string str9 = " 9. ASSIGN AND MODIFY:  CUSTOMER may not assign or transfer any rights duties or obligations hereunder without the prior written consent of MSD.  This Agreement, including all attachments hereto, constitutes the entire agreement between CUSTOMER and MSD and shall not be amended without written consent of both parties.  This Agreement shall be governed by and interpreted in accordance with the laws of the Commonwealth of Massachusetts, without regards to conflict of laws of principles thereof. ";

            string str10 = " 10. LIMIT OF LIABILITY:  In no event will MSD be liable for (I) any special, consequential, or incidental damage or (II) any cost or procurement of substitute products or services or (III) any damages whatsoever resulting from loss of use or profits that CUSTOMER may suffer directly or indirectly, regardless of the form of the action, whether in contract, tort (including negligence), strict product liability or otherwise, even if MSD has been advised of the possibility of such damage or (iv) any damages in excess of amounts that the CUSTOMER has paid to MSD in rental fees with respect to such device or (v) any damages arising from breach by CUSTOMER of its obligations with respect to proper installation and periodic maintenance of equipment, or any intentional, willful or negligent acts or omissions by CUSTOMER or its employees or agents with respect to patients.  This limitation of liability applies to all causes of action including, without limitation breach of contract, breach of warranty, negligence, strict liability, misrepresentation and other torts. ";

            string str11 = " 11. INDEMNIFICATION:  CUSTOMER agrees to indemnify, hold harmless and defend MSD from and against all losses, claims, suits, damages, actions, causes of action, proceedings, demands, assessments, settlements, judgments, costs, expenses, and any other liabilities of any kind or nature, including reasonable attorneys’ fees, imposed on or arising out of, or relating to CUSTOMER’S negligence or willful act or omission.  MSD shall not be required to take any action or make any claim against a third person as a precondition to seeking indemnification hereunder.  MSD shall timely notify CUSTOMER of any intended claim for indemnification. ";

            string str12 = " 12. GENERAL PROVISIONS: This document contains the entire agreement relating to the subject matter contained herein and supersedes all prior or contemporaneous agreements, written or oral, between the parties Agreement. If any provision of this Agreement is held by a court of competent jurisdiction to be invalid or unenforceable such provision shall be severed from this Agreement and the remaining provisions will remain in full force and effect. ";

            oDV = DV_GroupEnd_Open();

            oDV = DV_GroupEnd_Pop(oDV, str1);
            oDV = DV_GroupEnd_Pop(oDV, str2);
            oDV = DV_GroupEnd_Pop(oDV, str3);
            oDV = DV_GroupEnd_Pop(oDV, str4);
            oDV = DV_GroupEnd_Pop(oDV, str5);
            oDV = DV_GroupEnd_Pop(oDV, str6);
            oDV = DV_GroupEnd_Pop(oDV, str7);
            oDV = DV_GroupEnd_Pop(oDV, str8);
            oDV = DV_GroupEnd_Pop(oDV, str9);
            oDV = DV_GroupEnd_Pop(oDV, str10);
            oDV = DV_GroupEnd_Pop(oDV, str11);
            oDV = DV_GroupEnd_Pop(oDV, str12);

            return oDV;
        }

        private DataView DV_GroupEnd_Open()
        {
            DataTable oDT = new DataTable("GroupEnd");
            //---------------------------------------------------------------
            DataColumn ALTCOLOR = new DataColumn("ALTCOLOR");
            ALTCOLOR.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(ALTCOLOR);

            DataColumn TEXT = new DataColumn("TEXT");
            TEXT.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT);

            //---------------------------------------------------------------

            DataView oDV = new DataView();
            oDV.Table = oDT;
            return oDV;
        }

        //-------------------------------------------------------------------------------------

        private DataView DV_GroupEnd_Pop(DataView oDV, string strVal1)
        {
            DataRow oDR = oDV.Table.NewRow();

            oDR["ALTCOLOR"] = "";
            oDR["TEXT"] = strVal1;
            //--------------------------------------- 
            oDV.Table.Rows.Add(oDR);
            oDR = null;
            oDR = oDV.Table.NewRow();

            return oDV;
        }

        //-------------------------------------------------------------------------------------

        public DataView DV_SectionHeader2_Fill(DataView oDV, string strCustomerID, string strInvoiceNumber)
        {
            oDV = DV_SectionHeader_Open();

            oDV = DV_SectionHeader_Pop(oDV, strCustomerID, strInvoiceNumber);
            return oDV;
        }

        public DataView DV_PageHeader_Fill(DataView oDV)
        {
            oDV = DV_SectionHeader_Open();

            oDV = DV_SectionHeader_Pop(oDV, "Medical Specialties Distributors, LLC", "");
            oDV = DV_SectionHeader_Pop(oDV, "PO Box 206639", "FOR ALL INQUIRIES CONCERNING THIS ORDER");
            oDV = DV_SectionHeader_Pop(oDV, "Dallas, TX  75320-6639", "PLEASE CALL (800)967-6400");
            oDV = DV_SectionHeader_Pop(oDV, "", "");

            return oDV;
        }

        private DataView DV_SectionHeader_Open()
        {
            DataTable oDT = new DataTable("PageHeader");
            //---------------------------------------------------------------
            DataColumn ALTCOLOR = new DataColumn("ALTCOLOR");
            ALTCOLOR.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(ALTCOLOR);

            DataColumn LABEL = new DataColumn("LABEL");
            LABEL.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(LABEL);

            DataColumn TEXT = new DataColumn("TEXT");
            TEXT.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT);

            //---------------------------------------------------------------

            DataView oDV = new DataView();
            oDV.Table = oDT;
            return oDV;
        }

        //-------------------------------------------------------------------------------------

        private DataView DV_SectionHeader_Pop(DataView oDV, string strVal1, string strVal2)
        {
            DataRow oDR = oDV.Table.NewRow();

            oDR["ALTCOLOR"] = "";
            oDR["LABEL"] = strVal1;
            oDR["TEXT"] = strVal2;
            //--------------------------------------- 
            oDV.Table.Rows.Add(oDR);
            oDR = null;
            oDR = oDV.Table.NewRow();

            return oDV;
        }

        //-------------------------------------------------------------------------------------

        public DataView DV_SectionHeader3_Fill(DataView oDV, string strBillTo, string strShipTo)
        {

            oDV = DV_SectionHeader3_Open();
            oDV = DV_SectionHeader3_Pop(oDV, "BILL TO", strBillTo, "SHIP TO", strShipTo);
            return oDV;
        }

        //-----------------------------------------------------------------------        

        private DataView DV_SectionHeader3_Open()
        {
            DataTable oDT = new DataTable("PageHeader");
            //---------------------------------------------------------------
            DataColumn ALTCOLOR = new DataColumn("ALTCOLOR");
            ALTCOLOR.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(ALTCOLOR);

            DataColumn LABEL0 = new DataColumn("LABEL0");
            LABEL0.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(LABEL0);

            DataColumn LABEL1 = new DataColumn("LABEL1");
            LABEL1.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(LABEL1);

            DataColumn TEXT1 = new DataColumn("TEXT1");
            TEXT1.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT1);

            DataColumn LABEL2 = new DataColumn("LABEL2");
            LABEL2.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(LABEL2);

            DataColumn TEXT2 = new DataColumn("TEXT2");
            TEXT2.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT2);

            //---------------------------------------------------------------

            DataView oDV = new DataView();
            oDV.Table = oDT;
            return oDV;
        }

        //-------------------------------------------------------------------------------------

        private DataView DV_SectionHeader3_Pop(DataView oDV, string strVal11, string strVal12, string strVal21, string strVal22)
        {
            DataRow oDR = oDV.Table.NewRow();

            oDR["ALTCOLOR"] = "";
            oDR["LABEL0"] = "";
            oDR["LABEL1"] = strVal11;
            oDR["TEXT1"] = strVal12;
            oDR["LABEL2"] = strVal21;
            oDR["TEXT2"] = strVal22;
            //--------------------------------------- 
            oDV.Table.Rows.Add(oDR);
            oDR = null;
            oDR = oDV.Table.NewRow();

            return oDV;
        }

        //-----------------------------------------------------------------------        
        //-----------------------------------------------------------------------        
        //-----------------------------------------------------------------------        

        public DataView DV_SectionHeader4_Fill(DataView oDV, string strVal1, string strVal2, string strVal3, string strVal4, string strVal5, string strVal6)
        {
            oDV = DV_SectionHeader4_Open();
            oDV = DV_SectionHeader4_Pop(oDV, strVal1, strVal2, strVal3, strVal4, strVal5, strVal6);

            return oDV;
        }

        //-----------------------------------------------------------------------        

        public DataView DV_SectionHeader4_Open()
        {
            DataTable oDT = new DataTable("PageHeader");
            //---------------------------------------------------------------
            DataColumn ALTCOLOR = new DataColumn("ALTCOLOR");
            ALTCOLOR.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(ALTCOLOR);

            DataColumn TEXT1 = new DataColumn("TEXT1");
            TEXT1.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT1);

            DataColumn TEXT2 = new DataColumn("TEXT2");
            TEXT2.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT2);

            DataColumn TEXT3 = new DataColumn("TEXT3");
            TEXT3.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT3);

            DataColumn TEXT4 = new DataColumn("TEXT4");
            TEXT4.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT4);

            DataColumn TEXT5 = new DataColumn("TEXT5");
            TEXT5.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT5);

            DataColumn TEXT6 = new DataColumn("TEXT6");
            TEXT6.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT6);
            //---------------------------------------------------------------

            DataView oDV = new DataView();
            oDV.Table = oDT;
            return oDV;
        }

        //-------------------------------------------------------------------------------------

        private DataView DV_SectionHeader4_Pop(DataView oDV, string strVal1, string strVal2, string strVal3, string strVal4, string strVal5, string strVal6)
        {
            DataRow oDR = oDV.Table.NewRow();

            oDR["ALTCOLOR"] = "";
            oDR["TEXT1"] = strVal1;
            oDR["TEXT2"] = strVal2;
            oDR["TEXT3"] = strVal3;
            oDR["TEXT4"] = strVal4;
            oDR["TEXT5"] = strVal5;
            oDR["TEXT6"] = strVal6;
            //--------------------------------------- 
            oDV.Table.Rows.Add(oDR);
            oDR = null;
            oDR = oDV.Table.NewRow();

            return oDV;
        }

        //-----------------------------------------------------------------------        
        //-----------------------------------------------------------------------        
        //-----------------------------------------------------------------------        

        public DataView DV_SectionHeader5_Fill(DataView oDV, string strVal1)
        {
            oDV = DV_SectionHeader5_Open();
            oDV = DV_SectionHeader5_Pop(oDV, strVal1);

            return oDV;
        }

        //-----------------------------------------------------------------------        

        public DataView DV_SectionHeader5_Open()
        {
            DataTable oDT = new DataTable("PageHeader");
            //---------------------------------------------------------------
            DataColumn ALTCOLOR = new DataColumn("ALTCOLOR");
            ALTCOLOR.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(ALTCOLOR);

            DataColumn TEXT1 = new DataColumn("TEXT1");
            TEXT1.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT1);

            //---------------------------------------------------------------

            DataView oDV = new DataView();
            oDV.Table = oDT;
            return oDV;
        }

        //-------------------------------------------------------------------------------------

        private DataView DV_SectionHeader5_Pop(DataView oDV, string strVal1)
        {
            DataRow oDR = oDV.Table.NewRow();

            oDR["ALTCOLOR"] = "";
            oDR["TEXT1"] = strVal1;
            //--------------------------------------- 
            oDV.Table.Rows.Add(oDR);
            oDR = null;
            oDR = oDV.Table.NewRow();

            return oDV;
        }

        //-----------------------------------------------------------------------        
        //-----------------------------------------------------------------------        
        //-----------------------------------------------------------------------        

        public DataView DV_Section1_Fill(DataView oDV, string strVal1, string strVal2, string strVal3, string strVal4, string strVal5, string strVal6, string strVal7, string strVal8)
        {

            oDV = DV_Section1_Pop(oDV, strVal1, strVal2, strVal3, strVal4, strVal5, strVal6, strVal7, strVal8);

            return oDV;
        }

        //-----------------------------------------------------------------------        

        public DataView DV_Section1_Open()
        {
            DataTable oDT = new DataTable("PageHeader");
            //---------------------------------------------------------------
            DataColumn ALTCOLOR = new DataColumn("ALTCOLOR");
            ALTCOLOR.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(ALTCOLOR);

            DataColumn TEXT1 = new DataColumn("TEXT1");
            TEXT1.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT1);

            DataColumn TEXT2 = new DataColumn("TEXT2");
            TEXT2.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT2);

            DataColumn TEXT3 = new DataColumn("TEXT3");
            TEXT3.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT3);

            DataColumn TEXT4 = new DataColumn("TEXT4");
            TEXT4.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT4);

            DataColumn TEXT5 = new DataColumn("TEXT5");
            TEXT5.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT5);

            DataColumn TEXT6 = new DataColumn("TEXT6");
            TEXT6.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT6);

            DataColumn TEXT7 = new DataColumn("TEXT7");
            TEXT7.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT7);

            DataColumn TEXT8 = new DataColumn("TEXT8");
            TEXT8.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT8);
            //---------------------------------------------------------------

            DataView oDV = new DataView();
            oDV.Table = oDT;
            return oDV;
        }

        //-------------------------------------------------------------------------------------

        private DataView DV_Section1_Pop(DataView oDV, string strVal1, string strVal2, string strVal3, string strVal4, string strVal5, string strVal6, string strVal7, string strVal8)
        {
            DataRow oDR = oDV.Table.NewRow();

            oDR["ALTCOLOR"] = "";
            oDR["TEXT1"] = strVal1;
            oDR["TEXT2"] = strVal2;
            oDR["TEXT3"] = strVal3;
            oDR["TEXT4"] = strVal4;
            oDR["TEXT5"] = strVal5;
            oDR["TEXT6"] = strVal6;
            oDR["TEXT7"] = strVal7;
            oDR["TEXT8"] = strVal8;
            //--------------------------------------- 
            oDV.Table.Rows.Add(oDR);
            oDR = null;
            oDR = oDV.Table.NewRow();

            return oDV;
        }

        //-----------------------------------------------------------------------        
        //-----------------------------------------------------------------------        
        //-----------------------------------------------------------------------        

        public DataView DV_Section2_Fill(DataView oDV, string strVal1, string strVal2)
        {
            oDV = DV_Section2_Pop(oDV, strVal1, strVal2);

            return oDV;
        }

        //-----------------------------------------------------------------------        

        public DataView DV_Section2_Open()
        {
            DataTable oDT = new DataTable("PageHeader");
            //---------------------------------------------------------------
            DataColumn ALTCOLOR = new DataColumn("ALTCOLOR");
            ALTCOLOR.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(ALTCOLOR);

            DataColumn TEXT1 = new DataColumn("TEXT1");
            TEXT1.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT1);

            DataColumn TEXT2 = new DataColumn("TEXT2");
            TEXT2.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT2);

            //---------------------------------------------------------------

            DataView oDV = new DataView();
            oDV.Table = oDT;
            return oDV;
        }

        //-------------------------------------------------------------------------------------

        private DataView DV_Section2_Pop(DataView oDV, string strVal1, string strVal2)
        {
            DataRow oDR = oDV.Table.NewRow();

            oDR["ALTCOLOR"] = "";
            oDR["TEXT1"] = strVal1;
            oDR["TEXT2"] = strVal2;
            //--------------------------------------- 
            oDV.Table.Rows.Add(oDR);
            oDR = null;
            oDR = oDV.Table.NewRow();

            return oDV;
        }

        //-----------------------------------------------------------------------        
        //-----------------------------------------------------------------------        
        //-----------------------------------------------------------------------        

        public DataView DV_Section3_Fill(DataView oDV, string strVal1)
        {
            oDV = DV_Section3_Open();
            oDV = DV_Section3_Pop(oDV, strVal1);

            return oDV;
        }

        //-----------------------------------------------------------------------        

        public DataView DV_Section3_Open()
        {
            DataTable oDT = new DataTable("PageHeader");
            //---------------------------------------------------------------
            DataColumn ALTCOLOR = new DataColumn("ALTCOLOR");
            ALTCOLOR.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(ALTCOLOR);

            DataColumn TEXT1 = new DataColumn("TEXT1");
            TEXT1.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT1);

            //---------------------------------------------------------------

            DataView oDV = new DataView();
            oDV.Table = oDT;
            return oDV;
        }

        //-------------------------------------------------------------------------------------

        private DataView DV_Section3_Pop(DataView oDV, string strVal1)
        {
            DataRow oDR = oDV.Table.NewRow();

            oDR["ALTCOLOR"] = "";
            oDR["TEXT1"] = strVal1;

            //--------------------------------------- 
            oDV.Table.Rows.Add(oDR);
            oDR = null;
            oDR = oDV.Table.NewRow();

            return oDV;
        }


        //-----------------------------------------------------------------------        
        //-----------------------------------------------------------------------        
        //-----------------------------------------------------------------------        

        public DataView DV_Section4A_Fill(DataView oDV)
        {
            oDV = DV_Section4_Open();

            string sRemitTo = string.Empty;
            sRemitTo = "Medical Specialties Distributors, LLC" + (char)13 + "PO Box 206639" + (char)13 + "Dallas, TX 75320-6639";
            oDV = DV_Section4_Pop(oDV, sRemitTo);

            return oDV;
        }

        public DataView DV_Section4B_Fill(DataView oDV)
        {
            oDV = DV_Section4_Open();

            string sRemitTo = string.Empty;
            sRemitTo = "SWIFT: WFBIUS65" + (char)13 +
                       "ABA: 121000248" + (char)13 +
                       "ACCT #: 4130021082" + (char)13 +
                       "FBO: Medical Specialties Distributors, LLC";
            oDV = DV_Section4_Pop(oDV, sRemitTo);

            return oDV;
        }

        //-----------------------------------------------------------------------        

        public DataView DV_Section4_Open()
        {
            DataTable oDT = new DataTable("PageHeader");
            //---------------------------------------------------------------
            DataColumn ALTCOLOR = new DataColumn("ALTCOLOR");
            ALTCOLOR.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(ALTCOLOR);

            DataColumn TEXT1 = new DataColumn("TEXT1");
            TEXT1.DataType = System.Type.GetType("System.String");
            oDT.Columns.Add(TEXT1);

            //---------------------------------------------------------------

            DataView oDV = new DataView();
            oDV.Table = oDT;
            return oDV;
        }

        //-------------------------------------------------------------------------------------

        private DataView DV_Section4_Pop(DataView oDV, string strVal1)
        {
            DataRow oDR = oDV.Table.NewRow();

            oDR["ALTCOLOR"] = "";
            oDR["TEXT1"] = strVal1;

            //--------------------------------------- 
            oDV.Table.Rows.Add(oDR);
            oDR = null;
            oDR = oDV.Table.NewRow();

            return oDV;
        }

        //-----------------------------------------------------------------------        
        //-----------------------------------------------------------------------        
        //-----------------------------------------------------------------------        



        ~dataDetails()
        {
        }

    }
}

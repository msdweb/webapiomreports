﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OTInvoices
{
    public static class DbAccess
    {
        private static string _ConnectionString = "Data Source=MSDSQL,1433; Initial Catalog=WrenTrack; User ID=OTUser; Password=aD@U723C";

        private static string MSDSQLServer = "MSDSQL,1433";
        private static string MSDSQL01Server = "MSDSQL01.msd.local";
        private static string MSDSQL13 = "MSDSQL13";
        private static string MSDSQL14LS = "MSDSQL14-LS";

        private static string database = "WrenTrack";
        private static string user_id = "OTUser";
        private static string password = "aD@U723C";

        //sSqlConnection = "server=MSDSQL,1433;" +
        //                 "database=OneMedSystem; " +
        //                 "user id=oms;" +
        //                 "password=6#s53_9B;" +
        //                 "connection timeout=30";

        public static void Set_MSDSQL()
        {
            _ConnectionString = "server=" + MSDSQLServer + "; " +
                             "database=" + database + "; " +
                             "user id=" + user_id + "; " +
                             "password=" + password + "; " +
                             "connection timeout=30";
        }

        public static void Set_MSDSQL01()
        {
            _ConnectionString = "server=" + MSDSQL01Server + "; " +
                             "database=" + database + "; " +
                             "user id=" + user_id + "; " +
                             "password=" + password + "; " +
                             "connection timeout=30";
        }

        public static void Set_MSDSQL13()
        {
            _ConnectionString = "server=" + MSDSQL13 + "; " +
                             "database=" + database + "; " +
                             "user id=" + user_id + "; " +
                             "password=" + password + "; " +
                             "connection timeout=30";
        }

        public static void Set_MSDSQL14LS()
        {
            _ConnectionString = "server=" + MSDSQL14LS + "; " +
                             "database=" + database + "; " +
                             "user id=" + user_id + "; " +
                             "password=" + password + "; " +
                             "connection timeout=30";
        }

        public static string ConnectionString
        {
            get { return _ConnectionString; }
            set { _ConnectionString = value; }
        }
    }

    //-------------------------------------------------------------------------
}

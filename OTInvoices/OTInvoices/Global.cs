﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.IO;
using System.Data.SqlClient;

namespace OTInvoices
{
    sealed class MainClass
    {
        public static void Main()
        {
            GLB.Customer_no = "124388"; //"138964"; //"129214"; //"112769"; //"102412";
            //GLB.Invoice_no = "W000659828"; //"W000031943"; //"W000600177"; //"W000600107"; 
            GLB.Invoice_no = "W000688960"; //"W000632600"; //"W000524243"; //"W000524279"; //"W000524287"; 

            //GLB.AssocID = "00882-0061";

            //GLB.InvoiceListType = 0;

            //GLB.InvoiceType = "Rental"; // "Repair"; // "Rental"
            //GLB.InvoiceType = GLB.InvoiceType.ToString().ToUpper();

            //GLB.InvoiceBatch = 2;

            //GLB.InvoiceStartDate = "01/31/2010";  // "10/01/2013"; // "01/31/2010";
            //GLB.InvoiceEndDate = "01/31/2010"; // "10/31/2013"; // "01/31/2010";

            //GLB.InvoiceStatus = 1; // 0 - Unapproved / 1 - Approved;

            string sConn;

            //DbAccess.Set_MSDSQL();
            //DbAccess.Set_MSDSQL01();
            //DbAccess.Set_MSDSQL13();
            DbAccess.Set_MSDSQL14LS();

            sConn = DbAccess.ConnectionString;

            GLB.GetData(sConn);
            Application.Run(new frmOTInvoices());
        }
    }

    public class OTInvoices
    {
        public static string Create_OTInvoices(string Customer_no, string Invoice_no, string sConn)
        {
            string FilePath = string.Empty;

            try
            {
                GLB.InvoiceListType = 0;
                GLB.Set_OTInvoiceType(Invoice_no, sConn);
                GLB.Customer_no = Customer_no;
                GLB.Invoice_no = Invoice_no;

                GLB.AssocID = string.Empty;

                GLB.GetData(sConn);
                frmOTInvoicesPrint frmPCSReport = new frmOTInvoicesPrint();
                if (GLB.OTInvoices.Rows.Count > 0)
                {
                    FilePath = "OTInvoices/MSD-Invoice-" + GLB.Invoice_no + ".xps";
                    FilePath = string.Format("{0}?t={1}", FilePath, DateTime.Now.Ticks);
                    frmPCSReport.PCS_Salva();
                }
                return FilePath;
            }
            catch (Exception ex)
            {
                return ""; //ex.Message;
            }
        }

        public static string Create_OTInvoicesBatch(string assocId, object[] parameters, string sConn)
        {
            string FilePath = string.Empty;

            GLB.Customer_no = string.Empty;
            GLB.Invoice_no = string.Empty;

            GLB.AssocID = assocId;

            try
            {
                GLB.InvoiceListType = Convert.ToInt32(parameters[0]); //InvoiceListType;
                GLB.InvoiceType = (string)parameters[1]; //InvoiceType;
                GLB.InvoiceBatch = Convert.ToInt32(parameters[2]); //InvoiceBatch;
                GLB.InvoiceStartDate = (string)parameters[3]; //InvoiceStartDate;
                GLB.InvoiceEndDate = (string)parameters[4]; //InvoiceEndDate;
                GLB.InvoiceStatus = Convert.ToInt32(parameters[5]); //InvoiceStatus;

                GLB.InvoiceType = GLB.InvoiceType.ToString().ToUpper();

                GLB.GetData(sConn);
                frmOTInvoicesPrint frmPCSReport = new frmOTInvoicesPrint();

                if (GLB.OTInvoices.Rows.Count > 0)
                {
                    FilePath = "OTInvoices/MSD-Invoices-" + GLB.AssocID + ".xps";
                    FilePath = string.Format("{0}?t={1}", FilePath, DateTime.Now.Ticks);
                    frmPCSReport.PCS_Salva();
                }
                return FilePath;
            }
            catch (Exception ex)
            {
                return ""; //ex.Message;
            }
        }
    }

    public class  GLB
    {
        public static DataTable OTInvoices;

        public static string Customer_no = string.Empty;
        public static string Invoice_no = string.Empty;

        public static string AssocID = string.Empty;
        public static int InvoiceListType = 0;
        public static string InvoiceType = string.Empty;
        public static int InvoiceBatch = 0;
        public static string InvoiceStartDate = string.Empty;
        public static string InvoiceEndDate = string.Empty;
        public static int InvoiceStatus = 0;

        public static void Set_OTInvoiceType(string invoiceno, string sConn)
        {

            SqlCommand oCmd = new SqlCommand();

            string sSqlConnection;

            string sSqlSelectCmd = string.Empty;

            sSqlConnection = sConn;
            //###################################################################

            DataSet oDS = new DataSet();

            sSqlSelectCmd = "SELECT [InvoiceHeader].[WrenTrack Invoice Type] FROM [WrenTrack].[dbo].[InvoiceHeader] [InvoiceHeader] WHERE [InvoiceHeader].[Invoice Number] = '" + invoiceno + "' ";

            using (SqlConnection _SQLConn = new SqlConnection(sSqlConnection))
            {
                _SQLConn.Open();

                using (SqlCommand SQLCmd = new SqlCommand())
                {
                    SQLCmd.Connection = _SQLConn;
                    SQLCmd.CommandType = CommandType.Text;

                    SQLCmd.CommandText = sSqlSelectCmd;

                    using (SqlDataReader SQLRdr = SQLCmd.ExecuteReader())
                    {
                        while (SQLRdr.Read())
                        {
                            InvoiceType = SQLRdr["WrenTrack Invoice Type"].ToString();
                            InvoiceType = InvoiceType.ToString().ToUpper();
                        }
                    }
                }
            }

            return;
        }

        public static void GetData(string sConn)
        {
            SqlCommand oCmd = new SqlCommand();

            string sSqlConnection;

            string sSqlSelectCmd = string.Empty;

            sSqlConnection = sConn;
            //###################################################################

            DataSet oDS = new DataSet();

            sSqlSelectCmd = GetSelect();

            try
            {
                SqlDataAdapter oDA = default(SqlDataAdapter);
                oDA = new SqlDataAdapter(sSqlSelectCmd, sSqlConnection);
                oDA.SelectCommand.CommandTimeout = 120;

                oDA.Fill(oDS);
                oDA.Dispose();

            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
                //  if (oConn.State == ConnectionState.Open) oConn.Close();
            }

            OTInvoices = oDS.Tables[0];
            return;
        }

        private static string GetSelect()
        {
            string sSqlSelectService = "";
            string sSqlSelectRental = "";
            string sSqlSelectCmd = string.Empty;

            //------------------------------------------------------------------------------------------------------------------------------------------------------------
            // Rental
            //------------------------------------------------------------------------------------------------------------------------------------------------------------

            sSqlSelectRental += " SELECT [InvoiceHeader].[Invoice Number], [InvoiceHeader].[Invoice Date], [InvoiceHeader].[WrenTrack ClientID], [InvoiceLine].[Item ID], ";
            sSqlSelectRental += "[InvoiceLine].[Item Description], [InvoiceLine].[Extended Price], [InvoiceHeader].[WrenTrack Invoice Note], [InvoiceHeader].[Bill To Name], ";
            sSqlSelectRental += "[InvoiceHeader].[Bill To Address 1], [InvoiceHeader].[Bill To Address 2], ";
            sSqlSelectRental += "[InvoiceHeader].[Bill To City], [InvoiceHeader].[Bill To State], [InvoiceHeader].[Bill To Postal Code], ";
            sSqlSelectRental += "[InvoiceHeader].[Ship To Name], [InvoiceHeader].[Ship To Address 1], [InvoiceHeader].[Ship To Address 2], ";
            sSqlSelectRental += "[InvoiceHeader].[Ship To City], [InvoiceHeader].[Ship To State], [InvoiceHeader].[Ship To Postal Code], ";
            sSqlSelectRental += "[InvoiceLine].[Tax Item], [InvoiceHeader].[Purchase Order Number], [p21_view_terms].[terms_desc], [InvoiceLine].[SerialNumber], ";
            sSqlSelectRental += "[InvoiceLine].[BillableDays], [InvoiceLine].[MaxBillingDaysPerMonth], [InvoiceHeader].[Customer ID], [InvoiceLine].[MonthlyRate], ";
            sSqlSelectRental += "[InvoiceLine].[DailyRate], [InvoiceLine].[DateRange], [InvoiceLine].[PO_Number],  [Clients].[Batch] ";

            sSqlSelectRental += "FROM ([WrenTrack].[dbo].[InvoiceLine] [InvoiceLine] INNER JOIN [WrenTrack].[dbo].[InvoiceHeader] [InvoiceHeader] ON ";
            sSqlSelectRental += "[InvoiceLine].[Invoice Number] = [InvoiceHeader].[Invoice Number]) LEFT OUTER JOIN [WrenTrack].[dbo].[p21_view_terms] [p21_view_terms] ON ";
            sSqlSelectRental += "[InvoiceHeader].[Terms ID] = [p21_view_terms].[terms_id] INNER JOIN [WrenTrack].[dbo].[Clients] [Clients] ON [InvoiceHeader].[WrenTrack ClientID] = [Clients].[ID] ";

            if (GLB.Invoice_no != "")
            {
                sSqlSelectRental += "WHERE [InvoiceHeader].[Invoice Number] = '" + GLB.Invoice_no + "' ";
                sSqlSelectRental += "AND [InvoiceHeader].[Customer ID]='" + GLB.Customer_no + "' ";
            }
            else
            {
                sSqlSelectRental += "WHERE  [InvoiceHeader].[WrenTrack Invoice Type]=N'" + GLB.InvoiceType + "' AND [Clients].[Batch]='" + GLB.InvoiceBatch + "' ";
                sSqlSelectRental += "AND ([InvoiceHeader].[Invoice Date] BETWEEN '" + GLB.InvoiceStartDate + "' AND '" + GLB.InvoiceEndDate + "') AND [InvoiceHeader].[Ready For Export]='" + GLB.InvoiceStatus + "' ";
                if (GLB.InvoiceStatus == 1)
                {
                    sSqlSelectRental += "AND [InvoiceHeader].[Total Amount]<>0 ";
                }
            }

            //------------------------------------------------------------------------------------------------------------------------------------------------------------
            // Services
            //------------------------------------------------------------------------------------------------------------------------------------------------------------

            sSqlSelectService += "SELECT [InvoiceHeader].[Invoice Number], [InvoiceHeader].[Invoice Date], [InvoiceLine].[Item ID], [InvoiceLine].[Item Description], ";
            sSqlSelectService += "[InvoiceLine].[Unit Price], [InvoiceLine].[Extended Price], [InvoiceHeader].[WrenTrack Invoice Note], [InvoiceLine].[Quantity Shipped], ";
            sSqlSelectService += "[InvoiceHeader].[Bill To Name], [InvoiceHeader].[Bill To Address 1], [InvoiceHeader].[Bill To Address 2], [InvoiceHeader].[Bill To City], ";
            sSqlSelectService += "[InvoiceHeader].[Bill To State], [InvoiceHeader].[Bill To Postal Code], [InvoiceHeader].[Ship To Name], [InvoiceHeader].[Ship To Address 1], ";
            sSqlSelectService += "[InvoiceHeader].[Ship To Address 2], [InvoiceHeader].[Ship To City], [InvoiceHeader].[Ship To State], [InvoiceHeader].[Ship To Postal Code], ";
            sSqlSelectService += "[InvoiceLine].[Line Number], [InvoiceLine].[Tax Item], [InvoiceHeader].[Purchase Order Number], [p21_view_terms].[terms_desc], [InvoiceHeader].[WrenTrack ClientID], ";
            sSqlSelectService += "[InvoiceHeader].[Customer ID], [InvoiceLine].[SerialNumber], [InvoiceLine].[PO_Number], ";
            sSqlSelectService += "[Clients].[Batch], [p21_view_contacts].[first_name], [p21_view_contacts].[mi], [p21_view_contacts].[last_name] ";

            sSqlSelectService += "FROM   ([WrenTrack].[dbo].[p21_view_contacts] [p21_view_contacts] RIGHT OUTER JOIN ";
            sSqlSelectService += "([WrenTrack].[dbo].[InvoiceLine] [InvoiceLine] INNER JOIN [WrenTrack].[dbo].[InvoiceHeader] [InvoiceHeader] ON ";
            sSqlSelectService += "[InvoiceLine].[Invoice Number]=[InvoiceHeader].[Invoice Number]) ON ";
            sSqlSelectService += "[p21_view_contacts].[id]=[InvoiceHeader].[Salesrep ID]) LEFT OUTER JOIN [WrenTrack].[dbo].[p21_view_terms] [p21_view_terms] ON ";
            sSqlSelectService += "[InvoiceHeader].[Terms ID]=[p21_view_terms].[terms_id] INNER JOIN [WrenTrack].[dbo].[Clients] [Clients] ON ";
            sSqlSelectService += "[InvoiceHeader].[WrenTrack ClientID]=[Clients].[ID] ";

            if (GLB.Invoice_no != "")
            {
                sSqlSelectService += "WHERE [InvoiceHeader].[Invoice Number] = '" + GLB.Invoice_no + "' ";
                sSqlSelectService += "AND [InvoiceHeader].[Customer ID]='" + GLB.Customer_no + "' ";
            }
            else
            {
                sSqlSelectService += "WHERE  [InvoiceHeader].[WrenTrack Invoice Type]=N'" + GLB.InvoiceType + "' AND [Clients].[Batch]='" + GLB.InvoiceBatch + "' ";
                sSqlSelectService += "AND ([InvoiceHeader].[Invoice Date] BETWEEN '" + GLB.InvoiceStartDate + "' AND '" + GLB.InvoiceEndDate + "') AND [InvoiceHeader].[Ready For Export]='" + GLB.InvoiceStatus + "' ";
                if (GLB.InvoiceStatus == 1)
                {
                    sSqlSelectService += "AND [InvoiceHeader].[Total Amount]<>0 ";
                }
            }

            //
            // -------------------------------------------------------------------------------------------------------------------------------------------------------------
            //

            if (InvoiceListType == 0)
            {
                //------------------------------------------------------------------------------------------------------------------------------------------------------------
                // Rental
                //------------------------------------------------------------------------------------------------------------------------------------------------------------

                sSqlSelectRental += "ORDER BY [InvoiceHeader].[Ship To Name], [InvoiceHeader].[Invoice Number], [InvoiceLine].[Item Description] ";

                //------------------------------------------------------------------------------------------------------------------------------------------------------------
                // Services
                //------------------------------------------------------------------------------------------------------------------------------------------------------------

                sSqlSelectService += "ORDER BY [InvoiceHeader].[Ship To Name], [InvoiceHeader].[Invoice Number], [InvoiceLine].[Line Number]";

                sSqlSelectCmd = sSqlSelectService;
                if (InvoiceType == "RENTAL") { sSqlSelectCmd = sSqlSelectRental; }
            }
            else if (InvoiceListType == 1)
            {
                // Services To Look at

                sSqlSelectService += "AND NOT (([InvoiceHeader].[total amount]>0 ";
                sSqlSelectService += "         AND NOT EXISTS (Select * from InvoiceLine l2 Where l2.[Invoice Number]=[InvoiceHeader].[Invoice Number] ";
                sSqlSelectService += "                    AND ((l2.[Extended Price] is Null) OR (l2.[Unit Price] is Null))))) ";

                sSqlSelectService += "ORDER BY [InvoiceHeader].[Ship To Name], [InvoiceHeader].[Invoice Number], [InvoiceLine].[Line Number]";
                sSqlSelectCmd = sSqlSelectService;
            }
            else if (InvoiceListType == 2)
            {
                // Services To Print

                sSqlSelectService += "AND (([InvoiceHeader].[total amount]>0 ";
                sSqlSelectService += "         AND NOT EXISTS (Select * from InvoiceLine l2 Where l2.[Invoice Number]=[InvoiceHeader].[Invoice Number] ";
                sSqlSelectService += "                    AND ((l2.[Extended Price] is Null) OR (l2.[Unit Price] is Null))))) ";

                sSqlSelectService += "ORDER BY [InvoiceHeader].[Ship To Name], [InvoiceHeader].[Invoice Number], [InvoiceLine].[Line Number]";
                sSqlSelectCmd = sSqlSelectService;
            }
            else if (InvoiceListType == 3)
            {
                // Rental To Look at

                sSqlSelectRental += "AND (([InvoiceHeader].[total amount]>0) OR ([InvoiceHeader].[total amount]=0 ";
                sSqlSelectRental += "AND EXISTS ";
                sSqlSelectRental += "(Select * from InvoiceLine l1, Devices d, DeviceCodes dc ";
                sSqlSelectRental += "Where l1.[Invoice Number]=[InvoiceHeader].[Invoice Number] AND l1.[SerialNumber]=d.SerialNumber AND d.DeviceCode=dc.Code AND ";
                sSqlSelectRental += "(l1.[Unit Price]>0)))) ";

                sSqlSelectRental += "AND EXISTS ";
                sSqlSelectRental += "   (Select * from InvoiceLine l2 ";
                sSqlSelectRental += "     Where l2.[Invoice Number]=[InvoiceHeader].[Invoice Number] AND l2.[Tax Item]='N' AND ";
                sSqlSelectRental += "	        ((l2.[Extended Price] is Null) OR (l2.[Unit Price] is Null))) ";

                sSqlSelectRental += "ORDER BY [InvoiceHeader].[Ship To Name], [InvoiceHeader].[Invoice Number], [InvoiceLine].[Item Description] ";
                sSqlSelectCmd = sSqlSelectRental;
            }
            else if (InvoiceListType == 4)
            {
                // Rental To Print

                sSqlSelectRental += "AND (([InvoiceHeader].[total amount]>0) OR ([InvoiceHeader].[total amount]=0 ";
                sSqlSelectRental += "AND EXISTS ";
                sSqlSelectRental += "(Select * from InvoiceLine l1, Devices d, DeviceCodes dc ";
                sSqlSelectRental += "Where l1.[Invoice Number]=[InvoiceHeader].[Invoice Number] AND l1.[SerialNumber]=d.SerialNumber AND d.DeviceCode=dc.Code AND ";
                sSqlSelectRental += "(l1.[Unit Price]>0)))) ";

                sSqlSelectRental += "AND NOT EXISTS ";
                sSqlSelectRental += "   (Select * from InvoiceLine l2 ";
                sSqlSelectRental += "     Where l2.[Invoice Number]=[InvoiceHeader].[Invoice Number] AND l2.[Tax Item]='N' AND ";
                sSqlSelectRental += "	        ((l2.[Extended Price] is Null) OR (l2.[Unit Price] is Null))) ";

                sSqlSelectRental += "ORDER BY [InvoiceHeader].[Ship To Name], [InvoiceHeader].[Invoice Number], [InvoiceLine].[Item Description] ";
                sSqlSelectCmd = sSqlSelectRental;
            }
            else if (InvoiceListType == 5)
            {
                // Zeros Amount list

                //------------------------------------------------------------------------------------------------------------------------------------------------------------
                // Rental
                //------------------------------------------------------------------------------------------------------------------------------------------------------------

                sSqlSelectRental += "AND NOT (([InvoiceHeader].[total amount]>0) OR ([InvoiceHeader].[total amount]=0 ";
                sSqlSelectRental += "AND EXISTS ";
                sSqlSelectRental += "(Select * from InvoiceLine l1, Devices d, DeviceCodes dc ";
                sSqlSelectRental += "Where l1.[Invoice Number]=[InvoiceHeader].[Invoice Number] AND l1.[SerialNumber]=d.SerialNumber AND d.DeviceCode=dc.Code AND ";
                sSqlSelectRental += "(l1.[Unit Price]>0)))) ";

                sSqlSelectRental += "ORDER BY [InvoiceHeader].[Ship To Name], [InvoiceHeader].[Invoice Number], [InvoiceLine].[Item Description] ";

                //------------------------------------------------------------------------------------------------------------------------------------------------------------
                // Services
                //------------------------------------------------------------------------------------------------------------------------------------------------------------

                sSqlSelectService += "AND [InvoiceHeader].[total amount]=0 ";
                sSqlSelectService += "ORDER BY [InvoiceHeader].[Ship To Name], [InvoiceHeader].[Invoice Number], [InvoiceLine].[Line Number]";

                sSqlSelectCmd = sSqlSelectService;
                if (InvoiceType == "RENTAL") { sSqlSelectCmd = sSqlSelectRental; }
            }

            return sSqlSelectCmd;
        }

        //
        //******************************************************************************
        //
        public static bool IsNumeric(object Expression)
        {
            bool isNum;
            double retNum;
            isNum = Double.TryParse(Convert.ToString(Expression),
                    System.Globalization.NumberStyles.Any, 
                    System.Globalization.NumberFormatInfo.InvariantInfo, 
                    out retNum);
            return isNum;
        }

        public static bool HasAnyNum(string numberString)
        { 
          char [] ca = numberString.ToCharArray();

          foreach (char i in ca)
          {
              if (char.IsNumber(i))
                  return true;
          } 
          return false; 
        }
        public static bool HasAnyNum1(string numberString)
        {
            char[] ca = numberString.ToCharArray();
            for (int i = 0; i < ca.Length; i++)
            {
                if (char.IsNumber(ca[i]))
                    return true;
            }
            return false;
        }

        ~GLB()
        {
        }

    }
    
    public class AddValue
    {
        private string m_Display;
        private string m_Value;
        public AddValue(string Display, string Value)
        {
            m_Display = Display;
            m_Value = Value;
        }
        public string Display
        {
            get { return m_Display; }
        }
        public string Value
        {
            get { return m_Value; }
        }
    }
}

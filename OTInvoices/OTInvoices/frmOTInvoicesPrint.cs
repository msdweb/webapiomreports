﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Text;
using System.Windows.Forms;

namespace OTInvoices
{
    public partial class frmOTInvoicesPrint : Form
    {
        private DataView DVPageHeader;
        private DataView DVEndGroup;
        
        DataView DVSection1;
        DataView DVSection2;
        DataView DVSection3;
        DataView DVSection4A;
        DataView DVSection4B;

        private decimal NonTaxAmount = 0;
        private decimal TaxAmount = 0;

        private Printer DataGridPrinter1 = null;

        public frmOTInvoicesPrint()
        {
            InitializeComponent();
        }

        //####################################################################################
        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            Graphics g = e.Graphics;
            bool more = DataGridPrinter1.DrawDataGrid(g);
            if (more == true)
            {
                e.HasMorePages = true;
                DataGridPrinter1.PageNumber++;
                DataGridPrinter1.TotalPageNumber++;
            }

        }

        public void PCS_Preview()
        {
            if (GLB.OTInvoices.Rows.Count > 0)
            {
                SetupGridPrinter();

                frmPrinterControl PC = new frmPrinterControl();
                // if (DataGridPrinter1.
                PC.RPT_Preview(DataGridPrinter1);
                PC.ShowDialog();
                PC = null;
            }
        }

        public void PCS_Print()
        {
            if (GLB.OTInvoices.Rows.Count > 0)
            {
                SetupGridPrinter();

                frmPrinterControl PC = new frmPrinterControl();
                PC.RPT_Print(DataGridPrinter1);
                PC = null;
            }
        }

        public void PCS_Salva()
        {
            if (GLB.OTInvoices.Rows.Count > 0)
            {
                string s = System.AppDomain.CurrentDomain.BaseDirectory;
                printDocument1.DefaultPageSettings.PrinterSettings.PrinterName = "Microsoft XPS Document Writer";
                string FileName = string.Empty;

                if (GLB.AssocID == "")
                {
                    FileName = "C:\\temp\\OT-Report\\MSD-Invoice-" + GLB.Invoice_no + ".xps";
                }
                else
                {
                    FileName = "C:\\temp\\OT-Report\\MSD-Invoices-" + GLB.AssocID + ".xps";
                }
                printDocument1.DefaultPageSettings.PrinterSettings.PrintFileName = FileName;
                printDocument1.DefaultPageSettings.PrinterSettings.PrintToFile = true;

                SetupGridPrinter();

                frmPrinterControl PC = new frmPrinterControl();

                PC.RPT_Print(DataGridPrinter1);
                PC = null;

            }
        }

        void SetupGridPrinter()
        {
            dataDetails dataDetails = new dataDetails();

            DVPageHeader = dataDetails.DV_PageHeader_Fill(DVPageHeader);

            if (GLB.InvoiceType != "RENTAL")
            {
                DVEndGroup = dataDetails.DV_GroupEnd1_Fill(DVEndGroup);
            }
            else
            {
                DVEndGroup = dataDetails.DV_GroupEnd2_Fill(DVEndGroup);
            }

            lblFooter.Text = dataDetails.Get_LastReview();
            dgvEmptyMessage.ColumnHeadersVisible = false;

            dgvGroup1.BorderStyle = BorderStyle.None;
            dgvGroup1.ColumnHeadersVisible = true;
            DataGridPrinter1 = new Printer(printDocument1,
                                           dgvGroup1,
                                           dgvEmptyMessage,
                                           dgvEndOTInvoices,
                                           DVPageHeader.Table.DefaultView, lblFooter);

            //----------------------------------------------------------------------------
            //----------------------------------------------------------------------------
            //----------------------------------------------------------------------------

            string currentInvoice = string.Empty;
            int g = 0;

            foreach (DataRow row in GLB.OTInvoices.Rows)
            {

                if (row["Invoice Number"].ToString() != currentInvoice)
                {

                    currentInvoice = row["Invoice Number"].ToString();

                    dgvEndGroupMsg.ColumnHeadersVisible = true;
                    dgvGroup2.ColumnHeadersVisible = false;
                    DataGridPrinter1.AddGroup(dgvGroup2,
                                              dgvEndGroupMsg, DVEndGroup);




                    DataGridView dgvGroupSectionHeader = new DataGridView();
                    DataView DVGroupSection = new DataView();


                    dgvSectionHeader1.ColumnHeadersVisible = true;
                    DataGridPrinter1.AddSectionHeader(g, false,
                                                      null,
                                                      dgvSectionHeader1,
                                                      DVGroupSection);

                    //----------------------------------------------------------------------------

                    DataView DVSectionHeader2 = new DataView();
                    DVSectionHeader2 = dataDetails.DV_SectionHeader2_Fill(DVSectionHeader2, "", row["Invoice Number"].ToString());

                    dgvSectionHeader2.ColumnHeadersVisible = false;
                    dgvSectionHeader2.BorderStyle = BorderStyle.None;
                    DataGridPrinter1.AddSectionHeader(g, false,
                                                      null,
                                                      dgvSectionHeader2,
                                                      DVSectionHeader2);

                    //
                    //------------------------------------------------------------------------------------------
                    //

                    string sBill= string.Empty;

                    if (row["Bill To Name"].ToString() != null)
                    {
                        sBill += row["Bill To Name"].ToString().Trim();
                    }

                    if (row["Bill To Address 1"].ToString() != null)
                    {
                        sBill += (char)13;
                        sBill += row["Bill To Address 1"].ToString().Trim();
                    }

                    if (row["Bill To Address 2"].ToString() != null)
                    {
                        if (row["Bill To Address 2"].ToString().Trim() != "")
                        {
                            sBill += (char)13;
                            sBill += row["Bill To Address 2"].ToString().Trim();
                        }
                    }

                    if (row["Bill To City"].ToString() != null)
                    {
                        sBill += (char)13;
                        sBill += row["Bill To City"].ToString().Trim();
                    }

                    if (row["Bill To State"].ToString() != null)
                    {
                        sBill += ", "; //(char)13;
                        sBill += row["Bill To State"].ToString().Trim();
                    }

                    if (row["Bill To Postal Code"].ToString() != null)
                    {
                        sBill += ", "; //(char)13;
                        sBill += row["Bill To Postal Code"].ToString().Trim();
                    }

                    //-------------------------------------------------------------

                    string sShip = string.Empty;

                    if (row["Ship To Name"].ToString() != null)
                    {
                        sShip += row["Ship To Name"].ToString().Trim();
                    }

                    if (row["Ship To Address 1"].ToString() != null)
                    {
                        sShip += (char)13;
                        sShip += row["Ship To Address 1"].ToString().Trim();
                    }

                    if (row["Ship To Address 2"].ToString() != null)
                    {
                        if (row["Ship To Address 2"].ToString().Trim() != "")
                        {
                            sShip += (char)13;
                            sShip += row["Ship To Address 2"].ToString().Trim();
                        }
                    }

                    if (row["Ship To City"].ToString() != null)
                    {
                        sShip += (char)13;
                        sShip += row["Ship To City"].ToString().Trim();
                    }

                    if (row["Ship To State"].ToString() != null)
                    {
                        sShip += ", "; //(char)13;
                        sShip += row["Ship To State"].ToString().Trim();
                    }

                    if (row["Ship To Postal Code"].ToString() != null)
                    {
                        sShip += ", "; //(char)13;
                        sShip += row["Ship To Postal Code"].ToString().Trim();
                    }

                    DataView DVSectionHeader3 = new DataView();
                    DVSectionHeader3 = dataDetails.DV_SectionHeader3_Fill(DVSectionHeader3, sBill, sShip);

                    dgvSectionHeader3.ColumnHeadersVisible = false;
                    dgvSectionHeader3.BorderStyle = BorderStyle.None;
                    DataGridPrinter1.AddSectionHeader(g, false,
                                                      null,
                                                      dgvSectionHeader3,
                                                      DVSectionHeader3);

                    //
                    //------------------------------------------------------------------------------------------
                    //

                    string salesperson = "";
                    DateTime dt = Convert.ToDateTime(row["Invoice Date"]);

                    if (GLB.InvoiceType != "RENTAL")
                    {
                        if (row["first_name"].ToString() != "") { salesperson += row["first_name"].ToString().Trim(); }
                        if (row["mi"].ToString() != "") { salesperson += " " + row["mi"].ToString().Trim(); }
                        if (row["last_name"].ToString() != "") { salesperson += " " + row["last_name"].ToString().Trim(); }
                    }

                    DataView DVSectionHeader4 = new DataView();
                    DVSectionHeader4 = dataDetails.DV_SectionHeader4_Fill(DVSectionHeader4,
                                                                          row["Customer ID"].ToString(), 
                                                                          salesperson,
                                                                          dt.ToString("MM/dd/yyy"),
                                                                          "",
                                                                          row["Purchase Order Number"].ToString(),
                                                                          row["terms_desc"].ToString().ToUpper());

                    dgvSectionHeader4.ColumnHeadersVisible = true;
                    dgvSectionHeader4.BorderStyle = BorderStyle.FixedSingle;
                    DataGridPrinter1.AddSectionHeader(g, false,
                                                      null,
                                                      dgvSectionHeader4,
                                                      DVSectionHeader4);

                    //
                    //------------------------------------------------------------------------------------------
                    //

                    DataView DVSectionHeader5 = new DataView();
                    DVSectionHeader5 = dataDetails.DV_SectionHeader5_Fill(DVSectionHeader5, row["WrenTrack Invoice Note"].ToString());

                    dgvSectionHeader5.ColumnHeadersVisible = false;
                    dgvSectionHeader5.BorderStyle = BorderStyle.FixedSingle;
                    DataGridPrinter1.AddSectionHeader(g, false,
                                                      null,
                                                      dgvSectionHeader5,
                                                      DVSectionHeader5);

                    //
                    //------------------------------------------------------------------------------------------
                    //


                    DataView DVSection0 = new DataView();
                    dataGridView1.ColumnHeadersVisible = false;
                    dataGridView1.BorderStyle = BorderStyle.FixedSingle;
                    DataGridPrinter1.AddSectionHeader(g, false,
                                                      null,
                                                      dataGridView1,
                                                      DVSection0);


                    if (DVSection1 != null)
                    {

                        if (GLB.InvoiceType == "RENTAL")
                        {
                            dgvSectionRental.ColumnHeadersVisible = true;
                            dgvSectionRental.BorderStyle = BorderStyle.FixedSingle;
                            DataGridPrinter1.AddSection(g, false,
                                                        null,
                                                        dgvSectionRental,
                                                        DVSection1);
                        }
                        else
                        {
                            {
                                dgvSectionService.ColumnHeadersVisible = true;
                                dgvSectionService.BorderStyle = BorderStyle.FixedSingle;
                                DataGridPrinter1.AddSection(g, false,
                                                            null,
                                                            dgvSectionService,
                                                            DVSection1);
                            }
                        }

                        //
                        //------------------------------------------------------------------------------------------
                        //

                        DVSection4A = dataDetails.DV_Section4A_Fill(DVSection3);

                        dgvSection4A.ColumnHeadersVisible = true;
                        dgvSection4A.BorderStyle = BorderStyle.FixedSingle;
                        DataGridPrinter1.AddSection(g, true,
                                                    null,
                                                    dgvSection4A,
                                                    DVSection4A);

                        DVSection4B = dataDetails.DV_Section4B_Fill(DVSection3);

                        dgvSection4A.ColumnHeadersVisible = true;
                        dgvSection4A.BorderStyle = BorderStyle.FixedSingle;
                        DataGridPrinter1.AddSection(g, true,
                                                    null,
                                                    dgvSection4B,
                                                    DVSection4B);

                        //
                        //------------------------------------------------------------------------------------------
                        //
                        
                        string sNonTaxAmount = string.Format("{0:C}", NonTaxAmount);
                        string sTaxAmount = string.Format("{0:C}", TaxAmount);

                        DVSection2 = dataDetails.DV_Section2_Open();

                        DVSection2 = dataDetails.DV_Section2_Fill(DVSection2, "SUB TOTAL", sNonTaxAmount);
                        DVSection2 = dataDetails.DV_Section2_Fill(DVSection2, "TAX", sTaxAmount);

                        dgvSection2.ColumnHeadersVisible = false;
                        dgvSection2.BorderStyle = BorderStyle.FixedSingle;
                        DataGridPrinter1.AddSection(g, false,
                                                    null,
                                                    dgvSection2,
                                                    DVSection2);
                        //
                        //------------------------------------------------------------------------------------------
                        //

                        decimal TotalAmount = NonTaxAmount + TaxAmount;
                        string sTotalAmount = string.Format("{0:C}", TotalAmount);

                        DVSection3 = dataDetails.DV_Section3_Fill(DVSection3, sTotalAmount);

                        dgvSection3.ColumnHeadersVisible = true;
                        dgvSection3.BorderStyle = BorderStyle.FixedSingle;
                        DataGridPrinter1.AddSection(g, false,
                                                    null,
                                                    dgvSection3,
                                                    DVSection3);

                        //
                        //------------------------------------------------------------------------------------------
                        //
                        NonTaxAmount = 0;
                        TaxAmount = 0;

                    }

                    DVSection1 = dataDetails.DV_Section1_Open();
                    ++g;
                    //if (g > 15) { break; }
                }

                //----------------------------------------------------------------------------

                string qty = string.Empty;
                string itemID = string.Empty;
                string itemDescription = string.Empty;

                string dateRange = string.Empty;

                string SerialNumber = string.Empty;
                string PONumber = string.Empty;

                string rate = string.Empty;
                string billableDays = string.Empty;
                string maxBillingDaysPerMonth = string.Empty;

                string UnitPrice = string.Empty;
                string ExtendPrice = string.Empty;

                if (GLB.InvoiceType == "RENTAL")
                {
                    //dateRange = row["DateRange"].ToString();

                    //billableDays = row["BillableDays"].ToString();

                    //maxBillingDaysPerMonth = row["MaxBillingDaysPerMonth"].ToString();

                    if (GLB.OTInvoices.Columns.Contains("DateRange"))
                    {
                        dateRange = row["DateRange"].ToString();
                    }
                    else
                    {
                        dateRange = string.Empty;
                    }

                    if (GLB.OTInvoices.Columns.Contains("BillableDays"))
                    {
                        billableDays = row["BillableDays"].ToString();
                    }
                    else
                    {
                        billableDays = string.Empty;
                    }

                    if (GLB.OTInvoices.Columns.Contains("MaxBillingDaysPerMonth"))
                    {
                        maxBillingDaysPerMonth = row["MaxBillingDaysPerMonth"].ToString();
                    }
                    else
                    {
                        maxBillingDaysPerMonth = string.Empty;
                    }


                    //if ((billableDays != "") && (maxBillingDaysPerMonth != ""))
                    //{
                    //    decimal decRate = 0;
                    //    if (Convert.ToInt32(billableDays) >= Convert.ToInt32(maxBillingDaysPerMonth))
                    //    {
                    //        decRate = Convert.ToDecimal(row["MonthlyRate"].ToString());
                    //    }
                    //    else
                    //    {
                    //        decRate = Convert.ToDecimal(row["DailyRate"].ToString());

                    //    }
                    //    rate = decRate.ToString("0.##");
                    //}

                    if ((billableDays != "") && (maxBillingDaysPerMonth != ""))
                    {
                        decimal decRate = 0;
                        if (Convert.ToInt32(billableDays) >= Convert.ToInt32(maxBillingDaysPerMonth))
                        {
                            if (GLB.OTInvoices.Columns.Contains("MonthlyRate") && !Convert.IsDBNull(row["MonthlyRate"]))
                            {
                                decRate = Convert.ToDecimal(row["MonthlyRate"].ToString());
                            }
                            else
                            {
                                decRate = Convert.ToDecimal("0");
                            }
                        }
                        else
                        {
                            if (GLB.OTInvoices.Columns.Contains("DailyRate"))
                            {
                                decRate = Convert.ToDecimal(row["DailyRate"].ToString());
                            }
                            else
                            {
                                decRate = Convert.ToDecimal("0");
                            }
                        }
                        rate = decRate.ToString("0.##");
                    }
                }

                if (GLB.InvoiceType != "RENTAL")
                {
                    decimal decQTY = Convert.ToDecimal(row["Quantity Shipped"]);
                    qty = decQTY.ToString("0.##");
                    if (row["Unit Price"].ToString() != "")
                    {
                        decimal decUnitPrice = Convert.ToDecimal(row["Unit Price"]);
                        UnitPrice = string.Format("{0:C}", decUnitPrice);
                    }
                }

                itemID = row["item ID"].ToString();
                itemDescription = row["item Description"].ToString();
                SerialNumber = row["SerialNumber"].ToString();
                PONumber = row["PO_Number"].ToString();

                decimal decExtendPrice = 0;

                if (row["Extended Price"].ToString() != null && row["Extended Price"].ToString() != "")
                {
                    string t = row["Extended Price"].ToString();
                    decExtendPrice = Convert.ToDecimal(row["Extended Price"]);                    
                }

                ExtendPrice = string.Format("{0:C}", decExtendPrice);
                                
                if (row["Tax Item"].ToString() != "Y")
                {
                    if (GLB.InvoiceType == "RENTAL")
                    {
                        DVSection1 = dataDetails.DV_Section1_Fill(DVSection1, itemID, itemDescription, dateRange, SerialNumber, PONumber, rate, billableDays, ExtendPrice);
                    }
                    else
                    {
                        DVSection1 = dataDetails.DV_Section1_Fill(DVSection1, qty, itemID, itemDescription, SerialNumber, PONumber, UnitPrice, ExtendPrice, "");
                    }
                    NonTaxAmount += decExtendPrice;
                }

                if (row["Tax Item"].ToString() == "Y")
                {
                    TaxAmount += decExtendPrice;
                }                 
                
                //
                //------------------------------------------------------------------------
                //


            }

            if (DVSection1 != null)
            {

                if (GLB.InvoiceType == "RENTAL")
                {
                    dgvSectionRental.ColumnHeadersVisible = true;
                    dgvSectionRental.BorderStyle = BorderStyle.FixedSingle;
                    DataGridPrinter1.AddSection(g, false,
                                                null,
                                                dgvSectionRental,
                                                DVSection1);
                }
                else
                {
                    {
                        dgvSectionService.ColumnHeadersVisible = true;
                        dgvSectionService.BorderStyle = BorderStyle.FixedSingle;
                        DataGridPrinter1.AddSection(g, false,
                                                    null,
                                                    dgvSectionService,
                                                    DVSection1);
                    }
                }

                //
                //------------------------------------------------------------------------------------------
                //

                DVSection4A = dataDetails.DV_Section4A_Fill(DVSection3);

                dgvSection4A.ColumnHeadersVisible = true;
                dgvSection4A.BorderStyle = BorderStyle.FixedSingle;
                DataGridPrinter1.AddSection(g, true,
                                            null,
                                            dgvSection4A,
                                            DVSection4A);

                DVSection4B = dataDetails.DV_Section4B_Fill(DVSection3);

                dgvSection4A.ColumnHeadersVisible = true;
                dgvSection4A.BorderStyle = BorderStyle.FixedSingle;
                DataGridPrinter1.AddSection(g, true,
                                            null,
                                            dgvSection4B,
                                            DVSection4B);

                //
                //------------------------------------------------------------------------------------------
                //

                string sNonTaxAmount = string.Format("{0:C}", NonTaxAmount);
                string sTaxAmount = string.Format("{0:C}", TaxAmount);

                DVSection2 = dataDetails.DV_Section2_Open();

                DVSection2 = dataDetails.DV_Section2_Fill(DVSection2, "SUB TOTAL", sNonTaxAmount);
                DVSection2 = dataDetails.DV_Section2_Fill(DVSection2, "TAX", sTaxAmount);

                dgvSection2.ColumnHeadersVisible = false;
                dgvSection2.BorderStyle = BorderStyle.FixedSingle;
                DataGridPrinter1.AddSection(g, false,
                                            null,
                                            dgvSection2,
                                            DVSection2);
                //
                //------------------------------------------------------------------------------------------
                //

                decimal TotalAmount = NonTaxAmount + TaxAmount;
                string sTotalAmount = string.Format("{0:C}", TotalAmount);

                DVSection3 = dataDetails.DV_Section3_Fill(DVSection3, sTotalAmount);

                dgvSection3.ColumnHeadersVisible = true;
                dgvSection3.BorderStyle = BorderStyle.FixedSingle;
                DataGridPrinter1.AddSection(g, false,
                                            null,
                                            dgvSection3,
                                            DVSection3);

                //
                //------------------------------------------------------------------------------------------
                //
                NonTaxAmount = 0;
                TaxAmount = 0;

            }

            DataGridPrinter1.PrintLogo = true;
            DataGridPrinter1.PrintLogoCenter = false;
        }
    }
}

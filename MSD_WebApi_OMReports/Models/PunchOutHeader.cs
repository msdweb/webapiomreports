﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace MSD_WebApi_OMReports.Models
{
    public class punchoutHeader
    {
        [XmlElement(ElementName = "From", Order = 1)]
        public punchoutFromTo From;

        [XmlElement(ElementName = "To", Order = 2)]
        public punchoutFromTo To;

        [XmlElement(ElementName = "Sender", Order = 3)]
        public punchoutSender Sender;

        public punchoutHeader()
        {
            From = new punchoutFromTo();
            To = new punchoutFromTo();
            Sender = new punchoutSender();
        }
    }

    public class punchoutFromTo
    {
        [XmlElement(ElementName = "Credential")]
        public List<punchoutCredential> Credentials { get; set; }
        //public punchoutCredential Credential;
        //public string UserAgent { get; set; }

        public punchoutFromTo()
        {
            Credentials = new List<punchoutCredential>();
            //Credential = new punchoutCredential();
        }

    }

    public class punchoutSender
    {
        [XmlElement(ElementName = "Credential")]
        public List<punchoutSenderCredential> Credentials { get; set; }
        //public punchoutSenderCredential Credential;
        public string UserAgent { get; set; }

        public punchoutSender()
        {
            Credentials = new List<punchoutSenderCredential>();
            //Credential = new punchoutSenderCredential();
        }
    }

    public class punchoutCredential
    {
        [XmlAttribute]
        public string domain { get; set; }

        [XmlElement(ElementName = "Identity")]
        public string Identity { get; set; }
    }

    public class punchoutSenderCredential
    {
        [XmlAttribute]
        public string domain { get; set; }

        [XmlElement(ElementName = "Identity", Order = 1)]
        public string Identity { get; set; }

        [XmlElement(ElementName = "SharedSecret", Order = 2)]
        public string SharedSecret { get; set; }
    }

    //-------------------------------------------------------------------------------------
}


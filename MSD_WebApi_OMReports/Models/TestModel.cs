﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using System.Xml;
using System.ServiceModel;

namespace MSD_WebApi_OMReports.Models
{
    [XmlRoot(ElementName = "cXML", DataType = "string", IsNullable = false)]
    [Serializable]
    public class MyRequest
    {
        [XmlAttribute]
        public string timestamp { get; set; }
        [XmlAttribute]
        public string payloadID { get; set; }
        [XmlAttribute("xml:lang")]
        public string lang { get; set; }

        [XmlElement(ElementName = "Header")]
        public punchoutHeader Header;
        public MyRequest()
        {
            Header = new punchoutHeader();
        }
    }

    [XmlRoot(ElementName = "MyResponse")]
    public class MyResponse
    {
        [XmlElement(ElementName = "Name", Order = 1)]
        public string Name { get; set; }

        [XmlElement(ElementName = "Age", Order = 2)]
        public int Age { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace MSD_WebApi_OMReports.Models
{
    //public class punchoutOrderMessageString
    //{
    //    [XmlElement(ElementName = "Header")]
    //    public punchoutOrderMessage punchoutOrderMessage;

    //    public punchoutOrderMessageString()
    //    {
    //        punchoutOrderMessage = new punchoutOrderMessage();
    //    }
    //}

    [XmlRoot(ElementName = "cXML", DataType = "string", IsNullable = false)]
    public class punchoutOrderMessageRootEmpty
    {
        [XmlAttribute]
        public string timestamp { get; set; }
        [XmlAttribute]
        public string payloadID { get; set; }
        [XmlAttribute("xml:lang")]
        public string lang { get; set; }
        [XmlElement(ElementName = "Header")]
        public punchoutHeader Header;

        [XmlElement(ElementName = "Message")]
        public punchoutMessageEmpty Message;

        public punchoutOrderMessageRootEmpty()
        {
            Header = new punchoutHeader();
            Message = new punchoutMessageEmpty();
        }
    }


    [XmlRoot(ElementName = "cXML", DataType = "string", IsNullable = false)]
    public class punchoutOrderMessageRoot
    {
        [XmlAttribute]
        public string timestamp { get; set; }
        [XmlAttribute]
        public string payloadID { get; set; }
        [XmlAttribute("xml:lang")]
        public string lang { get; set; }
        [XmlElement(ElementName = "Header")]
        public punchoutHeader Header;

        [XmlElement(ElementName = "Message")]
        public punchoutMessage Message;

        public punchoutOrderMessageRoot()
        {
            Header = new punchoutHeader();
            Message = new punchoutMessage();
        }
    }

    public class punchoutMessageEmpty
    {
        [XmlElement(ElementName = "PunchOutOrderMessage", Order = 1)]
        public string PunchOutOrderMessage { get; set; }

        public punchoutMessageEmpty()
        {
        }
    }
    public class punchoutMessage
    {
        [XmlElement(ElementName = "PunchOutOrderMessage", Order = 1)]
        public punchoutOrderMessage PunchOutOrderMessage;

        public punchoutMessage()
        {
            PunchOutOrderMessage = new punchoutOrderMessage();
        }
    }
    public class punchoutOrderMessage
    {
        [XmlElement(ElementName = "BuyerCookie", Order = 1)]
        public string OrderBuyerCookie { get; set; }

        [XmlElement(ElementName = "PunchOutOrderMessageHeader", Order = 2)]
        public punchoutOrderMessageBodyHeader PunchOutOrderMessageHeader;

        [XmlElement(ElementName = "ItemIn", Order = 3)]
        //public punchoutOrderMessageBodyItemIn ItemIn;
        public List<punchoutOrderMessageBodyItemIn> ItemIn;


        public punchoutOrderMessage()
        {
            PunchOutOrderMessageHeader = new punchoutOrderMessageBodyHeader();
            ItemIn = new List<punchoutOrderMessageBodyItemIn>();
        }
    }

    public class punchoutOrderMessageBodyHeader
    {
        [XmlAttribute]
        public string operationAllowed { get; set; }
        [XmlElement(ElementName = "Total")]
        public PunchOutOrderMessageTotal moneyTotal;
        public punchoutOrderMessageBodyHeader()
        {
            moneyTotal = new PunchOutOrderMessageTotal();
        }
    }

    public class punchoutOrderMessageBodyItemIn
    {
        [XmlAttribute]
        public string quantity { get; set; }
        [XmlElement]
        public PunchOutOrderMessageItemID ItemID;
        [XmlElement]
        public PunchOutOrderMessageItemDetail ItemDetail;

        public punchoutOrderMessageBodyItemIn()
        {
            ItemID = new PunchOutOrderMessageItemID();
            ItemDetail = new PunchOutOrderMessageItemDetail();
        }
    }

    public class PunchOutOrderMessageTotal
    {
        [XmlElement(ElementName = "Money")]
        public PunchOutOrderMoneyTotal moneyTotal;

        public PunchOutOrderMessageTotal()
        {
            moneyTotal = new PunchOutOrderMoneyTotal();
        }
    }

    public class PunchOutOrderMoneyTotal
    {
        [XmlAttribute]
        public string currency { get; set; }
        [XmlText]
        public string Value { get; set; }
    }

    public class PunchOutOrderMessageItemIn
    {
        [XmlAttribute]
        public string quantity { get; set; }
        [XmlElement]
        public PunchOutOrderMessageItemID ItemID;
        [XmlElement]
        public PunchOutOrderMessageItemDetail ItemDetail;

        public PunchOutOrderMessageItemIn()
        {
            ItemID = new PunchOutOrderMessageItemID();
            ItemDetail = new PunchOutOrderMessageItemDetail();
        }
    }

    public class PunchOutOrderMessageItemID
    {
        [XmlElement]
        public string SupplierPartID { get; set; }
        [XmlElement]
        public string SupplierPartAuxiliaryID { get; set; }

    }

    public class PunchOutOrderMessageItemDetail
    {
        [XmlElement(ElementName = "UnitPrice")]
        public PunchOutOrderUnitPrice unitPrice;
        [XmlElement(ElementName = "Description")]
        public PunchOutOrderDescription orderDesc { get; set; }
        [XmlElement]
        public string UnitOfMeasure { get; set; }
        [XmlElement(ElementName = "Classification")]
        public PunchOutOrderClassification orderClassification { get; set; }
        [XmlElement]
        public string ManufacturerPartID { get; set; }
        public string ManufacturerName { get; set; }
        public PunchOutOrderMessageItemDetail()
        {
            unitPrice = new PunchOutOrderUnitPrice();
            orderDesc = new PunchOutOrderDescription();
            orderClassification = new PunchOutOrderClassification();
        }
    }

    public class PunchOutOrderUnitPrice
    {
        [XmlElement(ElementName = "Money")]
        public PunchOutUnitPriceMoneyCurrency moneyCurrency;

        public PunchOutOrderUnitPrice()
        {
            moneyCurrency = new PunchOutUnitPriceMoneyCurrency();
        }
    }

    public class PunchOutUnitPriceMoneyCurrency
    {
        [XmlAttribute]
        public string currency { get; set; }
        [XmlText]
        public string value { get; set; }
    }

    public class PunchOutOrderDescription
    {
        [XmlAttribute(AttributeName = "xml:lang")]
        public string language { get; set; }
        [XmlText]
        public string value { get; set; }
    }

    public class PunchOutOrderClassification
    {
        [XmlAttribute]
        public string domain { get; set; }
        [XmlText]
        public string value { get; set; }
    }
}
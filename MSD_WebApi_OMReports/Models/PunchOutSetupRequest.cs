﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace MSD_WebApi_OMReports.Models
{
    //public class punchoutRequestString
    //{
    //    [XmlElement(ElementName = "Header")]
    //    public punchoutRequest punchoutRequest;

    //    public punchoutRequestString()
    //    {
    //        punchoutRequest = new punchoutRequest();
    //    }
    //}

    [XmlRoot(ElementName = "cXML", DataType = "string", IsNullable = false)]
    public class punchoutRequest
    {
        [XmlAttribute]
        public string timestamp { get; set; }
        [XmlAttribute]
        public string payloadID { get; set; }
        [XmlAttribute("xml:lang")]
        public string lang { get; set; }
        [XmlElement(ElementName = "Header")]
        public punchoutHeader Header;
        [XmlElement(ElementName = "Request")]
        public punchoutBody Request;
        public punchoutRequest()
        {
            Header = new punchoutHeader();
            Request = new punchoutBody();
        }
    }

    public class punchoutBody
    {
        [XmlElement(ElementName = "PunchOutSetupRequest")]
        public punchoutSetupRequest PunchOutSetupRequest;

        public punchoutBody()
        {
            PunchOutSetupRequest = new punchoutSetupRequest();
        }
    }

    public class punchoutSetupRequest

    {
        [XmlAttribute]
        public string operation { get; set; }

        [XmlElement(ElementName = "BuyerCookie", Order = 1)]
        public string BuyerCookie { get; set; }

        [XmlElement(ElementName = "Extrinsic", Order = 2)]
        public List<punchoutExtrinsic> Extrinsics { get; set; }
        //public punchoutExtrinsic Extrinsic;

        [XmlElement(ElementName = "BrowserFormPost", Order = 3)]
        public punchoutURLParent BrowserFormPost;

        [XmlElement(ElementName = "SupplierSetup", Order = 4)]
        public punchoutURLParent SupplierSetup;

        [XmlElement(ElementName = "ShipTo", Order = 5)]
        public punchoutShipTo ShipTo;

        public punchoutSetupRequest()
        {
            Extrinsics = new List<punchoutExtrinsic>();
            //Extrinsic = new punchoutExtrinsic();
            BrowserFormPost = new punchoutURLParent();
            SupplierSetup = new punchoutURLParent();
            ShipTo = new punchoutShipTo();
        }
    }

    public class punchoutExtrinsic
    {
        [XmlAttribute]
        public string name { get; set; }
        [XmlText]
        public string Value { get; set; }
    }

    //-------------------------------------------------------------------------------------

    public class punchoutShipTo
    {
        [XmlElement(ElementName = "Address", Order = 1)]
        public punchoutSTAddress Address;

        public punchoutShipTo()
        {
            Address = new punchoutSTAddress();
        }
    }

    public class punchoutSTAddress
    {
        [XmlAttribute("addressID")]
        public string addressID { get; set; }

        [XmlElement(ElementName = "Name", Order = 1)]
        public punchoutSTName Name;

        [XmlElement(ElementName = "PostalAddress", Order = 2)]
        public punchoutSTPostalAddress PostalAddress;

        public punchoutSTAddress()
        {
            Name = new punchoutSTName();
            PostalAddress = new punchoutSTPostalAddress();
        }
    }

    public class punchoutSTName
    {
        [XmlAttribute("xml:lang")]
        public string lang { get; set; }

        [XmlText]
        public string Value { get; set; }
    }

    public class punchoutSTPostalAddress
    {
        [XmlAttribute("name")]
        public string name { get; set; }

        [XmlElement(ElementName = "DeliverTo", Order = 1)]
        public string DeliverTo { get; set; }

        [XmlElement(ElementName = "Street", Order = 2)]
        public string Street { get; set; }

        [XmlElement(ElementName = "City", Order = 3)]
        public string City { get; set; }

        [XmlElement(ElementName = "State", Order = 4)]
        public string State { get; set; }

        [XmlElement(ElementName = "PostalCode", Order = 5)]
        public string PostalCode { get; set; }

        [XmlElement(ElementName = "Country", Order = 6)]
        public punchoutSTCountry Country;

        public punchoutSTPostalAddress()
        {
            Country = new punchoutSTCountry();
        }
    }

    public class punchoutSTCountry
    {
        [XmlAttribute("isoCountryCode")]
        public string isoCountryCode { get; set; }

        [XmlText]
        public string Value { get; set; }
    }

    //-------------------------------------------------------------------------------------

    [XmlRoot(ElementName = "cXML", DataType = "string", IsNullable = false)]
    public class xmlRespond
    {
        [XmlAttribute]
        public string timestamp { get; set; }
        [XmlAttribute]
        public string payloadID { get; set; }
        [XmlAttribute("xml:lang")]
        public string lang { get; set; }
        [XmlElement(ElementName = "Response")]
        public punchoutResponse Response;
        public xmlRespond()
        {
            Response = new punchoutResponse();
        }
    }


    [XmlRoot(ElementName = "cXML", DataType = "string", IsNullable = false)]
    public class punchoutRespond
    {
        [XmlAttribute]
        public string timestamp { get; set; }
        [XmlAttribute]
        public string payloadID { get; set; }
        [XmlAttribute("xml:lang")]
        public string lang { get; set; }
        [XmlElement(ElementName = "Response")]
        public punchoutResponse Response;
        public punchoutRespond()
        {
            Response = new punchoutResponse();
        }
    }

    public class punchoutResponse
    {
        [XmlElement(ElementName = "Status", Order = 1)]
        public punchoutStatus Status;

        [XmlElement(ElementName = "PunchOutSetupResponse", Order = 2)]
        public punchoutSetupResponse SetupResponse;

        public punchoutResponse()
        {
            Status = new punchoutStatus();
            SetupResponse = new punchoutSetupResponse();

        }
    }

    public class punchoutStatus
    {
        [XmlAttribute]
        public string text { get; set; }

        [XmlAttribute]
        public string code { get; set; }
    }

    public class SetupResponse
    {
        [XmlElement(ElementName = "punchoutSetupResponse", Order = 1)]
        public punchoutSetupResponse punchoutSetupResponse;

        public SetupResponse()
        {
            punchoutSetupResponse = new punchoutSetupResponse();
        }
    }
    public class punchoutSetupResponse
    {
        [XmlElement(ElementName = "StartPage", Order = 1)]
        public punchoutURLParent StartPage;

        public punchoutSetupResponse()
        {
            StartPage = new punchoutURLParent();
        }
    }

    //-------------------------------------------------------------------------------------

    public class punchoutURLParent
    {
        public string URL { get; set; }
        //[XmlElement(ElementName = "URL")]
        //public punchoutURL URL;

        //public punchoutURLParent()
        //{
        //    URL = new punchoutURL();
        //}
    }

    public class punchoutURL
    {
        public string URL { get; set; }
    }

}

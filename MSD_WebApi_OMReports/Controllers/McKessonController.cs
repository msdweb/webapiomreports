﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Security.Cryptography;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Xml;
using System.Xml.Serialization;
using System.Web.Script.Serialization;
using Newtonsoft.Json;


namespace MSD_WebApi_OMReports.Controllers
{
    public class McKessonController : ApiController
    {

        //https://qawebapi.msdonline.com/api/websites/msdonline/mckesson-migration-validate-user/msd-admin4

        [HttpPost]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/websites/{website}/mckesson-migration-validate-user/")]
        public bool MckessonMigrationValidateUser1(string website, string username, [FromBody]string password)
        {

            string filename = "";
            string extention = ".txt";
            string strNow = "";
            string pathfilename = "";
            string lines = "";

            //
            //-----------------------------------------------------------------------------------------------------
            //

            System.IO.StreamWriter filetxt;

            filename = "C:\\temp\\MSD-Migration\\MckessonMigrationValidateUser-";
            strNow = Convert.ToString(DateTime.Now);
            strNow = strNow.Replace(":", "-");
            strNow = strNow.Replace("/", "-");

            pathfilename = (filename + strNow + extention).Replace(" ", "-");
            filetxt = new System.IO.StreamWriter(pathfilename);

            lines = "website: " + website + "  -  username: " + username + "  -  password: " + password;
            filetxt.WriteLine(lines);

            filetxt.Close();

            //
            //-----------------------------------------------------------------------------------------------------
            //


            bool Ret = false;

            try
            {
                switch (website)
                {
                    case "msdonline":
                        Ret = Get_MSDOnlineValidation(username, password);
                        break;
                    case "onemed":
                        Ret = Get_OnemedValidation(username, password);
                        break;
                    default:
                        Ret = false;
                        break;
                }
            }
            catch
            {
                Ret = false;
            }

            Update_MigrationValidateUserLog(website, username, Ret);

            return Ret;
        }

        [HttpPost]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/websites/{website}/mckesson-migration-validate-user/{username}/")]
        public bool MckessonMigrationValidateUser(string website, string username, [FromBody]string password)
        {

            string filename = "";
            string extention = ".txt";
            string strNow = "";
            string pathfilename = "";
            string lines = "";

            //
            //-----------------------------------------------------------------------------------------------------
            //

            System.IO.StreamWriter filetxt;

            filename = "C:\\temp\\MSD-Migration\\MckessonMigrationValidateUser-";
            strNow = Convert.ToString(DateTime.Now);
            strNow = strNow.Replace(":", "-");
            strNow = strNow.Replace("/", "-");

            pathfilename = (filename + strNow + extention).Replace(" ", "-");
            filetxt = new System.IO.StreamWriter(pathfilename);

            lines = "website: " + website + "  -  username: " + username + "  -  password: " + password;
            filetxt.WriteLine(lines);

            filetxt.Close();

            //
            //-----------------------------------------------------------------------------------------------------
            //


            bool Ret = false;

            try
            {
                switch (website)
                {
                    case "msdonline":
                        Ret = Get_MSDOnlineValidation(username, password);
                        break;
                    case "onemed":
                        Ret = Get_OnemedValidation(username, password);
                        break;
                    default:
                        Ret = false;
                        break;
                }
            }
            catch
            {
                Ret = false;
            }

            Update_MigrationValidateUserLog(website, username, Ret);

            return Ret;
        }

        protected void Update_MigrationValidateUserLog(string website, string username, bool result)
        {
            try
            {
                string connectionString;

                try
                {
                    connectionString = ConfigurationManager.ConnectionStrings["OMReportsConnectionString"].ConnectionString;
                }
                catch
                {
                    connectionString = "Data Source=10.1.1.20; Initial Catalog=medcc_custom; User ID=MsdWebApi; Password=pwd4MsdWebApi";
                }

                //connectionString = "Data Source=devsql03.msddev.local; Initial Catalog=medcc_custom; User ID=MsdWebApi; Password=pwd4MsdWebApi";

                using (var connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    var cmd = new SqlCommand("[wsp_webapi_MckessonValidateUserLog]", connection);
                    cmd.Parameters.AddWithValue("@website", website);
                    cmd.Parameters.AddWithValue("@username", username);
                    cmd.Parameters.AddWithValue("@result", result);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = int.MaxValue;

                    cmd.ExecuteNonQuery();
                    cmd.Dispose();
                    connection.Close();
                }
            }
            catch (Exception ex) { }

        }
        protected bool Get_OnemedValidation(string UserName, string Password)
        {
            string filename = "";
            string extention = ".txt";
            string strNow = "";
            string pathfilename = "";
            string lines = "";

            bool Ret = false;

            try
            {
                string connectionString;

                try
                {
                    connectionString = ConfigurationManager.ConnectionStrings["OMReportsConnectionString"].ConnectionString;
                }
                catch
                {
                    connectionString = "Data Source=10.1.1.20; Initial Catalog=medcc_custom; User ID=MsdWebApi; Password=pwd4MsdWebApi";
                }

                //connectionString = "Data Source=devsql03.msddev.local; Initial Catalog=medcc_custom; User ID=MsdWebApi; Password=pwd4MsdWebApi";

                var result = string.Empty;

                using (var connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    var cmd = new SqlCommand("[wsp_webapi_MckessonValidateUserOM]", connection);
                    cmd.Parameters.AddWithValue("@username", UserName);
                    cmd.Parameters.AddWithValue("@password", Password);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = int.MaxValue;

                    SqlDataReader oDR = default(SqlDataReader);
                    oDR = cmd.ExecuteReader();

                    oDR.Read();
                    result = oDR["result"].ToString();
                    oDR.Close();
                    cmd.Dispose();
                    connection.Close();

                    if (result != null)
                    {
                        if (result == "True")
                        {
                            Ret = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //
                //-----------------------------------------------------------------------------------------------------
                //

                System.IO.StreamWriter filetxterr;

                filename = "C:\\temp\\MSD-Migration\\Get_MSDOnlineValidationErr-";
                strNow = Convert.ToString(DateTime.Now);
                strNow = strNow.Replace(":", "-");
                strNow = strNow.Replace("/", "-");

                pathfilename = (filename + strNow + extention).Replace(" ", "-");
                filetxterr = new System.IO.StreamWriter(pathfilename);

                lines = ex.Message;
                filetxterr.WriteLine(lines);

                filetxterr.Close();

                //
                //-----------------------------------------------------------------------------------------------------
                //

                Ret = false;
            }

            return Ret;
        }

        protected bool Get_MSDOnlineValidation(string UserName, string Password)
        {
            string filename = "";
            string extention = ".txt";
            string strNow = "";
            string pathfilename = "";
            string lines = "";

            //string uri = "http://localhost:80/msdonline/Account/ValidateUser";
            //string uri = "http://172.16.0.18:80/msdonline/Account/ValidateUser";
            string uri = "https://www.msdonline.com/Account/ValidateUser";
            //string uri = "https://mig.msdonline.com/Account/ValidateUser";

            JavaScriptSerializer js1 = new JavaScriptSerializer();
            bool Ret = false;

            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                HttpWebRequest request = WebRequest.Create(uri) as HttpWebRequest;
                request.Method = "POST";
                request.ContentType = "application/json";

                //-----------------------------------------------------------------------------------------------------

                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    object[] parameters =
                    {
                        UserName,
                        Password
                    };

                    streamWriter.Write(JsonConvert.SerializeObject(parameters));
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                //-----------------------------------------------------------------------------------------------------

                var response = request.GetResponse();
                var result = string.Empty;

                using (var responseStream = response.GetResponseStream())
                {
                    if (responseStream == null)
                        throw new EndOfStreamException("Steam Empty");

                    var reader = new StreamReader(responseStream, Encoding.UTF8);
                    //var jsonContent = reader.ReadToEnd();
                    result = reader.ReadToEnd(); //JsonConvert.DeserializeObject<string>(jsonContent);
                }

                if (result != null)
                {
                    if (result == "True")
                    {
                        Ret = true;
                    }
                }
            }
            catch (Exception ex)
            {
                //
                //-----------------------------------------------------------------------------------------------------
                //

                System.IO.StreamWriter filetxterr;

                filename = "C:\\temp\\MSD-Migration\\Get_MSDOnlineValidationErr-";
                strNow = Convert.ToString(DateTime.Now);
                strNow = strNow.Replace(":", "-");
                strNow = strNow.Replace("/", "-");

                pathfilename = (filename + strNow + extention).Replace(" ", "-");
                filetxterr = new System.IO.StreamWriter(pathfilename);

                lines = ex.Message;
                filetxterr.WriteLine(lines);

                filetxterr.Close();

                //
                //-----------------------------------------------------------------------------------------------------
                //
            }

            return Ret;
        }

        [HttpPost]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/customers/{customerId}/mckesson-message-info/{website}")]
        public string MckessonMessageInfo(string customerId, string website)
        {
            string filename = "";
            string extention = ".txt";
            string strNow = "";
            string pathfilename = "";
            string lines = "";

            //
            //-----------------------------------------------------------------------------------------------------
            //

            //System.IO.StreamWriter filetxt;

            //filename = "C:\\temp\\MSD-Migration\\MckessonMessageInfo-";
            //strNow = Convert.ToString(DateTime.Now);
            //strNow = strNow.Replace(":", "-");
            //strNow = strNow.Replace("/", "-");

            //pathfilename = (filename + strNow + extention).Replace(" ", "-");
            //filetxt = new System.IO.StreamWriter(pathfilename);

            //lines = "customerId: " + customerId + "  -  website: " + website;
            //filetxt.WriteLine(lines);

            //filetxt.Close();

            //
            //-----------------------------------------------------------------------------------------------------
            //


            string[] Data = new string[7];

            try
            {
                var ds = new DataSet();
                string connectionString;

                try
                {
                    connectionString = ConfigurationManager.ConnectionStrings["OMReportsConnectionString"].ConnectionString;
                }
                catch
                {
                    connectionString = "Data Source=10.1.1.20; Initial Catalog=medcc_custom; User ID=MsdWebApi; Password=pwd4MsdWebApi";
                }

                //connectionString = "Data Source=devsql03.msddev.local; Initial Catalog=medcc_custom; User ID=MsdWebApi; Password=pwd4MsdWebApi";

                using (var connection = new SqlConnection(connectionString))
                {
                    var cmd = new SqlCommand("[wsp_webapi_MckessonMessageInfo]", connection);
                    cmd.Parameters.AddWithValue("@customer_id", customerId);
                    cmd.Parameters.AddWithValue("@website", website);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = int.MaxValue;

                    using (var adapter = new SqlDataAdapter(cmd))
                    {
                        adapter.Fill(ds);
                    }

                    Data[0] = "";
                    Data[1] = "";
                    Data[2] = "";
                    Data[3] = "";
                    Data[4] = "";
                    Data[5] = "";
                    Data[6] = "";

                    if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        Data[0] = string.Format("{0}", ds.Tables[0].Rows[0]["ImageLoginPath"]);
                        Data[1] = string.Format("{0}", ds.Tables[0].Rows[0]["ImageDashboardTopPath"]);
                        Data[2] = string.Format("{0}", ds.Tables[0].Rows[0]["ImageDashboardSidePath"]);
                        Data[3] = string.Format("{0}", ds.Tables[0].Rows[0]["ReadMoreLink"]);
                        Data[4] = string.Format("{0}", ds.Tables[0].Rows[0]["SupplyManagerLink"]);
                        Data[5] = string.Format("{0}", ds.Tables[0].Rows[0]["RegisterLink"]);
                        Data[6] = string.Format("{0}", ds.Tables[0].Rows[0]["LockCustomer"]);
                    }
                }
            }
            catch (Exception ex)
            {
                //
                //-----------------------------------------------------------------------------------------------------
                //

                System.IO.StreamWriter filetxterr;

                filename = "C:\\temp\\MSD-Migration\\MckessonMessageInfo-";
                strNow = Convert.ToString(DateTime.Now);
                strNow = strNow.Replace(":", "-");
                strNow = strNow.Replace("/", "-");

                pathfilename = (filename + strNow + extention).Replace(" ", "-");
                filetxterr = new System.IO.StreamWriter(pathfilename);

                lines = ex.Message;
                filetxterr.WriteLine(lines);

                filetxterr.Close();

                //
                //-----------------------------------------------------------------------------------------------------
                //

            }

            //
            //-----------------------------------------------------------------------------------------------------
            //

            //System.IO.StreamWriter filetxtData;

            //filename = "C:\\temp\\MSD-Migration\\MckessonMessageInfo-";
            //strNow = Convert.ToString(DateTime.Now);
            //strNow = strNow.Replace(":", "-");
            //strNow = strNow.Replace("/", "-");

            //pathfilename = (filename + strNow + extention).Replace(" ", "-");
            //filetxtData = new System.IO.StreamWriter(pathfilename);

            //lines = "Data: " + Data[0] + " - " + Data[1] + " - " + Data[2] + " - " + Data[3] + " - " + Data[4] + " - " + Data[5] + " - " + Data[6];
            //filetxtData.WriteLine(lines);

            //filetxtData.Close();

            //
            //-----------------------------------------------------------------------------------------------------
            //

            return JsonConvert.SerializeObject(Data);
        }
    }
}
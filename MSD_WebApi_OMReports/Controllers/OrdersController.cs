﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web.Http;
using System.Web;
using System.Collections.Specialized;
using MSD.Models;

namespace msd_webapi.Controllers
{
    public class OrdersController : ApiController
    {
        public IHttpActionResult Get()
        {
            try
            {
                NameValueCollection nvc = HttpUtility.ParseQueryString(Request.RequestUri.Query);

                var CustomerId = nvc["CustomerId"];
                if (CustomerId == null) CustomerId = "";

                var OrderId = nvc["OrderId"];
                if (OrderId == null) OrderId = "";

                var pageSize = Convert.ToInt32(nvc["pageSize"]);
                if (pageSize == null) pageSize = 0;

                var page = Convert.ToInt32(nvc["page"]);
                if (page == null) page = 0;

                if (pageSize == 0) pageSize = 400;
                if (page == 0) page = 1;

                string sSqlSelectCmd = string.Empty;
                string sFields = string.Empty;

                string strWhere = "where  delete_flag='N' and completed='N' and company_id='1' and cancel_flag='N' and source_code_no='707'";

                string strOffSetRows = string.Empty;

                if (CustomerId != "")
                {
                    strWhere += " and [customer_id] = " + CustomerId;
                }

                if (OrderId != "")
                {
                    strWhere += " and [order_no] = " + OrderId;
                }

                strOffSetRows = "OFFSET " + ((page * pageSize) - pageSize) + " ROWS FETCH NEXT " + pageSize + " ROWS ONLY";

                sFields = "[order_no],[customer_id],[order_date],[ship2_name],[ship2_add1],[ship2_add2],[ship2_city],[ship2_state],[ship2_zip],[ship2_country],[ship2_email_address],[ship_to_phone],[requested_date],[location_id],[carrier_id]";


                sSqlSelectCmd = "SELECT " + sFields + " FROM [medcc_live].[dbo].[oe_hdr] " +
                                strWhere + " ORDER BY [order_date],[order_no] " + strOffSetRows;

                //string sSqlActiveItemsCmd = "[dbo].wsp_webapi_activeitems";

                string sSqlSelectTCmd = "SELECT Count(*) as total FROM [medcc_live].[dbo].[oe_hdr] " + strWhere;

                

                string connectionString = string.Empty;
                try
                {
                    connectionString = ConfigurationManager.ConnectionStrings["webapiConnectionString"].ConnectionString;
                }
                catch
                {
                    connectionString = "Data Source=10.1.1.20; Initial Catalog=medcc_custom; User ID=MsdWebApi; Password=pwd4MsdWebApi";
                }

                // connectionString = "Data Source=MSDSQL03.msd.local; Initial Catalog=medcc_custom; User ID=MsdWebApi; Password=pwd4MsdWebApi";

                var Orders = new List<Order>();

                SqlDataReader oDR = default(SqlDataReader);

                using (SqlConnection _SQLConn = new SqlConnection(connectionString))
                {
                    _SQLConn.Open();

                    using (SqlCommand SQLCmd = new SqlCommand())
                    {
                        SQLCmd.Connection = _SQLConn;
                        SQLCmd.CommandType = CommandType.Text;

                        SQLCmd.CommandText = sSqlSelectTCmd;
                        oDR = SQLCmd.ExecuteReader();

                        oDR.Read();
                        var TotalResult = oDR["Total"].ToString();
                        oDR.Close();

                        SQLCmd.CommandText = sSqlSelectCmd;

                        using (SqlDataReader SQLRdr = SQLCmd.ExecuteReader())
                        {
                            while (SQLRdr.Read())
                            {
                                var Order = new Order();

                                Order.OrderNo = SQLRdr["order_no"].ToString();
                                Order.CustomerId = SQLRdr["customer_id"].ToString();
                                Order.OrderDate = SQLRdr["order_date"].ToString();

                                Order.Ship2Name = SQLRdr["ship2_name"].ToString();
                                Order.Ship2Address1 = SQLRdr["ship2_add1"].ToString();
                                Order.Ship2Address2 = SQLRdr["ship2_add2"].ToString();
                                Order.Ship2City = SQLRdr["ship2_city"].ToString();
                                Order.Ship2State = SQLRdr["ship2_state"].ToString();
                                Order.Ship2Zip = SQLRdr["ship2_zip"].ToString();
                                Order.Ship2Country = SQLRdr["ship2_country"].ToString();
                                Order.Ship2EmailAddress = SQLRdr["ship2_email_address"].ToString();
                                Order.ShipToPhone = SQLRdr["ship_to_phone"].ToString();
                                Order.RequestedDate = SQLRdr["requested_date"].ToString();
                                Order.LocationId = SQLRdr["location_id"].ToString();
                                Order.CarrierId = SQLRdr["carrier_id"].ToString();

                                Order.TotalResult = TotalResult;
                                
                                Orders.Add(Order);
                            }
                        }
                    }
                }


                if (Orders.Count > 0)
                {
                    return Ok(Orders);
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Http.Cors;
using System.Linq;
using System.Text;
using System.Web.Http;
using System.Web;

namespace MSD_WebApi_OMReports.Controllers
{
    public class OTInvoicesBatchController : ApiController
    {
        [HttpPost]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/associates/{associateId}/ot-invoices")]
        public IHttpActionResult OTInvoicesBatch(string associateId, [FromBody]object[] parameters)
        {
            try
            {
                string FilePath = string.Empty;

                string connectionString;

                try
                {
                    connectionString = ConfigurationManager.ConnectionStrings["OTInvoiceConnectionString"].ConnectionString;
                }
                catch
                {
                    connectionString = "Data Source=MSDSQL14-LS; Initial Catalog=WrenTrack; Persist Security Info=True; User ID=OTWebApi;Password=|M9^[?VP3qNU";
                }

                //connectionString = "Data Source=MSDSQL13; Initial Catalog=WrenTrack; Persist Security Info=True; User ID=OTUser; Password=aD@U723C";
                connectionString = "Data Source=MSDSQL14-LS; Initial Catalog=WrenTrack; Persist Security Info=True; User ID=OTWebApi;Password=|M9^[?VP3qNU";

                FilePath = OTInvoices.OTInvoices.Create_OTInvoicesBatch(associateId, parameters, connectionString);

                var baseUrl = Request.RequestUri.GetLeftPart(UriPartial.Authority);

                if (!string.IsNullOrWhiteSpace(FilePath))
                {
                    return Ok(baseUrl + "/" + FilePath);
                }
                else
                {
                    return Ok(""); //NotFound();
                }
            }
            catch (Exception ex)
            {
                return Ok(""); // InternalServerError(ex);
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Http.Cors;
using System.Linq;
using System.Text;
using System.Web.Http;
using System.Web;

namespace MSD_WebApi_OMReports.Controllers
{
    public class OMOrdersController : ApiController
    {
        //[EnableCors(origins: "*", headers: "*", methods: "*")]
        //public IHttpActionResult Get(string id = "")
        //{
        //    try
        //    {
        //        string orderno = id;

        //        string connectionString;

        //        try
        //        {
        //            connectionString = ConfigurationManager.ConnectionStrings["OMOrderConnectionString"].ConnectionString;
        //        }
        //        catch
        //        {
        //            connectionString = "Data Source=10.1.1.113; Initial Catalog=medcc_custom; User ID=MsdWebApi; Password=pwd4MsdWebApi";
        //        }

        //        connectionString = "Data Source=devsql03.msddev.local; Initial Catalog=medcc_custom; User ID=MsdWebApi; Password=pwd4MsdWebApi";

        //        string FilePath = string.Empty;

        //        var baseUrl = Request.RequestUri.GetLeftPart(UriPartial.Authority);

        //        FilePath = OMOrders.OMOrders.Create_OMOrderHistory("", orderno, connectionString);

        //        if (!string.IsNullOrWhiteSpace(FilePath))
        //        {
        //            return Ok(baseUrl + "/" + FilePath);
        //        }
        //        else
        //        {
        //            return NotFound();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return InternalServerError(ex);
        //    }
        //}

        [HttpPost]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/customers/{customerId}/om-orders/{orderno}")]
        public IHttpActionResult OMOrder(string customerId, string orderNo)
        {
            try
            {
                string FilePath = string.Empty;

                string orderno = orderNo;

                string connectionString;

                try
                {
                    connectionString = ConfigurationManager.ConnectionStrings["OMOrderConnectionString"].ConnectionString;
                }
                catch
                {
                    connectionString = "Data Source=10.1.1.113; Initial Catalog=medcc_custom; User ID=MsdWebApi; Password=pwd4MsdWebApi";
                }

                //connectionString = "Data Source=devsql03.msddev.local; Initial Catalog=medcc_custom; User ID=MsdWebApi; Password=pwd4MsdWebApi";

                FilePath = OMOrders.OMOrders.Create_OMOrderHistory(customerId, orderno, connectionString);

                var baseUrl = Request.RequestUri.GetLeftPart(UriPartial.Authority);

                if (!string.IsNullOrWhiteSpace(FilePath))
                {
                    return Ok(baseUrl + "/" + FilePath);
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}

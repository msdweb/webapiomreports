﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml;
using System.Web.Http.Cors;
using System.Web.Http;
using MSD_WebApi_OMReports.Models;

namespace MSD_WebApi_OMReports.Controllers
{
    //.Net XmlSerializer.
    //datacontract xml attribute example
    public class testXMLController : ApiController
    {
        [HttpGet]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/gettest")]
        public string Get()
        {
            string value = "hello";

            return value;
        }

        //[HttpPost]
        //[EnableCors(origins: "*", headers: "*", methods: "*")]
        //[Route("api/testXML")]
        //public MyRequest Post([FromBody] MyRequest value)
        //{
        //    MyRequest value1 = new MyRequest();
        //    return value;
        //}
    }
    
}
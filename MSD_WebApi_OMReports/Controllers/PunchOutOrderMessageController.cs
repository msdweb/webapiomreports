﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Security.Cryptography;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Xml;
using System.Xml.Serialization;
using System.Threading.Tasks;
using System.Web.Hosting;
using Newtonsoft.Json;
using MSD_WebApi_OMReports.Models;

namespace MSD_WebApi_OMReports.Controllers
{
    public class PunchOutOrderMessageController : ApiController
    {
        [HttpPost]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/PunchOutOrderMessage")]
        public void PunchOutOrderMessage([FromBody] punchoutOrderMessageRoot objpunchoutOrderMessage)
        {

            try
            {
                //string strPunchoutOrderMessage = string.Empty;
                XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                ns.Add("", "");

                ////-----------------------------------------------------------------------------------------------------

                //var stringwriter = new System.IO.StringWriter();

                //using (XmlWriter writer = XmlWriter.Create(stringwriter))
                //{
                //    writer.WriteDocType("cXML", null, "http://xml.cxml.org/schemas/cXML/1.2.038/cXML.dtd", null);
                //    var serializer = new XmlSerializer(objpunchoutOrderMessage.GetType());
                //    serializer.Serialize(writer, objpunchoutOrderMessage, ns);
                //}
                //strPunchoutOrderMessage = stringwriter.ToString();
                ////strPunchoutOrderMessage = "=" + strPunchoutOrderMessage;
                //strPunchoutOrderMessage = strPunchoutOrderMessage.Replace("utf-16", "utf-8");

                //-----------------------------------------------------------------------------------------------------

                //System.Threading.Thread.Sleep(8000);

                //
                //-----------------------------------------------------------------------------------------------------
                //

                string filename = "C:\\temp\\MSD-PunchOut\\punchoutOrderMessage-";
                string extention = ".xml";

                string strNow = Convert.ToString(DateTime.Now);
                strNow = strNow.Replace(":", "-");
                strNow = strNow.Replace("/", "-");

                string pathfilename = (filename + strNow + extention).Replace(" ", "-");

                FileStream file = File.Create(pathfilename);
                using (XmlWriter writer = XmlWriter.Create(file))
                {
                    writer.WriteDocType("cXML", null, "http://xml.cxml.org/schemas/cXML/1.2.014/cXML.dtd", null);
                    var ser = new XmlSerializer(objpunchoutOrderMessage.GetType());
                    ser.Serialize(writer, objpunchoutOrderMessage, ns);
                }
                file.Close();

                //
                //-----------------------------------------------------------------------------------------------------
                //

                //System.IO.StreamWriter filetxt;

                //filename = "C:\\temp\\MSD-PunchOut\\punchoutOrderMessage-";
                //extention = ".txt";

                //strNow = Convert.ToString(DateTime.Now);
                //strNow = strNow.Replace(":", "-");
                //strNow = strNow.Replace("/", "-");

                //pathfilename = (filename + strNow + extention).Replace(" ", "-");
                //filetxt = new System.IO.StreamWriter(pathfilename);

                //string lines = "";

                //lines = strPunchoutOrderMessage;
                //filetxt.WriteLine(lines);

                //filetxt.Close();

                //
                //-----------------------------------------------------------------------------------------------------
                //

                //SendOrderMessage(uri, strPunchoutOrderMessage);

            }
            catch (Exception ex)
            {
                // TODO: handle exception
            }

            return;
        }

        protected void SendOrderMessage(string uri, string strPunchoutOrderMessage)
        {
            System.IO.StreamWriter file;

            string filename = "C:\\temp\\MSD-PunchOut\\punchoutOrderMessageStatus-";
            string extention = ".txt";

            string strNow = Convert.ToString(DateTime.Now);
            strNow = strNow.Replace(":", "-");
            strNow = strNow.Replace("/", "-");

            string pathfilename = (filename + strNow + extention).Replace(" ", "-");
            file = new System.IO.StreamWriter(pathfilename);

            string lines = "";

            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                string results = string.Empty;

                //byte[] response1 = Encoding.ASCII.GetBytes(strPunchoutOrderMessage);

                //var request1 = (HttpWebRequest)WebRequest.Create(uri);
                //request1.ContentType = "application/x-www-form-urlencoded";
                //request1.Method = "POST";

                //try
                //{
                //    request1.GetRequestStream().Write(response1, 0, response1.Length);
                //}
                //catch (Exception e)
                //{
                //    lines = "Error: " + e.Message + "\r\n";
                //    file.WriteLine(lines);
                //}

                //WebResponse webresponse = null;
                //try
                //{
                //    webresponse = request1.GetResponse();

                //    var ReceiveStream = webresponse.GetResponseStream();
                //    Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                //    Char[] read = new Char[256];
                //    StreamReader readStream = new StreamReader(ReceiveStream, encode);
                //    int count = readStream.Read(read, 0, 256);
                //    while (count > 0)
                //    {
                //        String str = new String(read, 0, count);
                //        results += str;
                //        count = readStream.Read(read, 0, 256);
                //    }
                //}
                //catch (Exception e)
                //{
                //    lines = "Error: " + e.Message + "\r\n";
                //    file.WriteLine(lines);
                //}

                string response = HttpUtility.HtmlEncode(strPunchoutOrderMessage);

                HttpWebRequest request = WebRequest.Create(uri) as HttpWebRequest;
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.Accept = "text/xml";
                request.ContentLength = response.Length;

                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    streamWriter.Write(response);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                //-----------------------------------------------------------------------------------------------------

                using (var responseStream = request.GetResponse() as HttpWebResponse)
                {
                    if (responseStream == null) throw new EndOfStreamException("Steam Empty");
                    var reader = new StreamReader(responseStream.GetResponseStream());
                    var strContent = reader.ReadToEnd();
                    results = strContent;
                }

                //-----------------------------------------------------------------------------------------------------

                lines = "Start Message: " + results + " \r\n";
                file.WriteLine(lines);


            }
            catch (Exception e)
            {
                lines = "Error: " + e.Message + "\r\n";
                file.WriteLine(lines);
            }
            finally
            {
                file.Close();
            }

        }
    }
}
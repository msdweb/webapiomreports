﻿using System;
using System.Data;
using System.IO;
using System.Security.Cryptography;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Xml;
using System.Xml.Serialization;
using Newtonsoft.Json;
using MSD_WebApi_OMReports.Models;

namespace MSD_WebApi_OMReports.Controllers
{
    public class AribaOrderController : ApiController
    {
        [HttpPost]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/AribaOrder")]
        public void AribaOrder([FromBody] punchoutOrderMessageRoot parPunchoutOrderMessage)
        {

            //-----------------------------------------------------------------------------------------------------

            punchoutOrderMessageRoot objpunchoutOrderMessage = new punchoutOrderMessageRoot();

            //var serializer = new XmlSerializer(objpunchoutOrderMessage.GetType());

            //using (TextReader reader = new StringReader((string)param))
            //{
            //    objpunchoutOrderMessage = (punchoutOrderMessageRoot)serializer.Deserialize(reader);
            //}

            objpunchoutOrderMessage = parPunchoutOrderMessage;

            //
            //-----------------------------------------------------------------------------------------------------
            //

            //System.Threading.Thread.Sleep(8000);

            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add("", "");

            string filename = "C:\\temp\\MSD-PunchOut\\punchoutOrderMessage-";
            string extention = ".xml";

            string strNow = Convert.ToString(DateTime.Now);
            strNow = strNow.Replace(":", "-");
            strNow = strNow.Replace("/", "-");

            string pathfilename = (filename + strNow + extention).Replace(" ", "-");

            using (FileStream file = File.Create(pathfilename))
            {
                XmlSerializer oWriter = new XmlSerializer(objpunchoutOrderMessage.GetType());
                oWriter.Serialize(file, objpunchoutOrderMessage, ns);
                file.Close();
            }

            //
            //-----------------------------------------------------------------------------------------------------
            //

            return;

            //
            //-----------------------------------------------------------------------------------------------------
            //


            //-----------------------------------------------------------------------------------------------------

        }
    }
}

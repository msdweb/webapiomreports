﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Http.Cors;
using System.Linq;
using System.Text;
using System.Web.Http;
using System.Web;

namespace MSD_WebApi_OMReports.Controllers
{
    public class OMInvoicesController : ApiController
    {
        //[EnableCors(origins: "*", headers: "*", methods: "*")]
        //public IHttpActionResult Get(string id = "")
        //{
        //    try
        //    {
        //        string invoiceno = id;


        //        string connectionString;
        //        try
        //        {
        //            connectionString = ConfigurationManager.ConnectionStrings["webapiConnectionString"].ConnectionString;
        //        }
        //        catch
        //        {
        //            connectionString = "Data Source=10.1.1.113; Initial Catalog=medcc_custom; User ID=MsdWebApi; Password=pwd4MsdWebApi";
        //        }

        //        //connectionString = "Data Source=devsql03.msddev.local; Initial Catalog=medcc_custom; User ID=MsdWebApi; Password=pwd4MsdWebApi";

        //        string FilePath = string.Empty;

        //        var baseUrl = Request.RequestUri.GetLeftPart(UriPartial.Authority);

        //        FilePath = OMInvoices.OMInvoices.Create_OMInvoices("", invoiceno, connectionString);

        //        if (!string.IsNullOrWhiteSpace(FilePath))
        //        {
        //            return Ok(baseUrl + "/" + FilePath);
        //        }
        //        else
        //        {
        //            return NotFound();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return InternalServerError(ex);
        //    }
        //}

        [HttpPost]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/customers/{customerId}/om-invoices/{invoiceNo}")]
        public IHttpActionResult OMInvoice(string customerId, string invoiceNo)
        {
            try
            {
                string FilePath = string.Empty;

                string invoiceno = invoiceNo;

                string connectionString;

                if (invoiceno.Substring(0, 1) != "W")
                {
                    try
                    {
                        connectionString = ConfigurationManager.ConnectionStrings["OMInvoiceConnectionString"].ConnectionString;
                    }
                    catch
                    {
                        connectionString = "Data Source=10.1.1.113; Initial Catalog=medcc_custom; User ID=MsdWebApi; Password=pwd4MsdWebApi";
                    }

                    //connectionString = "Data Source=devsql03.msddev.local; Initial Catalog=medcc_custom; User ID=MsdWebApi; Password=pwd4MsdWebApi";

                    FilePath = OMInvoices.OMInvoices.Create_OMInvoices(customerId, invoiceno, connectionString);
                }
                else
                {
                    try
                    {
                        connectionString = ConfigurationManager.ConnectionStrings["OTInvoiceConnectionString"].ConnectionString;
                    }
                    catch
                    {
                        connectionString = "Data Source=MSDSQL14-LS; Initial Catalog=WrenTrack; Persist Security Info=True; User ID=OTWebApi;Password=|M9^[?VP3qNU";
                    }
                    //connectionString = "Data Source=MSDSQL13; Initial Catalog=WrenTrack; Persist Security Info=True; User ID=OTUser; Password=aD@U723C";
                    //connectionString = "Data Source=MSDSQL14-LS; Initial Catalog=WrenTrack; Persist Security Info=True; User ID=OTWebApi;Password=|M9^[?VP3qNU";

                    FilePath = OTInvoices.OTInvoices.Create_OTInvoices(customerId, invoiceno, connectionString);
                }

                var baseUrl = Request.RequestUri.GetLeftPart(UriPartial.Authority);

                if (!string.IsNullOrWhiteSpace(FilePath))
                {
                    return Ok(baseUrl + "/" + FilePath);
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web.Http;
using System.Web;
using System.Collections.Specialized;
using MSD.Models;

namespace msd_webapi.Controllers
{
    public class OrderLinesController : ApiController
    {
        public IHttpActionResult Get()
        {
            try
            {
                NameValueCollection nvc = HttpUtility.ParseQueryString(Request.RequestUri.Query);

                var order_no = nvc["OrderId"];
                if (order_no == null) order_no = "";

                string sSqlSelectCmd = string.Empty;
                string sFields = string.Empty;

                string strOffSetRows = string.Empty;

                string strWhere = "where delete_flag='N' and cancel_flag='N'";

                if (order_no != "")
                {
                    strWhere += " and [order_no] = " + order_no;
                }

                strOffSetRows = "";

                sFields = "[order_no],[line_no],[extended_desc],left([unit_of_measure],2) as unit_of_measure,[unit_size],[unit_price],[unit_quantity],[qty_ordered],[extended_price],[ship_loc_id],[supplier_id],[expedite_date],[inv_mast_uid]";


                sSqlSelectCmd = "SELECT " + sFields + " FROM [medcc_live].[dbo].[oe_line] " +
                                strWhere + " ORDER BY [order_no],[line_no] " + strOffSetRows;

                //string sSqlActiveItemsCmd = "[dbo].wsp_webapi_activeitems";

                string connectionString = string.Empty;
                try
                {
                    connectionString = ConfigurationManager.ConnectionStrings["webapiConnectionString"].ConnectionString;
                }
                catch
                {
                    connectionString = "Data Source=10.1.1.20; Initial Catalog=medcc_custom; User ID=MsdWebApi; Password=pwd4MsdWebApi";
                }

                // connectionString = "Data Source=MSDSQL03.msd.local; Initial Catalog=medcc_custom; User ID=MsdWebApi; Password=pwd4MsdWebApi";

                var OrderLines = new List<OrderLine>();

                SqlDataReader oDR = default(SqlDataReader);

                using (SqlConnection _SQLConn = new SqlConnection(connectionString))
                {
                    _SQLConn.Open();

                    using (SqlCommand SQLCmd = new SqlCommand())
                    {
                        SQLCmd.Connection = _SQLConn;
                        SQLCmd.CommandType = CommandType.Text;

                        SQLCmd.CommandText = sSqlSelectCmd;

                        using (SqlDataReader SQLRdr = SQLCmd.ExecuteReader())
                        {
                            while (SQLRdr.Read())
                            {
                                var OrderLine = new OrderLine();

                                OrderLine.order_no = SQLRdr["order_no"].ToString();
                                OrderLine.line_no = SQLRdr["line_no"].ToString();
                                OrderLine.extended_desc = SQLRdr["extended_desc"].ToString();
                                OrderLine.unit_of_measure = SQLRdr["unit_of_measure"].ToString();
                                OrderLine.unit_size = SQLRdr["unit_size"].ToString();

                                OrderLine.unit_price = SQLRdr["unit_price"].ToString();
                                OrderLine.unit_quantity = SQLRdr["unit_quantity"].ToString();
                                OrderLine.qty_ordered = SQLRdr["qty_ordered"].ToString();
                                OrderLine.extended_price = SQLRdr["extended_price"].ToString();
                                OrderLine.ship_loc_id = SQLRdr["ship_loc_id"].ToString();
                                OrderLine.supplier_id = SQLRdr["supplier_id"].ToString();
                                OrderLine.expedite_date = SQLRdr["expedite_date"].ToString();

                                OrderLine.inv_mast_uid = SQLRdr["inv_mast_uid"].ToString();

                                OrderLines.Add(OrderLine);
                            }
                        }
                    }
                }


                if (OrderLines.Count > 0)
                {
                    return Ok(OrderLines);
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
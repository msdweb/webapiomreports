﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Http.Cors;
using System.Linq;
using System.Text;
using System.Web.Http;
using System.Web;

namespace MSD_WebApi_OMReports.Controllers
{
    public class OMPurchaseHistoryController : ApiController
    {
        //[EnableCors(origins: "*", headers: "*", methods: "*")]
        //public IHttpActionResult Get(string id = "", string startdate = "", string enddate = "")
        //{
        //    try
        //    {
        //        DateTime dtStartDate = Convert.ToDateTime(startdate);
        //        DateTime dtEndDate = Convert.ToDateTime(enddate);

        //        string connectionString;

        //        try
        //        {
        //            connectionString = ConfigurationManager.ConnectionStrings["OMReportsConnectionString"].ConnectionString;
        //        }
        //        catch
        //        {
        //            connectionString = "Data Source=10.1.1.113; Initial Catalog=medcc_custom; User ID=MsdWebApi; Password=pwd4MsdWebApi";
        //        }

        //        connectionString = "Data Source=devsql03.msddev.local; Initial Catalog=medcc_custom; User ID=MsdWebApi; Password=pwd4MsdWebApi";

        //        string FilePath = string.Empty;

        //        var baseUrl = Request.RequestUri.GetLeftPart(UriPartial.Authority);

        //        FilePath = OMReports.OMPurchaseHistory.Create_OMPurchaseHistory(id, dtStartDate, dtEndDate, connectionString);

        //        if (!string.IsNullOrWhiteSpace(FilePath))
        //        {
        //            return Ok(baseUrl + "/" + FilePath);
        //        }
        //        else
        //        {
        //            return NotFound();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return InternalServerError(ex);
        //    }
        //}

        [HttpPost]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/customers/{customerId}/purchase-history/{startDate}/{endDate}")]
        public IHttpActionResult PurchaseHistory(string customerId, string startDate, string endDate)
        {
            try
            {
                string FilePath = string.Empty;

                DateTime dtStartDate = Convert.ToDateTime(startDate);
                DateTime dtEndDate = Convert.ToDateTime(endDate);

                string connectionString;

                try
                {
                    connectionString = ConfigurationManager.ConnectionStrings["OMReportsConnectionString"].ConnectionString;
                }
                catch
                {
                    connectionString = "Data Source=10.1.1.113; Initial Catalog=medcc_custom; User ID=MsdWebApi; Password=pwd4MsdWebApi";
                }

                //connectionString = "Data Source=devsql03.msddev.local; Initial Catalog=medcc_custom; User ID=MsdWebApi; Password=pwd4MsdWebApi";

                FilePath = OMReports.OMPurchaseHistory.Create_OMPurchaseHistory(customerId, dtStartDate, dtEndDate, connectionString);

                var baseUrl = Request.RequestUri.GetLeftPart(UriPartial.Authority);

                if (!string.IsNullOrWhiteSpace(FilePath))
                {
                    return Ok(baseUrl + "/" + FilePath);
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}

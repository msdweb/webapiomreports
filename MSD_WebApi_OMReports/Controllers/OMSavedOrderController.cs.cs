﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Http.Cors;
using System.Linq;
using System.Text;
using System.Web.Http;
using System.Web;

namespace MSD_WebApi_OMReports.Controllers
{
    public class OMSavedOrderController : ApiController
    {
        [HttpPost]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/customers/{customerId}/saved-order")]
        public IHttpActionResult SavedOrder(string customerId, [FromBody]string jsonorder)
        {
            try
            {
                string FilePath = string.Empty;

                FilePath = OMReports.OMSavedOrder.Create_OMSavedOrder(jsonorder);

                var baseUrl = Request.RequestUri.GetLeftPart(UriPartial.Authority);

                if (!string.IsNullOrWhiteSpace(FilePath))
                {
                    return Ok(baseUrl + "/" + FilePath);
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
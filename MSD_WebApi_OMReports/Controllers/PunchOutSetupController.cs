﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Security.Cryptography;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Xml;
using System.Xml.Serialization;
using Newtonsoft.Json;
using MSD_WebApi_OMReports.Models;
using MSD_WebApi_OMReports.Filters;
using System.Threading.Tasks;
//using Flurl;
//using Flurl.Http;

namespace MSD_WebApi_OMReports.Controllers
{
    public class PunchOutSetupController : ApiController
    {
        //[HttpStringDecodeFilter]
        [HttpPost]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/PunchOutSetup")]
        public HttpResponseMessage response([FromBody] punchoutRequest punchoutRequest)
        {

            //-----------------------------------------------------------------------------------------------------
            //-----------------------------------------------------------------------------------------------------
            //-----------------------------------------------------------------------------------------------------

            string connectionString = ConfigurationManager.ConnectionStrings["PunchOutWebsite"].ConnectionString;
            System.Data.SqlClient.SqlConnectionStringBuilder connBuilder = new System.Data.SqlClient.SqlConnectionStringBuilder(connectionString);
            var server = connBuilder.DataSource;

            string website;

            try
            {
                switch (server)
                {
                    case "websiteprod":
                        website = System.Configuration.ConfigurationManager.AppSettings["websiteprod"];
                        break;
                    case "websiteuat":
                        website = System.Configuration.ConfigurationManager.AppSettings["websiteuat"];
                        break;
                    default:
                        website = System.Configuration.ConfigurationManager.AppSettings["websiteprod"];
                        break;
                }

            }
            catch
            {
                website = "https://msdonline.com/";
            }

            //website = "http://localhost:80/";
            //website = "http://qa.msdonline.com/";
            //website = "https://uat.msdonline.com/";
            //website = "https://msdonline.com/";

            //-----------------------------------------------------------------------------------------------------

            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add("", "");

            //-----------------------------------------------------------------------------------------------------

            punchoutRequest objpunchoutRequest = new punchoutRequest();
            objpunchoutRequest = punchoutRequest;

            //var serializer = new XmlSerializer(objpunchoutRequest.GetType());

            //using (TextReader reader = new StringReader((string)punchoutRequest))
            //{
            //    objpunchoutRequest = (punchoutRequest)serializer.Deserialize(reader);
            //}

            //
            //-----------------------------------------------------------------------------------------------------
            //

            string filenameRequest = "C:\\temp\\MSD-PunchOut\\PunchOutSetupRequest-";
            string extentionxml = ".xml";

            string strNow = Convert.ToString(DateTime.Now);
            strNow = strNow.Replace(":", "-");
            strNow = strNow.Replace("/", "-");

            string pathfilename = (filenameRequest + strNow + extentionxml).Replace(" ", "-");

            FileStream file = File.Create(pathfilename);
            using (XmlWriter writer = XmlWriter.Create(file))
            {
                writer.WriteDocType("cXML", null, "cXML.dtd", null);
                var ser = new XmlSerializer(objpunchoutRequest.GetType());
                ser.Serialize(writer, objpunchoutRequest, ns);
            }
            file.Close();

            //-----------------------------------------------------------------------------------------------------

            //string extentiontxt = ".txt";

            //string pathfilenametxt = (filenameRequest + strNow + extentiontxt).Replace(" ", "-");

            //FileStream filereqtxt = File.Create(pathfilenametxt);
            //using (XmlWriter writer = XmlWriter.Create(filereqtxt))
            //{
            //    writer.WriteDocType("cXML", null, "cXML.dtd", null);
            //    var ser = new XmlSerializer(objpunchoutRequest.GetType());
            //    ser.Serialize(writer, objpunchoutRequest, ns);
            //}
            //filereqtxt.Close();

            //
            //-----------------------------------------------------------------------------------------------------
            //

            var FromDomain = string.Empty;
            var FromIdentity = string.Empty;

            foreach (punchoutCredential oFromCredential in (List<punchoutCredential>)objpunchoutRequest.Header.From.Credentials)
            {
                FromDomain = oFromCredential.domain.ToString();
                FromIdentity = oFromCredential.Identity.ToString();
                //if (oFromCredential.domain == "NetworkID")
                //{
                //    FromDomain = oFromCredential.domain.ToString();
                //    FromIdentity = oFromCredential.Identity.ToString();
                //}
            }

            var ToDomain = string.Empty;
            var ToIdentity = string.Empty;

            foreach (punchoutCredential oToCredential in (List<punchoutCredential>)objpunchoutRequest.Header.To.Credentials)
            {
                ToDomain = oToCredential.domain.ToString();
                ToIdentity = oToCredential.Identity.ToString();
                //if (oToCredential.domain == "NetworkID")
                //{
                //    ToDomain = oToCredential.domain.ToString();
                //    ToIdentity = oToCredential.Identity.ToString();
                //}
            }

            var SenderDomain = string.Empty;
            var SenderIdentity = string.Empty;
            var SenderSharedSecret = string.Empty;

            foreach (punchoutSenderCredential oSenderCredential in (List<punchoutSenderCredential>)objpunchoutRequest.Header.Sender.Credentials)
            {
                SenderDomain = oSenderCredential.domain.ToString();
                SenderIdentity = oSenderCredential.Identity.ToString();
                SenderSharedSecret = oSenderCredential.SharedSecret.ToString();
                //if (oSenderCredential.domain == "AribaNetworkUserId")
                //{
                //    SenderDomain = oSenderCredential.domain.ToString();
                //    SenderIdentity = oSenderCredential.Identity.ToString();
                //    SenderSharedSecret = oSenderCredential.SharedSecret.ToString();
                //}
            }

            var UserEmail = string.Empty;
            var UniqueName = string.Empty;
            foreach (punchoutExtrinsic oExtrinsic in (List<punchoutExtrinsic>)objpunchoutRequest.Request.PunchOutSetupRequest.Extrinsics)
            {
                if (oExtrinsic.name == "UserEmail")
                {
                    UserEmail = oExtrinsic.Value.ToString();
                }

                if (oExtrinsic.name == "UniqueName")
                {
                    UniqueName = oExtrinsic.Value.ToString();
                }
            }

            var AddressID = objpunchoutRequest.Request.PunchOutSetupRequest.ShipTo.Address.addressID;

            //
            //-----------------------------------------------------------------------------------------------------
            //

            punchoutRespond objpunchoutRespond = new punchoutRespond();

            var BrowserFormPost = objpunchoutRequest.Request.PunchOutSetupRequest.BrowserFormPost.URL.ToString();
            var BuyerCookie = objpunchoutRequest.Request.PunchOutSetupRequest.BuyerCookie.ToString();
            var Lang = objpunchoutRequest.lang.ToString();

            var SSharedSecret = SenderSharedSecret;

            //--------------------------------------------------------------------------------------------

            string timestamp = DateTimeOffset.Now.ToString("o");

            string strDateTime = DateTime.Now.ToString("yyyyMMddHHMMss");
            Random rand = new Random(DateTime.Now.Millisecond);
            string strRandom = rand.Next(1, 9999).ToString();
            string strHostId = "01054477490@msdistributors.com";

            string payloadID = strDateTime + "." + strRandom + "." + strHostId;

            objpunchoutRespond.timestamp = timestamp;
            objpunchoutRespond.payloadID = payloadID;
            objpunchoutRespond.lang = objpunchoutRequest.lang;

            var ShipToID = string.Empty;
            var Result = new List<string>();
            var UserName = string.Empty;
            var CustomerNumber = string.Empty;

            try
            {
                ShipToID = GetShipToID(objpunchoutRequest.Request.PunchOutSetupRequest.ShipTo.Address.addressID);
                Result = Get_PunchOutValidation(ShipToID, UserEmail);

                UserName = Result[0];
                CustomerNumber = Result[1];

                //
                //-----------------------------------------------------------------------------------------------------
                //

                if ((!string.IsNullOrEmpty(ShipToID)) && (!string.IsNullOrEmpty(UserName)))
                {
                    objpunchoutRespond.Response.Status.text = "success";
                    objpunchoutRespond.Response.Status.code = "200";


                    var SD = ToDomain;
                    var SI = ToIdentity;
                    var SS = SSharedSecret;
                    var BD = FromDomain; //SenderDomain;
                    var BI = FromIdentity; //SenderIdentity;
                    var BS = SenderSharedSecret;
                    var UE = UserName;
                    var UN = UniqueName;
                    var ST = ShipToID;
                    var FP = BrowserFormPost;
                    var BC = BuyerCookie;
                    var PL = Lang;
                    var TS = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");

                    object[] parameters = { SD, SI, SS, BD, BI, BS, UE, UN, ST, FP, BC, PL, TS };

                    string adata = JsonConvert.SerializeObject(parameters);

                    var etype = "1";
                    if (etype == "1")
                    {
                        var sKey = "6DD7B55A2FA9C319D0254164";
                        var sIV = "6DD7B55A";
                        adata = EncryptString(adata, sKey, sIV);
                    }

                    adata = HttpUtility.UrlEncode(adata);
                    objpunchoutRespond.Response.SetupResponse.StartPage.URL = website + "Account/SignInPOSR?posrdata=" + adata;

                    //--------------------------------------------------------------------------------------------
                    //adata = "LiSUWBnoZL%2bBHhvyZgNwgUo%2fo9TOHZoV%2fwaY%2bw1VBE%2fF1uV1%2fBvu6imgG08OTF4Vz7BNlse8LzRFdz2VuqlMlUUAjDzKkMLSeME61I0yPHmy8H0RPXqlUwj8SePu1Ah%2bXAf9nIJriWV12dUozvyRzTv6fV1qlCgyApJpsY4PK%2fp6DWhfX5dkNn5dU%2fQ8RECLsPn9rFWmMqjGbD3mtptt%2fZ22lzztiXD9uAyShoZyggP6JzBTrZk9I8no0fDywoO6Pb5HJTYjn6hY7fTCXNHonBYFcDxE%2fYTrZv4du6wcRxLJWwH65tqA4sN%2bAQUUYZYXpIF6X6fz100lKE2Yd%2bySR%2fHVwIsLOfJg3t%2fxtGuQhiVhYz2xqI9nYo4t%2bHgomYcjnR7khuj2FtZa8b4K%2bb%2bQilfLXCjZq2Yv7Y2TAyiflVm2bTSsnZCvpQ%3d%3d";
                    //var btdata2 = HttpUtility.UrlDecode(adata);
                    //var sKey1 = "6DD7B55A2FA9C319D0254164";
                    //var sIV1 = "6DD7B55A";
                    //var btdata3 = DecryptString(btdata2, sKey1, sIV1);
                    //--------------------------------------------------------------------------------------------
                    //--------------------------------------------------------------------------------------------
                    //var stringwriter = new StringWriter();
                    //using (XmlWriter writer = XmlWriter.Create(stringwriter))
                    //{
                    //    writer.WriteDocType("cXML", null, "http://xml.cxml.org/schemas/cXML/1.2.014/cXML.dtd", null);
                    //    var ser = new XmlSerializer(objpunchoutRespond.GetType());
                    //    ser.Serialize(writer, objpunchoutRespond, ns);
                    //}
                    //string strpunchoutRespond = stringwriter.ToString();
                    //punchoutRespond objpunchoutRespond2 = new punchoutRespond();
                    //objpunchoutRespond2.timestamp = "2018-02-15T08:46:00-07:00";
                    //objpunchoutRespond2.payloadID = "933694607739";
                    //objpunchoutRespond2.lang = "en-US";
                    //objpunchoutRespond2.Response.Status.text = "success";
                    //objpunchoutRespond2.Response.Status.code = "200";
                    //objpunchoutRespond2.Response.SetupResponse.StartPage.URL = "http://xml.workchairs.com/retrieve?reqUrl=20626;Initial=TRUE";
                    //--------------------------------------------------------------------------------------------
                }
                else
                {
                    objpunchoutRespond.Response.Status.text = "data not found";
                    objpunchoutRespond.Response.Status.code = "500";

                    pathfilename = ("C:\\temp\\MSD-PunchOut\\PunchOutSetupRespondError-" + strNow + ".txt").Replace(" ", "-");
                    System.IO.StreamWriter filetxt=new System.IO.StreamWriter(pathfilename);
                    string lines = "";
                    lines = "ShipToID " + ShipToID + " \r\n" + Result[1].ToString() + " \r\n";
                    filetxt.WriteLine(lines);
                    filetxt.Close();
                }
            }
            catch(Exception e)
            {
                objpunchoutRespond.Response.Status.text = "data not found";
                objpunchoutRespond.Response.Status.code = "500"; //HttpStatusCode.InternalServerError.ToString();

                pathfilename = ("C:\\temp\\MSD-PunchOut\\PunchOutSetupRespondError-" + strNow + ".txt").Replace(" ", "-");
                System.IO.StreamWriter filetxt = new System.IO.StreamWriter(pathfilename);
                string lines = "";
                lines = "Error " + e.Message + " \r\n";
                filetxt.WriteLine(lines);
                filetxt.Close();
            }

            //
            //-----------------------------------------------------------------------------------------------------
            //

            var stringwriter1 = new Utf8StringWriter();
            using (XmlWriter writer = XmlWriter.Create(stringwriter1))
            {
                writer.WriteDocType("cXML", null, "http://xml.cxml.org/schemas/cXML/1.2.014/cXML.dtd", null);
                var ser = new XmlSerializer(objpunchoutRespond.GetType());
                ser.Serialize(writer, objpunchoutRespond, ns);
            }
            string strpunchoutRespond = stringwriter1.ToString();

            //
            //-----------------------------------------------------------------------------------------------------
            //

            string filenameRespond = "C:\\temp\\MSD-PunchOut\\PunchOutSetupRespond-";

            pathfilename = (filenameRespond + strNow + extentionxml).Replace(" ", "-");

            FileStream fileresxml = File.Create(pathfilename);
            using (XmlWriter writer = XmlWriter.Create(fileresxml))
            {
                writer.WriteDocType("cXML", null, "http://xml.cxml.org/schemas/cXML/1.2.014/cXML.dtd", null);
                var ser = new XmlSerializer(objpunchoutRespond.GetType());
                ser.Serialize(writer, objpunchoutRespond, ns);
            }
            fileresxml.Close();

            //
            //-----------------------------------------------------------------------------------------------------
            //

            //pathfilenametxt = (filenameRespond + strNow + extentiontxt).Replace(" ", "-");

            //FileStream filerestxt = File.Create(pathfilenametxt);
            //using (XmlWriter writer = XmlWriter.Create(filerestxt))
            //{
            //    writer.WriteDocType("cXML", null, "http://xml.cxml.org/schemas/cXML/1.2.014/cXML.dtd", null);
            //    var ser = new XmlSerializer(objpunchoutRespond.GetType());
            //    ser.Serialize(writer, objpunchoutRespond, ns);
            //}
            //filerestxt.Close();

            //
            //-----------------------------------------------------------------------------------------------------
            //

            var Content = new StringContent(strpunchoutRespond, Encoding.UTF8, "text/xml");
            HttpResponseMessage httpResponse = this.Request.CreateResponse(HttpStatusCode.OK); //new HttpResponseMessage();
            httpResponse.Content = Content;
            httpResponse.Headers.ConnectionClose = true;
            httpResponse.Headers.CacheControl = new CacheControlHeaderValue();
            httpResponse.Headers.CacheControl.Public = true;
            return httpResponse;
        }
        protected string EncryptString(string plainText, string sKey, string sIV)
        {
            byte[] Key = ASCIIEncoding.ASCII.GetBytes(sKey);
            byte[] IV = ASCIIEncoding.ASCII.GetBytes(sIV);

            // Instantiate a new MemoryStream object to contain the encrypted bytes
            MemoryStream memoryStream = new MemoryStream();

            // Create a new TripleDES object.
            TripleDES tripleDESalg = TripleDES.Create();

            // Instantiate a new CryptoStream object to process the data and write it to the 
            // memory stream
            CryptoStream cryptoStream = new CryptoStream(memoryStream, tripleDESalg.CreateEncryptor(Key, IV), CryptoStreamMode.Write);

            // Convert the plainText string into a byte array
            byte[] plainBytes = Encoding.ASCII.GetBytes(plainText);

            // Encrypt the input plaintext string
            cryptoStream.Write(plainBytes, 0, plainBytes.Length);

            // Complete the encryption process
            cryptoStream.FlushFinalBlock();

            // Convert the encrypted data from a MemoryStream to a byte array
            byte[] cipherBytes = memoryStream.ToArray();

            // Close both the MemoryStream and the CryptoStream
            memoryStream.Close();
            cryptoStream.Close();

            // Convert the encrypted byte array to a base64 encoded string
            string cipherText = Convert.ToBase64String(cipherBytes, 0, cipherBytes.Length);

            // Return the encrypted data as a string
            return cipherText;
        }

        protected string DecryptString(string cipherText, string sKey, string sIV)
        {
            byte[] Key = ASCIIEncoding.ASCII.GetBytes(sKey);
            byte[] IV = ASCIIEncoding.ASCII.GetBytes(sIV);

            // Instantiate a new MemoryStream object to contain the encrypted bytes
            MemoryStream memoryStream = new MemoryStream();

            // Create a new TripleDES object.
            TripleDES tripleDESalg = TripleDES.Create();

            // Instantiate a new CryptoStream object to process the data and write it to the 
            // memory stream
            CryptoStream cryptoStream = new CryptoStream(memoryStream, tripleDESalg.CreateDecryptor(Key, IV), CryptoStreamMode.Write);

            // Will contain decrypted plaintext
            string plainText = String.Empty;

            try
            {
                // Convert the ciphertext string into a byte array
                byte[] cipherBytes = Convert.FromBase64String(cipherText);

                // Decrypt the input ciphertext string
                cryptoStream.Write(cipherBytes, 0, cipherBytes.Length);

                // Complete the decryption process
                cryptoStream.FlushFinalBlock();

                // Convert the decrypted data from a MemoryStream to a byte array
                byte[] plainBytes = memoryStream.ToArray();

                // Convert the encrypted byte array to a base64 encoded string
                plainText = Encoding.ASCII.GetString(plainBytes, 0, plainBytes.Length);
            }
            finally
            {
                // Close both the MemoryStream and the CryptoStream
                memoryStream.Close();
                cryptoStream.Close();
            }

            // Return the encrypted data as a string
            return plainText;
        }

        protected string GetShipToID(string id)
        {
            var code = string.Empty;

            var ds = new DataSet();

            string connectionString;

            try
            {
                connectionString = ConfigurationManager.ConnectionStrings["PunchOutERPConnectionString"].ConnectionString;
            }
            catch
            {
                connectionString = "Data Source=10.1.1.113; Initial Catalog=medcc_custom; User ID=MsdWebApi; Password=pwd4MsdWebApi";
            }

            //connectionString = "Data Source=10.1.1.113; Initial Catalog=medcc_custom; User ID=MsdWebApi; Password=pwd4MsdWebApi";
            //connectionString = "Data Source=devsql03.msddev.local; Initial Catalog=medcc_custom; User ID=MsdWebApi; Password=pwd4MsdWebApi";

            using (var connection = new SqlConnection(connectionString))
            {
                var cmd = new SqlCommand("[wsp_webapi_AribaGetShipToID]", connection);
                cmd.Parameters.AddWithValue("@parAribaShipToID", id);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = int.MaxValue;

                using (var adapter = new SqlDataAdapter(cmd))
                {
                    adapter.Fill(ds);
                }

                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    code = string.Format("{0}", ds.Tables[0].Rows[0]["ShipToID"]);
                }
            }

            return code;
        }

        protected List<string> Get_PunchOutValidation(string ShipToID, string UserEmail)
        {
            var Result = new List<string>();

            var ds = new DataSet();

            string connectionString;

            try
            {
                connectionString = ConfigurationManager.ConnectionStrings["PunchOutMSDONConnectionString"].ConnectionString;
            }
            catch
            {
                connectionString = "Data Source=10.1.1.50; Initial Catalog=Insite.MSD;User Id=Insite;Password=NorthPoint2015!";
            }

            //connectionString = "Data Source=10.1.1.50; Initial Catalog=Insite.MSD;User Id=Insite;Password=NorthPoint2015!"; // PROD
            //connectionString = "Data Source=10.1.1.77; Initial Catalog=Insite.MSD;User Id=Insite;Password=NorthPoint2015!"; // UAT
            //connectionString = "Data Source=10.1.1.75; Initial Catalog=Insite.MSD;User Id=Insite;Password=NorthPoint2015!"; // QA

            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    var cmd = new SqlCommand("[msd].[sp_PunchOut_Validation]", connection);
                    cmd.Parameters.AddWithValue("@parShipToID", ShipToID);
                    cmd.Parameters.AddWithValue("@parUserEmail", UserEmail);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = int.MaxValue;

                    using (var adapter = new SqlDataAdapter(cmd))
                    {
                        adapter.Fill(ds);
                    }

                    if (null != ds && null != ds.Tables && ds.Tables.Count > 0)
                    {
                        Result.Add(string.Format("{0}", ds.Tables[0].Rows[0]["UserName"]));
                        Result.Add(string.Format("{0}", ds.Tables[0].Rows[0]["CustomerNumber"]));
                    }
                }
            }
            catch (Exception e)
            {
                Result.Add(string.Format("{0}", ""));
                Result.Add(string.Format("{0}", "CustomerNumberError: " + e.Message));
            }

            return Result;
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Http.Cors;
using System.Linq;
using System.Text;
using System.Web.Http;
using System.Web;

namespace MSD_WebApi_OMReports.Controllers
{
    public class OMOrderConfirmationController : ApiController
    {
        [HttpPost]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/customers/{customerId}/order-confirmation")]
        public IHttpActionResult OrderConfirmation(string customerId, [FromBody]string jsonorder)
        {
            try
            {
                string FilePath = string.Empty;

                FilePath = OMReports.OMOrderConfirmation.Create_OMOrderConfirmation(jsonorder);

                var baseUrl = Request.RequestUri.GetLeftPart(UriPartial.Authority);

                if (!string.IsNullOrWhiteSpace(FilePath))
                {
                    return Ok(baseUrl + "/" + FilePath);
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Http.Cors;
using System.Linq;
using System.Text;
using System.Web.Http;
using System.Web;

namespace MSD_WebApi_OMReports.Controllers
{
    public class OMInvoiceListController : ApiController
    {
        [HttpPost]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/customers/{customerId}/om-invoice-list")]
        public IHttpActionResult OMInvoiceList(string customerId, [FromBody]object[] parameters)
        {
            try
            {
                string FilePath = string.Empty;

                string connectionString;

                try
                {
                    connectionString = ConfigurationManager.ConnectionStrings["OMInvoiceConnectionString"].ConnectionString;
                }
                catch
                {
                    connectionString = "Data Source=10.1.1.113; Initial Catalog=medcc_custom; User ID=MsdWebApi; Password=pwd4MsdWebApi";
                }

                //connectionString = "Data Source=devsql03.msddev.local; Initial Catalog=medcc_custom; User ID=MsdWebApi; Password=pwd4MsdWebApi";

                FilePath = OMReports.OMInvoiceList.Create_OMInvoiceList(customerId, parameters, connectionString);

                var baseUrl = Request.RequestUri.GetLeftPart(UriPartial.Authority);

                if (!string.IsNullOrWhiteSpace(FilePath))
                {
                    return Ok(baseUrl + "/" + FilePath);
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
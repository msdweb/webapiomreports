﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace OMOrders
{
    public partial class frmOMOrder : Form
    {

        private Hashtable colPickTicketTracking = new Hashtable();
        private Hashtable colPickTicketDetail = new Hashtable();
        private Hashtable colPickTicketDetailLot = new Hashtable();

        public frmOMOrder()
        {
            InitializeComponent();
        }

        private void frmOMOrder_Load(object sender, EventArgs e)
        {
            PaintGrid();
        }

        //
        //-------------------------------------------------------------
        //

        private void btnDisplay_Click(object sender, EventArgs e)
        {
            frmOrderPrint frmPCSReport = new frmOrderPrint();
            frmPCSReport.PCS_Preview();
            return;
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            frmOrderPrint frmPCSReport = new frmOrderPrint();
            frmPCSReport.PCS_Print();
        }

        private void btnSalvarRelatorio_Click(object sender, EventArgs e)
        {
            frmOrderPrint frmPCSReport = new frmOrderPrint();
            frmPCSReport.PCS_Salva();
        }

        //
        //-------------------------------------------------------------
        //


        //
        //-------------------------------------------------------------
        //

        private void PaintGrid()
        {
           // dataGridView1.DataSource = GLB.OMInvoices;
            dataGridView1.DataSource = GetTableOrderHistoryDetail(GLB.orderHistoryDetail);
            dataGridView7.DataSource = GetTableOrderHistoryAll(GLB.orderHistoryDetail.OrderHistoryAll);
            dataGridView2.DataSource = GetTableOrderHistoryOpen(GLB.orderHistoryDetail.OrderHistoryOpen);
            dataGridView3.DataSource = GetTableOrderHistoryPickTicket(GLB.orderHistoryDetail.OrderHistoryPickTicket);
        }


        //
        //-----------------------------------------------------------------        
        //

        private DataTable GetTableOrderHistoryDetail(OrderHistoryDetail OrderHistoryDetail)
        {
            DataTable table = new DataTable();
            table.Columns.Add("OrderNumber", typeof(string));
            table.Columns.Add("OrderDate", typeof(string));
            table.Columns.Add("DueDate", typeof(string));
            table.Columns.Add("CustomerNumber", typeof(string));
            table.Columns.Add("CustomerPurchaseOrder", typeof(string));
            table.Columns.Add("PlacedBy", typeof(string));

            table.Columns.Add("BillingCompanyName", typeof(string));
            table.Columns.Add("BillingAddress1", typeof(string));
            table.Columns.Add("BillingAddress2", typeof(string));
            table.Columns.Add("BillingCity", typeof(string));
            table.Columns.Add("BillingState", typeof(string));
            table.Columns.Add("BillingPostalCode", typeof(string));
            table.Columns.Add("BillingCountry", typeof(string));

            table.Columns.Add("ShippingCompanyName", typeof(string));
            table.Columns.Add("ShippingAddress1", typeof(string));
            table.Columns.Add("ShippingAddress2", typeof(string));
            table.Columns.Add("ShippingCity", typeof(string));
            table.Columns.Add("ShippingState", typeof(string));
            table.Columns.Add("ShippingPostalCode", typeof(string));
            table.Columns.Add("ShippingCountry", typeof(string));

            table.Columns.Add("DeliveryInstructions", typeof(string));

            table.Columns.Add("Terms", typeof(string));

            table.Columns.Add("SourceLocationId", typeof(string));
            table.Columns.Add("SourceLocationName", typeof(string));

            table.Columns.Add("CustomerType", typeof(string));

            table.Columns.Add("FreightInfoOption", typeof(string));
            table.Columns.Add("OrderStatus", typeof(string));

            table.Columns.Add("AcknowledgedDate", typeof(string));
            table.Columns.Add("PickedDate", typeof(string));
            table.Columns.Add("ShippedDate", typeof(string));
            table.Columns.Add("DeliveredDate", typeof(string));

            table.Rows.Add(
                            OrderHistoryDetail.OrderNumber,
                            OrderHistoryDetail.OrderDate,
                            OrderHistoryDetail.DueDate,
                            OrderHistoryDetail.CustomerNumber,
                            OrderHistoryDetail.CustomerPurchaseOrder,
                            OrderHistoryDetail.PlacedBy,

                            OrderHistoryDetail.BillingCompanyName,
                            OrderHistoryDetail.BillingAddress1,
                            OrderHistoryDetail.BillingAddress2,
                            OrderHistoryDetail.BillingCity,
                            OrderHistoryDetail.BillingState,
                            OrderHistoryDetail.BillingPostalCode,
                            OrderHistoryDetail.BillingCountry,

                            OrderHistoryDetail.ShippingCompanyName,
                            OrderHistoryDetail.ShippingAddress1,
                            OrderHistoryDetail.ShippingAddress2,
                            OrderHistoryDetail.ShippingCity,
                            OrderHistoryDetail.ShippingState,
                            OrderHistoryDetail.ShippingPostalCode,
                            OrderHistoryDetail.ShippingCountry,

                            OrderHistoryDetail.DeliveryInstructions,

                            OrderHistoryDetail.Terms,
                            OrderHistoryDetail.SourceLocationId,
                            OrderHistoryDetail.SourceLocationName,
                            OrderHistoryDetail.CustomerType,

                            OrderHistoryDetail.FreightInfoOption,
                            OrderHistoryDetail.OrderStatus,

                            OrderHistoryDetail.AcknowledgedDate,
                            OrderHistoryDetail.PickedDate,
                            OrderHistoryDetail.ShippedDate,
                            OrderHistoryDetail.DeliveredDate

           );

            return table;
        }

        private DataTable GetTableOrderHistoryAll(List<OrderHistoryAll> ListOrderHistoryAll)
        {
            DataTable table = new DataTable();

            table.Columns.Add("unit_quantity", typeof(string));
            table.Columns.Add("unit_of_measure", typeof(string));
            table.Columns.Add("item_id", typeof(string));
            table.Columns.Add("item_desc", typeof(string));
            table.Columns.Add("qty_backorder", typeof(string));
            table.Columns.Add("unit_price", typeof(string));
            table.Columns.Add("extended_price", typeof(string));
            table.Columns.Add("total_units", typeof(string));

            foreach (OrderHistoryAll OrderHistoryAll in ListOrderHistoryAll)
            {
                table.Rows.Add(
                        OrderHistoryAll.unit_quantity,
                        OrderHistoryAll.unit_of_measure,
                        OrderHistoryAll.item_id,
                        OrderHistoryAll.item_desc,
                        OrderHistoryAll.qty_backorder,
                        OrderHistoryAll.unit_price,
                        OrderHistoryAll.extended_price,
                        OrderHistoryAll.total_units
                );
            }

            return table;
        }

        private DataTable GetTableOrderHistoryOpen(List<OrderHistoryOpen> ListOrderHistoryOpen)
        {
            DataTable table = new DataTable();

            table.Columns.Add("item_id", typeof(string));
            table.Columns.Add("item_desc", typeof(string));
            table.Columns.Add("unit_of_measure", typeof(string));
            table.Columns.Add("qty_ordered", typeof(string));
            table.Columns.Add("qty_allocated", typeof(string));
            table.Columns.Add("qty_on_pick_tickets", typeof(string));
            table.Columns.Add("qty_invoiced", typeof(string));
            table.Columns.Add("qty_open", typeof(string));
            table.Columns.Add("qty_backorder", typeof(string));

            foreach (OrderHistoryOpen OrderHistoryOpen in ListOrderHistoryOpen)
            {
                table.Rows.Add(
                        OrderHistoryOpen.item_id,
                        OrderHistoryOpen.item_desc,
                        OrderHistoryOpen.unit_of_measure,
                        OrderHistoryOpen.qty_ordered,
                        OrderHistoryOpen.qty_allocated,
                        OrderHistoryOpen.qty_on_pick_tickets,
                        OrderHistoryOpen.qty_invoiced,
                        OrderHistoryOpen.qty_open,
                        OrderHistoryOpen.qty_backorder
                );
            }

            return table;
        }

        private void dataGridView3_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            string str;

            if (e.RowIndex >= 0)
            {
                str = dataGridView3.Rows[e.RowIndex].Cells[0].Value.ToString();

                List<OrderHistoryPickTicketTracking> ListOrderHistoryPickTicketTracking = (List<OrderHistoryPickTicketTracking>)colPickTicketTracking[str];
                List<OrderHistoryPickTicketDetail> ListOrderHistoryPickTicketDetail = (List<OrderHistoryPickTicketDetail>)colPickTicketDetail[str];

                dataGridView4.DataSource = GetTableOrderHistoryPickTicketTracking(ListOrderHistoryPickTicketTracking);
                dataGridView5.DataSource = GetTableOrderHistoryPickTicketDetail(ListOrderHistoryPickTicketDetail);
            }
        }

        private void dataGridView5_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            string str;

            if (e.RowIndex >= 0)
            {
                str = dataGridView5.Rows[e.RowIndex].Cells[0].Value.ToString();

                List<OrderHistoryPickTicketDetailLot> ListOrderHistoryPickTicketDetailLot = (List<OrderHistoryPickTicketDetailLot>)colPickTicketDetailLot[str];

                dataGridView6.DataSource = GetTableOrderHistoryPickTicketDetailLot(ListOrderHistoryPickTicketDetailLot);
            }

        }

        private DataTable GetTableOrderHistoryPickTicket(List<OrderHistoryPickTicket> ListOrderHistoryPickTicket)
        {

            colPickTicketTracking.Clear();
            colPickTicketDetail.Clear();

            dataGridView3.DataSource = null;
            dataGridView4.DataSource = null;
            dataGridView5.DataSource = null;
            dataGridView6.DataSource = null;

            DataTable table = new DataTable();

            table.Columns.Add("pick_ticket_no", typeof(string));
            table.Columns.Add("invoice_no", typeof(string));
            table.Columns.Add("location_id", typeof(string));
            table.Columns.Add("carrier_name", typeof(string));

            foreach (OrderHistoryPickTicket OrderHistoryPickTicket in ListOrderHistoryPickTicket)
            {
                table.Rows.Add(
                        OrderHistoryPickTicket.pick_ticket_no,
                        OrderHistoryPickTicket.invoice_no,
                        OrderHistoryPickTicket.location_id,
                        OrderHistoryPickTicket.carrier_name
                );

                colPickTicketTracking[OrderHistoryPickTicket.pick_ticket_no.ToString()] = OrderHistoryPickTicket.OrderHistoryPickTicketTracking;
                colPickTicketDetail[OrderHistoryPickTicket.pick_ticket_no.ToString()] = OrderHistoryPickTicket.OrderHistoryPickTicketDetail;
            }

            return table;
        }

        private DataTable GetTableOrderHistoryPickTicketTracking(List<OrderHistoryPickTicketTracking> ListOrderHistoryPickTicketTracking)
        {

            DataTable table = new DataTable();

            table.Columns.Add("tracking_no", typeof(string));
            table.Columns.Add("shipped_date", typeof(string));
            table.Columns.Add("carrrierURL", typeof(string));
            table.Columns.Add("poddelivered_date", typeof(string));
            table.Columns.Add("poddelivered_time", typeof(string));
            table.Columns.Add("poddelivered_notes", typeof(string));
            table.Columns.Add("package_weight", typeof(string));

            foreach (OrderHistoryPickTicketTracking OrderHistoryPickTicketTracking in ListOrderHistoryPickTicketTracking)
            {
                table.Rows.Add(
                        OrderHistoryPickTicketTracking.tracking_no,
                        OrderHistoryPickTicketTracking.shipped_date,
                        OrderHistoryPickTicketTracking.carrrierURL,
                        OrderHistoryPickTicketTracking.poddelivered_date,
                        OrderHistoryPickTicketTracking.poddelivered_time,
                        OrderHistoryPickTicketTracking.poddelivered_notes,
                        OrderHistoryPickTicketTracking.package_weight
                );
            }

            return table;
        }

        private DataTable GetTableOrderHistoryPickTicketDetail(List<OrderHistoryPickTicketDetail> ListOrderHistoryPickTicketDetail)
        {

            colPickTicketDetailLot.Clear();

            DataTable table = new DataTable();

            table.Columns.Add("item_id", typeof(string));
            table.Columns.Add("item_desc", typeof(string));
            table.Columns.Add("unit_of_measure", typeof(string));
            table.Columns.Add("unit_quantity", typeof(string));

            foreach (OrderHistoryPickTicketDetail OrderHistoryPickTicketDetail in ListOrderHistoryPickTicketDetail)
            {
                table.Rows.Add(
                        OrderHistoryPickTicketDetail.item_id,
                        OrderHistoryPickTicketDetail.item_desc,
                        OrderHistoryPickTicketDetail.unit_of_measure,
                        OrderHistoryPickTicketDetail.unit_quantity
                );

                colPickTicketDetailLot[OrderHistoryPickTicketDetail.item_id.ToString()] = OrderHistoryPickTicketDetail.OrderHistoryPickTicketDetailLot;
            }

            return table;
        }

        private DataTable GetTableOrderHistoryPickTicketDetailLot(List<OrderHistoryPickTicketDetailLot> ListOrderHistoryPickTicketDetailLot)
        {

            DataTable table = new DataTable();

            table.Columns.Add("lot", typeof(string));
            table.Columns.Add("expiration_date", typeof(string));
            table.Columns.Add("unit_of_measure", typeof(string));
            table.Columns.Add("unit_quantity", typeof(string));

            foreach (OrderHistoryPickTicketDetailLot OrderHistoryPickTicketDetailLot in ListOrderHistoryPickTicketDetailLot)
            {
                table.Rows.Add(
                        OrderHistoryPickTicketDetailLot.lot,
                        OrderHistoryPickTicketDetailLot.expiration_date,
                        OrderHistoryPickTicketDetailLot.unit_of_measure,
                        OrderHistoryPickTicketDetailLot.unit_quantity
                );
            }

            return table;

        }


     }
}

﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
//using System.Web.Http;
using System.Web;
using System.Collections.Specialized;

namespace OMOrders
{
    public class OrderHistoryDetails
    {
        public static OrderHistoryDetail Get_OrderHistoryDetail(string Customer_no, string Order_no, string sConn)
        {

            var orderHistoryDetail = new OrderHistoryDetail();

            using (SqlConnection _SQLConn = new SqlConnection(sConn))
            {
                const string sSqlOrderHistoryCmd = "[dbo].[wsp_webapi_OrderHistoryDetail]";
                _SQLConn.Open();

                using (var sqlCmd = new SqlCommand())
                {
                    sqlCmd.Connection = _SQLConn;
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.CommandText = sSqlOrderHistoryCmd;
                    sqlCmd.Parameters.AddWithValue("@orderNumber", Order_no);
                    sqlCmd.CommandTimeout = 120;

                    using (SqlDataReader sqlRdr = sqlCmd.ExecuteReader())
                    {
                        while (sqlRdr.Read())
                        {
                            if (sqlRdr["CustomerNumber"].ToString() != Customer_no) { return orderHistoryDetail; }

                            if (sqlRdr["OrderNumber"] != null) orderHistoryDetail.OrderNumber = sqlRdr["OrderNumber"].ToString();
                            if (sqlRdr["OrderDate"] != null) orderHistoryDetail.OrderDate = Convert.ToDateTime(sqlRdr["OrderDate"]);
                            if (sqlRdr["DueDate"] != null) orderHistoryDetail.DueDate = Convert.ToDateTime(sqlRdr["DueDate"]);
                            if (sqlRdr["CustomerNumber"] != null) orderHistoryDetail.CustomerNumber = sqlRdr["CustomerNumber"].ToString();
                            if (sqlRdr["CustomerPO"] != null) orderHistoryDetail.CustomerPurchaseOrder = sqlRdr["CustomerPO"].ToString();
                            if (sqlRdr["PlacedBy"] != null) orderHistoryDetail.PlacedBy = sqlRdr["PlacedBy"].ToString();
                            
                            if (sqlRdr["BillingCompanyName"] != null) orderHistoryDetail.BillingCompanyName = sqlRdr["BillingCompanyName"].ToString();
                            if (sqlRdr["BillingAddress1"] != null) orderHistoryDetail.BillingAddress1 = sqlRdr["BillingAddress1"].ToString();
                            if (sqlRdr["BillingAddress2"] != null) orderHistoryDetail.BillingAddress2 = sqlRdr["BillingAddress2"].ToString();
                            if (sqlRdr["BillingCity"] != null) orderHistoryDetail.BillingCity = sqlRdr["BillingCity"].ToString();
                            if (sqlRdr["BillingState"] != null) orderHistoryDetail.BillingState = sqlRdr["BillingState"].ToString();
                            if (sqlRdr["BillingPostalCode"] != null) orderHistoryDetail.BillingPostalCode = sqlRdr["BillingPostalCode"].ToString();
                            if (sqlRdr["BillingCountry"] != null) orderHistoryDetail.BillingCountry = sqlRdr["BillingCountry"].ToString();
                            if (sqlRdr["ShippingCompanyName"] != null) orderHistoryDetail.ShippingCompanyName = sqlRdr["ShippingCompanyName"].ToString();
                            if (sqlRdr["ShippingAddress1"] != null) orderHistoryDetail.ShippingAddress1 = sqlRdr["ShippingAddress1"].ToString();
                            if (sqlRdr["ShippingAddress2"] != null) orderHistoryDetail.ShippingAddress2 = sqlRdr["ShippingAddress2"].ToString();
                            if (sqlRdr["ShippingCity"] != null) orderHistoryDetail.ShippingCity = sqlRdr["ShippingCity"].ToString();
                            if (sqlRdr["ShippingState"] != null) orderHistoryDetail.ShippingState = sqlRdr["ShippingState"].ToString();
                            if (sqlRdr["ShippingPostalCode"] != null) orderHistoryDetail.ShippingPostalCode = sqlRdr["ShippingPostalCode"].ToString();
                            if (sqlRdr["ShippingCountry"] != null) orderHistoryDetail.ShippingCountry = sqlRdr["ShippingCountry"].ToString();

                            if (sqlRdr["DeliveryInstructions"] != null) orderHistoryDetail.DeliveryInstructions = sqlRdr["DeliveryInstructions"].ToString();

                            if (sqlRdr["Terms"] != null) orderHistoryDetail.Terms = sqlRdr["Terms"].ToString();
                            if (sqlRdr["SourceLocationId"] != null) orderHistoryDetail.SourceLocationId = sqlRdr["SourceLocationId"].ToString();
                            if (sqlRdr["SourceLocationName"] != null) orderHistoryDetail.SourceLocationName = sqlRdr["SourceLocationName"].ToString();

                            if (sqlRdr["CustomerType"] != null) orderHistoryDetail.CustomerType = sqlRdr["CustomerType"].ToString();

                            if (sqlRdr["FreightInfoOption"] != null) orderHistoryDetail.FreightInfoOption = sqlRdr["FreightInfoOption"].ToString();
                            if (sqlRdr["OrderStatus"] != null) orderHistoryDetail.OrderStatus = sqlRdr["OrderStatus"].ToString();

                            if (sqlRdr["AcknowledgedDate"] != null) orderHistoryDetail.AcknowledgedDate = sqlRdr["AcknowledgedDate"].ToString();
                            if (sqlRdr["PickedDate"] != null) orderHistoryDetail.PickedDate = sqlRdr["PickedDate"].ToString();
                            if (sqlRdr["ShippedDate"] != null) orderHistoryDetail.ShippedDate = sqlRdr["ShippedDate"].ToString();
                            if (sqlRdr["DeliveredDate"] != null) orderHistoryDetail.DeliveredDate = sqlRdr["DeliveredDate"].ToString();
                        }
                        sqlRdr.NextResult();

                        while (sqlRdr.Read())
                        {
                            OrderHistoryAll OrderHistoryAll = new OrderHistoryAll();

                            OrderHistoryAll.unit_quantity = sqlRdr["unit_quantity"].ToString();
                            OrderHistoryAll.unit_of_measure = sqlRdr["unit_of_measure"].ToString();
                            OrderHistoryAll.item_id = sqlRdr["item_id"].ToString();
                            OrderHistoryAll.item_desc = sqlRdr["item_desc"].ToString();
                            OrderHistoryAll.qty_backorder = sqlRdr["qty_backorder"].ToString();
                            OrderHistoryAll.unit_price = sqlRdr["unit_price"].ToString();
                            OrderHistoryAll.extended_price = sqlRdr["extended_price"].ToString();
                            OrderHistoryAll.total_units = sqlRdr["total_units"].ToString();

                            orderHistoryDetail.OrderHistoryAll.Add(OrderHistoryAll);
                        }
                        sqlRdr.NextResult();

                        while (sqlRdr.Read())
                        {
                            OrderHistoryOpen OrderHistoryOpen = new OrderHistoryOpen();

                            OrderHistoryOpen.item_id = sqlRdr["item_id"].ToString();
                            OrderHistoryOpen.item_desc = sqlRdr["item_desc"].ToString();
                            OrderHistoryOpen.qty_ordered = sqlRdr["qty_ordered"].ToString();
                            OrderHistoryOpen.qty_allocated = sqlRdr["qty_allocated"].ToString();
                            OrderHistoryOpen.qty_on_pick_tickets = sqlRdr["qty_on_pick_tickets"].ToString();
                            OrderHistoryOpen.qty_invoiced = sqlRdr["qty_invoiced"].ToString();
                            OrderHistoryOpen.qty_open = sqlRdr["qty_open"].ToString();
                            OrderHistoryOpen.qty_backorder = sqlRdr["qty_backorder"].ToString();
                            OrderHistoryOpen.unit_of_measure = sqlRdr["unit_of_measure"].ToString();

                            orderHistoryDetail.OrderHistoryOpen.Add(OrderHistoryOpen);
                        }

                        sqlRdr.NextResult();
                        while (sqlRdr.Read())
                        {
                            OrderHistoryPickTicket OrderHistoryPickTicket = new OrderHistoryPickTicket();

                            OrderHistoryPickTicket.pick_ticket_no = sqlRdr["pick_ticket_no"].ToString();
                            OrderHistoryPickTicket.invoice_no = sqlRdr["invoice_no"].ToString();
                            OrderHistoryPickTicket.location_id = sqlRdr["location_id"].ToString();
                            OrderHistoryPickTicket.carrier_name = sqlRdr["carrier_name"].ToString();

                            string Trackings = sqlRdr["Trackings"].ToString();
                            if (!string.IsNullOrWhiteSpace(Trackings))
                            {
                                OrderHistoryPickTicket = Get_Trackings(Trackings, OrderHistoryPickTicket);
                            }
                            string Details = sqlRdr["Details"].ToString();
                            if (!string.IsNullOrWhiteSpace(Details))
                            {
                                OrderHistoryPickTicket = Get_Details(Details, OrderHistoryPickTicket);
                            }
                            orderHistoryDetail.OrderHistoryPickTicket.Add(OrderHistoryPickTicket);
                        }
                    }
                }
            }
            return orderHistoryDetail;
        }

        private static OrderHistoryPickTicket Get_Trackings(string Trackings, OrderHistoryPickTicket OrderHistoryPickTicket)
        {
            OrderHistoryPickTicketTracking OrderHistoryPickTicketTracking;
            string[] splitTrackings;
            splitTrackings = Trackings.Split('^');
            foreach (string Tracking in splitTrackings)
            {
                if (!string.IsNullOrWhiteSpace(Tracking))
                {
                    string[] splitTracking;
                    splitTracking = Tracking.Split(',');

                    OrderHistoryPickTicketTracking = new OrderHistoryPickTicketTracking();
                    OrderHistoryPickTicketTracking.tracking_no = splitTracking[0].ToString();
                    OrderHistoryPickTicketTracking.shipped_date = splitTracking[1].ToString();
                    OrderHistoryPickTicketTracking.carrrierURL = splitTracking[2].ToString();
                    OrderHistoryPickTicketTracking.poddelivered_date = splitTracking[3].ToString();
                    OrderHistoryPickTicketTracking.poddelivered_time = splitTracking[4].ToString();
                    OrderHistoryPickTicketTracking.poddelivered_notes = splitTracking[5].ToString();
                    OrderHistoryPickTicketTracking.package_weight = splitTracking[6].ToString();

                    OrderHistoryPickTicket.OrderHistoryPickTicketTracking.Add(OrderHistoryPickTicketTracking);
                }
            }
            return OrderHistoryPickTicket;
        }

        private static OrderHistoryPickTicket Get_Details(string Details, OrderHistoryPickTicket OrderHistoryPickTicket)
        {
            OrderHistoryPickTicketDetail OrderHistoryPickTicketDetail;
            string[] splitDetails;
            splitDetails = Details.Split(new string[] { "[d0]" }, StringSplitOptions.None);
            foreach (string Detail in splitDetails)
            {
                if (!string.IsNullOrWhiteSpace(Detail))
                {
                    string[] splitDetail;
                    splitDetail = Detail.Split(new string[] { "[d1]" }, StringSplitOptions.None);

                    OrderHistoryPickTicketDetail = new OrderHistoryPickTicketDetail();
                    OrderHistoryPickTicketDetail.item_id = splitDetail[0].ToString();
                    OrderHistoryPickTicketDetail.item_desc = splitDetail[1].ToString();
                    OrderHistoryPickTicketDetail.unit_of_measure = splitDetail[2];
                    OrderHistoryPickTicketDetail.unit_quantity = splitDetail[3].ToString();

                    string Lots = splitDetail[4].ToString();
                    if (!string.IsNullOrWhiteSpace(Lots))
                    {
                        OrderHistoryPickTicketDetail = Get_Lots(Lots, OrderHistoryPickTicketDetail);
                    }

                    OrderHistoryPickTicket.OrderHistoryPickTicketDetail.Add(OrderHistoryPickTicketDetail);
                }
            }
            return OrderHistoryPickTicket;
        }

        private static OrderHistoryPickTicketDetail Get_Lots(string Lots, OrderHistoryPickTicketDetail OrderHistoryPickTicketDetail)
        {
            OrderHistoryPickTicketDetailLot OrderHistoryPickTicketDetailLot;
            string[] splitLots;
            splitLots = Lots.Split(new string[] { "[d2]" }, StringSplitOptions.None);


            foreach (string Lot in splitLots)
            {
                if (!string.IsNullOrWhiteSpace(Lot))
                {
                    string[] splitLot;
                    splitLot = Lot.Split(new string[] { "[d3]" }, StringSplitOptions.None);

                    OrderHistoryPickTicketDetailLot = new OrderHistoryPickTicketDetailLot();
                    OrderHistoryPickTicketDetailLot.lot = splitLot[0].ToString();
                    OrderHistoryPickTicketDetailLot.expiration_date = splitLot[1].ToString();
                    OrderHistoryPickTicketDetailLot.unit_of_measure = splitLot[2];
                    OrderHistoryPickTicketDetailLot.unit_quantity = splitLot[3].ToString();

                    OrderHistoryPickTicketDetail.OrderHistoryPickTicketDetailLot.Add(OrderHistoryPickTicketDetailLot);
                }
            }
            return OrderHistoryPickTicketDetail;
        }
    }
}

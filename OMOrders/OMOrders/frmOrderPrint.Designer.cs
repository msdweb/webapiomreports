﻿namespace OMOrders
{
    partial class frmOrderPrint
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle40 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle41 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle42 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle43 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle45 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle46 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle44 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle47 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle48 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle49 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle50 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle51 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle52 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle55 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle56 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle53 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle54 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle57 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle58 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle61 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle62 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle59 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle60 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle63 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle64 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle65 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle66 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle67 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle68 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle76 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle77 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle69 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle70 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle71 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle72 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle73 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle74 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle75 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle78 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle79 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle88 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle89 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle80 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle81 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle82 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle83 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle84 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle85 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle86 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle87 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle90 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle91 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle98 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle99 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle92 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle93 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle94 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle95 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle96 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle97 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle100 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle101 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle106 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle107 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle102 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle103 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle104 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle105 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle108 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle109 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle116 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle117 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle110 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle111 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle112 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle113 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle114 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle115 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabHeader = new System.Windows.Forms.TabPage();
            this.dgvPageHeader = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvGroup1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn31 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvEndOTInvoices = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvEndGroupMsg = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblFooter = new System.Windows.Forms.Label();
            this.dgvEmptyMessage = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn32 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabHeaderSections = new System.Windows.Forms.TabPage();
            this.dgvSectionHeader2 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvSectionHeaderPickTickets = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvSectionHeaderOpenItems = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvSectionHeaderYourOrder = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvSectionHeaderA0 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvSectionHeaderA1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvSectionLine6 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn42 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvSectionHeader22 = new System.Windows.Forms.DataGridView();
            this.BillToAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ShipTo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ShipToAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabSections = new System.Windows.Forms.TabPage();
            this.dgvSectionYourOrderPHD = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn33 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn34 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn35 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn36 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvSectionPickTicketsLines = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvSectionPickTicketsHeader = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DeliveryInfo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvSectionOpenItems = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvSectionYourOrder = new System.Windows.Forms.DataGridView();
            this.Field1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Field2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Field3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Field4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Field5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Field6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvSectionHeaderA2 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn38 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabControl1.SuspendLayout();
            this.tabHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPageHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEndOTInvoices)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEndGroupMsg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmptyMessage)).BeginInit();
            this.tabHeaderSections.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeader2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeaderPickTickets)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeaderOpenItems)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeaderYourOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeaderA0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeaderA1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionLine6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeader22)).BeginInit();
            this.tabSections.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionYourOrderPHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionPickTicketsLines)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionPickTicketsHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionOpenItems)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionYourOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeaderA2)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabHeader);
            this.tabControl1.Controls.Add(this.tabHeaderSections);
            this.tabControl1.Controls.Add(this.tabSections);
            this.tabControl1.Location = new System.Drawing.Point(12, 5);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(832, 569);
            this.tabControl1.TabIndex = 18;
            // 
            // tabHeader
            // 
            this.tabHeader.Controls.Add(this.dgvPageHeader);
            this.tabHeader.Controls.Add(this.dgvGroup1);
            this.tabHeader.Controls.Add(this.dgvEndOTInvoices);
            this.tabHeader.Controls.Add(this.dgvEndGroupMsg);
            this.tabHeader.Controls.Add(this.lblFooter);
            this.tabHeader.Controls.Add(this.dgvEmptyMessage);
            this.tabHeader.Location = new System.Drawing.Point(4, 22);
            this.tabHeader.Name = "tabHeader";
            this.tabHeader.Padding = new System.Windows.Forms.Padding(3);
            this.tabHeader.Size = new System.Drawing.Size(824, 543);
            this.tabHeader.TabIndex = 1;
            this.tabHeader.Text = "Header";
            this.tabHeader.UseVisualStyleBackColor = true;
            // 
            // dgvPageHeader
            // 
            this.dgvPageHeader.AllowUserToAddRows = false;
            this.dgvPageHeader.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvPageHeader.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvPageHeader.BackgroundColor = System.Drawing.Color.White;
            this.dgvPageHeader.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvPageHeader.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvPageHeader.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvPageHeader.ColumnHeadersHeight = 50;
            this.dgvPageHeader.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Arial", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvPageHeader.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvPageHeader.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvPageHeader.GridColor = System.Drawing.Color.Black;
            this.dgvPageHeader.Location = new System.Drawing.Point(580, 17);
            this.dgvPageHeader.MultiSelect = false;
            this.dgvPageHeader.Name = "dgvPageHeader";
            this.dgvPageHeader.RowHeadersVisible = false;
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvPageHeader.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvPageHeader.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvPageHeader.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPageHeader.Size = new System.Drawing.Size(200, 30);
            this.dgvPageHeader.TabIndex = 40;
            // 
            // dataGridViewTextBoxColumn2
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewTextBoxColumn2.HeaderText = "Order Details";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 200;
            // 
            // dgvGroup1
            // 
            this.dgvGroup1.AllowUserToAddRows = false;
            this.dgvGroup1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvGroup1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvGroup1.BackgroundColor = System.Drawing.Color.Gray;
            this.dgvGroup1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvGroup1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dgvGroup1.ColumnHeadersHeight = 30;
            this.dgvGroup1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn31});
            this.dgvGroup1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvGroup1.GridColor = System.Drawing.Color.Black;
            this.dgvGroup1.Location = new System.Drawing.Point(35, 126);
            this.dgvGroup1.MultiSelect = false;
            this.dgvGroup1.Name = "dgvGroup1";
            this.dgvGroup1.RowHeadersVisible = false;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvGroup1.RowsDefaultCellStyle = dataGridViewCellStyle8;
            this.dgvGroup1.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvGroup1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvGroup1.Size = new System.Drawing.Size(745, 28);
            this.dgvGroup1.TabIndex = 39;
            // 
            // dataGridViewTextBoxColumn31
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn31.DefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridViewTextBoxColumn31.HeaderText = "Group Header";
            this.dataGridViewTextBoxColumn31.Name = "dataGridViewTextBoxColumn31";
            this.dataGridViewTextBoxColumn31.Width = 745;
            // 
            // dgvEndOTInvoices
            // 
            this.dgvEndOTInvoices.AllowUserToAddRows = false;
            this.dgvEndOTInvoices.AllowUserToDeleteRows = false;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvEndOTInvoices.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle9;
            this.dgvEndOTInvoices.BackgroundColor = System.Drawing.Color.Gray;
            this.dgvEndOTInvoices.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvEndOTInvoices.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dgvEndOTInvoices.ColumnHeadersHeight = 30;
            this.dgvEndOTInvoices.ColumnHeadersVisible = false;
            this.dgvEndOTInvoices.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1});
            this.dgvEndOTInvoices.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvEndOTInvoices.GridColor = System.Drawing.Color.Black;
            this.dgvEndOTInvoices.Location = new System.Drawing.Point(35, 340);
            this.dgvEndOTInvoices.MultiSelect = false;
            this.dgvEndOTInvoices.Name = "dgvEndOTInvoices";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEndOTInvoices.RowHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dgvEndOTInvoices.RowHeadersVisible = false;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle11.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvEndOTInvoices.RowsDefaultCellStyle = dataGridViewCellStyle11;
            this.dgvEndOTInvoices.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvEndOTInvoices.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvEndOTInvoices.Size = new System.Drawing.Size(745, 25);
            this.dgvEndOTInvoices.TabIndex = 38;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "";
            this.Column1.Name = "Column1";
            this.Column1.Width = 745;
            // 
            // dgvEndGroupMsg
            // 
            this.dgvEndGroupMsg.AllowUserToAddRows = false;
            this.dgvEndGroupMsg.AllowUserToDeleteRows = false;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle12.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvEndGroupMsg.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle12;
            this.dgvEndGroupMsg.BackgroundColor = System.Drawing.Color.White;
            this.dgvEndGroupMsg.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvEndGroupMsg.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvEndGroupMsg.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEndGroupMsg.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.dgvEndGroupMsg.ColumnHeadersHeight = 30;
            this.dgvEndGroupMsg.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn3});
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvEndGroupMsg.DefaultCellStyle = dataGridViewCellStyle15;
            this.dgvEndGroupMsg.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvEndGroupMsg.GridColor = System.Drawing.Color.Black;
            this.dgvEndGroupMsg.Location = new System.Drawing.Point(35, 283);
            this.dgvEndGroupMsg.MultiSelect = false;
            this.dgvEndGroupMsg.Name = "dgvEndGroupMsg";
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEndGroupMsg.RowHeadersDefaultCellStyle = dataGridViewCellStyle16;
            this.dgvEndGroupMsg.RowHeadersVisible = false;
            dataGridViewCellStyle17.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle17.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvEndGroupMsg.RowsDefaultCellStyle = dataGridViewCellStyle17;
            this.dgvEndGroupMsg.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvEndGroupMsg.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvEndGroupMsg.Size = new System.Drawing.Size(745, 31);
            this.dgvEndGroupMsg.TabIndex = 37;
            // 
            // dataGridViewTextBoxColumn3
            // 
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle14;
            this.dataGridViewTextBoxColumn3.HeaderText = "End Group Header";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 745;
            // 
            // lblFooter
            // 
            this.lblFooter.BackColor = System.Drawing.Color.White;
            this.lblFooter.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFooter.ForeColor = System.Drawing.Color.Black;
            this.lblFooter.Location = new System.Drawing.Point(35, 382);
            this.lblFooter.Name = "lblFooter";
            this.lblFooter.Size = new System.Drawing.Size(745, 15);
            this.lblFooter.TabIndex = 27;
            this.lblFooter.Text = "MSD";
            // 
            // dgvEmptyMessage
            // 
            this.dgvEmptyMessage.AllowUserToAddRows = false;
            this.dgvEmptyMessage.AllowUserToDeleteRows = false;
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle18.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle18.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvEmptyMessage.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle18;
            this.dgvEmptyMessage.BackgroundColor = System.Drawing.Color.Gray;
            this.dgvEmptyMessage.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvEmptyMessage.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle19.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle19.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEmptyMessage.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle19;
            this.dgvEmptyMessage.ColumnHeadersHeight = 20;
            this.dgvEmptyMessage.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn32});
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle21.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle21.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle21.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle21.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle21.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvEmptyMessage.DefaultCellStyle = dataGridViewCellStyle21;
            this.dgvEmptyMessage.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvEmptyMessage.GridColor = System.Drawing.Color.Black;
            this.dgvEmptyMessage.Location = new System.Drawing.Point(35, 412);
            this.dgvEmptyMessage.MultiSelect = false;
            this.dgvEmptyMessage.Name = "dgvEmptyMessage";
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle22.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle22.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle22.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle22.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle22.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEmptyMessage.RowHeadersDefaultCellStyle = dataGridViewCellStyle22;
            this.dgvEmptyMessage.RowHeadersVisible = false;
            dataGridViewCellStyle23.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle23.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle23.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle23.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvEmptyMessage.RowsDefaultCellStyle = dataGridViewCellStyle23;
            this.dgvEmptyMessage.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvEmptyMessage.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvEmptyMessage.Size = new System.Drawing.Size(745, 19);
            this.dgvEmptyMessage.TabIndex = 26;
            // 
            // dataGridViewTextBoxColumn32
            // 
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn32.DefaultCellStyle = dataGridViewCellStyle20;
            this.dataGridViewTextBoxColumn32.HeaderText = "No items in this section";
            this.dataGridViewTextBoxColumn32.Name = "dataGridViewTextBoxColumn32";
            this.dataGridViewTextBoxColumn32.Width = 745;
            // 
            // tabHeaderSections
            // 
            this.tabHeaderSections.Controls.Add(this.dgvSectionHeaderA2);
            this.tabHeaderSections.Controls.Add(this.dgvSectionHeader2);
            this.tabHeaderSections.Controls.Add(this.dgvSectionHeaderPickTickets);
            this.tabHeaderSections.Controls.Add(this.dgvSectionHeaderOpenItems);
            this.tabHeaderSections.Controls.Add(this.dgvSectionHeaderYourOrder);
            this.tabHeaderSections.Controls.Add(this.dgvSectionHeaderA0);
            this.tabHeaderSections.Controls.Add(this.dgvSectionHeaderA1);
            this.tabHeaderSections.Controls.Add(this.dgvSectionLine6);
            this.tabHeaderSections.Controls.Add(this.dgvSectionHeader22);
            this.tabHeaderSections.Location = new System.Drawing.Point(4, 22);
            this.tabHeaderSections.Name = "tabHeaderSections";
            this.tabHeaderSections.Size = new System.Drawing.Size(824, 543);
            this.tabHeaderSections.TabIndex = 2;
            this.tabHeaderSections.Text = "HeaderSection";
            this.tabHeaderSections.UseVisualStyleBackColor = true;
            // 
            // dgvSectionHeader2
            // 
            this.dgvSectionHeader2.AllowUserToAddRows = false;
            this.dgvSectionHeader2.AllowUserToDeleteRows = false;
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle28.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle28.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle28.SelectionBackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle28.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeader2.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle28;
            this.dgvSectionHeader2.BackgroundColor = System.Drawing.Color.White;
            this.dgvSectionHeader2.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvSectionHeader2.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle29.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle29.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle29.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle29.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle29.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle29.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSectionHeader2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle29;
            this.dgvSectionHeader2.ColumnHeadersHeight = 22;
            this.dgvSectionHeader2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn15,
            this.dataGridViewTextBoxColumn24,
            this.dataGridViewTextBoxColumn25,
            this.dataGridViewTextBoxColumn27,
            this.dataGridViewTextBoxColumn28});
            dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle30.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle30.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle30.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle30.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle30.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle30.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSectionHeader2.DefaultCellStyle = dataGridViewCellStyle30;
            this.dgvSectionHeader2.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSectionHeader2.GridColor = System.Drawing.Color.Black;
            this.dgvSectionHeader2.Location = new System.Drawing.Point(35, 92);
            this.dgvSectionHeader2.MultiSelect = false;
            this.dgvSectionHeader2.Name = "dgvSectionHeader2";
            this.dgvSectionHeader2.RowHeadersVisible = false;
            dataGridViewCellStyle31.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle31.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeader2.RowsDefaultCellStyle = dataGridViewCellStyle31;
            this.dgvSectionHeader2.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvSectionHeader2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSectionHeader2.Size = new System.Drawing.Size(745, 22);
            this.dgvSectionHeader2.TabIndex = 71;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.HeaderText = "Bill To:";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.Width = 240;
            // 
            // dataGridViewTextBoxColumn24
            // 
            this.dataGridViewTextBoxColumn24.HeaderText = "";
            this.dataGridViewTextBoxColumn24.Name = "dataGridViewTextBoxColumn24";
            this.dataGridViewTextBoxColumn24.Width = 15;
            // 
            // dataGridViewTextBoxColumn25
            // 
            this.dataGridViewTextBoxColumn25.HeaderText = "Ship To:";
            this.dataGridViewTextBoxColumn25.Name = "dataGridViewTextBoxColumn25";
            this.dataGridViewTextBoxColumn25.Width = 240;
            // 
            // dataGridViewTextBoxColumn27
            // 
            this.dataGridViewTextBoxColumn27.HeaderText = "";
            this.dataGridViewTextBoxColumn27.Name = "dataGridViewTextBoxColumn27";
            this.dataGridViewTextBoxColumn27.Width = 15;
            // 
            // dataGridViewTextBoxColumn28
            // 
            this.dataGridViewTextBoxColumn28.HeaderText = "Status History";
            this.dataGridViewTextBoxColumn28.Name = "dataGridViewTextBoxColumn28";
            this.dataGridViewTextBoxColumn28.Width = 235;
            // 
            // dgvSectionHeaderPickTickets
            // 
            this.dgvSectionHeaderPickTickets.AllowUserToAddRows = false;
            this.dgvSectionHeaderPickTickets.AllowUserToDeleteRows = false;
            dataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle32.BackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle32.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle32.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle32.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeaderPickTickets.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle32;
            this.dgvSectionHeaderPickTickets.BackgroundColor = System.Drawing.Color.Gray;
            this.dgvSectionHeaderPickTickets.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvSectionHeaderPickTickets.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvSectionHeaderPickTickets.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle33.BackColor = System.Drawing.SystemColors.AppWorkspace;
            dataGridViewCellStyle33.Font = new System.Drawing.Font("Times New Roman", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle33.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle33.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle33.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle33.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSectionHeaderPickTickets.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle33;
            this.dgvSectionHeaderPickTickets.ColumnHeadersHeight = 33;
            this.dgvSectionHeaderPickTickets.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn16});
            dataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle35.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle35.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle35.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle35.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle35.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle35.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSectionHeaderPickTickets.DefaultCellStyle = dataGridViewCellStyle35;
            this.dgvSectionHeaderPickTickets.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSectionHeaderPickTickets.GridColor = System.Drawing.Color.Black;
            this.dgvSectionHeaderPickTickets.Location = new System.Drawing.Point(35, 243);
            this.dgvSectionHeaderPickTickets.MultiSelect = false;
            this.dgvSectionHeaderPickTickets.Name = "dgvSectionHeaderPickTickets";
            this.dgvSectionHeaderPickTickets.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvSectionHeaderPickTickets.RowHeadersVisible = false;
            dataGridViewCellStyle36.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle36.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle36.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle36.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeaderPickTickets.RowsDefaultCellStyle = dataGridViewCellStyle36;
            this.dgvSectionHeaderPickTickets.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvSectionHeaderPickTickets.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSectionHeaderPickTickets.Size = new System.Drawing.Size(745, 33);
            this.dgvSectionHeaderPickTickets.TabIndex = 70;
            // 
            // dataGridViewTextBoxColumn16
            // 
            dataGridViewCellStyle34.NullValue = null;
            this.dataGridViewTextBoxColumn16.DefaultCellStyle = dataGridViewCellStyle34;
            this.dataGridViewTextBoxColumn16.HeaderText = "Shipping Information";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.Width = 745;
            // 
            // dgvSectionHeaderOpenItems
            // 
            this.dgvSectionHeaderOpenItems.AllowUserToAddRows = false;
            this.dgvSectionHeaderOpenItems.AllowUserToDeleteRows = false;
            dataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle37.BackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle37.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle37.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle37.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeaderOpenItems.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle37;
            this.dgvSectionHeaderOpenItems.BackgroundColor = System.Drawing.Color.Gray;
            this.dgvSectionHeaderOpenItems.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvSectionHeaderOpenItems.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvSectionHeaderOpenItems.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle38.BackColor = System.Drawing.SystemColors.AppWorkspace;
            dataGridViewCellStyle38.Font = new System.Drawing.Font("Times New Roman", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle38.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle38.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle38.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle38.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSectionHeaderOpenItems.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle38;
            this.dgvSectionHeaderOpenItems.ColumnHeadersHeight = 33;
            this.dgvSectionHeaderOpenItems.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn5});
            dataGridViewCellStyle40.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle40.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle40.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle40.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle40.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle40.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle40.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSectionHeaderOpenItems.DefaultCellStyle = dataGridViewCellStyle40;
            this.dgvSectionHeaderOpenItems.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSectionHeaderOpenItems.GridColor = System.Drawing.Color.Black;
            this.dgvSectionHeaderOpenItems.Location = new System.Drawing.Point(35, 204);
            this.dgvSectionHeaderOpenItems.MultiSelect = false;
            this.dgvSectionHeaderOpenItems.Name = "dgvSectionHeaderOpenItems";
            this.dgvSectionHeaderOpenItems.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvSectionHeaderOpenItems.RowHeadersVisible = false;
            dataGridViewCellStyle41.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle41.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle41.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle41.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeaderOpenItems.RowsDefaultCellStyle = dataGridViewCellStyle41;
            this.dgvSectionHeaderOpenItems.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvSectionHeaderOpenItems.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSectionHeaderOpenItems.Size = new System.Drawing.Size(745, 33);
            this.dgvSectionHeaderOpenItems.TabIndex = 69;
            // 
            // dataGridViewTextBoxColumn5
            // 
            dataGridViewCellStyle39.NullValue = null;
            this.dataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle39;
            this.dataGridViewTextBoxColumn5.HeaderText = "Open Items";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Width = 745;
            // 
            // dgvSectionHeaderYourOrder
            // 
            this.dgvSectionHeaderYourOrder.AllowUserToAddRows = false;
            this.dgvSectionHeaderYourOrder.AllowUserToDeleteRows = false;
            dataGridViewCellStyle42.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle42.BackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle42.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle42.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle42.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeaderYourOrder.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle42;
            this.dgvSectionHeaderYourOrder.BackgroundColor = System.Drawing.Color.Gray;
            this.dgvSectionHeaderYourOrder.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvSectionHeaderYourOrder.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvSectionHeaderYourOrder.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle43.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle43.BackColor = System.Drawing.SystemColors.AppWorkspace;
            dataGridViewCellStyle43.Font = new System.Drawing.Font("Times New Roman", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle43.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle43.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle43.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle43.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSectionHeaderYourOrder.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle43;
            this.dgvSectionHeaderYourOrder.ColumnHeadersHeight = 33;
            this.dgvSectionHeaderYourOrder.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn4});
            dataGridViewCellStyle45.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle45.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle45.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle45.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle45.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle45.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle45.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSectionHeaderYourOrder.DefaultCellStyle = dataGridViewCellStyle45;
            this.dgvSectionHeaderYourOrder.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSectionHeaderYourOrder.GridColor = System.Drawing.Color.Black;
            this.dgvSectionHeaderYourOrder.Location = new System.Drawing.Point(35, 165);
            this.dgvSectionHeaderYourOrder.MultiSelect = false;
            this.dgvSectionHeaderYourOrder.Name = "dgvSectionHeaderYourOrder";
            this.dgvSectionHeaderYourOrder.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvSectionHeaderYourOrder.RowHeadersVisible = false;
            dataGridViewCellStyle46.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle46.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle46.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle46.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeaderYourOrder.RowsDefaultCellStyle = dataGridViewCellStyle46;
            this.dgvSectionHeaderYourOrder.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvSectionHeaderYourOrder.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSectionHeaderYourOrder.Size = new System.Drawing.Size(745, 33);
            this.dgvSectionHeaderYourOrder.TabIndex = 68;
            // 
            // dataGridViewTextBoxColumn4
            // 
            dataGridViewCellStyle44.NullValue = null;
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle44;
            this.dataGridViewTextBoxColumn4.HeaderText = "Your Order";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Width = 745;
            // 
            // dgvSectionHeaderA0
            // 
            this.dgvSectionHeaderA0.AllowUserToAddRows = false;
            this.dgvSectionHeaderA0.AllowUserToDeleteRows = false;
            dataGridViewCellStyle47.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle47.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle47.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle47.SelectionBackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle47.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeaderA0.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle47;
            this.dgvSectionHeaderA0.BackgroundColor = System.Drawing.Color.White;
            this.dgvSectionHeaderA0.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvSectionHeaderA0.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvSectionHeaderA0.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle48.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle48.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle48.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle48.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle48.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle48.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle48.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSectionHeaderA0.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle48;
            this.dgvSectionHeaderA0.ColumnHeadersHeight = 22;
            this.dgvSectionHeaderA0.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1});
            dataGridViewCellStyle49.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle49.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle49.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle49.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle49.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle49.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle49.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSectionHeaderA0.DefaultCellStyle = dataGridViewCellStyle49;
            this.dgvSectionHeaderA0.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSectionHeaderA0.GridColor = System.Drawing.Color.Black;
            this.dgvSectionHeaderA0.Location = new System.Drawing.Point(35, 22);
            this.dgvSectionHeaderA0.MultiSelect = false;
            this.dgvSectionHeaderA0.Name = "dgvSectionHeaderA0";
            this.dgvSectionHeaderA0.RowHeadersVisible = false;
            dataGridViewCellStyle50.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle50.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeaderA0.RowsDefaultCellStyle = dataGridViewCellStyle50;
            this.dgvSectionHeaderA0.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvSectionHeaderA0.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSectionHeaderA0.Size = new System.Drawing.Size(250, 22);
            this.dgvSectionHeaderA0.TabIndex = 67;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 250;
            // 
            // dgvSectionHeaderA1
            // 
            this.dgvSectionHeaderA1.AllowUserToAddRows = false;
            this.dgvSectionHeaderA1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle51.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle51.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle51.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle51.SelectionBackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle51.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeaderA1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle51;
            this.dgvSectionHeaderA1.BackgroundColor = System.Drawing.Color.White;
            this.dgvSectionHeaderA1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvSectionHeaderA1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle52.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle52.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle52.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle52.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle52.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle52.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle52.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSectionHeaderA1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle52;
            this.dgvSectionHeaderA1.ColumnHeadersHeight = 22;
            this.dgvSectionHeaderA1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn19,
            this.dataGridViewTextBoxColumn21});
            dataGridViewCellStyle55.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle55.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle55.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle55.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle55.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle55.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle55.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSectionHeaderA1.DefaultCellStyle = dataGridViewCellStyle55;
            this.dgvSectionHeaderA1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSectionHeaderA1.GridColor = System.Drawing.Color.Black;
            this.dgvSectionHeaderA1.Location = new System.Drawing.Point(380, 22);
            this.dgvSectionHeaderA1.MultiSelect = false;
            this.dgvSectionHeaderA1.Name = "dgvSectionHeaderA1";
            this.dgvSectionHeaderA1.RowHeadersVisible = false;
            dataGridViewCellStyle56.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle56.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle56.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle56.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeaderA1.RowsDefaultCellStyle = dataGridViewCellStyle56;
            this.dgvSectionHeaderA1.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvSectionHeaderA1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSectionHeaderA1.Size = new System.Drawing.Size(400, 22);
            this.dgvSectionHeaderA1.TabIndex = 66;
            // 
            // dataGridViewTextBoxColumn19
            // 
            this.dataGridViewTextBoxColumn19.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle53.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dataGridViewTextBoxColumn19.DefaultCellStyle = dataGridViewCellStyle53;
            this.dataGridViewTextBoxColumn19.HeaderText = "Order Header";
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.Width = 150;
            // 
            // dataGridViewTextBoxColumn21
            // 
            dataGridViewCellStyle54.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dataGridViewTextBoxColumn21.DefaultCellStyle = dataGridViewCellStyle54;
            this.dataGridViewTextBoxColumn21.HeaderText = "";
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            this.dataGridViewTextBoxColumn21.Width = 250;
            // 
            // dgvSectionLine6
            // 
            this.dgvSectionLine6.AllowUserToAddRows = false;
            this.dgvSectionLine6.AllowUserToDeleteRows = false;
            dataGridViewCellStyle57.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle57.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle57.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle57.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle57.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionLine6.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle57;
            this.dgvSectionLine6.BackgroundColor = System.Drawing.Color.Gray;
            this.dgvSectionLine6.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvSectionLine6.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvSectionLine6.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle58.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle58.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle58.Font = new System.Drawing.Font("Times New Roman", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle58.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle58.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle58.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle58.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSectionLine6.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle58;
            this.dgvSectionLine6.ColumnHeadersHeight = 22;
            this.dgvSectionLine6.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn26,
            this.dataGridViewTextBoxColumn42});
            dataGridViewCellStyle61.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle61.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle61.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle61.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle61.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle61.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle61.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSectionLine6.DefaultCellStyle = dataGridViewCellStyle61;
            this.dgvSectionLine6.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSectionLine6.GridColor = System.Drawing.Color.Black;
            this.dgvSectionLine6.Location = new System.Drawing.Point(98, 351);
            this.dgvSectionLine6.MultiSelect = false;
            this.dgvSectionLine6.Name = "dgvSectionLine6";
            this.dgvSectionLine6.RowHeadersVisible = false;
            dataGridViewCellStyle62.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle62.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle62.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle62.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionLine6.RowsDefaultCellStyle = dataGridViewCellStyle62;
            this.dgvSectionLine6.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvSectionLine6.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSectionLine6.Size = new System.Drawing.Size(600, 22);
            this.dgvSectionLine6.TabIndex = 65;
            // 
            // dataGridViewTextBoxColumn26
            // 
            dataGridViewCellStyle59.Alignment = System.Windows.Forms.DataGridViewContentAlignment.BottomCenter;
            dataGridViewCellStyle59.NullValue = null;
            this.dataGridViewTextBoxColumn26.DefaultCellStyle = dataGridViewCellStyle59;
            this.dataGridViewTextBoxColumn26.HeaderText = "Carrier";
            this.dataGridViewTextBoxColumn26.Name = "dataGridViewTextBoxColumn26";
            this.dataGridViewTextBoxColumn26.Width = 300;
            // 
            // dataGridViewTextBoxColumn42
            // 
            dataGridViewCellStyle60.Alignment = System.Windows.Forms.DataGridViewContentAlignment.BottomCenter;
            this.dataGridViewTextBoxColumn42.DefaultCellStyle = dataGridViewCellStyle60;
            this.dataGridViewTextBoxColumn42.HeaderText = "Tracking #";
            this.dataGridViewTextBoxColumn42.Name = "dataGridViewTextBoxColumn42";
            this.dataGridViewTextBoxColumn42.Width = 300;
            // 
            // dgvSectionHeader22
            // 
            this.dgvSectionHeader22.AllowUserToAddRows = false;
            this.dgvSectionHeader22.AllowUserToDeleteRows = false;
            dataGridViewCellStyle63.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle63.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle63.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle63.SelectionBackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle63.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeader22.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle63;
            this.dgvSectionHeader22.BackgroundColor = System.Drawing.Color.White;
            this.dgvSectionHeader22.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvSectionHeader22.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle64.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle64.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle64.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle64.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle64.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle64.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle64.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSectionHeader22.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle64;
            this.dgvSectionHeader22.ColumnHeadersHeight = 22;
            this.dgvSectionHeader22.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.BillToAddress,
            this.ShipTo,
            this.ShipToAddress,
            this.Column5});
            dataGridViewCellStyle65.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle65.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle65.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle65.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle65.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle65.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle65.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSectionHeader22.DefaultCellStyle = dataGridViewCellStyle65;
            this.dgvSectionHeader22.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSectionHeader22.GridColor = System.Drawing.Color.Black;
            this.dgvSectionHeader22.Location = new System.Drawing.Point(35, 64);
            this.dgvSectionHeader22.MultiSelect = false;
            this.dgvSectionHeader22.Name = "dgvSectionHeader22";
            this.dgvSectionHeader22.RowHeadersVisible = false;
            dataGridViewCellStyle66.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle66.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeader22.RowsDefaultCellStyle = dataGridViewCellStyle66;
            this.dgvSectionHeader22.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvSectionHeader22.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSectionHeader22.Size = new System.Drawing.Size(745, 22);
            this.dgvSectionHeader22.TabIndex = 46;
            // 
            // BillToAddress
            // 
            this.BillToAddress.HeaderText = "Bill To:";
            this.BillToAddress.Name = "BillToAddress";
            this.BillToAddress.Width = 300;
            // 
            // ShipTo
            // 
            this.ShipTo.HeaderText = "";
            this.ShipTo.Name = "ShipTo";
            this.ShipTo.Width = 80;
            // 
            // ShipToAddress
            // 
            this.ShipToAddress.HeaderText = "Ship To:";
            this.ShipToAddress.Name = "ShipToAddress";
            this.ShipToAddress.Width = 300;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "";
            this.Column5.Name = "Column5";
            this.Column5.Width = 65;
            // 
            // tabSections
            // 
            this.tabSections.Controls.Add(this.dgvSectionYourOrderPHD);
            this.tabSections.Controls.Add(this.dgvSectionPickTicketsLines);
            this.tabSections.Controls.Add(this.dgvSectionPickTicketsHeader);
            this.tabSections.Controls.Add(this.dgvSectionOpenItems);
            this.tabSections.Controls.Add(this.dgvSectionYourOrder);
            this.tabSections.Location = new System.Drawing.Point(4, 22);
            this.tabSections.Name = "tabSections";
            this.tabSections.Size = new System.Drawing.Size(824, 543);
            this.tabSections.TabIndex = 3;
            this.tabSections.Text = "Sections";
            this.tabSections.UseVisualStyleBackColor = true;
            // 
            // dgvSectionYourOrderPHD
            // 
            this.dgvSectionYourOrderPHD.AllowUserToAddRows = false;
            this.dgvSectionYourOrderPHD.AllowUserToDeleteRows = false;
            dataGridViewCellStyle67.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle67.BackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle67.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle67.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle67.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionYourOrderPHD.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle67;
            this.dgvSectionYourOrderPHD.BackgroundColor = System.Drawing.Color.Gray;
            this.dgvSectionYourOrderPHD.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvSectionYourOrderPHD.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvSectionYourOrderPHD.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle68.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            dataGridViewCellStyle68.BackColor = System.Drawing.SystemColors.AppWorkspace;
            dataGridViewCellStyle68.Font = new System.Drawing.Font("Times New Roman", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle68.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle68.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle68.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle68.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSectionYourOrderPHD.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle68;
            this.dgvSectionYourOrderPHD.ColumnHeadersHeight = 35;
            this.dgvSectionYourOrderPHD.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn29,
            this.Column7,
            this.dataGridViewTextBoxColumn30,
            this.dataGridViewTextBoxColumn33,
            this.dataGridViewTextBoxColumn34,
            this.dataGridViewTextBoxColumn35,
            this.dataGridViewTextBoxColumn36});
            dataGridViewCellStyle76.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle76.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle76.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle76.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle76.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle76.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle76.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSectionYourOrderPHD.DefaultCellStyle = dataGridViewCellStyle76;
            this.dgvSectionYourOrderPHD.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSectionYourOrderPHD.GridColor = System.Drawing.Color.Black;
            this.dgvSectionYourOrderPHD.Location = new System.Drawing.Point(35, 26);
            this.dgvSectionYourOrderPHD.MultiSelect = false;
            this.dgvSectionYourOrderPHD.Name = "dgvSectionYourOrderPHD";
            this.dgvSectionYourOrderPHD.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvSectionYourOrderPHD.RowHeadersVisible = false;
            dataGridViewCellStyle77.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle77.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle77.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle77.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionYourOrderPHD.RowsDefaultCellStyle = dataGridViewCellStyle77;
            this.dgvSectionYourOrderPHD.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvSectionYourOrderPHD.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSectionYourOrderPHD.Size = new System.Drawing.Size(745, 35);
            this.dgvSectionYourOrderPHD.TabIndex = 74;
            // 
            // dataGridViewTextBoxColumn29
            // 
            dataGridViewCellStyle69.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            dataGridViewCellStyle69.NullValue = null;
            this.dataGridViewTextBoxColumn29.DefaultCellStyle = dataGridViewCellStyle69;
            this.dataGridViewTextBoxColumn29.HeaderText = "Item";
            this.dataGridViewTextBoxColumn29.Name = "dataGridViewTextBoxColumn29";
            this.dataGridViewTextBoxColumn29.Width = 270;
            // 
            // Column7
            // 
            dataGridViewCellStyle70.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            this.Column7.DefaultCellStyle = dataGridViewCellStyle70;
            this.Column7.HeaderText = "Total Units";
            this.Column7.Name = "Column7";
            this.Column7.Width = 70;
            // 
            // dataGridViewTextBoxColumn30
            // 
            dataGridViewCellStyle71.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle71.NullValue = null;
            this.dataGridViewTextBoxColumn30.DefaultCellStyle = dataGridViewCellStyle71;
            this.dataGridViewTextBoxColumn30.HeaderText = "    UOM";
            this.dataGridViewTextBoxColumn30.Name = "dataGridViewTextBoxColumn30";
            this.dataGridViewTextBoxColumn30.Width = 60;
            // 
            // dataGridViewTextBoxColumn33
            // 
            dataGridViewCellStyle72.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle72.NullValue = null;
            this.dataGridViewTextBoxColumn33.DefaultCellStyle = dataGridViewCellStyle72;
            this.dataGridViewTextBoxColumn33.HeaderText = "Quantities";
            this.dataGridViewTextBoxColumn33.Name = "dataGridViewTextBoxColumn33";
            this.dataGridViewTextBoxColumn33.Width = 70;
            // 
            // dataGridViewTextBoxColumn34
            // 
            dataGridViewCellStyle73.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            this.dataGridViewTextBoxColumn34.DefaultCellStyle = dataGridViewCellStyle73;
            this.dataGridViewTextBoxColumn34.HeaderText = "Qty Backordered";
            this.dataGridViewTextBoxColumn34.Name = "dataGridViewTextBoxColumn34";
            this.dataGridViewTextBoxColumn34.Width = 95;
            // 
            // dataGridViewTextBoxColumn35
            // 
            dataGridViewCellStyle74.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            this.dataGridViewTextBoxColumn35.DefaultCellStyle = dataGridViewCellStyle74;
            this.dataGridViewTextBoxColumn35.HeaderText = "   Unit Price";
            this.dataGridViewTextBoxColumn35.Name = "dataGridViewTextBoxColumn35";
            this.dataGridViewTextBoxColumn35.Width = 85;
            // 
            // dataGridViewTextBoxColumn36
            // 
            dataGridViewCellStyle75.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            this.dataGridViewTextBoxColumn36.DefaultCellStyle = dataGridViewCellStyle75;
            this.dataGridViewTextBoxColumn36.HeaderText = "Extended Price";
            this.dataGridViewTextBoxColumn36.Name = "dataGridViewTextBoxColumn36";
            this.dataGridViewTextBoxColumn36.Width = 95;
            // 
            // dgvSectionPickTicketsLines
            // 
            this.dgvSectionPickTicketsLines.AllowUserToAddRows = false;
            this.dgvSectionPickTicketsLines.AllowUserToDeleteRows = false;
            dataGridViewCellStyle78.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle78.BackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle78.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle78.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle78.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionPickTicketsLines.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle78;
            this.dgvSectionPickTicketsLines.BackgroundColor = System.Drawing.Color.Gray;
            this.dgvSectionPickTicketsLines.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvSectionPickTicketsLines.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvSectionPickTicketsLines.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle79.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle79.BackColor = System.Drawing.SystemColors.AppWorkspace;
            dataGridViewCellStyle79.Font = new System.Drawing.Font("Times New Roman", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle79.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle79.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle79.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle79.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSectionPickTicketsLines.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle79;
            this.dgvSectionPickTicketsLines.ColumnHeadersHeight = 35;
            this.dgvSectionPickTicketsLines.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn23,
            this.Column3,
            this.Column4,
            this.dataGridViewTextBoxColumn17,
            this.dataGridViewTextBoxColumn20,
            this.dataGridViewTextBoxColumn22,
            this.Column8,
            this.Column6});
            dataGridViewCellStyle88.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle88.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle88.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle88.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle88.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle88.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle88.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSectionPickTicketsLines.DefaultCellStyle = dataGridViewCellStyle88;
            this.dgvSectionPickTicketsLines.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSectionPickTicketsLines.GridColor = System.Drawing.Color.Black;
            this.dgvSectionPickTicketsLines.Location = new System.Drawing.Point(35, 244);
            this.dgvSectionPickTicketsLines.MultiSelect = false;
            this.dgvSectionPickTicketsLines.Name = "dgvSectionPickTicketsLines";
            this.dgvSectionPickTicketsLines.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvSectionPickTicketsLines.RowHeadersVisible = false;
            dataGridViewCellStyle89.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle89.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle89.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle89.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionPickTicketsLines.RowsDefaultCellStyle = dataGridViewCellStyle89;
            this.dgvSectionPickTicketsLines.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvSectionPickTicketsLines.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSectionPickTicketsLines.Size = new System.Drawing.Size(745, 35);
            this.dgvSectionPickTicketsLines.TabIndex = 73;
            // 
            // dataGridViewTextBoxColumn23
            // 
            dataGridViewCellStyle80.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn23.DefaultCellStyle = dataGridViewCellStyle80;
            this.dataGridViewTextBoxColumn23.HeaderText = "P.Ticket #";
            this.dataGridViewTextBoxColumn23.Name = "dataGridViewTextBoxColumn23";
            this.dataGridViewTextBoxColumn23.Width = 57;
            // 
            // Column3
            // 
            dataGridViewCellStyle81.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column3.DefaultCellStyle = dataGridViewCellStyle81;
            this.Column3.HeaderText = "Invoice #";
            this.Column3.Name = "Column3";
            this.Column3.Width = 57;
            // 
            // Column4
            // 
            dataGridViewCellStyle82.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column4.DefaultCellStyle = dataGridViewCellStyle82;
            this.Column4.HeaderText = "Location";
            this.Column4.Name = "Column4";
            this.Column4.Width = 53;
            // 
            // dataGridViewTextBoxColumn17
            // 
            dataGridViewCellStyle83.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle83.NullValue = null;
            this.dataGridViewTextBoxColumn17.DefaultCellStyle = dataGridViewCellStyle83;
            this.dataGridViewTextBoxColumn17.HeaderText = "Carrier Name                                      Item";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.Width = 210;
            // 
            // dataGridViewTextBoxColumn20
            // 
            dataGridViewCellStyle84.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle84.NullValue = null;
            this.dataGridViewTextBoxColumn20.DefaultCellStyle = dataGridViewCellStyle84;
            this.dataGridViewTextBoxColumn20.HeaderText = "Trackings                  UOM";
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            this.dataGridViewTextBoxColumn20.Width = 140;
            // 
            // dataGridViewTextBoxColumn22
            // 
            dataGridViewCellStyle85.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle85.NullValue = null;
            this.dataGridViewTextBoxColumn22.DefaultCellStyle = dataGridViewCellStyle85;
            this.dataGridViewTextBoxColumn22.HeaderText = "Ship Dt  Quantities";
            this.dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
            this.dataGridViewTextBoxColumn22.Width = 60;
            // 
            // Column8
            // 
            dataGridViewCellStyle86.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column8.DefaultCellStyle = dataGridViewCellStyle86;
            this.Column8.HeaderText = "Wt.";
            this.Column8.Name = "Column8";
            this.Column8.Width = 20;
            // 
            // Column6
            // 
            dataGridViewCellStyle87.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.Column6.DefaultCellStyle = dataGridViewCellStyle87;
            this.Column6.HeaderText = "Delivery Info";
            this.Column6.Name = "Column6";
            this.Column6.Width = 148;
            // 
            // dgvSectionPickTicketsHeader
            // 
            this.dgvSectionPickTicketsHeader.AllowUserToAddRows = false;
            this.dgvSectionPickTicketsHeader.AllowUserToDeleteRows = false;
            dataGridViewCellStyle90.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle90.BackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle90.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle90.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle90.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionPickTicketsHeader.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle90;
            this.dgvSectionPickTicketsHeader.BackgroundColor = System.Drawing.Color.Gray;
            this.dgvSectionPickTicketsHeader.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvSectionPickTicketsHeader.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvSectionPickTicketsHeader.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle91.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle91.BackColor = System.Drawing.SystemColors.AppWorkspace;
            dataGridViewCellStyle91.Font = new System.Drawing.Font("Times New Roman", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle91.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle91.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle91.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle91.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSectionPickTicketsHeader.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle91;
            this.dgvSectionPickTicketsHeader.ColumnHeadersHeight = 35;
            this.dgvSectionPickTicketsHeader.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn13,
            this.dataGridViewTextBoxColumn14,
            this.Column2,
            this.DeliveryInfo});
            dataGridViewCellStyle98.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle98.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle98.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle98.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle98.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle98.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle98.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSectionPickTicketsHeader.DefaultCellStyle = dataGridViewCellStyle98;
            this.dgvSectionPickTicketsHeader.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSectionPickTicketsHeader.GridColor = System.Drawing.Color.Black;
            this.dgvSectionPickTicketsHeader.Location = new System.Drawing.Point(35, 182);
            this.dgvSectionPickTicketsHeader.MultiSelect = false;
            this.dgvSectionPickTicketsHeader.Name = "dgvSectionPickTicketsHeader";
            this.dgvSectionPickTicketsHeader.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvSectionPickTicketsHeader.RowHeadersVisible = false;
            dataGridViewCellStyle99.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle99.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle99.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle99.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionPickTicketsHeader.RowsDefaultCellStyle = dataGridViewCellStyle99;
            this.dgvSectionPickTicketsHeader.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvSectionPickTicketsHeader.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSectionPickTicketsHeader.Size = new System.Drawing.Size(745, 35);
            this.dgvSectionPickTicketsHeader.TabIndex = 72;
            // 
            // dataGridViewTextBoxColumn10
            // 
            dataGridViewCellStyle92.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle92.NullValue = null;
            this.dataGridViewTextBoxColumn10.DefaultCellStyle = dataGridViewCellStyle92;
            this.dataGridViewTextBoxColumn10.HeaderText = "Pick Ticket #";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.Width = 60;
            // 
            // dataGridViewTextBoxColumn11
            // 
            dataGridViewCellStyle93.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle93.NullValue = null;
            this.dataGridViewTextBoxColumn11.DefaultCellStyle = dataGridViewCellStyle93;
            this.dataGridViewTextBoxColumn11.HeaderText = "Invoice Number";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.Width = 60;
            // 
            // dataGridViewTextBoxColumn12
            // 
            dataGridViewCellStyle94.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle94.NullValue = null;
            this.dataGridViewTextBoxColumn12.DefaultCellStyle = dataGridViewCellStyle94;
            this.dataGridViewTextBoxColumn12.HeaderText = "Location";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.Width = 60;
            // 
            // dataGridViewTextBoxColumn13
            // 
            dataGridViewCellStyle95.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            this.dataGridViewTextBoxColumn13.DefaultCellStyle = dataGridViewCellStyle95;
            this.dataGridViewTextBoxColumn13.HeaderText = "Carrier Name";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.Width = 215;
            // 
            // dataGridViewTextBoxColumn14
            // 
            dataGridViewCellStyle96.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            this.dataGridViewTextBoxColumn14.DefaultCellStyle = dataGridViewCellStyle96;
            this.dataGridViewTextBoxColumn14.HeaderText = "Trackings";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.Width = 140;
            // 
            // Column2
            // 
            dataGridViewCellStyle97.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            this.Column2.DefaultCellStyle = dataGridViewCellStyle97;
            this.Column2.HeaderText = "Ship Date";
            this.Column2.Name = "Column2";
            this.Column2.Width = 60;
            // 
            // DeliveryInfo
            // 
            this.DeliveryInfo.HeaderText = "Delivery Info";
            this.DeliveryInfo.Name = "DeliveryInfo";
            this.DeliveryInfo.Width = 150;
            // 
            // dgvSectionOpenItems
            // 
            this.dgvSectionOpenItems.AllowUserToAddRows = false;
            this.dgvSectionOpenItems.AllowUserToDeleteRows = false;
            dataGridViewCellStyle100.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle100.BackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle100.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle100.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle100.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionOpenItems.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle100;
            this.dgvSectionOpenItems.BackgroundColor = System.Drawing.Color.Gray;
            this.dgvSectionOpenItems.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvSectionOpenItems.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvSectionOpenItems.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle101.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            dataGridViewCellStyle101.BackColor = System.Drawing.SystemColors.AppWorkspace;
            dataGridViewCellStyle101.Font = new System.Drawing.Font("Times New Roman", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle101.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle101.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle101.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle101.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSectionOpenItems.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle101;
            this.dgvSectionOpenItems.ColumnHeadersHeight = 35;
            this.dgvSectionOpenItems.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9});
            dataGridViewCellStyle106.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle106.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle106.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle106.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle106.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle106.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle106.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSectionOpenItems.DefaultCellStyle = dataGridViewCellStyle106;
            this.dgvSectionOpenItems.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSectionOpenItems.GridColor = System.Drawing.Color.Black;
            this.dgvSectionOpenItems.Location = new System.Drawing.Point(35, 126);
            this.dgvSectionOpenItems.MultiSelect = false;
            this.dgvSectionOpenItems.Name = "dgvSectionOpenItems";
            this.dgvSectionOpenItems.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvSectionOpenItems.RowHeadersVisible = false;
            dataGridViewCellStyle107.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle107.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle107.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle107.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionOpenItems.RowsDefaultCellStyle = dataGridViewCellStyle107;
            this.dgvSectionOpenItems.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvSectionOpenItems.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSectionOpenItems.Size = new System.Drawing.Size(745, 35);
            this.dgvSectionOpenItems.TabIndex = 71;
            // 
            // dataGridViewTextBoxColumn6
            // 
            dataGridViewCellStyle102.NullValue = null;
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle102;
            this.dataGridViewTextBoxColumn6.HeaderText = "Item";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Width = 505;
            // 
            // dataGridViewTextBoxColumn7
            // 
            dataGridViewCellStyle103.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle103.NullValue = null;
            this.dataGridViewTextBoxColumn7.DefaultCellStyle = dataGridViewCellStyle103;
            this.dataGridViewTextBoxColumn7.HeaderText = "    UOM";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.Width = 60;
            // 
            // dataGridViewTextBoxColumn8
            // 
            dataGridViewCellStyle104.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle104.NullValue = null;
            this.dataGridViewTextBoxColumn8.DefaultCellStyle = dataGridViewCellStyle104;
            this.dataGridViewTextBoxColumn8.HeaderText = "  Quantities";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.Width = 80;
            // 
            // dataGridViewTextBoxColumn9
            // 
            dataGridViewCellStyle105.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            this.dataGridViewTextBoxColumn9.DefaultCellStyle = dataGridViewCellStyle105;
            this.dataGridViewTextBoxColumn9.HeaderText = "Qty Backordered";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            // 
            // dgvSectionYourOrder
            // 
            this.dgvSectionYourOrder.AllowUserToAddRows = false;
            this.dgvSectionYourOrder.AllowUserToDeleteRows = false;
            dataGridViewCellStyle108.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle108.BackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle108.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle108.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle108.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionYourOrder.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle108;
            this.dgvSectionYourOrder.BackgroundColor = System.Drawing.Color.Gray;
            this.dgvSectionYourOrder.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvSectionYourOrder.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvSectionYourOrder.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle109.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            dataGridViewCellStyle109.BackColor = System.Drawing.SystemColors.AppWorkspace;
            dataGridViewCellStyle109.Font = new System.Drawing.Font("Times New Roman", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle109.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle109.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle109.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle109.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSectionYourOrder.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle109;
            this.dgvSectionYourOrder.ColumnHeadersHeight = 35;
            this.dgvSectionYourOrder.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Field1,
            this.Field2,
            this.Field3,
            this.Field4,
            this.Field5,
            this.Field6});
            dataGridViewCellStyle116.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle116.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle116.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle116.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle116.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle116.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle116.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSectionYourOrder.DefaultCellStyle = dataGridViewCellStyle116;
            this.dgvSectionYourOrder.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSectionYourOrder.GridColor = System.Drawing.Color.Black;
            this.dgvSectionYourOrder.Location = new System.Drawing.Point(35, 67);
            this.dgvSectionYourOrder.MultiSelect = false;
            this.dgvSectionYourOrder.Name = "dgvSectionYourOrder";
            this.dgvSectionYourOrder.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvSectionYourOrder.RowHeadersVisible = false;
            dataGridViewCellStyle117.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle117.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle117.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle117.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionYourOrder.RowsDefaultCellStyle = dataGridViewCellStyle117;
            this.dgvSectionYourOrder.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvSectionYourOrder.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSectionYourOrder.Size = new System.Drawing.Size(745, 35);
            this.dgvSectionYourOrder.TabIndex = 53;
            // 
            // Field1
            // 
            dataGridViewCellStyle110.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            dataGridViewCellStyle110.NullValue = null;
            this.Field1.DefaultCellStyle = dataGridViewCellStyle110;
            this.Field1.HeaderText = "Item";
            this.Field1.Name = "Field1";
            this.Field1.Width = 340;
            // 
            // Field2
            // 
            dataGridViewCellStyle111.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle111.NullValue = null;
            this.Field2.DefaultCellStyle = dataGridViewCellStyle111;
            this.Field2.HeaderText = "    UOM";
            this.Field2.Name = "Field2";
            this.Field2.Width = 60;
            // 
            // Field3
            // 
            dataGridViewCellStyle112.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle112.NullValue = null;
            this.Field3.DefaultCellStyle = dataGridViewCellStyle112;
            this.Field3.HeaderText = "Quantities";
            this.Field3.Name = "Field3";
            this.Field3.Width = 70;
            // 
            // Field4
            // 
            dataGridViewCellStyle113.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            this.Field4.DefaultCellStyle = dataGridViewCellStyle113;
            this.Field4.HeaderText = "Qty Backordered";
            this.Field4.Name = "Field4";
            this.Field4.Width = 95;
            // 
            // Field5
            // 
            dataGridViewCellStyle114.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            this.Field5.DefaultCellStyle = dataGridViewCellStyle114;
            this.Field5.HeaderText = "   Unit Price";
            this.Field5.Name = "Field5";
            this.Field5.Width = 85;
            // 
            // Field6
            // 
            dataGridViewCellStyle115.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            this.Field6.DefaultCellStyle = dataGridViewCellStyle115;
            this.Field6.HeaderText = "Extended Price";
            this.Field6.Name = "Field6";
            this.Field6.Width = 95;
            // 
            // dgvSectionHeaderA2
            // 
            this.dgvSectionHeaderA2.AllowUserToAddRows = false;
            this.dgvSectionHeaderA2.AllowUserToDeleteRows = false;
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle24.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle24.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle24.SelectionBackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle24.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeaderA2.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle24;
            this.dgvSectionHeaderA2.BackgroundColor = System.Drawing.Color.White;
            this.dgvSectionHeaderA2.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvSectionHeaderA2.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle25.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle25.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle25.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle25.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle25.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle25.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSectionHeaderA2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle25;
            this.dgvSectionHeaderA2.ColumnHeadersHeight = 22;
            this.dgvSectionHeaderA2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn38});
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle26.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle26.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle26.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle26.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle26.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle26.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSectionHeaderA2.DefaultCellStyle = dataGridViewCellStyle26;
            this.dgvSectionHeaderA2.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSectionHeaderA2.GridColor = System.Drawing.Color.Black;
            this.dgvSectionHeaderA2.Location = new System.Drawing.Point(35, 120);
            this.dgvSectionHeaderA2.MultiSelect = false;
            this.dgvSectionHeaderA2.Name = "dgvSectionHeaderA2";
            this.dgvSectionHeaderA2.RowHeadersVisible = false;
            dataGridViewCellStyle27.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle27.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSectionHeaderA2.RowsDefaultCellStyle = dataGridViewCellStyle27;
            this.dgvSectionHeaderA2.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvSectionHeaderA2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSectionHeaderA2.Size = new System.Drawing.Size(745, 22);
            this.dgvSectionHeaderA2.TabIndex = 73;
            // 
            // dataGridViewTextBoxColumn38
            // 
            this.dataGridViewTextBoxColumn38.HeaderText = "Delivery Instructions:";
            this.dataGridViewTextBoxColumn38.Name = "dataGridViewTextBoxColumn38";
            this.dataGridViewTextBoxColumn38.Width = 745;
            // 
            // frmOrderPrint
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(856, 600);
            this.Controls.Add(this.tabControl1);
            this.Name = "frmOrderPrint";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.tabControl1.ResumeLayout(false);
            this.tabHeader.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPageHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEndOTInvoices)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEndGroupMsg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmptyMessage)).EndInit();
            this.tabHeaderSections.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeader2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeaderPickTickets)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeaderOpenItems)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeaderYourOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeaderA0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeaderA1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionLine6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeader22)).EndInit();
            this.tabSections.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionYourOrderPHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionPickTicketsLines)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionPickTicketsHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionOpenItems)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionYourOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSectionHeaderA2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabHeader;
        private System.Windows.Forms.TabPage tabHeaderSections;
        private System.Windows.Forms.TabPage tabSections;
        private System.Windows.Forms.DataGridView dgvGroup1;
        private System.Windows.Forms.DataGridView dgvEndOTInvoices;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridView dgvEndGroupMsg;
        private System.Windows.Forms.Label lblFooter;
        private System.Windows.Forms.DataGridView dgvEmptyMessage;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn32;
        private System.Windows.Forms.DataGridView dgvSectionHeader22;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn31;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridView dgvSectionLine6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn26;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn42;
        private System.Windows.Forms.DataGridView dgvPageHeader;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridView dgvSectionHeaderA1;
        private System.Windows.Forms.DataGridView dgvSectionHeaderA0;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridView dgvSectionHeaderYourOrder;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridView dgvSectionHeaderOpenItems;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridView dgvSectionHeaderPickTickets;
        private System.Windows.Forms.DataGridView dgvSectionPickTicketsLines;
        private System.Windows.Forms.DataGridView dgvSectionPickTicketsHeader;
        private System.Windows.Forms.DataGridView dgvSectionOpenItems;
        private System.Windows.Forms.DataGridView dgvSectionYourOrder;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn DeliveryInfo;
        private System.Windows.Forms.DataGridViewTextBoxColumn BillToAddress;
        private System.Windows.Forms.DataGridViewTextBoxColumn ShipTo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ShipToAddress;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.DataGridViewTextBoxColumn Field1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Field2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Field3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Field4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Field5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Field6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridView dgvSectionHeader2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn24;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn25;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn27;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn28;
        private System.Windows.Forms.DataGridView dgvSectionYourOrderPHD;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn29;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn30;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn33;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn34;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn35;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn36;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn23;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridView dgvSectionHeaderA2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn38;
    }
}
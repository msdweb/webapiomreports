﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.IO;
using System.Data.SqlClient;

namespace OMOrders
{
    sealed class MainClass
    {
        public static void Main()
        {
            GLB.Customer_no = "144252"; // "133089"; //"138860"; //"141215"; //"103846"; //"141215"; //"101527"; //"127317";
            GLB.Order_no = "4843380";  //"4853113"; //"4843380";  //"4010506"; //"4012360"; //"3832818";  //"4007737"; //"3832818"; //"3789344"; //"3639424"; // "3590179"; //"3639424"; //"3473896"; //"3645444"; //"3656543";

            string sConn;
            //DbAccess.Set_MSDSQL();
            DbAccess.Set_MSDSQL01();
            //DbAccess.Set_MSDSQL03();
            sConn = DbAccess.ConnectionString;

            GLB.GetData(sConn);
            Application.Run(new frmOMOrder());
        }
    }

    //
    //******************************************************************************
    //

    public class OMOrders
    {
        public static string Create_OMOrderHistory(string Customer_no, string Order_no, string sConn)
        {
            string FilePath = string.Empty;
            GLB.Customer_no = Customer_no;
            GLB.Order_no = Order_no;
            GLB.GetData(sConn); 
            frmOrderPrint frmPCSReport = new frmOrderPrint();
            if (GLB.orderHistoryDetail.OrderHistoryAll.Count > 0)
            {
                FilePath = "OMOrders/MSD-Order-" + Order_no + ".xps";
                FilePath = string.Format("{0}?t={1}", FilePath, DateTime.Now.Ticks);
                frmPCSReport.PCS_Salva();
            }
            return FilePath;
        }
    }

    //
    //******************************************************************************
    //

    public class  GLB
    {

        public static DataTable OMOrder;

        public static OrderHistoryDetail orderHistoryDetail = new OrderHistoryDetail();

        public static string Customer_no = string.Empty;
        public static string Order_no = string.Empty;

        public static void GetData(string sConn)
        {

            //###################################################################

            orderHistoryDetail = OrderHistoryDetails.Get_OrderHistoryDetail(Customer_no, Order_no, sConn);

            return;
        }

        //
        //******************************************************************************
        //
        public static bool IsNumeric(object Expression)
        {
            bool isNum;
            double retNum;
            isNum = Double.TryParse(Convert.ToString(Expression),
                    System.Globalization.NumberStyles.Any, 
                    System.Globalization.NumberFormatInfo.InvariantInfo, 
                    out retNum);
            return isNum;
        }

        public static bool HasAnyNum(string numberString)
        { 
          char [] ca = numberString.ToCharArray();

          foreach (char i in ca)
          {
              if (char.IsNumber(i))
                  return true;
          } 
          return false; 
        }
        public static bool HasAnyNum1(string numberString)
        {
            char[] ca = numberString.ToCharArray();
            for (int i = 0; i < ca.Length; i++)
            {
                if (char.IsNumber(ca[i]))
                    return true;
            }
            return false;
        }

        ~GLB()
        {
        }

    }
    
    public class AddValue
    {
        private string m_Display;
        private string m_Value;
        public AddValue(string Display, string Value)
        {
            m_Display = Display;
            m_Value = Value;
        }
        public string Display
        {
            get { return m_Display; }
        }
        public string Value
        {
            get { return m_Value; }
        }
    }
}

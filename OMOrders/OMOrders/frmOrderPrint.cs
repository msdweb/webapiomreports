﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Text;
using System.Windows.Forms;

namespace OMOrders
{
    public partial class frmOrderPrint : Form
    {
        private DataView DVPageHeader;
        
        DataView DVSection1;
        DataView DVSection1_PHD;

        DataView DVSection2;
        DataView DVSection3;

        private Printer DataGridPrinter1 = null;

        public frmOrderPrint()
        {
            InitializeComponent();
        }

        //####################################################################################
        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            Graphics g = e.Graphics;
            bool more = DataGridPrinter1.DrawDataGrid(g);
            if (more == true)
            {
                e.HasMorePages = true;
                DataGridPrinter1.PageNumber++;
                DataGridPrinter1.TotalPageNumber++;
            }
        }

        public void PCS_Preview()
        {
            if (GLB.orderHistoryDetail.OrderHistoryAll.Count > 0)
            {
                SetupGridPrinter();

                frmPrinterControl PC = new frmPrinterControl();
                // if (DataGridPrinter1.
                PC.RPT_Preview(DataGridPrinter1);
                PC.ShowDialog();
                PC = null;
            }
        }

        public void PCS_Print()
        {
            if (GLB.orderHistoryDetail.OrderHistoryAll.Count > 0)
            {
                SetupGridPrinter();

                frmPrinterControl PC = new frmPrinterControl();
                PC.RPT_Print(DataGridPrinter1);
                PC = null;
            }
        }

        public void PCS_Salva()
        {
            if (GLB.orderHistoryDetail.OrderHistoryAll.Count > 0)
            {
                string s = System.AppDomain.CurrentDomain.BaseDirectory;
                printDocument1.DefaultPageSettings.PrinterSettings.PrinterName = "Microsoft XPS Document Writer";
                string FileName = "C:\\temp\\OM-Report\\MSD-Order-" + GLB.Order_no + ".xps";
                printDocument1.DefaultPageSettings.PrinterSettings.PrintFileName = FileName;
                printDocument1.DefaultPageSettings.PrinterSettings.PrintToFile = true;

                SetupGridPrinter();

                frmPrinterControl PC = new frmPrinterControl();

                PC.RPT_Print(DataGridPrinter1);
                PC = null;
            }
        }

        void SetupGridPrinter()
        {

            dataDetails dataDetails = new dataDetails();

            lblFooter.Text = dataDetails.Get_LastReview();
            dgvEmptyMessage.ColumnHeadersVisible = false;

            DVPageHeader = dataDetails.DV_PageHeader_Fill(DVPageHeader, "");

            dgvPageHeader.BorderStyle = BorderStyle.None;
            dgvPageHeader.ColumnHeadersVisible = true;
            DataGridPrinter1 = new Printer(printDocument1,
                                           dgvPageHeader,
                                           dgvEmptyMessage,
                                           null,
                                           DVPageHeader.Table.DefaultView, lblFooter);

            //----------------------------------------------------------------------------
            //----------------------------------------------------------------------------
            //----------------------------------------------------------------------------

            decimal decSubTotal = 0;

            DVSection1 = dataDetails.DV_Section1_Open();
            DVSection1_PHD = dataDetails.DV_Section1_PHD_Open();

            string sAlternateColor = "1";

            dgvSectionYourOrderPHD.Columns[1].HeaderText = "";
            if (GLB.orderHistoryDetail.CustomerType.ToString() == "PHD")
            {
                dgvSectionYourOrderPHD.Columns[1].HeaderText = "Total Units";
            }

            foreach (OrderHistoryAll OrderHistoryAll in GLB.orderHistoryDetail.OrderHistoryAll)
            {

                string itemID_Description = string.Empty;
                string itemID = string.Empty;
                string itemDescription = string.Empty;

                string total_units = string.Empty;
                
                string UOM = string.Empty;

                string qtyOrdered = string.Empty;
                string qtyBackorder = string.Empty;

                string UnitPrice = string.Empty;
                string ExtendPrice = string.Empty;

                itemID = OrderHistoryAll.item_id.ToString();
                itemID_Description = OrderHistoryAll.item_id.ToString() + (char)13 + OrderHistoryAll.item_desc.ToString();

                if (OrderHistoryAll.total_units.ToString() != "")
                {
                    decimal dectotal_units = Convert.ToDecimal(OrderHistoryAll.total_units);
                    if (dectotal_units > 0)
                    {
                        total_units = dectotal_units.ToString("0.##");
                    }
                }

                UOM = OrderHistoryAll.unit_of_measure.ToString();

                decimal decqtyOrdered = Convert.ToDecimal(OrderHistoryAll.unit_quantity);
                qtyOrdered = decqtyOrdered.ToString("0.##");

                decimal decqtyBackorder = Convert.ToDecimal(OrderHistoryAll.qty_backorder);
                if (decqtyBackorder > 0)
                {
                    qtyBackorder = decqtyBackorder.ToString("0.##");
                }

                if (OrderHistoryAll.unit_price.ToString() != "")
                {
                    decimal decUnitPrice = Convert.ToDecimal(OrderHistoryAll.unit_price);
                    UnitPrice = string.Format("{0:C}", decUnitPrice);
                }

                if (OrderHistoryAll.extended_price.ToString() != "")
                {
                    decimal decExtendPrice = Convert.ToDecimal(OrderHistoryAll.extended_price.ToString());
                    decSubTotal += decExtendPrice;
                    ExtendPrice = string.Format("{0:C}", decExtendPrice);
                }

                //
                //------------------------------------------------------------------------------------------
                //

                if (sAlternateColor == "0")
                { sAlternateColor = "1"; }
                else { sAlternateColor = "0"; }

                //DVSection1 = dataDetails.DV_Section1_Fill(DVSection1, sAlternateColor, itemID_Description, UOM, qtyOrdered, qtyBackorder, UnitPrice, ExtendPrice);
                DVSection1_PHD = dataDetails.DV_Section1_PHD_Fill(DVSection1_PHD, sAlternateColor, itemID_Description, total_units, UOM, qtyOrdered, qtyBackorder, UnitPrice, ExtendPrice);

                //
                //------------------------------------------------------------------------
                //
            }


            //----------------------------------------------------------------------------
            //----------------------------------------------------------------------------
            //----------------------------------------------------------------------------

            string currentInvoice = string.Empty;
            int g = 0;


            dgvGroup1.ColumnHeadersVisible = false;
            DataGridPrinter1.AddGroup(dgvGroup1, null, null);

            //----------------------------------------------------------------------------

            string strVal1 = string.Empty;
            //if (GLB.orderHistoryDetail.ShippingState != null)
            //{
            //    if (GLB.orderHistoryDetail.ShippingState.ToString() != "FL")
            //    {
            //        strVal1 += "800 Technology Center Drive" + (char)13;
            //        strVal1 += "Stoughton, MA 02072" + (char)13;
            //        strVal1 += "800.967.6400    781.344.7244";
            //    }
            //    else
            //    {
            //        strVal1 += "11600 Miramar Parkway Suite 300" + (char)13;
            //        strVal1 += "Miramar, FL 33025" + (char)13;
            //        strVal1 += "954.986.2000            954.983.2503";
            //    }
            //}
            strVal1 += "800 Technology Center Drive" + (char)13;
            strVal1 += "Stoughton, MA 02072" + (char)13;
            strVal1 += "800.967.6400    781.344.7244";

            DataView DVSectionHeaderA0 = new DataView();
            DVSectionHeaderA0 = dataDetails.DV_SectionHeaderA1_Fill(DVSectionHeaderA0, strVal1);
            dgvSectionHeaderA0.ColumnHeadersVisible = false;
            DataGridPrinter1.AddSectionHeader(g, false,
                                              null,
                                              dgvSectionHeaderA0,
                                              DVSectionHeaderA0);

            //----------------------------------------------------------------------------

            DateTime dOrderDate = Convert.ToDateTime(GLB.orderHistoryDetail.OrderDate);
            string sdOrderDate = dOrderDate.ToString("MM/dd/yyyy");

            string sSubTotal = string.Format("{0:C}", decSubTotal);

            string sFreightDetails = string.Empty;
            sFreightDetails = GLB.orderHistoryDetail.FreightInfoOption.ToString();
            //sFreightDetails = "Signature Required, Saturday Delivery, Inside Delivery, Guaranteed Delivery";

            string sSourceLocation = string.Empty;
            sSourceLocation = GLB.orderHistoryDetail.SourceLocationId.ToString() + "-" + GLB.orderHistoryDetail.SourceLocationName.ToString();

            DataView DVSectionHeaderA1 = new DataView();
            DVSectionHeaderA1 = dataDetails.DV_Section_TwoFields_Open();

            DVSectionHeaderA1 = dataDetails.DV_Section_TwoFields_Fill(DVSectionHeaderA1, "                                 PO #", " " + GLB.orderHistoryDetail.CustomerPurchaseOrder.ToString());
            DVSectionHeaderA1 = dataDetails.DV_Section_TwoFields_Fill(DVSectionHeaderA1, "                            Order #", " " + GLB.orderHistoryDetail.OrderNumber.ToString());
            DVSectionHeaderA1 = dataDetails.DV_Section_TwoFields_Fill(DVSectionHeaderA1, "                      Order Date", " " + sdOrderDate);
            DVSectionHeaderA1 = dataDetails.DV_Section_TwoFields_Fill(DVSectionHeaderA1, "                        Account #", " " + GLB.orderHistoryDetail.CustomerNumber.ToString());
            DVSectionHeaderA1 = dataDetails.DV_Section_TwoFields_Fill(DVSectionHeaderA1, "            Product Subtotal", " " + sSubTotal);
            DVSectionHeaderA1 = dataDetails.DV_Section_TwoFields_Fill(DVSectionHeaderA1, "                Freight Details", " " + sFreightDetails);
            DVSectionHeaderA1 = dataDetails.DV_Section_TwoFields_Fill(DVSectionHeaderA1, "             Source Location", " " + sSourceLocation);
            DVSectionHeaderA1 = dataDetails.DV_Section_TwoFields_Fill(DVSectionHeaderA1, "                     Ordered By", " " + GLB.orderHistoryDetail.PlacedBy.ToString());

            dgvSectionHeaderA1.ColumnHeadersVisible = false;
            DataGridPrinter1.AddSectionHeader(g, false,
                                              null,
                                              dgvSectionHeaderA1,
                                              DVSectionHeaderA1);
            //
            //------------------------------------------------------------------------------------------
            //

            string sBill = string.Empty;

            if (GLB.orderHistoryDetail.BillingCompanyName != null)
            {
                sBill += GLB.orderHistoryDetail.BillingCompanyName.ToString().Trim();
            }

            if (GLB.orderHistoryDetail.BillingAddress1 != null)
            {
                sBill += (char)13;
                sBill += GLB.orderHistoryDetail.BillingAddress1.ToString().Trim();
            }

            if (GLB.orderHistoryDetail.BillingAddress2 != null)
            {
                if (GLB.orderHistoryDetail.BillingAddress2.ToString().Trim() != "")
                {
                    sBill += (char)13;
                    sBill += GLB.orderHistoryDetail.BillingAddress2.ToString().Trim();
                }
            }

            if (GLB.orderHistoryDetail.BillingCity != null)
            {
                sBill += (char)13;
                sBill += GLB.orderHistoryDetail.BillingCity.ToString().Trim();
            }

            if (GLB.orderHistoryDetail.BillingState != null)
            {
                sBill += ", "; //(char)13;
                sBill += GLB.orderHistoryDetail.BillingState.ToString().Trim();
            }

            if (GLB.orderHistoryDetail.BillingPostalCode != null)
            {
                sBill += ", "; //(char)13;
                sBill += GLB.orderHistoryDetail.BillingPostalCode.ToString().Trim();
            }

            //-------------------------------------------------------------

            string sShip = string.Empty;

            if (GLB.orderHistoryDetail.ShippingCompanyName != null)
            {
                sShip += GLB.orderHistoryDetail.ShippingCompanyName.ToString().Trim();
            }

            if (GLB.orderHistoryDetail.ShippingAddress1 != null)
            {
                sShip += (char)13;
                sShip += GLB.orderHistoryDetail.ShippingAddress1.ToString().Trim();
            }

            if (GLB.orderHistoryDetail.ShippingAddress2 != null)
            {
                if (GLB.orderHistoryDetail.ShippingAddress2.ToString().Trim() != "")
                {
                    sShip += (char)13;
                    sShip += GLB.orderHistoryDetail.ShippingAddress2.ToString().Trim();
                }
            }

            if (GLB.orderHistoryDetail.ShippingCity != null)
            {
                sShip += (char)13;
                sShip += GLB.orderHistoryDetail.ShippingCity.ToString().Trim();
            }

            if (GLB.orderHistoryDetail.ShippingState != null)
            {
                sShip += ", "; //(char)13;
                sShip += GLB.orderHistoryDetail.ShippingState.ToString().Trim();
            }

            if (GLB.orderHistoryDetail.ShippingPostalCode != null)
            {
                sShip += ", "; //(char)13;
                sShip += GLB.orderHistoryDetail.ShippingPostalCode.ToString().Trim();
            }


            string sStatusHistory = string.Empty;

            if (GLB.orderHistoryDetail.OrderStatus != null)
            {
                dgvSectionHeader2.Columns[4].HeaderText = "Order Status: " + GLB.orderHistoryDetail.OrderStatus;
            }


            //if (GLB.orderHistoryDetail.OrderDate != null)
            //{
            //    DateTime dOrderedDate = Convert.ToDateTime(GLB.orderHistoryDetail.OrderDate);
            //    string sdOrderedDate = dOrderDate.ToString("MM/dd/yyyy");
            //    sStatusHistory += "Ordered: " + sdOrderedDate;
            //}

            if (!String.IsNullOrEmpty(GLB.orderHistoryDetail.AcknowledgedDate))
            {
                DateTime dAcknowledgedDate = Convert.ToDateTime(GLB.orderHistoryDetail.AcknowledgedDate);
                string sdAcknowledgedDate = dAcknowledgedDate.ToString("MM/dd/yyyy HH:mm");
                
                if (sStatusHistory !="") { sStatusHistory += (char)13; }
                sStatusHistory += "Acknowledged: " + sdAcknowledgedDate;
            }

            if (!String.IsNullOrEmpty(GLB.orderHistoryDetail.PickedDate))
            {
                DateTime dPickedDate = Convert.ToDateTime(GLB.orderHistoryDetail.PickedDate);
                string sdPickedDate = dPickedDate.ToString("MM/dd/yyyy HH:mm");

                if (sStatusHistory != "") { sStatusHistory += (char)13; }
                sStatusHistory += "Picked: " + sdPickedDate;
            }

            if (!String.IsNullOrEmpty(GLB.orderHistoryDetail.ShippedDate))
            {
                DateTime dShippedDate = Convert.ToDateTime(GLB.orderHistoryDetail.ShippedDate);
                string sdShippedDate = dShippedDate.ToString("MM/dd/yyyy HH:mm");

                if (sStatusHistory != "") { sStatusHistory += (char)13; }
                sStatusHistory += "Shipped: " + sdShippedDate;
            }

            if (!String.IsNullOrEmpty(GLB.orderHistoryDetail.DeliveredDate))
            {
                DateTime dDeliveredDate = Convert.ToDateTime(GLB.orderHistoryDetail.DeliveredDate);
                string sdDeliveredDate = dDeliveredDate.ToString("MM/dd/yyyy HH:mm");

                if (sStatusHistory != "") { sStatusHistory += (char)13; }
                sStatusHistory += "Delivered: " + sdDeliveredDate;
            }

            DataView DVSectionHeader2 = new DataView();
            DVSectionHeader2 = dataDetails.DV_SectionHeader2_Fill(DVSectionHeader2, sBill, sShip, sStatusHistory);

            dgvSectionHeader2.ColumnHeadersVisible = true;
            dgvSectionHeader2.BorderStyle = BorderStyle.None;
            DataGridPrinter1.AddSectionHeader(g, false,
                                              null,
                                              dgvSectionHeader2,
                                              DVSectionHeader2);

            //DataView DVSectionHeader22 = new DataView();
            //DVSectionHeader22 = dataDetails.DV_SectionHeader22_Fill(DVSectionHeader22, sBill, sShip);

            //dgvSectionHeader22.ColumnHeadersVisible = true;
            //dgvSectionHeader22.BorderStyle = BorderStyle.None;
            //DataGridPrinter1.AddSectionHeader(g, false,
            //                                  null,
            //                                  dgvSectionHeader22,
            //                                  DVSectionHeader22);

            //
            //------------------------------------------------------------------------------------------
            //

            DataView DVSectionHeaderA2 = new DataView();
            DVSectionHeaderA2 = dataDetails.DV_SectionHeaderA1_Fill(DVSectionHeaderA2, GLB.orderHistoryDetail.DeliveryInstructions);
            dgvSectionHeaderA2.ColumnHeadersVisible = true;
            dgvSectionHeaderA2.BorderStyle = BorderStyle.None;
            DataGridPrinter1.AddSectionHeader(g, false,
                                              null,
                                              dgvSectionHeaderA2,
                                              DVSectionHeaderA2);
            //
            //------------------------------------------------------------------------------------------
            //


            //DataGridPrinter1.AddSection(1, false,
            //                dgvSectionHeaderYourOrder,
            //                dgvSectionYourOrder,
            //                DVSection1);


            DataGridPrinter1.AddSection(1, false,
                            dgvSectionHeaderYourOrder,
                            dgvSectionYourOrderPHD,
                            DVSection1_PHD);

            //
            //------------------------------------------------------------------------------------------
            //


            if (GLB.orderHistoryDetail.OrderHistoryOpen.Count > 0)
            {
                DVSection2 = dataDetails.DV_Section2_Open();

                sAlternateColor = "1";

                foreach (OrderHistoryOpen OrderHistoryOpen in GLB.orderHistoryDetail.OrderHistoryOpen)
                {

                    string itemID_Description = string.Empty;
                    string UOM = string.Empty;

                    string qtyOrdered = string.Empty;
                    string qtyBackorder = string.Empty;

                    itemID_Description = OrderHistoryOpen.item_id.ToString() + (char)13 + OrderHistoryOpen.item_desc.ToString();
                    UOM = OrderHistoryOpen.unit_of_measure.ToString();

                    decimal decqtyOrdered = Convert.ToDecimal(OrderHistoryOpen.qty_ordered);
                    qtyOrdered = decqtyOrdered.ToString("0.##");

                    decimal decqtyBackorder = Convert.ToDecimal(OrderHistoryOpen.qty_backorder);
                    if (decqtyBackorder > 0)
                    {
                        qtyBackorder = decqtyBackorder.ToString("0.##");
                    }

                    //
                    //------------------------------------------------------------------------------------------
                    //

                    if (sAlternateColor == "0")
                    { sAlternateColor = "1"; }
                    else { sAlternateColor = "0"; }

                    DVSection2 = dataDetails.DV_Section2_Fill(DVSection2, sAlternateColor, itemID_Description, UOM, qtyOrdered, qtyBackorder);

                    //
                    //------------------------------------------------------------------------
                    //
                }

                DataGridPrinter1.AddSection(1, false,
                                            dgvSectionHeaderOpenItems,
                                            dgvSectionOpenItems,
                                            DVSection2);
            }

            //
            //------------------------------------------------------------------------------------------
            //


            if (GLB.orderHistoryDetail.OrderHistoryPickTicket.Count > 0)
            {

                //dataGridView1.ColumnHeadersVisible = false;
                //dataGridView1.BorderStyle = BorderStyle.None;
                //DataGridPrinter1.AddSection(1, false,
                //                            dgvSectionHeaderPickTickets,
                //                            dataGridView1,
                //                            DVPageHeader);

                //
                //------------------------------------------------------------------------------------------
                //

                DVSection3 = dataDetails.DV_Section3_Open();

                sAlternateColor = "1";

                foreach (OrderHistoryPickTicket OrderHistoryPickTicket in GLB.orderHistoryDetail.OrderHistoryPickTicket)
                {

                    string PickTicketNo = string.Empty;
                    string InvoiceNo = string.Empty;
                    string LocationID = string.Empty;
                    string Carrier = string.Empty;
                    string DeliveryInfo = string.Empty;

                    string TrackingNo = string.Empty;
                    string ShipDate = string.Empty;
                    DateTime dShipDate;

                    string PackageWeight = string.Empty;

                    string itemID_Description = string.Empty;
                    string UOM = string.Empty;
                    string UnitQuantity = string.Empty;

                    string LotNo = string.Empty;
                    string LotUnitQuantity = string.Empty;
                    string LotUOM = string.Empty;
                    string LotExpirationDate = string.Empty;
                    DateTime dLotExpirationDate;

                    string LotInfo = string.Empty;

                    PickTicketNo = OrderHistoryPickTicket.pick_ticket_no.ToString();
                    InvoiceNo = OrderHistoryPickTicket.invoice_no.ToString();
                    LocationID = OrderHistoryPickTicket.location_id.ToString();
                    Carrier = OrderHistoryPickTicket.carrier_name.ToString();

                    if (sAlternateColor == "0")
                    { sAlternateColor = "1"; }
                    else { sAlternateColor = "0"; }

                    if (OrderHistoryPickTicket.OrderHistoryPickTicketTracking.Count > 0)
                    {
                        foreach (OrderHistoryPickTicketTracking OrderHistoryPickTicketTracking in OrderHistoryPickTicket.OrderHistoryPickTicketTracking)
                        {
                            TrackingNo = OrderHistoryPickTicketTracking.tracking_no.ToString();
                            dShipDate = Convert.ToDateTime(OrderHistoryPickTicketTracking.shipped_date);
                            ShipDate = dShipDate.ToString("MM/dd/yyyy");
                            PackageWeight = OrderHistoryPickTicketTracking.package_weight.ToString();
                            DeliveryInfo = OrderHistoryPickTicketTracking.poddelivered_date.ToString();
                            DeliveryInfo += " " + OrderHistoryPickTicketTracking.poddelivered_time.ToString();
                            DeliveryInfo += " " + OrderHistoryPickTicketTracking.poddelivered_notes.ToString();

                            DVSection3 = dataDetails.DV_Section3_Fill(DVSection3, sAlternateColor, PickTicketNo, InvoiceNo, LocationID, Carrier, TrackingNo, ShipDate, PackageWeight, DeliveryInfo);

                            PickTicketNo = "";
                            InvoiceNo = "";
                            LocationID = "";
                            Carrier = "";
                            DeliveryInfo = "";
                        }
                    }
                    else
                    {
                        DVSection3 = dataDetails.DV_Section3_Fill(DVSection3, sAlternateColor, PickTicketNo, InvoiceNo, LocationID, Carrier, TrackingNo, ShipDate, PackageWeight, DeliveryInfo);
                    }


                    //if (OrderHistoryPickTicket.OrderHistoryPickTicketDetail.Count > 0)
                    //{

                    //    DVSection3 = dataDetails.DV_Section3_Fill(DVSection3, sAlternateColor, "_______", "_______", "________", "___________________________________", "_______________________", "_________", "____________________________");
                    //}

                    foreach (OrderHistoryPickTicketDetail OrderHistoryPickTicketDetail in OrderHistoryPickTicket.OrderHistoryPickTicketDetail)
                    {

                        itemID_Description = OrderHistoryPickTicketDetail.item_id.ToString() + (char)13 + OrderHistoryPickTicketDetail.item_desc.ToString();
                        UOM = OrderHistoryPickTicketDetail.unit_of_measure.ToString();

                        decimal decUnitQuantity = 0;
                        UnitQuantity = OrderHistoryPickTicketDetail.unit_quantity.ToString();
                        if (!string.IsNullOrWhiteSpace(UnitQuantity))
                        {
                            decUnitQuantity = Convert.ToDecimal(UnitQuantity);
                            UnitQuantity = decUnitQuantity.ToString("0.##");
                        }

                        LotNo = string.Empty;
                        LotUnitQuantity = string.Empty;
                        LotUOM = string.Empty;
                        LotExpirationDate = string.Empty;

                        LotInfo = string.Empty;

                        if (sAlternateColor == "0")
                        { sAlternateColor = "1"; }
                        else { sAlternateColor = "0"; }

                        DVSection3 = dataDetails.DV_Section3_Fill(DVSection3, sAlternateColor, "", "", "", itemID_Description, UOM, UnitQuantity, "","");

                        if (OrderHistoryPickTicketDetail.OrderHistoryPickTicketDetailLot.Count > 0)
                        {
                            DVSection3 = dataDetails.DV_Section3_Fill(DVSection3, sAlternateColor, "", "", "", "__________________________________", "_______________________", "_________", "", "");

                            foreach (OrderHistoryPickTicketDetailLot OrderHistoryPickTicketDetailLot in OrderHistoryPickTicketDetail.OrderHistoryPickTicketDetailLot)
                            {
                                LotNo = OrderHistoryPickTicketDetailLot.lot.ToString();

                                decimal decLotUnitQuantity = 0;
                                LotUnitQuantity = OrderHistoryPickTicketDetailLot.unit_quantity.ToString();
                                if (!string.IsNullOrWhiteSpace(LotUnitQuantity))
                                {
                                    decLotUnitQuantity = Convert.ToDecimal(LotUnitQuantity);
                                    LotUnitQuantity = decLotUnitQuantity.ToString("0.##");
                                }

                                LotUOM = OrderHistoryPickTicketDetailLot.unit_of_measure.ToString();
                                dLotExpirationDate = Convert.ToDateTime(OrderHistoryPickTicketDetailLot.expiration_date);
                                LotExpirationDate = dLotExpirationDate.ToString("MM/dd/yyyy");

                                LotInfo = "Lot# " + LotNo + "      Lot Qty: " + LotUnitQuantity + "/" + LotUOM;

                                DVSection3 = dataDetails.DV_Section3_Fill(DVSection3, sAlternateColor, "", "", "", LotInfo, "            Lot Expiration Date", LotExpirationDate, "", "");

                                //itemID_Description = "";
                                //UnitQuantity = "";
                                //UOM = "";
                            }

                            DVSection3 = dataDetails.DV_Section3_Fill(DVSection3, sAlternateColor, "", "", "", "__________________________________", "_______________________", "_________", "", "");
                        }
                        else
                        {
                            //DVSection3 = dataDetails.DV_Section3_Fill(DVSection3, sAlternateColor, LotNo, LotUnitQuantity + (char)13 + LotUOM, LotExpirationDate, itemID_Description, UOM, UnitQuantity);
                        }

                        //if (OrderHistoryPickTicket.OrderHistoryPickTicketDetail.Count > 0)
                        //{

                        //    DVSection3 = dataDetails.DV_Section3_Fill(DVSection3, sAlternateColor, "_______", "_______", "________", "___________________________________", "_______________________", "_________", "____________________________");
                        //}

                        //////DVSection3 = dataDetails.DV_Section3_Fill(DVSection3, sAlternateColor, "", "", "", "", "", "");


                    }

                }

                //DataGridPrinter1.AddSection(1, false,
                //                            dgvSectionPickTicketsHeader,
                //                            dgvSectionPickTicketsLines,
                //                            DVSection3);

                DataGridPrinter1.AddSection(1, false,
                                            dgvSectionHeaderPickTickets,
                                            dgvSectionPickTicketsLines,
                                            DVSection3);
            }

            //
            //------------------------------------------------------------------------------------------
            //


            DataGridPrinter1.PrintLogo = true;
            DataGridPrinter1.PrintLogoCenter = false;
        }
    }
}

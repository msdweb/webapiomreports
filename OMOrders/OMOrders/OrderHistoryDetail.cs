﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OMOrders
{
    public class OrderHistoryDetail
    {
        public bool CanReorderItems { get; set; }
        public string ProductSubtotal { get; set; }
        public string OrderNumber { get; set; }
        public DateTime OrderDate { get; set; }
        public DateTime DueDate { get; set; }
        public string CustomerNumber { get; set; }
        public string CustomerPurchaseOrder { get; set; }
        public string BillingCompanyName { get; set; }
        public string BillingAddress1 { get; set; }
        public string BillingAddress2 { get; set; }
        public string BillingCity { get; set; }
        public string BillingState { get; set; }
        public string BillingPostalCode { get; set; }
        public string BillingCountry { get; set; }
        public string ShippingCompanyName { get; set; }
        public string ShippingAddress1 { get; set; }
        public string ShippingAddress2 { get; set; }
        public string ShippingCity { get; set; }
        public string ShippingState { get; set; }
        public string ShippingPostalCode { get; set; }
        public string ShippingCountry { get; set; }
        public string Terms { get; set; }
        public string SourceLocationId { get; set; }
        public string SourceLocationName { get; set; }
        public string CustomerType { get; set; }
        public string FreightInfoOption { get; set; }
        public string WebOrderNumber { get; set; }
        public List<OrderHistoryAll> OrderHistoryAll { get; set; }
        public List<OrderHistoryOpen> OrderHistoryOpen { get; set; }
        public List<OrderHistoryPickTicket> OrderHistoryPickTicket { get; set; }
        public string PlacedBy { get; set; }
        public string DeliveryInstructions { get; set; }
        public string OrderStatus { get; set; }
        public string AcknowledgedDate { get; set; }
        public string PickedDate { get; set; }
        public string ShippedDate { get; set; }
        public string DeliveredDate { get; set; }

        public OrderHistoryDetail()
        {
            OrderHistoryAll = new List<OrderHistoryAll>();
            OrderHistoryOpen = new List<OrderHistoryOpen>();
            OrderHistoryPickTicket = new List<OrderHistoryPickTicket>();
        }
    }

    public class OrderHistoryAll
    {
        public string unit_quantity { get; set; }
        public string unit_of_measure { get; set; }
        public string item_id { get; set; }
        public string item_desc { get; set; }
        public string qty_backorder { get; set; }
        public string unit_price { get; set; }
        public string extended_price { get; set; }
        public string total_units { get; set; }
    }

    public class OrderHistoryOpen
    {
        public string item_id { get; set; }
        public string unit_of_measure { get; set; }
        public string item_desc { get; set; }
        public string qty_ordered { get; set; }
        public string qty_allocated { get; set; }
        public string qty_on_pick_tickets { get; set; }
        public string qty_invoiced { get; set; }
        public string qty_open { get; set; }
        public string unit_price { get; set; }
        public string extended_price { get; set; }
        public string qty_backorder { get; set; }
    }

    public class OrderHistoryPickTicket
    {
        public string pick_ticket_no { get; set; }
        public string invoice_no { get; set; }
        public string location_id { get; set; }
        public string carrier_name { get; set; }
        public List<OrderHistoryPickTicketTracking> OrderHistoryPickTicketTracking { get; set; }
        public List<OrderHistoryPickTicketDetail> OrderHistoryPickTicketDetail { get; set; }

        public OrderHistoryPickTicket()
        {
            OrderHistoryPickTicketTracking = new List<OrderHistoryPickTicketTracking>();
            OrderHistoryPickTicketDetail = new List<OrderHistoryPickTicketDetail>();
        }
    }

    public class OrderHistoryPickTicketTracking
    {
        public string tracking_no { get; set; }
        public string shipped_date { get; set; }
        public string carrrierURL { get; set; }
        public string poddelivered_date { get; set; }
        public string poddelivered_time { get; set; }
        public string poddelivered_notes { get; set; }
        public string package_weight { get; set; }
    }

    public class OrderHistoryPickTicketDetail
    {
        public string item_id { get; set; }
        public string item_desc { get; set; }
        public string unit_of_measure { get; set; }
        public string unit_quantity { get; set; }
        public List<OrderHistoryPickTicketDetailLot> OrderHistoryPickTicketDetailLot { get; set; }

        public OrderHistoryPickTicketDetail()
        {
            OrderHistoryPickTicketDetailLot = new List<OrderHistoryPickTicketDetailLot>();
        }
    }

    public class OrderHistoryPickTicketDetailLot
    {
        public string lot { get; set; }
        public string expiration_date { get; set; }
        public string unit_of_measure { get; set; }
        public string unit_quantity { get; set; }
    }

}

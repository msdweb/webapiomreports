﻿using System;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Printing;
using System.Data;
using System.Collections;
using System.Collections.Generic;

namespace OMOrders
{
    public class Group
    {
        public List<Section> SectionsHeader { get; set; }
        public int TotalPages { get; set; }
        private DataGridView m_dgvGroup;
        private DataGridView m_dgvEndGroup;
        private DataView m_dvEndGroup;

        public Group(DataGridView pdgvGroup,
                     DataGridView pdgvEndGroup, DataView pdvEndGroup)
        {
            m_dgvGroup = pdgvGroup;
            m_dgvEndGroup = pdgvEndGroup;
            m_dvEndGroup = pdvEndGroup;
            SectionsHeader = new List<Section>();
        }

        public DataGridView dgvGroup
        {
            get { return m_dgvGroup; }
        }

        public DataGridView dgvEndGroup
        {
            get { return m_dgvEndGroup; }
        }

        public DataView dvEndGroup
        {
            get { return m_dvEndGroup; }
        }
    }

    public class Section
    {
        private int m_iGroup;
        private Boolean m_bBreakLine;
        private DataGridView m_dgvHeaderSection;
        private DataGridView m_dgvSection;
        private DataView m_dvSection;

        public Section(int piGroup, Boolean pbBreakLine,
                       DataGridView pdgvHeaderSection,
                       DataGridView pdgvSection,
                       DataView pdvSection)
        {
            m_iGroup = piGroup;
            m_bBreakLine = pbBreakLine;
            m_dgvHeaderSection = pdgvHeaderSection;
            m_dgvSection = pdgvSection;
            m_dvSection = pdvSection;
        }

        public int iGroup
        {
            get { return m_iGroup; }
        }

        public Boolean bBreakLine
        {
            get { return m_bBreakLine; }
        }

        public DataGridView dgvHeaderSection
        {
            get { return m_dgvHeaderSection; }
        }

        public DataGridView dgvSection
        {
            get { return m_dgvSection; }
        }

        public DataView dvSection
        {
            get { return m_dvSection; }
        }
    }

    public class Printer
    {
        public PrintDocument ThePrintDocument;
        public bool PrintLogo = true;
        public bool PrintLogoCenter = true;
        private int SectionIndex = 0;
        private int RowCount = 0;  // current count of rows;
        private int SubRowCount = 0;  // current count of rows;
        public int PageNumber = 1;
        public int TotalPageNumber = 1;
        private int TotalPages = 1;
        private bool CalcTotalPages = true;
        private bool bEndGroup = false;
        private bool bEndGroupPage = false;

        private ArrayList Lines = new ArrayList();
        //
        private int PageWidth;
        private int PageHeight;
        private int TopMargin;
        private int BottomMargin;
        private int LeftMargin;
        private int RightMargin;
        private int linewidth;
        private int TableY;
        private int CurrentY;
        private int CurrentY_BreakLine;
        //
        //----------------------------------------------------------------------------
        //
        private List<Group> Groups = new List<Group>();
        private List<Section> Sections = new List<Section>();

        private DataGridView dgvPageHeader;
        private DataGridView dgvEmptyMessage;
        private DataGridView dgvEndReportMsg;
        private DataView dvPageHeader;
        private Label lblFooter;

        public Printer(PrintDocument pPrintDocument,
                       DataGridView pdgvPageHeader,
                       DataGridView pdgvEmptyMessage,
                       DataGridView pdgvEndReportMsg,
                       DataView pdvPageHeader,
                       Label plblFooter)
        {
            //
            // TODO: Add constructor logic here
            //

            ThePrintDocument = pPrintDocument;

            lblFooter = plblFooter;
            dgvPageHeader = pdgvPageHeader;
            dgvEmptyMessage = pdgvEmptyMessage;
            dgvEndReportMsg = pdgvEndReportMsg;
            dvPageHeader = pdvPageHeader;

            if (ThePrintDocument.DefaultPageSettings.Landscape == false)
            {
                PageWidth = ThePrintDocument.DefaultPageSettings.PaperSize.Width;
                PageHeight = ThePrintDocument.DefaultPageSettings.PaperSize.Height - 50;
                TopMargin = ThePrintDocument.DefaultPageSettings.Margins.Top;
                LeftMargin = ThePrintDocument.DefaultPageSettings.Margins.Left;
                BottomMargin = ThePrintDocument.DefaultPageSettings.Margins.Bottom;
                RightMargin = ThePrintDocument.DefaultPageSettings.Margins.Right;
            }
            else
            {
                PageWidth = ThePrintDocument.DefaultPageSettings.PaperSize.Height;
                PageHeight = ThePrintDocument.DefaultPageSettings.PaperSize.Width - 50;
                TopMargin = ThePrintDocument.DefaultPageSettings.Margins.Left;
                LeftMargin = ThePrintDocument.DefaultPageSettings.Margins.Top;
                BottomMargin = ThePrintDocument.DefaultPageSettings.Margins.Right;
                RightMargin = ThePrintDocument.DefaultPageSettings.Margins.Bottom;
            }
        }

        //-----------------------------------------------------------------------------------------
        public void AddGroup(DataGridView pdgvGroup,
                             DataGridView pdgvEndGroup,
                               DataView pdvEndGroup)
        {
            Groups.Add(new Group(pdgvGroup,
                                 pdgvEndGroup,
                                 pdvEndGroup));
            return;
        }

        //-----------------------------------------------------------------------------------------
        public void AddSectionHeader(int piGroup, Boolean pbBreakLine,
                                     DataGridView pdgvHeaderSection,
                                     DataGridView pdgvSection,
                                     DataView pdvSection)
        {
            Groups[piGroup].SectionsHeader.Add(new Section(piGroup,pbBreakLine,
                                                             pdgvHeaderSection,
                                                             pdgvSection,
                                                             pdvSection));
            return;
        }

        //-----------------------------------------------------------------------------------------
        public void AddSection(int piGroup, Boolean pbBreakLine,
                               DataGridView pdgvHeaderSection,
                               DataGridView pdgvSection,
                               DataView pdvSection)
        {
            Sections.Add(new Section(piGroup, pbBreakLine,
                                     pdgvHeaderSection,
                                     pdgvSection,
                                     pdvSection));
            return;
        }

        //-----------------------------------------------------------------------------------------
        public bool DrawDataGrid(Graphics g)
        {
            try
            {
                if (CalcTotalPages == true)
                {
                    bool more = true;

                    while (more == true)
                    {
                        more = CalcDrawDataGrid(g);
                        if (more == true)
                        {
                            TotalPages++;
                        }
                        else
                        {
                            CalcTotalPages = false;
                            Reset_Flags();
                        }
                    }
                }
                bool bContinue = true;
                bool bBreakPage = false;
                Lines.Clear();

                CurrentY = TopMargin - 80;
                if (PrintLogo == true) { DrawPageHeaderLogo(g); }

                //---------------------------------------------------------------------------------------

                DrawFooter(g, lblFooter, dgvPageHeader);
                if (bEndGroup == false)
                {
                    bBreakPage = DrawRows(g, null, dgvPageHeader, dvPageHeader, dgvEmptyMessage, true);
                    //
                    //
                    DrawTableHeader(g, Groups[Sections[SectionIndex].iGroup - 1].dgvGroup, false);

                    CurrentY = CurrentY + 10;

                    int SectionRowCount = RowCount;

                    RowCount = 0;
                    
                    //A1
                    bBreakPage = DrawRows(g,
                                          Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[0].dgvHeaderSection,
                                          Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[0].dgvSection,
                                          Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[0].dvSection,
                                          dgvEmptyMessage,
                                          false);

                    CurrentY = CurrentY - 110;

                    RowCount = 0;

                    //B1
                    bBreakPage = DrawRows(g,
                                          Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[1].dgvHeaderSection,
                                          Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[1].dgvSection,
                                          Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[1].dvSection,
                                          dgvEmptyMessage,
                                          false);

                    //CurrentY = CurrentY - 100;

                    RowCount = 0;

                    //C1
                    bBreakPage = DrawRows(g,
                                          Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[2].dgvHeaderSection,
                                          Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[2].dgvSection,
                                          Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[2].dvSection,
                                          dgvEmptyMessage,
                                          false);


                    RowCount = 0;

                    //D1
                    bBreakPage = DrawRows(g,
                                          Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[3].dgvHeaderSection,
                                          Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[3].dgvSection,
                                          Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[3].dvSection,
                                          dgvEmptyMessage,
                                          false);

                    //CurrentY = CurrentY - 8;
                   
                    //RowCount = 0;

                    ////E1
                    //bBreakPage = DrawRows(g,
                    //                      Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[4].dgvHeaderSection,
                    //                      Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[4].dgvSection,
                    //                      Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[4].dvSection,
                    //                      dgvEmptyMessage,
                    //                      false);

                    //RowCount = 0;

                    //bBreakPage = DrawRows(g,
                    //                      Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[5].dgvHeaderSection,
                    //                      Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[5].dgvSection,
                    //                      Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[5].dvSection,
                    //                      dgvEmptyMessage,
                    //                      false);

                    //CurrentY = CurrentY + 22;

                    //RowCount = 0;

                    //bBreakPage = DrawRows(g,
                    //                      Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[6].dgvHeaderSection,
                    //                      Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[6].dgvSection,
                    //                      Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[6].dvSection,
                    //                      dgvEmptyMessage,
                    //                      false);

                    //RowCount = 0;

                    //bBreakPage = DrawRows(g,
                    //                      Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[7].dgvHeaderSection,
                    //                      Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[7].dgvSection,
                    //                      Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[7].dvSection,
                    //                      dgvEmptyMessage,
                    //                      false);

                    //RowCount = 0;

                    //bBreakPage = DrawRows(g,
                    //                      Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[8].dgvHeaderSection,
                    //                      Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[8].dgvSection,
                    //                      Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[8].dvSection,
                    //                      dgvEmptyMessage,
                    //                      false);

                    //RowCount = 0;

                    //bBreakPage = DrawRows(g,
                    //                      Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[9].dgvHeaderSection,
                    //                      Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[9].dgvSection,
                    //                      Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[9].dvSection,
                    //                      dgvEmptyMessage,
                    //                      false);                    


                    CurrentY = CurrentY + 10;

                    RowCount = SectionRowCount;
                }
                else
                {
                    CurrentY = CurrentY + 50;
                }

                //---------------------------------------------------------------------------------------

                while (bBreakPage == false)
                {
                    if (bEndGroup == false)
                    {
                        CurrentY_BreakLine = CurrentY;
                        bBreakPage = DrawRows(g,
                                              Sections[SectionIndex].dgvHeaderSection,
                                              Sections[SectionIndex].dgvSection,
                                              Sections[SectionIndex].dvSection,
                                              dgvEmptyMessage,
                                              false);
                        if (Sections[SectionIndex].bBreakLine == true) { CurrentY = CurrentY_BreakLine; }
                    }
                    if (bBreakPage == false)
                    {
                        if (SectionIndex < Sections.Count - 1)
                        {
                            if (Groups[Sections[SectionIndex].iGroup - 1] != Groups[Sections[(SectionIndex + 1)].iGroup - 1])
                            {
                                if (Groups[Sections[SectionIndex].iGroup - 1].dgvEndGroup != null)
                                {
                                    // DrawTableHeader(g,
                                    //                 Groups[Sections[SectionIndex].iGroup - 1].dgvEndGroupMsg,
                                    //                 false);
                                    if (bEndGroup == false)
                                    {
                                        bEndGroup = true;
                                        bEndGroupPage = true;
                                        bBreakPage = true;
                                        RowCount = 0;
                                        break;
                                    }

                                    if (bEndGroupPage == true)
                                    {
                                        bBreakPage = DrawRows(g,
                                                              null,
                                                              Groups[Sections[SectionIndex].iGroup - 1].dgvEndGroup,
                                                              Groups[Sections[SectionIndex].iGroup - 1].dvEndGroup,
                                                              dgvEmptyMessage,
                                                              false);
                                        bEndGroupPage = false;
                                        if (IsOdd(PageNumber))
                                        {
                                            bBreakPage = true;
                                            RowCount = 0;
                                            break;
                                        }
                                    }

                                }
                                if (bBreakPage == false)
                                {
                                    bEndGroup = false;
                                    bBreakPage = true;
                                    PageNumber = 0;
                                    bContinue = true;
                                    SectionIndex++;
                                    RowCount = 0;
                                    SubRowCount = 0;
                                    Lines.Clear();
                                }
                            }
                            else
                            {
                                bContinue = true;
                                SectionIndex++;
                                RowCount = 0;
                                SubRowCount = 0;
                                Lines.Clear();
                            }
                        }
                        else
                        {
                            if (Groups[Sections[SectionIndex].iGroup - 1].dgvEndGroup != null)
                            {
                                // DrawTableHeader(g,
                                //                 Groups[Sections[SectionIndex].iGroup - 1].dgvEndGroupMsg,
                                //                 false);
                                if (bEndGroup == false)
                                {
                                    bEndGroup = true;
                                    bBreakPage = true;
                                    RowCount = 0;
                                    break;
                                }

                                bBreakPage = DrawRows(g,
                                                      null,
                                                      Groups[Sections[SectionIndex].iGroup - 1].dgvEndGroup,
                                                      Groups[Sections[SectionIndex].iGroup - 1].dvEndGroup,
                                                      dgvEmptyMessage,
                                                      false);
                            }
                            if (bBreakPage == false)
                            {
                                bEndGroup = false;
                                bContinue = false;
                                bBreakPage = true;
                                if (dgvEndReportMsg != null)
                                {
                                    DrawTableHeader(g,
                                                    dgvEndReportMsg,
                                                    false);
                                }
                            }
                        }
                    }
                }
                return bContinue;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
                return false;
            }
        }

        //-----------------------------------------------------------------------------------------

        public void Reset_Flags()
        {
            SectionIndex = 0;
            RowCount = 0;
            SubRowCount = 0;
            PageNumber = 1;
            TotalPageNumber = 1;
        }

        private bool IsOdd(int value)
        {
            return value % 2 != 0;
        }

        //-----------------------------------------------------------------------------------------

        private void DrawPageHeaderLogo(Graphics g)
        {
            int startposition = LeftMargin - 65;
            if (PrintLogoCenter == true) { startposition = (PageWidth - Images.msd_new_logo.Width) / 2; }
            g.DrawImage(Images.msd_new_logo, startposition, CurrentY, 180, 80);
            CurrentY = CurrentY + Images.msd_new_logo.Height;
            CurrentY = CurrentY - 120;
        }

        //-----------------------------------------------------------------------------------------

        void DrawFooter(Graphics g, Label TheLabel, DataGridView TheDataGrid)
        {
            Pen TheLinePen = new Pen(TheDataGrid.GridColor, 1);

            g.DrawLine(TheLinePen,
                       LeftMargin,
                       PageHeight - TheLabel.Height,
                       PageWidth - RightMargin,
                       PageHeight - TheLabel.Height);

            g.FillRectangle(new SolidBrush(TheLabel.BackColor), TheLabel.Location.X, PageHeight - TheLabel.Height + 5, TheLabel.Size.Width, TheLabel.Size.Height);
            //
            string strPage = "Page " + PageNumber + " of " + Groups[Sections[SectionIndex].iGroup - 1].TotalPages;
            string strDateTime = DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();
            //string strDateTime = "batch pg. " + TotalPageNumber;
            int centerpage = (PageWidth - strPage.Length) / 2;


            StringFormat cellformat = new StringFormat();
            cellformat.Trimming = StringTrimming.EllipsisCharacter;
            cellformat.FormatFlags = StringFormatFlags.NoWrap | StringFormatFlags.LineLimit;
            cellformat.Alignment = Get_Alignment(TheDataGrid.ColumnHeadersDefaultCellStyle.Alignment.ToString());

            SizeF sf = new SizeF();
            sf = g.MeasureString(strDateTime, TheLabel.Font, 0, cellformat);
            int rightpage = (PageWidth - RightMargin - (int)sf.Width);
            //           
            string footertext = strDateTime;
            //
            g.DrawString(TheLabel.Text, TheLabel.Font, new SolidBrush(TheLabel.ForeColor), LeftMargin, PageHeight - TheLabel.Height + 5, new StringFormat());
            g.DrawString(strPage, TheLabel.Font, new SolidBrush(TheLabel.ForeColor), centerpage, PageHeight - TheLabel.Height + 5, new StringFormat());
            g.DrawString(strDateTime, TheLabel.Font, new SolidBrush(TheLabel.ForeColor), rightpage, PageHeight - TheLabel.Height + 5, new StringFormat());
        }

        //-----------------------------------------------------------------------------------------

        void DrawTableHeader(Graphics g,
                             DataGridView TheDataGrid,
                             bool IsTableHeader)
        {
            if (TheDataGrid.ColumnHeadersVisible == false) return;
            //
            SolidBrush ForeBrush = new SolidBrush(TheDataGrid.ColumnHeadersDefaultCellStyle.ForeColor);
            SolidBrush BackBrush = new SolidBrush(TheDataGrid.ColumnHeadersDefaultCellStyle.BackColor);
            Pen TheLinePen = new Pen(TheDataGrid.GridColor, 1);
            StringFormat cellformat = new StringFormat();
            cellformat.Trimming = StringTrimming.EllipsisCharacter;
            cellformat.FormatFlags = StringFormatFlags.NoWrap | StringFormatFlags.LineLimit;
            cellformat.Alignment = Get_Alignment(TheDataGrid.ColumnHeadersDefaultCellStyle.Alignment.ToString());
            //--------------------------------------------------------------------------------------------

            linewidth = 0;

            for (int k = 0; k < (TheDataGrid.ColumnCount); k++)
            {
                linewidth = linewidth + TheDataGrid.Columns[k].Width;
            }
            linewidth = linewidth + Convert.ToInt16(TheLinePen.Width * 2);

            //--------------------------------------------------------------------------------------------            

            float startxposition = TheDataGrid.Location.X;

            RectangleF nextcellbounds = new RectangleF(0, 0, 0, 0);

            RectangleF HeaderBounds = new RectangleF(0, 0, 0, 0);

            Font cellFont = TheDataGrid.ColumnHeadersDefaultCellStyle.Font;

            float cellHeight = cellFont.SizeInPoints * 2;

            //            CurrentY = CurrentY + Convert.ToInt16(cellHeight);
            CurrentY = CurrentY + 10;

            TableY = CurrentY;

            HeaderBounds.X = TheDataGrid.Location.X;
            HeaderBounds.Y = CurrentY;
            HeaderBounds.Height = cellHeight;
            HeaderBounds.Width = linewidth;

            ArrayList arrLines = new ArrayList();
            int Counter = 0;
            for (int k = 0; k < TheDataGrid.ColumnCount; k++)
            {
                string nextcolumn = TheDataGrid.Columns[k].HeaderText;
                string[] txtLines = (BreakItemText(g, nextcolumn, TheDataGrid.Columns[k].Width, cellFont, cellformat)).Split((char)13);
                arrLines.Add(txtLines);
                if ((txtLines.GetUpperBound(0) + 1) > Counter)
                {
                    Counter = (txtLines.GetUpperBound(0) + 1);
                    HeaderBounds.Height = (cellHeight * Counter);
                    if (Counter > 1) HeaderBounds.Height = (cellHeight * Counter) - Convert.ToInt16(cellHeight / 3);
                    g.FillRectangle(BackBrush, HeaderBounds);
                }
            }

            int kk = 0;
            foreach (string[] txtLines in arrLines)
            {
                for (int ii = 0; ii <= (txtLines.GetUpperBound(0)); ii++)
                {
                    RectangleF cellbounds = new RectangleF(startxposition,
                                                           CurrentY,
                                                           TheDataGrid.Columns[kk].Width,
                                                           HeaderBounds.Height);
                    nextcellbounds = cellbounds;
                    g.DrawString(txtLines[ii], cellFont, ForeBrush, cellbounds, cellformat);
                    CurrentY = CurrentY + Convert.ToInt16(cellHeight - (cellHeight / 4));
                }
                startxposition = startxposition + TheDataGrid.Columns[kk].Width;
                CurrentY = Convert.ToInt16(HeaderBounds.Top);
                kk++;
            }
            if (TheDataGrid.BorderStyle != BorderStyle.None)
            {
                int bottom = Convert.ToInt16(HeaderBounds.Bottom);
                g.DrawLine(TheLinePen, TheDataGrid.Location.X, HeaderBounds.Top, TheDataGrid.Location.X + linewidth, HeaderBounds.Top);
                g.DrawLine(TheLinePen, TheDataGrid.Location.X, HeaderBounds.Bottom, TheDataGrid.Location.X + linewidth, HeaderBounds.Bottom);
                DrawVerticalGridLines(g, TheLinePen, TheDataGrid, bottom);
            }

            if (IsTableHeader == true)
            {
                CurrentY = Convert.ToInt16(HeaderBounds.Bottom);
            }
            else
            {
                CurrentY = Convert.ToInt16(HeaderBounds.Bottom);
                CurrentY = CurrentY - 10;
                // CurrentY = CurrentY + Convert.ToInt16(cellHeight - (cellHeight / 2));
            }
        }

        //-----------------------------------------------------------------------------------------

        bool DrawRows(Graphics g,
                      DataGridView TheHeaderSection,
                      DataGridView TheDataGrid,
                      DataView TheView,
                      DataGridView TheEmptyMessage,
                      bool IsHeader)
        {
            int lastRowBottom = TopMargin;

            try
            {
                SolidBrush ForeBrush = new SolidBrush(TheDataGrid.DefaultCellStyle.ForeColor);
                SolidBrush BackBrush = new SolidBrush(TheDataGrid.DefaultCellStyle.BackColor);
                SolidBrush AlternatingBackBrush = new SolidBrush(TheDataGrid.AlternatingRowsDefaultCellStyle.BackColor);
                Pen TheLinePen = new Pen(TheDataGrid.GridColor, 1); //TheDataGrid.GridLineColor
                StringFormat cellformat = new StringFormat();
                cellformat.Trimming = StringTrimming.EllipsisCharacter;
                cellformat.FormatFlags = StringFormatFlags.NoWrap | StringFormatFlags.LineLimit;
                cellformat.Alignment = StringAlignment.Center;
                cellformat.LineAlignment = StringAlignment.Center;
                //--------------------------------------------------------------------------------------------
                linewidth = 0;

                for (int k = 0; k < (TheDataGrid.ColumnCount); k++)
                {
                    linewidth = linewidth + TheDataGrid.Columns[k].Width;
                }
                linewidth = linewidth + Convert.ToInt16(TheLinePen.Width * 2);

                //--------------------------------------------------------------------------------------------            

                int initialRowCount = RowCount;
                if (IsHeader == true) initialRowCount = 0;

                RectangleF nextcellbounds = new RectangleF(0, 0, 0, 0);

                RectangleF RowBounds = new RectangleF(0, 0, 0, 0);

                Font cellFont = TheDataGrid.DefaultCellStyle.Font;
                float cellHeight = cellFont.SizeInPoints * 2;

                //####################################################################################                    

                if ((CurrentY + (cellHeight * 2)) > (PageHeight - (lblFooter.Height * 4)))
                {
                    return true;
                }

                //####################################################################################

                if (TheHeaderSection != null) DrawTableHeader(g, TheHeaderSection, false);

                if (TheDataGrid.ColumnHeadersVisible == true)
                {
                    DrawTableHeader(g, TheDataGrid, true);
                }
                else
                {
                    RowBounds.X = TheDataGrid.Location.X;
                    RowBounds.Y = CurrentY;
                    RowBounds.Height = cellHeight;
                    RowBounds.Width = linewidth;
                    CurrentY = Convert.ToInt16(RowBounds.Bottom);
                    TableY = CurrentY;
                }
                lastRowBottom = CurrentY;

                //####################################################################################                    

                if (TheView.Count == 0)
                {
                    DrawTableHeader(g, TheEmptyMessage, false);
                }
                else
                {
                    for (int i = initialRowCount; i < TheView.Count; i++)
                    {
                        DataRowView dr = TheView[i];
                        int startxposition = TheDataGrid.Location.X;

                        RowBounds.X = TheDataGrid.Location.X;
                        RowBounds.Y = CurrentY;
                        RowBounds.Height = cellHeight;
                        RowBounds.Width = linewidth;
                        Lines.Add(RowBounds.Bottom);

                        ArrayList arrLines = new ArrayList();
                        int Counter = 0;
                        for (int k = 0; k < TheDataGrid.ColumnCount; k++)
                        {
                            string nextcolumn = dr[k + 1].ToString();
                            nextcolumn = nextcolumn.Replace("\n", "");
                            string[] txtLines = (BreakItemText(g, nextcolumn, TheDataGrid.Columns[k].Width, cellFont, cellformat)).Split((char)13);
                            arrLines.Add(txtLines);
                            if ((txtLines.GetUpperBound(0) + 1) > Counter)
                            {
                                Counter = (txtLines.GetUpperBound(0) + 1);
                                int iniSubRowCount = 0;
                                if (IsHeader == false) iniSubRowCount = SubRowCount;
                                int ll = 0;
                                for (int cc = iniSubRowCount; cc <= (txtLines.GetUpperBound(0)); cc++)
                                {
                                    Counter = ++ll;
                                    if (CurrentY + (Convert.ToInt16(cellHeight - (cellHeight / 4)) * Counter) > (PageHeight - (lblFooter.Height * 4)))
                                    {
                                        if (TheDataGrid.ColumnCount > 1)
                                        {
                                            if (i > 0)
                                            {
                                                if (TheDataGrid.BorderStyle != BorderStyle.None)
                                                {
                                                    g.DrawLine(TheLinePen, TheDataGrid.Location.X, RowBounds.Top, TheDataGrid.Location.X + linewidth, RowBounds.Top);
                                                    DrawVerticalGridLines(g, TheLinePen, TheDataGrid, Convert.ToInt16(RowBounds.Top));
                                                }
                                            }
                                            return true;
                                        }
                                        else
                                        {
                                            break;
                                        }
                                    }
                                }

                                RowBounds.Height = (cellHeight * Counter);
                                if (Counter > 1) RowBounds.Height = (Convert.ToInt16(cellHeight - (cellHeight / 4)) * Counter) + Convert.ToInt16(cellHeight / 3);
                                //   if (Convert.ToInt16(dr[0].ToString()) == 1) 
                                if (dr[0].ToString() == "1")
                                {
                                    g.FillRectangle(AlternatingBackBrush, RowBounds);
                                }
                                else
                                {
                                    g.FillRectangle(BackBrush, RowBounds);
                                }

                                //if (i % 2 == 0)
                                //{
                                //    g.FillRectangle(BackBrush, RowBounds);
                                //}
                                //else
                                //{
                                //    g.FillRectangle(AlternatingBackBrush, RowBounds);
                                //}
                            }
                        }

                        //####################################################################################                    

                        int kk = 0;
                        foreach (string[] txtLines in arrLines)
                        {
                            cellformat.Alignment = Get_Alignment(TheDataGrid.Columns[kk].DefaultCellStyle.Alignment.ToString());
                            //---------------------------------------------------------------------------------
                            int initialSubRowCount = 0;
                            if (IsHeader == false) initialSubRowCount = SubRowCount;
                            for (int ii = initialSubRowCount; ii <= (txtLines.GetUpperBound(0)); ii++)
                            {
                                RectangleF cellbounds = new RectangleF(startxposition,
                                                                       CurrentY,
                                                                       TheDataGrid.Columns[kk].Width,
                                                                       cellHeight);
                                nextcellbounds = cellbounds;
                                g.DrawString(txtLines[ii], cellFont, ForeBrush, cellbounds, cellformat);
                                CurrentY = CurrentY + Convert.ToInt16(cellHeight - (cellHeight / 4));
                                if (IsHeader == false) SubRowCount++;
                                if (SubRowCount < txtLines.GetUpperBound(0))
                                {
                                    if (CurrentY > (PageHeight - (lblFooter.Height * 4)))
                                    {
                                        if (TheDataGrid.BorderStyle != BorderStyle.None)
                                        {
                                            g.DrawLine(TheLinePen, TheDataGrid.Location.X, RowBounds.Top, TheDataGrid.Location.X + linewidth, RowBounds.Top);
                                            g.DrawLine(TheLinePen, TheDataGrid.Location.X, RowBounds.Bottom, TheDataGrid.Location.X + linewidth, RowBounds.Bottom);
                                            lastRowBottom = Convert.ToInt16(RowBounds.Bottom);
                                            DrawVerticalGridLines(g, TheLinePen, TheDataGrid, lastRowBottom);
                                        }
                                        return true;
                                    }
                                }
                            }
                            if (IsHeader == false) SubRowCount = 0;
                            startxposition = startxposition + TheDataGrid.Columns[kk].Width;
                            CurrentY = Convert.ToInt16(RowBounds.Top);
                            kk++;
                        }

                        //
                        //####################################################################################                    
                        //

                        lastRowBottom = Convert.ToInt16(RowBounds.Bottom);
                        if (TheDataGrid.BorderStyle != BorderStyle.None)
                        {
                            g.DrawLine(TheLinePen, TheDataGrid.Location.X, RowBounds.Top, TheDataGrid.Location.X + linewidth, RowBounds.Top);
                        }
                        CurrentY = Convert.ToInt16(RowBounds.Bottom);

                        if (IsHeader == false) RowCount++;

                        if (CurrentY > (PageHeight - (lblFooter.Height * 4)))
                        {
                            if (TheDataGrid.BorderStyle != BorderStyle.None)
                            {
                                g.DrawLine(TheLinePen, TheDataGrid.Location.X, RowBounds.Bottom, TheDataGrid.Location.X + linewidth, RowBounds.Bottom);
                                DrawVerticalGridLines(g, TheLinePen, TheDataGrid, lastRowBottom);
                            }
                            if ((i + 1) < TheView.Count)
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                    }
                    if (TheDataGrid.BorderStyle != BorderStyle.None)
                    {
                        g.DrawLine(TheLinePen, TheDataGrid.Location.X, RowBounds.Bottom, TheDataGrid.Location.X + linewidth, RowBounds.Bottom);
                    }
                }
                if (TheDataGrid.BorderStyle != BorderStyle.None)
                {
                    DrawVerticalGridLines(g, TheLinePen, TheDataGrid, lastRowBottom);
                }
                return false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
                return false;
            }

        }

        //-----------------------------------------------------------------------------------------


        //void DrawHorizontalLines(Graphics g, ArrayList lines, int linewidth)
        //{
        //    Pen TheLinePen = new Pen(TheDataGrid.GridColor, 1); // TheDataGrid.GridLineColor
        //
        //  //  if (TheDataGrid.GridLineStyle == DataGridLineStyle.None)
        //  //      return;
        //
        //    for (int i = 0; i < Lines.Count; i++)
        //    {
        //        g.DrawLine(TheLinePen, 
        //                   TheDataGrid.Location.X, 
        //                   (float)lines[i],
        //                   TheDataGrid.Location.X + linewidth, 
        //                   (float)lines[i]);
        //    }
        //}
        //-----------------------------------------------------------------------------------------

        void DrawVerticalGridLines(Graphics g,
                                   Pen TheLinePen,
                                   DataGridView TheDataGrid,
                                   int bottom)
        {
            int colwidth = 0;
            for (int k = 0; k < (TheDataGrid.ColumnCount); k++)
            {
                g.DrawLine(TheLinePen,
                           TheDataGrid.Location.X + colwidth,
                           TableY,
                           TheDataGrid.Location.X + colwidth,
                           bottom);
                colwidth = colwidth + TheDataGrid.Columns[k].Width;
            }
            colwidth = colwidth + Convert.ToInt16(TheLinePen.Width * 2);
            g.DrawLine(TheLinePen,
            TheDataGrid.Location.X + colwidth,
            TableY,
            TheDataGrid.Location.X + colwidth,
            bottom);
        }

        //-----------------------------------------------------------------------------------------

        private StringAlignment Get_Alignment(string Alignment)
        {
            StringFormat cellformat = new StringFormat();
            if ((Alignment == "TopLeft") |
               (Alignment == "MiddleLeft") |
               (Alignment == "BottomLeft"))
            {
                cellformat.Alignment = StringAlignment.Near;
            }

            if ((Alignment == "TopCenter") |
               (Alignment == "MiddleCenter") |
               (Alignment == "BottomCenter"))
            {
                cellformat.Alignment = StringAlignment.Center;
            }

            if ((Alignment == "TopRight") |
               (Alignment == "MiddleRight") |
               (Alignment == "BottomRight"))
            {
                cellformat.Alignment = StringAlignment.Far;
            }
            return cellformat.Alignment;
        }

        //-----------------------------------------------------------------------------------------

        private string BreakItemText(Graphics g, string Item, float width, Font ItemFont, StringFormat cellformat)
        {

            int iChar = 1;
            string newitem = "";
            string currentpiece = "";
            bool moreWords = true;
            string nextWord;
            try
            {
                SizeF sf = new SizeF();
                sf = g.MeasureString(Item, ItemFont, 0, cellformat);
                if (sf.Width < width)
                {
                    return Item;
                }
                else
                {

                    while (moreWords == true)
                    {
                        nextWord = Get_NextWord(Item, (iChar));
                        iChar = iChar + nextWord.Length;
                        sf = g.MeasureString(currentpiece + nextWord, ItemFont, 0, cellformat);
                        if (sf.Width < width)
                        {
                            currentpiece += nextWord;
                            if (nextWord.Substring((nextWord.Length - 1), 1) == "\r")
                            {
                                if (newitem != "")
                                {
                                    if (newitem.Substring((newitem.Length - 1), 1) != "\r")
                                    {
                                        newitem += (char)13;
                                    }
                                }
                                newitem += currentpiece;
                                currentpiece = "";
                            }
                        }
                        else
                        {
                            if (newitem != "")
                            {
                                if (newitem.Substring((newitem.Length - 1), 1) != "\r")
                                {
                                    newitem += (char)13;
                                }
                            }
                            newitem += currentpiece;
                            currentpiece = nextWord;
                        }
                        if (iChar >= Item.Length)
                        {
                            moreWords = false;
                        }
                    }
                    if (currentpiece != "")
                    {
                        if (newitem != "")
                        {
                            if (newitem.Substring((newitem.Length - 1), 1) != "\r")
                            {
                                newitem += (char)13;
                            }
                        }
                        newitem += currentpiece;
                        currentpiece = "";
                    }
                    return newitem;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
                return newitem;
            }
        }

        //-----------------------------------------------------------------------------------------

        private string Get_NextWord(string Str, int pos)
        {
            string nextWord = "";
            try
            {
                pos--;
                do
                {
                    if (Str.Substring((pos), 1) == "\r") break;
                    if (Str.Substring((pos), 1) == " ") break;
                    nextWord += Str.Substring((pos), 1);
                    pos++;
                } while (pos <= (Str.Length - 1));

                do
                {
                    if (pos >= (Str.Length - 1)) break;
                    nextWord += Str.Substring((pos), 1);
                    pos++;
                } while (Str.Substring((pos), 1) == " ");

                return nextWord;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
                return nextWord;
            }
        }

        //#########################################################################################
        // Calculate Total Pages
        //#########################################################################################

        bool CalcDrawDataGrid(Graphics g)
        {
            try
            {
                bool bContinue = true;
                bool bBreakPage = false;
                Lines.Clear();

                CurrentY = TopMargin - 80;
                if (PrintLogo == true) { CalcDrawPageHeaderLogo(); }

                //---------------------------------------------------------------------------------------

                if (bEndGroup == false)
                {
                    bBreakPage = CalcDrawRows(g, null, dgvPageHeader, dvPageHeader, dgvEmptyMessage, true);
                    //
                    CalcDrawTableHeader(g, Groups[Sections[SectionIndex].iGroup - 1].dgvGroup, false);

                    CurrentY = CurrentY + 10;

                    int SectionRowCount = RowCount;

                    RowCount = 0;

                    //A1
                    bBreakPage = CalcDrawRows(g,
                                          Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[0].dgvHeaderSection,
                                          Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[0].dgvSection,
                                          Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[0].dvSection,
                                          dgvEmptyMessage,
                                          false);

                    CurrentY = CurrentY - 110;

                    RowCount = 0;

                    //B1
                    bBreakPage = CalcDrawRows(g,
                                          Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[1].dgvHeaderSection,
                                          Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[1].dgvSection,
                                          Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[1].dvSection,
                                          dgvEmptyMessage,
                                          false);

                    //CurrentY = CurrentY - 100;

                    RowCount = 0;

                    //C1
                    bBreakPage = CalcDrawRows(g,
                                          Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[2].dgvHeaderSection,
                                          Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[2].dgvSection,
                                          Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[2].dvSection,
                                          dgvEmptyMessage,
                                          false);


                    RowCount = 0;

                    //D1
                    bBreakPage = CalcDrawRows(g,
                                          Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[3].dgvHeaderSection,
                                          Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[3].dgvSection,
                                          Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[3].dvSection,
                                          dgvEmptyMessage,
                                          false);

                    //CurrentY = CurrentY - 8;

                    //RowCount = 0;

                    ////E1
                    //bBreakPage = CalcDrawRows(g,
                    //                      Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[4].dgvHeaderSection,
                    //                      Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[4].dgvSection,
                    //                      Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[4].dvSection,
                    //                      dgvEmptyMessage,
                    //                      false);


                    //RowCount = 0;

                    //bBreakPage = CalcDrawRows(g,
                    //                      Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[5].dgvHeaderSection,
                    //                      Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[5].dgvSection,
                    //                      Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[5].dvSection,
                    //                      dgvEmptyMessage,
                    //                      false);

                    //CurrentY = CurrentY + 22;

                    //RowCount = 0;

                    //bBreakPage = CalcDrawRows(g,
                    //                      Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[6].dgvHeaderSection,
                    //                      Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[6].dgvSection,
                    //                      Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[6].dvSection,
                    //                      dgvEmptyMessage,
                    //                      false);

                    //RowCount = 0;

                    //bBreakPage = CalcDrawRows(g,
                    //                      Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[7].dgvHeaderSection,
                    //                      Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[7].dgvSection,
                    //                      Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[7].dvSection,
                    //                      dgvEmptyMessage,
                    //                      false);

                    //RowCount = 0;

                    //bBreakPage = CalcDrawRows(g,
                    //                      Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[8].dgvHeaderSection,
                    //                      Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[8].dgvSection,
                    //                      Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[8].dvSection,
                    //                      dgvEmptyMessage,
                    //                      false);

                    //RowCount = 0;

                    //bBreakPage = CalcDrawRows(g,
                    //                      Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[9].dgvHeaderSection,
                    //                      Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[9].dgvSection,
                    //                      Groups[Sections[SectionIndex].iGroup - 1].SectionsHeader[9].dvSection,
                    //                      dgvEmptyMessage,
                    //                      false);

                    CurrentY = CurrentY + 10;

                    RowCount = SectionRowCount;
                }
                else
                {
                    CurrentY = CurrentY + 50;
                }

                //---------------------------------------------------------------------------------------

                //
                while (bBreakPage == false)
                {
                    if (bEndGroup == false)
                    {
                        CurrentY_BreakLine = CurrentY;
                        bBreakPage = CalcDrawRows(g,
                                                  Sections[SectionIndex].dgvHeaderSection,
                                                  Sections[SectionIndex].dgvSection,
                                                  Sections[SectionIndex].dvSection,
                                                  dgvEmptyMessage,
                                                  false);
                        if (Sections[SectionIndex].bBreakLine == true) { CurrentY = CurrentY_BreakLine; }
                    }
                    if (bBreakPage == false)
                    {
                        if (SectionIndex < Sections.Count - 1)
                        {
                            if (Groups[Sections[SectionIndex].iGroup - 1] != Groups[Sections[(SectionIndex + 1)].iGroup - 1])
                            {
                                if (Groups[Sections[SectionIndex].iGroup - 1].dgvEndGroup != null)
                                {
                                    if (bEndGroup == false)
                                    {
                                        bEndGroup = true;
                                        bBreakPage = true;
                                        RowCount = 0;
                                        break;
                                    }

                                    bBreakPage = CalcDrawRows(g,
                                                          null,
                                                          Groups[Sections[SectionIndex].iGroup - 1].dgvEndGroup,
                                                          Groups[Sections[SectionIndex].iGroup - 1].dvEndGroup,
                                                          dgvEmptyMessage,
                                                          false);
                                    if (IsOdd(TotalPages))
                                    {
                                        TotalPages++;
                                    }
                                }

                                if (bBreakPage == false)
                                {
                                    bEndGroup = false;
                                    bBreakPage = true;
                                    Groups[Sections[SectionIndex].iGroup - 1].TotalPages = TotalPages;
                                    TotalPages = 0;
                                    bContinue = true;
                                    SectionIndex++;
                                    RowCount = 0;
                                    SubRowCount = 0;
                                    Lines.Clear();
                                }
                            }
                            else
                            {
                                bContinue = true;
                                SectionIndex++;
                                RowCount = 0;
                                SubRowCount = 0;
                                Lines.Clear();
                            }
                        }
                        else
                        {
                            if (Groups[Sections[SectionIndex].iGroup - 1].dgvEndGroup != null)
                            {
                                if (bEndGroup == false)
                                {
                                    bEndGroup = true;
                                    bBreakPage = true;
                                    RowCount = 0;
                                    break;
                                }

                                bBreakPage = CalcDrawRows(g,
                                                          null,
                                                          Groups[Sections[SectionIndex].iGroup - 1].dgvEndGroup,
                                                          Groups[Sections[SectionIndex].iGroup - 1].dvEndGroup,
                                                          dgvEmptyMessage,
                                                          false);
                            }
                            if (bBreakPage == false)
                            {
                                Groups[Sections[SectionIndex].iGroup - 1].TotalPages = TotalPages;
                                TotalPages = 0;
                                bEndGroup = false;
                                bContinue = false;
                                bBreakPage = true;
                                if (dgvEndReportMsg != null)
                                {
                                    CalcDrawTableHeader(g,
                                                        dgvEndReportMsg,
                                                        false);
                                }
                            }
                        }
                    }
                }
                return bContinue;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
                return false;
            }
        }

        //-----------------------------------------------------------------------------------------

        void CalcDrawPageHeaderLogo()
        {
            int centerpage = (PageWidth - Images.msd_new_logo.Width) / 2;
            CurrentY = CurrentY + Images.msd_new_logo.Height;
            CurrentY = CurrentY - 120;
        }

        //-----------------------------------------------------------------------------------------

        void CalcDrawTableHeader(Graphics g,
                                 DataGridView TheDataGrid,
                                 bool IsTableHeader)
        {
            if (TheDataGrid.ColumnHeadersVisible == false) return;
            //
            SolidBrush ForeBrush = new SolidBrush(TheDataGrid.ColumnHeadersDefaultCellStyle.ForeColor);
            SolidBrush BackBrush = new SolidBrush(TheDataGrid.ColumnHeadersDefaultCellStyle.BackColor);
            Pen TheLinePen = new Pen(TheDataGrid.GridColor, 1);
            StringFormat cellformat = new StringFormat();
            cellformat.Trimming = StringTrimming.EllipsisCharacter;
            cellformat.FormatFlags = StringFormatFlags.NoWrap | StringFormatFlags.LineLimit;
            cellformat.Alignment = Get_Alignment(TheDataGrid.ColumnHeadersDefaultCellStyle.Alignment.ToString());
            //--------------------------------------------------------------------------------------------

            linewidth = 0;

            for (int k = 0; k < (TheDataGrid.ColumnCount); k++)
            {
                linewidth = linewidth + TheDataGrid.Columns[k].Width;
            }
            linewidth = linewidth + Convert.ToInt16(TheLinePen.Width * 2);

            //--------------------------------------------------------------------------------------------            

            float startxposition = TheDataGrid.Location.X;

            RectangleF nextcellbounds = new RectangleF(0, 0, 0, 0);

            RectangleF HeaderBounds = new RectangleF(0, 0, 0, 0);

            Font cellFont = TheDataGrid.ColumnHeadersDefaultCellStyle.Font;

            float cellHeight = cellFont.SizeInPoints * 2;

            CurrentY = CurrentY + 10;

            TableY = CurrentY;

            HeaderBounds.X = TheDataGrid.Location.X;
            HeaderBounds.Y = CurrentY;
            HeaderBounds.Height = cellHeight;
            HeaderBounds.Width = linewidth;

            ArrayList arrLines = new ArrayList();
            int Counter = 0;
            for (int k = 0; k < TheDataGrid.ColumnCount; k++)
            {
                string nextcolumn = TheDataGrid.Columns[k].HeaderText;
                string[] txtLines = (BreakItemText(g, nextcolumn, TheDataGrid.Columns[k].Width, cellFont, cellformat)).Split((char)13);
                arrLines.Add(txtLines);
                if ((txtLines.GetUpperBound(0) + 1) > Counter)
                {
                    Counter = (txtLines.GetUpperBound(0) + 1);
                    HeaderBounds.Height = (cellHeight * Counter);
                    if (Counter > 1) HeaderBounds.Height = (cellHeight * Counter) - Convert.ToInt16(cellHeight / 3);
                }
            }

            int kk = 0;
            foreach (string[] txtLines in arrLines)
            {
                for (int ii = 0; ii <= (txtLines.GetUpperBound(0)); ii++)
                {
                    RectangleF cellbounds = new RectangleF(startxposition,
                                                           CurrentY,
                                                           TheDataGrid.Columns[kk].Width,
                                                           HeaderBounds.Height);
                    nextcellbounds = cellbounds;
                    CurrentY = CurrentY + Convert.ToInt16(cellHeight - (cellHeight / 4));
                }
                startxposition = startxposition + TheDataGrid.Columns[kk].Width;
                CurrentY = Convert.ToInt16(HeaderBounds.Top);
                kk++;
            }
            if (IsTableHeader == true)
            {
                CurrentY = Convert.ToInt16(HeaderBounds.Bottom);
            }
            else
            {
                CurrentY = Convert.ToInt16(HeaderBounds.Bottom);
                CurrentY = CurrentY - 10;
            }
        }

        //-----------------------------------------------------------------------------------------

        bool CalcDrawRows(Graphics g, DataGridView TheHeaderSection,
                          DataGridView TheDataGrid,
                          DataView TheView,
                          DataGridView TheEmptyMessage,
                          bool IsHeader)
        {
            int lastRowBottom = TopMargin;

            try
            {
                SolidBrush ForeBrush = new SolidBrush(TheDataGrid.DefaultCellStyle.ForeColor);
                SolidBrush BackBrush = new SolidBrush(TheDataGrid.DefaultCellStyle.BackColor);
                SolidBrush AlternatingBackBrush = new SolidBrush(TheDataGrid.AlternatingRowsDefaultCellStyle.BackColor);
                Pen TheLinePen = new Pen(TheDataGrid.GridColor, 1); //TheDataGrid.GridLineColor
                StringFormat cellformat = new StringFormat();
                cellformat.Trimming = StringTrimming.EllipsisCharacter;
                cellformat.FormatFlags = StringFormatFlags.NoWrap | StringFormatFlags.LineLimit;
                cellformat.Alignment = StringAlignment.Center;
                cellformat.LineAlignment = StringAlignment.Center;
                //--------------------------------------------------------------------------------------------
                linewidth = 0;

                for (int k = 0; k < (TheDataGrid.ColumnCount); k++)
                {
                    linewidth = linewidth + TheDataGrid.Columns[k].Width;
                }
                linewidth = linewidth + Convert.ToInt16(TheLinePen.Width * 2);

                //--------------------------------------------------------------------------------------------            

                int initialRowCount = RowCount;
                if (IsHeader == true) initialRowCount = 0;

                RectangleF nextcellbounds = new RectangleF(0, 0, 0, 0);

                RectangleF RowBounds = new RectangleF(0, 0, 0, 0);

                Font cellFont = TheDataGrid.DefaultCellStyle.Font;
                float cellHeight = cellFont.SizeInPoints * 2;

                //####################################################################################                    

                if ((CurrentY + (cellHeight * 2)) > (PageHeight - (lblFooter.Height * 4)))
                {
                    return true;
                }

                //####################################################################################

                if (TheHeaderSection != null) CalcDrawTableHeader(g, TheHeaderSection, false);

                if (TheDataGrid.ColumnHeadersVisible == true)
                {
                    CalcDrawTableHeader(g, TheDataGrid, true);
                }
                else
                {
                    RowBounds.X = TheDataGrid.Location.X;
                    RowBounds.Y = CurrentY;
                    RowBounds.Height = cellHeight;
                    RowBounds.Width = linewidth;
                    CurrentY = Convert.ToInt16(RowBounds.Bottom);
                    TableY = CurrentY;
                }
                lastRowBottom = CurrentY;

                //####################################################################################                    

                if (TheView.Count == 0)
                {
                    CalcDrawTableHeader(g, TheEmptyMessage, false);
                }
                else
                {
                    for (int i = initialRowCount; i < TheView.Count; i++)
                    {
                        DataRowView dr = TheView[i];
                        int startxposition = TheDataGrid.Location.X;

                        RowBounds.X = TheDataGrid.Location.X;
                        RowBounds.Y = CurrentY;
                        RowBounds.Height = cellHeight;
                        RowBounds.Width = linewidth;
                        Lines.Add(RowBounds.Bottom);

                        ArrayList arrLines = new ArrayList();
                        int Counter = 0;
                        for (int k = 0; k < TheDataGrid.ColumnCount; k++)
                        {
                            string nextcolumn = dr[k + 1].ToString();
                            nextcolumn = nextcolumn.Replace("\n", "");
                            string[] txtLines = (BreakItemText(g, nextcolumn, TheDataGrid.Columns[k].Width, cellFont, cellformat)).Split((char)13);
                            arrLines.Add(txtLines);
                            if ((txtLines.GetUpperBound(0) + 1) > Counter)
                            {
                                Counter = (txtLines.GetUpperBound(0) + 1);
                                int iniSubRowCount = 0;
                                if (IsHeader == false) iniSubRowCount = SubRowCount;
                                int ll = 0;
                                for (int cc = iniSubRowCount; cc <= (txtLines.GetUpperBound(0)); cc++)
                                {
                                    Counter = ++ll;
                                    if (CurrentY + (Convert.ToInt16(cellHeight - (cellHeight / 4)) * Counter) > (PageHeight - (lblFooter.Height * 4)))
                                    {
                                        if (TheDataGrid.ColumnCount > 1)
                                        {
                                            if (i > 0)
                                            {

                                            }
                                            return true;
                                        }
                                        else
                                        {
                                            break;
                                        }
                                    }
                                }

                                RowBounds.Height = (cellHeight * Counter);
                                if (Counter > 1) RowBounds.Height = (Convert.ToInt16(cellHeight - (cellHeight / 4)) * Counter) + Convert.ToInt16(cellHeight / 3);
                                //   if (Convert.ToInt16(dr[0].ToString()) == 1) 
                                if (dr[0].ToString() == "1")
                                {

                                }
                                else
                                {

                                }
                            }
                        }

                        //####################################################################################                    

                        int kk = 0;
                        foreach (string[] txtLines in arrLines)
                        {
                            cellformat.Alignment = Get_Alignment(TheDataGrid.Columns[kk].DefaultCellStyle.Alignment.ToString());
                            //---------------------------------------------------------------------------------
                            int initialSubRowCount = 0;
                            if (IsHeader == false) initialSubRowCount = SubRowCount;
                            for (int ii = initialSubRowCount; ii <= (txtLines.GetUpperBound(0)); ii++)
                            {
                                RectangleF cellbounds = new RectangleF(startxposition,
                                                                       CurrentY,
                                                                       TheDataGrid.Columns[kk].Width,
                                                                       cellHeight);
                                nextcellbounds = cellbounds;
                                CurrentY = CurrentY + Convert.ToInt16(cellHeight - (cellHeight / 4));
                                if (IsHeader == false) SubRowCount++;
                                if (SubRowCount < txtLines.GetUpperBound(0))
                                {
                                    if (CurrentY > (PageHeight - (lblFooter.Height * 4)))
                                    {
                                        lastRowBottom = Convert.ToInt16(RowBounds.Bottom);
                                        return true;
                                    }
                                }
                            }
                            if (IsHeader == false) SubRowCount = 0;
                            startxposition = startxposition + TheDataGrid.Columns[kk].Width;
                            CurrentY = Convert.ToInt16(RowBounds.Top);
                            kk++;
                        }

                        //
                        //####################################################################################                    
                        //

                        lastRowBottom = Convert.ToInt16(RowBounds.Bottom);
                        CurrentY = Convert.ToInt16(RowBounds.Bottom);

                        if (IsHeader == false) RowCount++;

                        if (CurrentY > (PageHeight - (lblFooter.Height * 4)))
                        {
                            if ((i + 1) < TheView.Count)
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                    }
                }
                return false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
                return false;
            }
        }
    }
}
